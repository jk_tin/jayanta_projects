package com.rtms.augersys.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="lims_data")
public class LimsDataEntity {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	int id;
	
	@Column(name="bin_num")
	int binNum;
	@Column(name="cscts_ref_num")
	String csCTSRefNumber;
	@Column(name="col_ref_num")
	String collRefNumber;
	
	@Column(name="sample_ref_num")
	String sampleRefNumber;
	
	@Column(name="part_size")
	int partSize;
	
	@Column(name="date_samp")
	String dateOfSampling;
	
	@Column(name="plant_code")
	String plantCode;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBinNum() {
		return binNum;
	}

	public void setBinNum(int binNum) {
		this.binNum = binNum;
	}

	public String getCsCTSRefNumber() {
		return csCTSRefNumber;
	}

	public void setCsCTSRefNumber(String csCTSRefNumber) {
		this.csCTSRefNumber = csCTSRefNumber;
	}

	public String getCollRefNumber() {
		return collRefNumber;
	}

	public void setCollRefNumber(String collRefNumber) {
		this.collRefNumber = collRefNumber;
	}

	public String getSampleRefNumber() {
		return sampleRefNumber;
	}

	public void setSampleRefNumber(String sampleRefNumber) {
		this.sampleRefNumber = sampleRefNumber;
	}

	public int getPartSize() {
		return partSize;
	}

	public void setPartSize(int partSize) {
		this.partSize = partSize;
	}

	public String getDateOfSampling() {
		return dateOfSampling;
	}

	public void setDateOfSampling(String dateOfSampling) {
		this.dateOfSampling = dateOfSampling;
	}

	public String getPlantCode() {
		return plantCode;
	}

	public void setPlantCode(String plantCode) {
		this.plantCode = plantCode;
	}

	@Override
	public String toString() {
		return "LimsDataEntity [id=" + id + ", binNum=" + binNum + ", csCTSRefNumber=" + csCTSRefNumber
				+ ", collRefNumber=" + collRefNumber + ", sampleRefNumber=" + sampleRefNumber + ", partSize=" + partSize
				+ ", dateOfSampling=" + dateOfSampling + ", plantCode=" + plantCode + "]";
	}

	 
	
	
	
}
