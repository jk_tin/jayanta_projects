package com.rtms.augersys.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="lims_data_json")
public class LimsDataEntityJson {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	int id;
	
	@Column(name="bin_num")
	int binNum;
	
	@Column(name="lims_data_json")
	String limsDataJson;
	
	@Column(name="is_processed")
	boolean isProcessed;
	
	

	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public int getBinNum() {
		return binNum;
	}



	public void setBinNum(int binNum) {
		this.binNum = binNum;
	}



	public String getLimsDataJson() {
		return limsDataJson;
	}



	public void setLimsDataJson(String limsDataJson) {
		this.limsDataJson = limsDataJson;
	}



	public boolean isProcessed() {
		return isProcessed;
	}



	public void setProcessed(boolean isProcessed) {
		this.isProcessed = isProcessed;
	}



	@Override
	public String toString() {
		return "LimsData [id=" + id + ", binNum=" + binNum + ", limsDataJson=" + limsDataJson + ", isProcessed="
				+ isProcessed + "]";
	}
	
	
	
}
