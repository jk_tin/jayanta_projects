package com.rtms.augersys.main;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.UnknownHttpStatusCodeException;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.rtms.augersys.controller.RTMSController;
import com.rtms.augersys.dto.Bin;
import com.rtms.augersys.dto.LimsData;
import com.rtms.augersys.dto.ResponseAug;
import com.rtms.augersys.dto.ResponseLims;
import com.rtms.augersys.entity.LimsDataEntityJson;
import com.rtms.augersys.repo.BinRepo;
import com.rtms.augersys.service.BinService;
import com.rtms.augersys.service.impl.RTMSServiceImpl;
import com.rtms.augersys.util.AppConstant;
import java.util.List;
@Service
public class MainApp extends Thread {
   
	private final RestTemplate restTemplate;
	//public static ArrayList<Bin> binMapRef;
	public static ArrayList<Bin> binMap;
	public static boolean allBinsEmpty;
	 
	BinService binService;
	
	private static final Logger log = LogManager.getLogger(MainApp.class);
	public MainApp(BinService binService)
	{
		 this.binService=binService;
		 RestTemplateBuilder restTemplateBuilder=new RestTemplateBuilder();
		 
		 this.restTemplate = restTemplateBuilder.build();
		
	}
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		initializeBinMap();

	    String csctsRefNumFull=null;
	    String csctsRefNum=null;	
	    String sampleType=null;
			
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//	 
		//binMapRef=binMap;
	    
		while(RTMSController.startFlagNew)
		{
			try {
				   
					csctsRefNumFull=null;		
					csctsRefNum=null;
					sampleType=null;
					  log.debug("loop started !!!");
				          
					  writeToCellWithRetry(AppConstant.COL_RFID_ERROR, AppConstant.NUMBER_ZERO);
				      int allocatedBin=-1;
				      int  augStatus=-1;
					  do //wait for sampling to completed or aborted
				      {
				    	   log.info("Waiting for Auger Status to become Ready");
				    	   Thread.sleep(5000);
				    	   augStatus=getNumericDatadFromCell(AppConstant.COL_AUG_STATUS);
				    							    		
				      }while((augStatus!=AppConstant.NUMBER_ZERO)&&(RTMSController.startFlagNew));
					  
					  if (AppConstant.AUGER_CALL_BYPASS!=1)
					    {
							 int augerMode=getNumericDatadFromCell(AppConstant.COL_AUG_MODE);  
							 String  augModeToSend=getAugerModeFromNum(augerMode);
							 String augStatusToSend= getAugerStatusFromNum(augStatus);
							 Boolean bFlag= false;
							 
							 do {
								 log.info("Updating Auger  Status Ready and Mode = " +augModeToSend);
								 ResponseEntity response=updateAugStatus(AppConstant.AUG_ID,augStatusToSend,augModeToSend,"",AppConstant.PLANT_CODE);
								 if(null!=response)
								 {
									 if(response.getStatusCode()==HttpStatus.OK)
									 {
										 bFlag=true;
										 
									 }
									 else
									 {
										 log.info("Auger  Status Update Failed...");
									 }
									 
								 }
								 else
								 {
									 log.info("Auger  Status Update Failed...");
								 }
								 
							 
							 }while (bFlag!=true);
							 
							 
					    
					    
					    }
					  
					  if(checkVehicleArrived()) //step 1
					  {
						  if(AppConstant.STEP1_CALL_BYPASS!=1) {
							  log.info("Step 1 Call By Pass Started");
							  if(AppConstant.AUGER_CALL_BYPASS!=1)
							  {
								  log.info("Step 1 Call -- getSampRateWithVehiclRegNum");
								  //csctsRefNumFull =getSampRateWithoutRefId(AppConstant.PLANT_CODE,AppConstant.AUG_ID);
								  csctsRefNumFull=getSampRateWithVehiclRegNum(AppConstant.PLANT_CODE,AppConstant.AUG_ID,"");
								  
								  

								  
							  
							  }
							  else
							  {
								  
								  log.info("Please eneter CSCTS Ref number in full--:");
								  Scanner in = new Scanner(System.in);
								  
								  csctsRefNumFull=in.nextLine();
								  log.info("Entered ref number is="+csctsRefNumFull);
								  
							  }
							 
							  
						  }
						 				  
						  
					  }
					 // log.info("***csctsRefNumFull="+csctsRefNumFull);
					  if((csctsRefNumFull==null)||(csctsRefNumFull.isEmpty())) //step 2
					  {
						  
						  writeToCellWithRetry(AppConstant.COL_COMM_ERROR, AppConstant.COMM_ERROR_LEVEL1);
						  String vehicleRegNum = "";
						  if(checkVehicleRegNumberEntered()) {
							  
							  writeToCellWithRetry(AppConstant.COL_RFID_ERROR, AppConstant.NUMBER_ONE);
							  //writeToCell(AppConstant.COL_COMM_ERROR, AppConstant.NUMBER_ZERO);
							  
							 vehicleRegNum=getVehicleRegNumber();
						      if(AppConstant.AUGER_CALL_BYPASS!=1) {
						    	  log.info("Step 2 Call -- getSampRateWithVehiclRegNum");
								  csctsRefNumFull=getSampRateWithVehiclRegNum(AppConstant.PLANT_CODE,AppConstant.AUG_ID,vehicleRegNum);
									
							  }
						      else 						      
						      {
						    	  log.info("Please eneter CSCTS Ref number in full--:");
								  Scanner in = new Scanner(System.in);
								  
								  csctsRefNumFull=in.nextLine();
								  log.info("Entered ref number is="+csctsRefNumFull);
						    	  
						    	  
						      }
						    
							  
							  		  
							 					       
							   if(csctsRefNumFull==null)				            				    
							   {
								   
								   writeToCellWithRetry(AppConstant.COL_COMM_ERROR, AppConstant.COMM_ERROR_LEVEL1);
								   
							   }
						  }
						  
					  }
					  
					  log.info("***csctsRefNumFull="+csctsRefNumFull);
					  if(csctsRefNumFull!=null) //step 3
					  {
						  
						        csctsRefNum=csctsRefNumFull.substring(AppConstant.CSCTS_PART_REF_NUM_START_INDEX,AppConstant.CSCTS_PART_REF_NUM_END_INDEX);
						        sampleType = csctsRefNumFull.substring(AppConstant.CSCTS_SAMPLING_TYPE_START_INDEX,AppConstant.CSCTS_SAMPLING_TYPE_END_INDEX);
							    allocatedBin=alloCateBin(csctsRefNum,binMap,csctsRefNumFull,sampleType);
							    
							    
							   
							    log.info("Allocated Bin="+(allocatedBin+1));
							    if(allocatedBin==-1)
							    {
							    	log.info("Bin Allocation Error");
							    	 
							    	
							    }
							    else {
							        
							    	
							    	binMap.get(allocatedBin).setCsctsRefNum(csctsRefNum);							    	 
							    	binMap.get(allocatedBin).getCsCTSrefNumFullList().add(csctsRefNumFull);
							    	binService.updateBinCSCTsNum(allocatedBin+1, csctsRefNum, csctsRefNumFull);
		
							    	if((allocatedBin==0)&&(allBinsEmpty))
							    	{
							    		
							    		allBinsEmpty=false;
							    	}
							    	 
							    	if(allocatedBin==binMap.size()-1)//special bin
							    	{
							    		 
							    		log.info("--Special Bin--");
							    		for (int count=0;count<AppConstant.SPECIAL_BIN_MAX_NUM_TIMES_FILLED;count++)
							    		{
							    			processBin(allocatedBin,AppConstant.SPECIAL_BIN_MAX_NUM_TIMES_FILLED);
							    			
							    		}
							    		
							    	}
							    	else
							    	{
							    		 
							    		processBin(allocatedBin,AppConstant.BIN_MAX_NUM_TIMES_FILLED);
							    	}
					    
							    }	 
					  	}
					}
				catch(Exception ex)
				{
					ex.printStackTrace();
					log.error(ex.toString());
							
					continue;
				}
			 
		}
							         
		
		log.info("Stopped");
		  
			
		 
	}
	
	 
	
	
	
	public String  getSampRateWithoutRefId(String plantCode,String augId) {
		// TODO Auto-generated method stub
		log.info("Inside getSampRateWithoutRefId");
		ResponseEntity<ResponseAug> response=null;
		String csctsReferNum=null;
		
		
	    try {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("plantCode", plantCode);
		
		//MultiValueMap<String, Object> map= new LinkedMultiValueMap<String, Object>();
		Map<String, Object> map = new HashMap<>();
		map.put("augurID",augId);
		map.put("rfid","");
		//HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		HttpEntity<Map<String, Object>> request = new HttpEntity<>(map, headers);
		
		String url=AppConstant.BASE_URL+":"+AppConstant.BASE_PORT+"/"+AppConstant.API_GET_SAMP_RATE;
		
		 
		 response = restTemplate.postForEntity( url, request , ResponseAug.class );
		 
		 
		 if(null!=response)
		 {
		 
			log.info(response.getBody().getStatusCode());
			log.info(response.getBody().getErrorMessage());	
			
			writeToCellWithRetry(AppConstant.COL_HTTP_STATUS_CODE,Integer.parseInt(response.getBody().getStatusCode()));
			writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ZERO);
		 }
		 else
		 {
			 log.info("Response Null -getSampRateWithoutRefId");
			 writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ONE);
			 
		 }
		
	    }
	    catch(HttpClientErrorException  ex)
	    {
	    	log.info("getSampRateWithoutRefId-HttpClientErrorException");
	    	ex.printStackTrace();
	    }
	    catch(UnknownHttpStatusCodeException  ex) {
	    	log.info("getSampRateWithoutRefId-UnknownHttpStatusCodeException");
	    	log.info(ex.getResponseBodyAsString());
	     	 
	    	writeToCellWithRetry(AppConstant.COL_RFID_ERROR, AppConstant.NUMBER_ONE); 
	    	writeToCellWithRetry(AppConstant.COL_HTTP_STATUS_CODE,ex.getRawStatusCode()); 			 
	    	
	    	
	        log.info(ex.getRawStatusCode());
	       
	    }
	    
		return csctsReferNum;
		
	}
	
	
	String getStringDataFromCell(String cellNumber)
	  {
		  String  ret= null;
		  
		  try
		  {
			  
			  String fileName=AppConstant.PLC_BASE_FILE_PATH+File.separator+AppConstant.PLC_FILE_NAME;			
			  FileInputStream file = new FileInputStream(new File(fileName));
			  XSSFWorkbook workbook = new XSSFWorkbook(file);
			  XSSFSheet sheet = workbook.getSheetAt(0);		  
			  XSSFRow rowi= sheet.getRow(0);
			  CellReference cellReference = new CellReference(cellNumber); 
			  Row row = sheet.getRow(cellReference.getRow());
			  Cell cell = row.getCell(cellReference.getCol()); 
			  ret=  cell.getStringCellValue();
			  workbook.close();	  
			  
		  }catch(Exception ex)
		  {
		    ex.printStackTrace();
			  
		  }	  
		  finally
		  {
			  
			  return ret;	  
			  
		  }
		
	  }
	  
	 void writeToCellWithRetry(String cellNumber,int value)
	 {
		int retryCount=0; //first Try
		Boolean retVal=false;
	 
		while((retVal==false)&&(retryCount<3))
		{
		 
			try {
				Thread.sleep(6000);
				retVal= writeToCell(cellNumber, value);				
				retVal=true;
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				retryCount++;
				e.printStackTrace();
			}catch (Exception ex)
			{
				retryCount++;
				ex.printStackTrace();
			}
			
		}
		 
	 }
	 
	 void writeToCellTextWithRetry(String cellNumber,String value)
	 {
		int retryCount=0; //first Try
		Boolean retVal=false;
	 
		while((retVal==false)&&(retryCount<3))
		{
		 
			try {
				Thread.sleep(6000);
				retVal= writeValCell(cellNumber, value);				
				retVal=true;
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				retryCount++;
				e.printStackTrace();
			}catch (Exception ex)
			{
				retryCount++;
				ex.printStackTrace();
			}
			
		}
		 
	 }
	
	
	  boolean writeToCell(String cellNumber,int value)
	  {
		  boolean retVal=false;
		  try {
			 
			  
			  String fileName=AppConstant.PLC_BASE_FILE_PATH+File.separator+AppConstant.PLC_FILE_NAME;			
			  FileInputStream file = new FileInputStream(new File(fileName));
			  XSSFWorkbook workbook = new XSSFWorkbook(file);
			  XSSFSheet sheet = workbook.getSheetAt(0);		  
			  XSSFRow rowi= sheet.getRow(0);
			  CellReference cellReference = new CellReference(cellNumber); 
			  Row row = sheet.getRow(cellReference.getRow());
			  Cell cell = row.getCell(cellReference.getCol()); 
			  int ret=(int) cell.getNumericCellValue();
			  
			  log.info(ret);
			  
			  cell.setCellValue(value);
			  file.close();
			  FileOutputStream outputStream = new FileOutputStream(fileName);
			  workbook.write(outputStream);
			  outputStream.close();
			  workbook.close();
			  
			 /* cellVeh.setCellValue(value);;
			  fileveh.close();
			  FileOutputStream outputStream = new FileOutputStream(fileName);
			  workbookveh.write(outputStream);
			  outputStream.close();
			  workbookveh.close();*/
			 
			  retVal=true;
			  
			  
		  }
		  catch(Exception ex)
		  {
			  ex.printStackTrace();
			  
			  
		  }
		  
		 return retVal;
	  }
	  
	  
	boolean clearBin(ArrayList<Bin>binMap,int binNumIndex) throws InterruptedException
	  {
		  log.info("Bin Clear command set for Bin ="+(binNumIndex+1));
		  if((null==binMap)|| (null==binMap.get(binNumIndex)))
		  {
			  log.info("Clear Bin -- Null");
			  return false;
			  
		  }
		  
		  boolean retval=false;
		  
		  binMap.get(binNumIndex).setBinFull(true);
		  binService.updateFillStatus(binNumIndex+1, true);
		  writeToCellWithRetry(AppConstant.COL_EMPTY_BIN_NUM,binNumIndex+1);
		  writeToCellWithRetry(AppConstant.COL_EMPTY_BIN_NUM_COMM,AppConstant.NUMBER_ONE);
		  
		  ArrayList<LimsData> limsData=new ArrayList<LimsData>();
		  ArrayList<String> csCTSrefNumFullList =binMap.get(binNumIndex).getCsCTSrefNumFullList();
		  log.info("clearBin-csCTSrefNumFullList="+csCTSrefNumFullList);
		  if(null !=csCTSrefNumFullList )
		  {
			  log.info("clearBin-Setting Lims Data ");
			  String colRefNum=getCollRefNum();
			  String sampleRefNum1=getSampRefNum1();
			  String sampleRefNum2=getSampRefNum2();
			  log.info("---sampleRefNum1--"+sampleRefNum1);
			  log.info("---sampleRefNum2--"+sampleRefNum2);
			  for(String ctsCtsFullRefNum:csCTSrefNumFullList)
			  {
			
				  LimsData data1 =new LimsData();
				  data1.setPlantCode(AppConstant.LIMS_PLANT_CODE);
				  data1.setCsCTSRefNumber(ctsCtsFullRefNum);		  
			      data1.setCollRefNumber(colRefNum);
			      data1.setSampleRefNumber(sampleRefNum1);
			      data1.setPartSize(AppConstant.AUG_SAMP_PARTSIZE_1);
			      data1.setDateOfSampling(getDate());
			      limsData.add(data1);
			      LimsData data2 =new LimsData();
				  data2.setPlantCode(AppConstant.LIMS_PLANT_CODE);
				  data2.setCsCTSRefNumber(ctsCtsFullRefNum);		  
			      data2.setCollRefNumber(colRefNum);
			      data2.setSampleRefNumber(sampleRefNum2);
			      data2.setPartSize(AppConstant.AUG_SAMP_PARTSIZE_2);
			      data2.setDateOfSampling(getDate());
			      limsData.add(data2);
			      binService.saveLimsData(binNumIndex+1, data1);
			      binService.saveLimsData(binNumIndex+1, data2);
			      binMap.get(binNumIndex).setLimsData(limsData);
			  }
		  }
	      
		//wait for BIN empty response
		   
		  while((true)&&(RTMSController.startFlagNew))
		  {
			  Thread.sleep(5000);
			  int response=getNumericDatadFromCell(AppConstant.COL_BIN_EMPTIED_PRINTING_DONE);
			  log.info("---Bin Emptied completion response awaited--");
			  if(response==AppConstant.BIN_EMPTIED)
			  {
				  
				   int emptyBinNum=getNumericDatadFromCell(AppConstant.COL_EMPTY_BIN_NUM);
				   if(emptyBinNum==1)
				   {
					   writeToCellWithRetry(AppConstant.COL_BIN1_NUM_TIMES_FILL,AppConstant.NUMBER_ZERO);
  
				   }
				   else if(emptyBinNum==2)
				   {
					   writeToCellWithRetry(AppConstant.COL_BIN2_NUM_TIMES_FILL,AppConstant.NUMBER_ZERO);
  
				   }
				   else if(emptyBinNum==3)
				   {
					   writeToCellWithRetry(AppConstant.COL_BIN3_NUM_TIMES_FILL,AppConstant.NUMBER_ZERO);
  
				   }
				   else if(emptyBinNum==4)
				   {
					   writeToCellWithRetry(AppConstant.COL_BIN4_NUM_TIMES_FILL,AppConstant.NUMBER_ZERO);
  
				   }
				   else if(emptyBinNum==5)
				   {
					   writeToCellWithRetry(AppConstant.COL_BIN5_NUM_TIMES_FILL,AppConstant.NUMBER_ZERO);
  
				   }
				   else if(emptyBinNum==6)
				   {
					   writeToCellWithRetry(AppConstant.COL_BIN6_NUM_TIMES_FILL,AppConstant.NUMBER_ZERO);
  
				   }
				   else if(emptyBinNum==7)
				   {
					   writeToCellWithRetry(AppConstant.COL_BIN7_NUM_TIMES_FILL,AppConstant.NUMBER_ZERO);
  
				   }
				   else if(emptyBinNum==8)
				   {
					   writeToCellWithRetry(AppConstant.COL_BIN8_NUM_TIMES_FILL,AppConstant.NUMBER_ZERO);
  
				   }
		
				   

				   writeToCellWithRetry(AppConstant.COL_EMPTY_BIN_NUM,AppConstant.NUMBER_ZERO);
				   writeToCellWithRetry(AppConstant.COL_EMPTY_BIN_NUM_COMM,AppConstant.NUMBER_ZERO);
				   ResponseEntity<ResponseLims> responseLims=null;
				   int retryCounter=0;
				   
				   responseLims=sendLimsData(binMap.get(binNumIndex));
//				   if((responseLims!=null))
//				   {
//					   
//					   if(!responseLims.getBody().getStatusCode().equals("200"))
//					   {
//						   log.info("Could not send data to LIMS-1"+responseLims);
//						   log.info("Could not send data to LIMS-2"+responseLims.getBody().getStatusCode());
//						   writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ONE);
//					   }
//					   else
//					   {
//						   writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ZERO);
//					   }
//					   
//				   }
				   
//				   do
//				   {
//					   log.info("sendLimsData try# "+(retryCounter+1));
//					   responseLims=sendLimsData(binMap.get(binNumIndex));
//					   
//					   retryCounter++;
//					   
//				   }while((responseLims!=null)&&(!responseLims.getBody().getStatusCode().equalsIgnoreCase("200"))&&(retryCounter!=AppConstant.LIMS_RETRY_ATTEMPT_LIMIT));
//				   
//				   if((retryCounter==AppConstant.LIMS_RETRY_ATTEMPT_LIMIT)&&(responseLims.getBody().getStatusCode()!="200"))
//				   {
//					   log.info("Could not send data to LIMS");
//					   //could not send data to LIMS after 3 attempts
//					   writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ONE);
//						  
//					   
//				   }
				   	   
				   printForBarCode(limsData);
				   Thread.sleep(1000);
				
				   Thread.sleep(AppConstant.COMM_DELAY_TIME);
				   //writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ZERO);
				   binMap.get(binNumIndex).setCsctsRefNum("");
				   binMap.get(binNumIndex).setNumOfTimesFilled(0);
				   binMap.get(binNumIndex).setBinFull(false);
 				   binMap.get(binNumIndex).getCsCTSrefNumFullList().clear();
 				   binMap.get(binNumIndex).getLimsData().clear();
 				   binService.cleanBin(binNumIndex+1);
 				   binService.deleteLimsData(binNumIndex+1);
				   log.info("Bin Clear command   for Bin ="+(binNumIndex+1)+ " Completed Successfully");
				   retval=true;
				   break;
			  }
				  						  
		  }
		  
		  for(Bin bin:binMap)
		  {
			  
			  if(bin.getNumOfTimesFilled()>0)
			  {
				  allBinsEmpty=false;
				  break;
				  
			  }
			  
			  
		  }
		  
		  
		  
		  
		  return retval;
		  
	  }



	 
	public boolean writeValCell(String cell, String value) {
		// TODO Auto-generated method stub
		boolean retVal=false;
		try {
			
			writeToCellWithRetry(cell,Integer.parseInt(value));
			retVal=true;
			
			
		}catch(Exception ex)
		{
			
			ex.printStackTrace();
			
		}
		
		
		  return retVal;
	}

	 
//	public void printBin(int binNum) {
//		
//		Bin b=binMapRef.get(binNum);
//		 
//		  log.info("Bin Num= "+(b.getBinNum()));
//		  
//		  log.info("CSCTS Ref Num= "+b.getCsctsRefNum());
//		  log.info("NumOfTimesFilled= "+b.getNumOfTimesFilled());
//		  
//		return ;
//	}

	 
	/*public void printForBarCode(int binNum) {
		
		Bin b=binMapRef.get(binNum);
		ArrayList<String>strList= b.getCsCTSrefNumList();
		for(String str:strList)
		{
			log.info("Barcode="+str);
			
		}
		
		
	}*/
	
	
	public void printForBarCode(ArrayList<LimsData> limsData) {
		log.info("--- Inside printForBarCode--");
		for(LimsData limsDataVal:limsData)
		{
			String printDataValue=limsDataVal.getSampleRefNumber();
			String imageName=printDataValue+"."+"png";
			
			 LocalDateTime myDateObj = LocalDateTime.now();			  
			 DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("ddMMyyyy");
			 String formattedDate = myDateObj.format(myFormatObj);
			 String imageFilePath=AppConstant.PLC_BASE_FILE_PATH+"\\"+AppConstant.BARCODE_IMAGE_FILE_PATH+"\\"+formattedDate;
			 log.info(imageFilePath+"\\"+imageName);
					
			 createImage(imageFilePath,imageName,printDataValue);
			
		}
		
		
	}


 
	public ResponseEntity<ResponseLims> sendLimsData(Bin bin) {
		// TODO Auto-generated method stub
		log.info("--Inside sendLimsData--");
		ResponseEntity<ResponseLims> response=null;
		//ResponseLims response=null;
		JsonArray jsonArray = new JsonArray();
		ArrayList<String> csCTSrefNumFullList = bin.getCsCTSrefNumFullList();
		if(null==csCTSrefNumFullList)
		{
		 log.info("sendLimsData-csCTSrefNumFullList is Null");
		 return null;
			
		}
		ArrayList<LimsData> limsDataList=bin.getLimsData();
		if(null==limsDataList)
		{
			log.info("sendLimsData-limsDataList is null");
			return null;
		}
		
		for(LimsData limsData:limsDataList)
		{
			 
			JsonObject  formObj= new JsonObject();
			log.info("limsData CSCTSRefNo=="+limsData.getCsCTSRefNumber());
			formObj.addProperty("PlantCode",limsData.getPlantCode());
			formObj.addProperty("CSCTSRefNo",limsData.getCsCTSRefNumber()); 
			formObj.addProperty("CollRefNo",limsData.getCollRefNumber()); 
			formObj.addProperty("SampleRefNo",limsData.getSampleRefNumber()); 
			formObj.addProperty("PartSize",limsData.getPartSize()); 
			formObj.addProperty("DateOfSampling",limsData.getDateOfSampling()); 
			
			jsonArray.add(formObj);
		
 		     
		
		}
		
		  
		 
		 
		log.info("limsData="+jsonArray);
		
	    try {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		//headers.add("plantCode", plantCode);
		
		 
		HttpEntity<String> entity = new HttpEntity<String>(jsonArray.toString(), headers);
		
		 String url=AppConstant.API_LIMS_URL;
		// response = restTemplate.postForEntity( url, entity , ResponseAug.class );
		 log.info("-sendLimsData-Before Call--");
		 //response= restTemplate.postForObject(url, entity,ResponseLims.class);
		 response= restTemplate.exchange(url, HttpMethod.POST, entity, ResponseLims.class);
		 if(null!=response)
		 {
			 log.info("-sendLimsData-After Call--");	 	
			 log.info(response.getBody().getStatusCode());
			 log.info(response.getBody().getMessage());	
			
			 if(!response.getBody().getStatusCode().equals("200"))
			 {
				 log.info("--Saving Lims JSon to DB-status code"+response.getBody().getStatusCode());
				 binService.saveLimsDataJson((bin.getBinNum()+1),jsonArray.toString());
				 
			 }
			 writeToCellWithRetry(AppConstant.COL_HTTP_STATUS_CODE,Integer.parseInt(response.getBody().getStatusCode()));
			 writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ZERO);
		 }
		 else
		 {
			 log.info("-sendLimsData-Response Null--");	 
			 binService.saveLimsDataJson((bin.getBinNum()+1),jsonArray.toString());
			 writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ONE);
			 
		 }
	    }
	    catch(HttpClientErrorException  ex)
	    {
	    	log.info("--Exception Inside sendLimsData-HttpClientErrorException");
	    	ex.printStackTrace();
	    	binService.saveLimsDataJson((bin.getBinNum()+1),jsonArray.toString());
	    }
	    catch(UnknownHttpStatusCodeException  ex) {
	      log.info("--Exception Inside sendLimsData-UnknownHttpStatusCodeException");
	      log.info(ex.getRawStatusCode());
	      writeToCellWithRetry(AppConstant.COL_HTTP_STATUS_CODE,ex.getRawStatusCode());
	      binService.saveLimsDataJson((bin.getBinNum()+1),jsonArray.toString()); 
	    }
	    catch(Exception ex)
	    {
	    	log.info("--Exception Inside sendLimsData-"+ex.getMessage());
	    	binService.saveLimsDataJson((bin.getBinNum()+1),jsonArray.toString());
	    	
	    }
		return response;
		
	}
	
	 int getNumericDatadFromCell(String cellNumber)
	  {
		  int ret=-1;
		  
		  try
		  {
			  Thread.sleep(12000);
			  
			  String fileName=AppConstant.PLC_BASE_FILE_PATH+File.separator+AppConstant.PLC_FILE_NAME;			
			  FileInputStream file = new FileInputStream(new File(fileName));
			  XSSFWorkbook workbook = new XSSFWorkbook(file);
			  XSSFSheet sheet = workbook.getSheetAt(0);		  
			  XSSFRow rowi= sheet.getRow(0);
			  CellReference cellReference = new CellReference(cellNumber); 
			  Row row = sheet.getRow(cellReference.getRow());
			  Cell cell = row.getCell(cellReference.getCol()); 
			  ret=(int) cell.getNumericCellValue();
			  workbook.close();	  
			  
		  }catch(Exception ex)
		  {
		    ex.printStackTrace();
			  
		  }	  
		  finally
		  {
			  
			  return ret;	  
			  
		  }
		
	  }
	  
	 boolean checkVehicleArrived()
		{
			boolean flag=true;			
					
			while((flag)&&(RTMSController.startFlagNew))
			{
				log.info("Entering for Vehicle Arrival Check");	
				 
				try {
				  Thread.sleep(5000);		  
			      int d=getNumericDatadFromCell(AppConstant.COL_VEH_ARRVD);     
	
				  if(d==1)
				  {
					   
					  break;
					  
				  }
//				  else
//				  {
//					  log.info("Vehicle not arrived yet");
//					  
//				  }
				  Thread.sleep(1000);
	  
				 }
				 catch(Exception ex)
				 {
					 ex.printStackTrace();
					 
					 
				 }
				
			}
			
			log.info("Exiting for Vehicle Arrival Check");
			return flag;
		}
		
		
		boolean checkVehicleRegNumberEntered()
		{
			boolean flag=true;
			 

			while((flag)&&(RTMSController.startFlagNew))
			{
				 try {
					 log.info("Waiting for  Vehicle Registration  Number:"); 
				    
						     int d= getNumericDatadFromCell(AppConstant.COL_VEH_REG_ENTERED);
			     
				  //log.info(d);
				  
				  
				  
				  if(d==1)
				  {
					   //reset 
					
					  
					  break;
					  
				  }
				  
				  Thread.sleep(1000);
				  
				  
				  
				 }
				 catch(Exception ex)
				 {
					 ex.printStackTrace();
					 
					 
				 }
				
				
				
			}
			
			log.info("Exiting from Vehicle Registration  Number check ");  
		
			return flag;
		}
		
		
		String  getVehicleRegNumber()
		{
			 
			     String vehicleRegNumber=null;
			 
				 try {
				  Thread.sleep(12000);
				  String fileName=AppConstant.PLC_BASE_FILE_PATH+File.separator+AppConstant.PLC_FILE_NAME;
				
				  FileInputStream file = new FileInputStream(new File(fileName));
				  XSSFWorkbook workbook = new XSSFWorkbook(file);
				  XSSFSheet sheet = workbook.getSheetAt(0);
				  
				  XSSFRow rowi= sheet.getRow(0);
				  CellReference cellReference = new CellReference(AppConstant.COL_VEH_REG_NUM); 
				  Row row = sheet.getRow(cellReference.getRow());
				  Cell cell = row.getCell(cellReference.getCol()); 
				  vehicleRegNumber= cell.getStringCellValue();
				  workbook.close();
				  
				
				  //reset 
				  FileInputStream fileveh = new FileInputStream(new File(fileName));
				  XSSFWorkbook workbookveh = new XSSFWorkbook(fileveh); 
				  XSSFSheet sheetveh = workbook.getSheetAt(0);
				  
				  XSSFRow rowii= sheet.getRow(0); CellReference cellReferenceVeh = new
				  CellReference(AppConstant.COL_VEH_REG_ENTERED); 
				  Row rowVeh = sheetveh.getRow(cellReferenceVeh.getRow()); 
				  Cell cellVeh =row.getCell(cellReferenceVeh.getCol()); 
				  cellVeh.setCellValue(1);
				  
				  //getStringDatadFromCell();
				  log.info("vehicleRegNumber="+vehicleRegNumber);
				  workbook.close();
				   
				  
				 }
				 catch(Exception ex)
				 {
					 ex.printStackTrace();
					 
					 
				 }
				
			
			return vehicleRegNumber;
		}
		
		 
	 
		public  String  getSampRateWithVehiclRegNum(String plantCode,String augId,String vehicleRegNum) {
			// TODO Auto-generated method stub
			log.info("Inside getSampRateWithVehiclRegNum");
			ResponseEntity<ResponseAug> response=null;
			String csctsReferNum=null;
		    try {
		    	writeToCellWithRetry(AppConstant.COL_DATA_ERROR,AppConstant.NUMBER_ZERO);
		    	//writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ZERO);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("plantCode", plantCode);
			
			//MultiValueMap<String, Object> map= new LinkedMultiValueMap<String, Object>();
			Map<String, Object> map = new HashMap<>();
			map.put("augurID",augId);
			map.put("vehRegNo",vehicleRegNum);
		 
			//HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
			HttpEntity<Map<String, Object>> request = new HttpEntity<>(map, headers);
			
			String url=AppConstant.BASE_URL+":"+AppConstant.BASE_PORT+"/"+AppConstant.API_GET_SAMP_RATE;
			 response = restTemplate.postForEntity( url, request , ResponseAug.class );
			if(null!=response)
			{
			 
			String responseCode= response.getBody().getStatusCode();
			
			log.info(response.getBody().getStatusCode());
			log.info(response.getBody().getErrorMessage());	
			
			if((responseCode!=null)&&(!responseCode.isEmpty()&&(responseCode.equalsIgnoreCase("200"))))
			{
				if(AppConstant.AUGER_CALL_BYPASS==1) {
					
					csctsReferNum="1111";
				}
				else
				{
					
					csctsReferNum=response.getBody().getSampleReferenceID();
					
				}
				
				
			
				
		 			
			}
			
			writeToCellWithRetry(AppConstant.COL_HTTP_STATUS_CODE,Integer.parseInt(response.getBody().getStatusCode()));
			writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ZERO);
			}
				else
				{
					log.info("Response NUll for getSampRateWithVehiclRegNum");
					writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ONE);
				}
		    }
		    
		    catch(UnknownHttpStatusCodeException  ex) {
		    	
		    	log.info("getSampRateWithVehiclRegNum-Data error"+ex.getMessage());		    		    
		    	log.info(ex.getRawStatusCode());
		    	writeToCellWithRetry(AppConstant.COL_DATA_ERROR,AppConstant.NUMBER_ONE);
		    	writeToCellWithRetry(AppConstant.COL_HTTP_STATUS_CODE,ex.getRawStatusCode());
		    	
		       
		    }
		   catch(Exception commError) {
			   
			   log.info("getSampRateWithVehiclRegNum-Communication Error: "+commError.getMessage());
			//   log.info(commError.getMessage());
			   writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ONE);
		   }
		     
		    
			return csctsReferNum;
			
		}
		
		
		int alloCateBin(String csctsRefrNum,ArrayList<Bin>binMap,String fullcsCTSRefNum,String sampleType)
		  {
			   
			  int retBinNum=-1;
			
			  if(sampleType==null)
			  {
				  return retBinNum;
				  
			  }
			  try {
			  
			 if(sampleType.equalsIgnoreCase("00"))
			 {
				 
				  log.info("Normal Sampling");
			 
				  //case 1 - first time all bins empty choose the first one
				  
				  if(allBinsEmpty)
				  {
					  log.info("--allBinsEmpty--");
					  retBinNum=0;
					  
				  }
				  else
				  {
				  
					  log.info("--Else Path--");
				  
				   //look out for sample matching ref number
				    for(int i=0;i<binMap.size()-1;i++) { //dont consider the special BIN
					   
						      Bin bin= binMap.get(i);
						      
						      if(bin.getCsctsRefNum()!=null && (bin.getCsctsRefNum().equalsIgnoreCase(csctsRefrNum))) {
								  //what is the filling status
								  if(bin.getNumOfTimesFilled()<AppConstant.BIN_MAX_NUM_TIMES_FILLED)//this bin can still take sample
								  {
									  retBinNum=i;
									  return retBinNum;
									  
								  }
					   	   
						       
						      }
				    }
						      
				  //NO match found
			      //find an empty bin
				      for(int j=0;j<binMap.size()-1;j++)
				      {
				    	  
				    	  Bin binempty= binMap.get(j);
				    	  if(binempty.getNumOfTimesFilled()==0)
				    	  {
				    		  retBinNum=j;
				    		  return retBinNum;
				    		  
				    	  }
				   	  
				      }
				      //All bins have some material find out the bin which has been filled max number of times
				      
				      int maxBinSizeIndex=0;//assume first 1 has the max value
				      Bin binFirst=binMap.get(0);
				      int maxNum=binFirst.getNumOfTimesFilled();
				      
				      for(int j=1;j<binMap.size()-1;j++) {
				    	  
				    	   Bin binnew=binMap.get(j);
				    	   if(binnew.getNumOfTimesFilled()>maxNum)
				    	   {
				    		   maxNum=binnew.getNumOfTimesFilled();
				    		   maxBinSizeIndex=j;
				    	   }
				    	  
				    	  
				      }
				      if(clearBin(binMap,maxBinSizeIndex))
				      {
				    	  doPostCleanUp(maxBinSizeIndex);
				    	  retBinNum= maxBinSizeIndex;
				    	  
				      }
			  
				    }
			  
				  }
				 else if(sampleType.equalsIgnoreCase("01"))
				 {
					 retBinNum=binMap.size()-1;
					 
				 }
			 
			  }catch(Exception ex)
			  {
				  
				  ex.printStackTrace();
				  log.error(ex.toString());
				  
			  }
			  finally {
				  return retBinNum;
			  }
		  }
		
		
		
		  void printBinMap(ArrayList<Bin>binMap)
		  {
			  log.info("Printing BIN MAP");
			  for(Bin b:binMap)
			  {
				  
				  log.info("Bin Num= "+(b.getBinNum()+1));
				  log.info("CSCTS Ref Num= "+b.getCsctsRefNum());
				  log.info("NumOfTimesFilled= "+b.getNumOfTimesFilled());
				  
				  
				  
			  }
			  
			  
		  }
		  
		  void initializeBinMap()
		  {
			   
			   log.info("binService="+binService);
			  		
			   List<Bin> binList=binService.findAllBin();  
			  
			   binMap=new ArrayList<Bin>();
				for(int i=0;i<AppConstant.NUM_OF_BINS;i++)
				{
					
//					Bin bin=new Bin();
//					bin.setBinNum(i);
					//binMap.add(bin);			
					binMap.add(i,binList.get(i));
					ArrayList<LimsData> limsData=binService.findLimsDataByBinNum(i+1);
					binMap.get(i).setLimsData(limsData);
					log.info("-- Printing Lims Data for bin =" +(i+1));
					log.info(limsData);
					
					initFillUpStatus(i+1,binList.get(i).getNumOfTimesFilled());
					
				}
				clearCommandStatus();
				log.info("-- Initializing BinMap");
				binMap.forEach(System.out::println) ;
				
				
			    
			    
		  }
		  
		  
		  public String getSampRefNum2()
		  {
		  	String colRefNum="Date";
		  	try
		  	{
		  		 
		  		//log.info(System.currentTimeMillis()/60000);
		  		Long lMin=(System.currentTimeMillis()/60000);
		  		String currMin=lMin.toString();
		  		currMin = currMin.substring(0, currMin.length()-1); //to reduce the length
		  		colRefNum=AppConstant.AUG_SAMP_NUM_2+AppConstant.PLANT_NUMERIC_CODE+AppConstant.AUGER_NUMERIC_CODE+currMin;
		  		
		  		//colRefNum= 
		  		
		  	}catch(Exception ex)
		  	{
		  		ex.printStackTrace();
		  	}
		  	
		  	finally
		  	{
		  		return colRefNum;
		  	}


		  }
		  
		  public String getSampRefNum1()
		  {
		  	String colRefNum="Date";
		  	try
		  	{
		  		 
		  		//log.info(System.currentTimeMillis()/60000);
		  		Long lMin=(System.currentTimeMillis()/60000);
		  		String currMin=lMin.toString();
		  		currMin = currMin.substring(0, currMin.length()-1); //to reduce the length
		  		
		  		
		  		colRefNum=AppConstant.AUG_SAMP_NUM_1+AppConstant.PLANT_NUMERIC_CODE+AppConstant.AUGER_NUMERIC_CODE+currMin;
		  		
		  		//colRefNum= 
		  		
		  	}catch(Exception ex)
		  	{
		  		ex.printStackTrace();
		  	}
		  	
		  	finally
		  	{
		  		return colRefNum;
		  	}


		  }
		  
		  public String getCollRefNum()
		  {
		  	String colRefNum="Date";
		  	try
		  	{
		  		LocalDate ld=LocalDate.now();
		  		log.info(ld.getYear());
		  		log.info(ld.getMonthValue());
		  		log.info(ld.getDayOfMonth());
		  		log.info(System.currentTimeMillis()/600);
		  		Long lMin=(System.currentTimeMillis()/600);
		  		String currMin=lMin.toString();
		  		
		  		colRefNum=AppConstant.PLANT_NUMERIC_CODE+AppConstant.AUGER_NUMERIC_CODE+currMin;
		  		
		  		//colRefNum= 
		  		
		  	}catch(Exception ex)
		  	{
		  		ex.printStackTrace();
		  	}
		  	
		  	finally
		  	{
		  		return colRefNum;
		  	}

		   
}
		  
		  
		  public String getDate()
		  {
		  	LocalDate localDate = LocalDate.now();
		  	String pattern = "dd/MM/YYYY";
		  	DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		  	String formatatedDate = localDate.format(formatter);
		  	//log.info("LocalDate.now: ");
		  	//log.info(localDate);
		  	//log.info("Formatted: ");
		  	//log.info(format);
		  	return formatatedDate;

		  }
		  
		  
		  void processBin(int allocatedBin,int mxTimeFilled)
		  {
			  try {
			  int startSampStatus=-1;
			  ArrayList<String> csCTSrefNumFullList =binMap.get(allocatedBin).getCsCTSrefNumFullList();
	    	  String currentCSCTSRefNum=csCTSrefNumFullList.get(csCTSrefNumFullList.size()-1);
	    	
				//start sampling 
			    log.info("Entered Process Bin Function");	
			    writeToCellWithRetry(AppConstant.COL_SAMP_BIN_NUM,allocatedBin+1);
		    	writeToCellWithRetry(AppConstant.COL_START_SAMP_COMM,AppConstant.NUMBER_ONE);
			    
		    	do //wait for sampling to completed or aborted
		    	{
		    	   log.info("Waiting for Sampling to begin");
		    	   Thread.sleep(5000);
		    	   startSampStatus=getNumericDatadFromCell(AppConstant.COL_AUG_SAMP_STATUS);
		    							    		
		    	}while((startSampStatus!=AppConstant.AUG_SAMP_STARTED)&&(RTMSController.startFlagNew));
		    	log.info("Updating Sampling Status as started");
		    	if(AppConstant.SAMP_STATUS_UPDATE_BYPASS!=1)
		    	{
		    		updateAugerSamplingStatusWithRetry(currentCSCTSRefNum);
		    	}
		    	 
		    	 if (AppConstant.AUGER_CALL_BYPASS!=1)
		    	 {
		    		 
					 int augerMode=getNumericDatadFromCell(AppConstant.COL_AUG_MODE);  
					 String  augModeToSend=getAugerModeFromNum(augerMode);
					 String  augStatusToSend=AppConstant.AUG_STATS_OCCUPIED;
					 log.info("Updating Auger  Status Occupied and Mode = " +augModeToSend);
					 updateAugStatus(AppConstant.AUG_ID,augStatusToSend,augModeToSend,"",AppConstant.PLANT_CODE);
		    	 }
		    	
		    	do //wait for sampling to completed or aborted
		    	{
		    	   log.info("Waiting for Sampling to end");
		    	   Thread.sleep(5000);
		    	   startSampStatus=getNumericDatadFromCell(AppConstant.COL_AUG_SAMP_STATUS);
		    							    		
		    	}while((startSampStatus!=AppConstant.AUG_SAMP_COMPLETED)&&(startSampStatus!=AppConstant.AUG_SAMP_ABORTED)&&(RTMSController.startFlagNew));
		    	
		    	writeToCellWithRetry(AppConstant.COL_SAMP_BIN_NUM,AppConstant.NUMBER_ZERO);
		    	writeToCellWithRetry(AppConstant.COL_START_SAMP_COMM,AppConstant.NUMBER_ZERO);
		    	 
		    	if(startSampStatus==AppConstant.AUG_SAMP_COMPLETED) //graecfully completed
		    	{
		    		//String augurID,String status,String mode,String reasonCode,String plantCode
		    		log.info("Updating Sampling Status as completed");
		    		if(AppConstant.SAMP_STATUS_UPDATE_BYPASS!=1)
			    	{
			    		updateAugerSamplingStatusWithRetry(currentCSCTSRefNum);
			    		
			    		int augerMode=getNumericDatadFromCell(AppConstant.COL_AUG_MODE);  
			    		String  augModeToSend=getAugerModeFromNum(augerMode);
			    		String  augStatusToSend=AppConstant.AUG_STATS_READY;
			    		log.info("Updating Auger  Status Ready and Mode = " +augModeToSend);
			    		updateAugStatus(AppConstant.AUG_ID,augStatusToSend,augModeToSend,"",AppConstant.PLANT_CODE);
			    	}
		    		
		    		//increase count
		    		int numTimesFilled=binMap.get(allocatedBin).getNumOfTimesFilled()+1;
		    		binMap.get(allocatedBin).setNumOfTimesFilled(numTimesFilled);
		    		binService.updateNumTimesFilled(allocatedBin+1, numTimesFilled);
		    		printBinMap(binMap);
		    		switch(allocatedBin) {
		    		 	case 0:
		    		 		writeToCellWithRetry(AppConstant.COL_BIN1_NUM_TIMES_FILL,binMap.get(allocatedBin).getNumOfTimesFilled());
		    		 		break;
		    		 	case 1:
		    		 		writeToCellWithRetry(AppConstant.COL_BIN2_NUM_TIMES_FILL,binMap.get(allocatedBin).getNumOfTimesFilled());
		    		 		break;
		    		 	case 2:
		    		 		writeToCellWithRetry(AppConstant.COL_BIN3_NUM_TIMES_FILL,binMap.get(allocatedBin).getNumOfTimesFilled());
		    		 		break;
		    		 	case 3:
		    		 		writeToCellWithRetry(AppConstant.COL_BIN4_NUM_TIMES_FILL,binMap.get(allocatedBin).getNumOfTimesFilled());
		    		 		break;
		    		 	case 4:
		    		 		writeToCellWithRetry(AppConstant.COL_BIN5_NUM_TIMES_FILL,binMap.get(allocatedBin).getNumOfTimesFilled());
		    		 		break;
		    		 	case 5:
		    		 		writeToCellWithRetry(AppConstant.COL_BIN6_NUM_TIMES_FILL,binMap.get(allocatedBin).getNumOfTimesFilled());
		    		 		break;
		    		 	case 6:
		    		 		writeToCellWithRetry(AppConstant.COL_BIN7_NUM_TIMES_FILL,binMap.get(allocatedBin).getNumOfTimesFilled());
		    		 		break;
		    		 	case 7:
		    		 		writeToCellWithRetry(AppConstant.COL_BIN8_NUM_TIMES_FILL,binMap.get(allocatedBin).getNumOfTimesFilled());
		    		 		break;
		    		 	default:
		    		 		log.info("Nothing Matched");
		    		 		break;
		    		}
		    		if(binMap.get(allocatedBin).getNumOfTimesFilled()==mxTimeFilled) 
		    		{
		    			  clearBin(binMap,allocatedBin);
		    			  doPostCleanUp(allocatedBin);
		    			  log.info("Printing Bin Map after cleaning--");
		    			  printBinMap(binMap);
		    		}
		    		 
		    		
		    		
		    	}
		    	else if(startSampStatus==AppConstant.AUG_SAMP_ABORTED)
		    	{
		    		log.info("--Abotring command received -- ");
		    		//if(AppConstant.SAMP_STATUS_UPDATE_BYPASS!=1)
			    	 
		    			//removing the last cstcts ref entry from the map
		    			ArrayList<String> csCTTSrefNumFullList =binMap.get(allocatedBin).getCsCTSrefNumFullList();		    			
		    			
		    			if((null!=csCTTSrefNumFullList)&& (csCTTSrefNumFullList.size()>=1))
		    			{		
		    				log.info("--deleteing CSCTSList -- ");
		    				binService.deleteLastCSTCTSListEntry(binMap.get(allocatedBin).getBinNum()+1);
		    				csCTTSrefNumFullList.remove(csCTTSrefNumFullList.size()-1);
		    				binMap.get(allocatedBin).setCsCTSrefNumFullList(csCTTSrefNumFullList);
		    				log.info("--Printing Map after removal  ");
		    				printBinMap(binMap);
		    			
		    			}
		    			if(AppConstant.SAMP_STATUS_UPDATE_BYPASS!=1)
				    	{
		    				
		    				updateAugerSamplingStatusWithRetry(currentCSCTSRefNum);
		    			    int augerMode=getNumericDatadFromCell(AppConstant.COL_AUG_MODE); 
		    			    int augerStatus=getNumericDatadFromCell(AppConstant.COL_AUG_STATUS);  
							String  augModeToSend=getAugerModeFromNum(augerMode);
							String  augStatusToSend=getAugerStatusFromNum(augerStatus);
							log.info("Updating Auger  Status "+augStatusToSend + " and mode =" +augModeToSend);
							updateAugStatus(AppConstant.AUG_ID,augStatusToSend,augModeToSend,"",AppConstant.PLANT_CODE);
					    }
			    	 
		    		
		    	}
		    	checkBoomStatusChangeNonScheduled();
		    	
		    	
			  }catch (Exception ex)
			  {
				  log.error("Error in process Bin:" + ex);
				  ex.printStackTrace();
				  
				  
			  }
			  
		  }
		  
		 
			public ResponseEntity updateAugStatus(String augurID,String status,String mode,String reasonCode,String plantCode) {
				// TODO Auto-generated method stub
				log.info("Inside updateAugStatus");
				ResponseEntity<ResponseAug> response=null;
			    try {
			    	writeToCellWithRetry(AppConstant.COL_DATA_ERROR,AppConstant.NUMBER_ZERO);
			    	//writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ZERO);
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON);
				headers.add("plantCode", plantCode);
				
				//MultiValueMap<String, Object> map= new LinkedMultiValueMap<String, Object>();
				Map<String, Object> map = new HashMap<>();
				map.put("augurID", augurID);
				map.put("status", status);		
				map.put("mode", mode);
				map.put("reason_code", reasonCode);
				//HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
				HttpEntity<Map<String, Object>> request = new HttpEntity<>(map, headers);
				log.info(request);
				String url=AppConstant.BASE_URL+":"+AppConstant.BASE_PORT+"/"+AppConstant.API_UPDATE_AUG_STATUS;
				
					
				response = restTemplate.postForEntity( url, request , ResponseAug.class );
				
				    if(null!=response)
				    {
					 
						log.info(response.getBody().getStatusCode());
						log.info(response.getBody().getErrorMessage());	
						
						writeToCellWithRetry(AppConstant.COL_HTTP_STATUS_CODE,Integer.parseInt(response.getBody().getStatusCode()));
						writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ZERO);
				    }
				    else
				    {
				    	log.info("Response Null - updateAugStatus");
				    	writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ONE);
				    }
				
			    }
			    catch(UnknownHttpStatusCodeException  ex) {
			    	
			    	log.info("updateAugStatus - Data error"+ex.getMessage());		    		    
			    	log.info(ex.getRawStatusCode());
			    	writeToCellWithRetry(AppConstant.COL_DATA_ERROR,AppConstant.NUMBER_ONE);
			    	writeToCellWithRetry(AppConstant.COL_HTTP_STATUS_CODE,ex.getRawStatusCode());
			       
			    }
			   catch(Exception commError) {
				   
				   log.info("updateAugStatus -Communication Error: "+commError.getMessage());
				//   log.info(commError.getMessage());
				   writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ONE);
			   }
			    return response; 
			    
			}
			    
			    
			    public ResponseEntity updateAugSampStatus(String augurID,String sampleRefId,String reasonCode,String plantCode) {
					// TODO Auto-generated method stub
					log.info("Inside updateAugSampStatus");
					int augerStatus=getNumericDatadFromCell(AppConstant.COL_AUG_SAMP_STATUS);
					int augerMode=getNumericDatadFromCell(AppConstant.COL_AUG_MODE);
					
					String status=getAugerSampStatusFromNum(augerStatus);
					if(status.equals(""))
					{
						log.info("updateAugSampStatus--status  empty");
						return null;
					}
					
					String mode=getAugerModeFromNum(augerMode);
					
					ResponseEntity<ResponseAug> response=null;
				    try {
				    	writeToCellWithRetry(AppConstant.COL_DATA_ERROR,AppConstant.NUMBER_ZERO);
				    	//writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ZERO);
					HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.add("plantCode", plantCode);
					
					//MultiValueMap<String, Object> map= new LinkedMultiValueMap<String, Object>();
					Map<String, Object> map = new HashMap<>();
					map.put("augurID", augurID);
					map.put("sampleRefId", sampleRefId);		
					map.put("status", status);		
					map.put("mode", mode);
					map.put("reason_code", reasonCode);
					//HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
					HttpEntity<Map<String, Object>> request = new HttpEntity<>(map, headers);
					System.out.println("--updateAugSampStatus--JSON");
					System.out.println(request);
					String url=AppConstant.BASE_URL+":"+AppConstant.BASE_PORT+"/"+AppConstant.API_UPDATE_AUG_SAMP_STATUS;
					log.info("--updateAugSampStatus-->URL="+url);
					response = restTemplate.postForEntity( url, request , ResponseAug.class );
					
					if(null!=response)
					{
						//response.getBody().getStatusCode();
						 
						log.info(response.getBody().getStatusCode());
						log.info(response.getBody().getErrorMessage());	
						
						writeToCellWithRetry(AppConstant.COL_HTTP_STATUS_CODE,Integer.parseInt(response.getBody().getStatusCode()));
						writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ZERO);
					}
				    
				    else
				    {
						writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ONE);
				    }
			       
				 }
			    catch(UnknownHttpStatusCodeException  ex) {
			    	
			    	log.info("updateAugSampStatus - Data error"+ex.getMessage());		    		    
			    	log.info(ex.getRawStatusCode());
			    	writeToCellWithRetry(AppConstant.COL_DATA_ERROR,AppConstant.NUMBER_ONE);
			    	writeToCellWithRetry(AppConstant.COL_HTTP_STATUS_CODE,ex.getRawStatusCode());
			       
			    }
			   catch(Exception commError) {
				   
				   log.info("updateAugSampStatus -Communication Error: "+commError.getMessage());
				//   log.info(commError.getMessage());
				   writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ONE);
			   }
			    
				return response;
				
			}
			
			
			public ResponseEntity updateBoomStatus(String augurID,String status,String boomId,String plantCode) {
				// TODO Auto-generated method stub
				log.info("Inside updateBoomStatus");
				ResponseEntity<ResponseAug> response=null;
			    try {
			    	writeToCellWithRetry(AppConstant.COL_DATA_ERROR,AppConstant.NUMBER_ZERO);
			    	//writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ZERO);
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON);
				headers.add("plantCode", plantCode);
				
				//MultiValueMap<String, Object> map= new LinkedMultiValueMap<String, Object>();
//				Map<String, Object> map = new HashMap<>();
//				map.put("augurID", augurID);
//				map.put("boomid", boomId);		
//				map.put("status", status);
				JsonObject jsonObject = new JsonObject();
				jsonObject.addProperty("augurID", augurID);
				jsonObject.addProperty("boomId", boomId);	
				jsonObject.addProperty("status", status);
				//HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
				//HttpEntity<Map<String, Object>> request = new HttpEntity<>(jsonObject, headers);
				HttpEntity<String> request = new HttpEntity<String>(jsonObject.toString(), headers);
				log.info("update Boom Status Request="+request);
				String url=AppConstant.BASE_URL+":"+AppConstant.BASE_PORT+"/"+AppConstant.API_UPDATE_BOOM_STATUS;
				 log.info("boom URl="+url); 
				 response = restTemplate.postForEntity( url, request , ResponseAug.class );
				 
				 if(null!=response)
				 {
										  	
					 log.info("updateBoomStatus "+response.getBody().getStatusCode());
					 log.info("updateBoomStatus "+ response.getBody().getErrorMessage());				
					 writeToCellWithRetry(AppConstant.COL_HTTP_STATUS_CODE,Integer.parseInt(response.getBody().getStatusCode()));
					 if(response.getBody().getStatusCode()=="200")
					 {
						 writeToCellWithRetry(AppConstant.COL_TRIGG_OUT_BOOM,AppConstant.NUMBER_ZERO);
					 }
					 writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ZERO);
				 }
				 else
				 {
					 log.info("updateBoomStatus response null");
					 
					 writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ONE);
				 }
				 
			    }
			    catch(UnknownHttpStatusCodeException  ex) {
			    	
			    	log.info("updateBoomStatus->Data error"+ex.getMessage());		    		    
			    	log.info(ex.getRawStatusCode());
			    	writeToCellWithRetry(AppConstant.COL_HTTP_STATUS_CODE,ex.getRawStatusCode());
			    	writeToCellWithRetry(AppConstant.COL_DATA_ERROR,AppConstant.NUMBER_ONE);
			    	
			       
			    }
			   catch(Exception commError) {
				   
				   log.info("updateBoomStatus->Communication Error: "+commError.getMessage());
				//   log.info(commError.getMessage());
				   writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ONE);
				   
			   }
			    
				return response;
				
			}
//			@Scheduled(cron = "0 */1 * ? * *")
//			void resetHeartBeat()
//			{
//				writeToCell(AppConstant.COL_HEART_BEAT,AppConstant.NUMBER_ZERO);
//				
//				
//			}
			
			
			
			//@Scheduled(cron = "*/30 * * * * *")
			//@Scheduled(fixedRate = 30000)
			void checkBoomStatusChangeScheduled()
			{
				log.info("--checkBoomStatusChange-Entered-");
				if (AppConstant.AUGER_CALL_BYPASS!=1) {
					
				//1. check for B12 1 
				//if 1 then call updateAugStatus with values
				log.info("--checkBoomStatusChange-Start-");
				int status = 0;
				int boomTrigger=getNumericDatadFromCell(AppConstant.COL_TRIGG_OUT_BOOM);
				 
				if(boomTrigger!=1)
				{
					return ;
				}
				
				//status=getNumericDatadFromCell(AppConstant.COL_STATS_MOD_CHANGE);
				
				if(boomTrigger==1) {
					 
					int boomStatusIn = 0;
					int boomStatusOut = 0;
					String boomStatusInToSend="";
					String boomStatusOutToSend="";
					boomStatusIn=getNumericDatadFromCell(AppConstant.COL_BOOM_ENTRY_STATUS);
					boomStatusOut=getNumericDatadFromCell(AppConstant.COL_BOOM_EXIT_STATUS);
					 	
					if (boomStatusIn==0) {
						boomStatusInToSend=AppConstant.BOOM_GATE_OPEN;
					}
					else if (boomStatusIn==1) {
						boomStatusInToSend=AppConstant.BOOM_GATE_CLOSE;
					}
					if (boomStatusOut==0) {
						boomStatusOutToSend=AppConstant.BOOM_GATE_OPEN;
					}
					else if (boomStatusOut==1) {
						boomStatusOutToSend=AppConstant.BOOM_GATE_CLOSE;
					}
					//updateBoomStatus(AppConstant.AUG_ID,boomStatusInToSend,"IN",AppConstant.PLANT_CODE);
					updateBoomStatus(AppConstant.AUG_ID,boomStatusOutToSend,"out",AppConstant.PLANT_CODE);
				
				}
				log.info("--checkBoomStatusChange-End-");
				}
				
			}
			
			
			void checkBoomStatusChangeNonScheduled() throws InterruptedException
			{
				log.info("--checkBoomStatusChangeNonScheduled-Entered-");
				if (AppConstant.AUGER_CALL_BYPASS!=1) {
					
				 
				log.info("--checkBoomStatusChangeNonScheduled-Start-");
				int status = 0;
				int boomTrigger=getNumericDatadFromCell(AppConstant.COL_TRIGG_OUT_BOOM);
				 
				if(boomTrigger!=1)
				{
					return ;
				}
				
			 
				
				if(boomTrigger==1) {
					
					int boomGateOpenStatus=-1;
					do //wait for sampling to completed or aborted
			    	{
			    	   log.info("Waiting for Boom Gate open Signal");
			    	   Thread.sleep(5000);
			    	   boomGateOpenStatus=getNumericDatadFromCell(AppConstant.COL_BOOM_EXIT_STATUS);
			    							    		
			    	}while((boomGateOpenStatus!=AppConstant.NUMBER_ZERO)&&(RTMSController.startFlagNew));
					
					
					updateBoomStatus(AppConstant.AUG_ID,AppConstant.BOOM_GATE_OPEN,AppConstant.BOOM_GATE_EXIT,AppConstant.PLANT_CODE);
					
					Thread.sleep(5000);			 
					 
					updateBoomStatus(AppConstant.AUG_ID,AppConstant.BOOM_GATE_CLOSE,AppConstant.BOOM_GATE_EXIT,AppConstant.PLANT_CODE);

				
				}
				log.info("--checkBoomStatusChangeNonScheduled-End-");
				}
				log.info("--checkBoomStatusChangeNonScheduled-Exited-");
			}
			
			
			//@Scheduled(cron = "* */2 * * * *")	
			void checkAugerStatusChange()
			{
				if (AppConstant.AUGER_CALL_BYPASS!=1) {
					
				//1. check for B12 1 
				//if 1 then call updateAugStatus with values
				
				int status = 0;
				
				status=getNumericDatadFromCell(AppConstant.COL_STATS_MOD_CHANGE);
				
				if(status==1) {
					log.info("--checkAugerStatusChange-Start-");
					int augerStatus = 0;
					int augerMode = 0;
					 
					String augStatusToSend="";
					String augModeToSend="";
					 
					augerStatus=getNumericDatadFromCell(AppConstant.COL_AUG_STATUS);
					augerMode=getNumericDatadFromCell(AppConstant.COL_AUG_MODE);
					 
					augStatusToSend=getAugerStatusFromNum(augerStatus);
					augModeToSend=getAugerModeFromNum(augerMode);
					if(augerStatus==0) {
						augStatusToSend=AppConstant.AUG_STATS_READY;
					}
					else if (augerStatus==1) {
						augStatusToSend=AppConstant.AUG_STATS_OCCUPIED;
					}
					else if (augerStatus==2) {
						augStatusToSend=AppConstant.AUG_STATS_DOWN;
					}
					else {
						augStatusToSend=AppConstant.AUG_STATS_EMPTY;
					}
					if(augerMode==0) {
						augModeToSend=AppConstant.AUG_MODE_MANUAL;
					}
					else if(augerMode==1) {
						augModeToSend=AppConstant.AUG_MODE_AUTO;
					}
					else if (augerMode==2) {
						augModeToSend=AppConstant.AUG_MODE_SEMI;
					}
					
					updateAugStatus(AppConstant.AUG_ID,augStatusToSend,augModeToSend,"",AppConstant.PLANT_CODE);
					
				 }
				log.info("--checkAugerStatusChange-End-");
				}
				
			}
			
			 
						  
			public  void doPostCleanUp(int currentBin)
			{
				  //printForBarCode(currentBin);
					writeToCellWithRetry(AppConstant.COL_BIN_EMPTIED_PRINTING_DONE,AppConstant.NUMBER_ZERO);
					writeToCellWithRetry(AppConstant.COL_DO_PRINTING,AppConstant.NUMBER_ZERO);
					//writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ZERO);
				    cleanFillUpStatus(currentBin);
			}
			
			void cleanFillUpStatus(int binNum)
			{
				
				switch(binNum) {
				
	    		 	case 0:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN1_NUM_TIMES_FILL,AppConstant.NUMBER_ZERO);
	    		 		break;
	    		 	case 1:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN2_NUM_TIMES_FILL,AppConstant.NUMBER_ZERO);
	    		 		break;
	    		 	case 2:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN3_NUM_TIMES_FILL,AppConstant.NUMBER_ZERO);
	    		 		break;
	    		 	case 3:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN4_NUM_TIMES_FILL,AppConstant.NUMBER_ZERO);
	    		 		break;
	    		 	case 4:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN5_NUM_TIMES_FILL,AppConstant.NUMBER_ZERO);
	    		 		break;
	    		 	case 5:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN6_NUM_TIMES_FILL,AppConstant.NUMBER_ZERO);
	    		 		break;
	    		 	case 6:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN7_NUM_TIMES_FILL,AppConstant.NUMBER_ZERO);
	    		 		break;
	    		 	case 7:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN8_NUM_TIMES_FILL,AppConstant.NUMBER_ZERO);
	    		 		break;
	    		 	default:
	    		 		log.info("Nothing Matched");
	    		 		break;
				}
			}
			
			void clearCommandStatus()
			{
				writeToCellWithRetry(AppConstant.COL_SAMP_BIN_NUM,AppConstant.NUMBER_ZERO);
				writeToCellWithRetry(AppConstant.COL_START_SAMP_COMM,AppConstant.NUMBER_ZERO);
				writeToCellWithRetry(AppConstant.COL_EMPTY_BIN_NUM,AppConstant.NUMBER_ZERO);
				writeToCellWithRetry(AppConstant.COL_START_SAMP_COMM,AppConstant.NUMBER_ZERO);
			}
			
			void initFillUpStatus(int binNum,int numTimesFilled)
			{
				
				switch(binNum) {
				
	    		 	case 1:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN1_NUM_TIMES_FILL,numTimesFilled);
	    		 		break;
	    		 	case 2:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN2_NUM_TIMES_FILL,numTimesFilled);
	    		 		break;
	    		 	case 3:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN3_NUM_TIMES_FILL,numTimesFilled);
	    		 		break;
	    		 	case 4:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN4_NUM_TIMES_FILL,numTimesFilled);
	    		 		break;
	    		 	case 5:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN5_NUM_TIMES_FILL,numTimesFilled);
	    		 		break;
	    		 	case 6:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN6_NUM_TIMES_FILL,numTimesFilled);
	    		 		break;
	    		 	case 7:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN7_NUM_TIMES_FILL,numTimesFilled);
	    		 		break;
	    		 	case 8:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN8_NUM_TIMES_FILL,numTimesFilled);
	    		 		break;
	    		 	default:
	    		 		log.info("Nothing Matched");
	    		 		break;
				}
			}
			
			void refreshFillUpStatus(int binNum,int numTimesFilled)
			{
				
				switch(binNum) {
				
	    		 	case 0:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN1_NUM_TIMES_FILL,numTimesFilled);
	    		 		break;
	    		 	case 1:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN2_NUM_TIMES_FILL,numTimesFilled);
	    		 		break;
	    		 	case 2:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN3_NUM_TIMES_FILL,numTimesFilled);
	    		 		break;
	    		 	case 3:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN4_NUM_TIMES_FILL,numTimesFilled);
	    		 		break;
	    		 	case 4:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN5_NUM_TIMES_FILL,numTimesFilled);
	    		 		break;
	    		 	case 5:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN6_NUM_TIMES_FILL,numTimesFilled);
	    		 		break;
	    		 	case 6:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN7_NUM_TIMES_FILL,numTimesFilled);
	    		 		break;
	    		 	case 7:
	    		 		writeToCellWithRetry(AppConstant.COL_BIN8_NUM_TIMES_FILL,numTimesFilled);
	    		 		break;
	    		 	default:
	    		 		log.info("Nothing Matched");
	    		 		break;
				}
			}
			
			public  boolean createImage(String imagePath,String imageName,String myString)  {
				boolean retVal=false;
				try {
			    
				Code128Bean code128 = new Code128Bean();
				code128.setHeight(7f);
				code128.setModuleWidth(0.3);
				code128.setQuietZone(10);
				code128.doQuietZone(true);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				BitmapCanvasProvider canvas = new BitmapCanvasProvider(baos, "image/x-png", 300, BufferedImage.TYPE_BYTE_BINARY, false, 0);
				code128.generateBarcode(canvas, myString);
				canvas.finish();
				//write to png file
				 
				Files.createDirectories(Paths.get(imagePath));
				String fullPath=imagePath+"//"+imageName;
				FileOutputStream fos = FileUtils.openOutputStream(new File(fullPath)); 
				fos.write(baos.toByteArray());
				fos.flush();
				fos.close();
				retVal=true;
				} catch (Exception e) {
					// TODO: handle exception
					log.info(e.toString());
				}
				finally {
					
					return retVal;
				}
			}
			
			public  void createImage_old(String image_name,String myString)  {
				try {
				Code128Bean code128 = new Code128Bean();
				code128.setHeight(7f);
				code128.setModuleWidth(0.3);
				code128.setQuietZone(10);
				code128.doQuietZone(true);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				BitmapCanvasProvider canvas = new BitmapCanvasProvider(baos, "image/x-png", 300, BufferedImage.TYPE_BYTE_BINARY, false, 0);
				code128.generateBarcode(canvas, myString);
				canvas.finish();
				//write to png file
				FileOutputStream fos = new FileOutputStream(myString+image_name,false);
				fos.write(baos.toByteArray());
				fos.flush();
				fos.close();
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			 	
			
			@Scheduled(cron = "0 15 * * * *")			 
			void refreshBinFillStatus()
			{
				log.info("---refreshBinFillStatus-Start--");
				
				if((binMap!=null)&&(!binMap.isEmpty()))
				{
					for (Bin bin:binMap)
					{
						refreshFillUpStatus(bin.getBinNum(),bin.getNumOfTimesFilled());				
										
					}
				}
								
				log.info("---refreshBinFillStatus-Start--");
			}
			
//			@Scheduled(cron = "0 0/1 * * * *")			 
//			void masterReset()
//			{
//				log.info("---masterReset check-Start--");
//				
//				try { 
//					
//					 int masterResetFlag=getNumericDatadFromCell(AppConstant.COL_MASTER_RESET);
//					 
//					 if(masterResetFlag==AppConstant.NUMBER_ONE)
//					 {
//						 log.info("---masterReset pressed---");
//						 writeToCellWithRetry(AppConstant.COL_VEH_ARRVD,AppConstant.NUMBER_ZERO); 
//						 writeToCellWithRetry(AppConstant.COL_VEH_REG_NUM,AppConstant.NUMBER_ZERO); 
//						 writeToCellWithRetry(AppConstant.COL_VEH_REG_ENTERED,AppConstant.NUMBER_ZERO); 
//						 writeToCellWithRetry(AppConstant.COL_SAMP_BIN_NUM,AppConstant.NUMBER_ZERO); 
//						 writeToCellWithRetry(AppConstant.COL_START_SAMP_COMM,AppConstant.NUMBER_ZERO); 
//						 writeToCellWithRetry(AppConstant.COL_EMPTY_BIN_NUM,AppConstant.NUMBER_ZERO); 
//						 writeToCellWithRetry(AppConstant.COL_EMPTY_BIN_NUM_COMM,AppConstant.NUMBER_ZERO); 						
//						 writeToCellWithRetry(AppConstant.COL_BIN_EMPTIED_PRINTING_DONE,AppConstant.NUMBER_ZERO);
//						 writeToCellWithRetry(AppConstant.COL_DO_PRINTING,AppConstant.NUMBER_ZERO);
//						 writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ZERO);
//						 writeToCellWithRetry(AppConstant.COL_RFID_ERROR,AppConstant.NUMBER_ZERO);
//						 writeToCellWithRetry(AppConstant.COL_DATA_ERROR,AppConstant.NUMBER_ZERO);
//			    	    
//						for (Bin bin:binMap)
//						{
//							cleanFillUpStatus(bin.getBinNum());
//							
//							
//						}
//						
//						writeToCellWithRetry(AppConstant.COL_MASTER_RESET,AppConstant.NUMBER_ZERO);  
//						binMap.clear();
//						initializeBinMap();
//					 }
//				}catch (Exception ex)
//				{
//					log.info("--error in masterReset-- ");
//					log.info(ex.toString());
//				}
//				
//				log.info("---masterReset check-End--");
//				
//			}
//			
			String getAugerStatusFromNum(int augerStatus)
			{
				String augStatusToSend="";
				
				if(augerStatus==0) {
					augStatusToSend=AppConstant.AUG_STATS_READY;
				}
				else if (augerStatus==1) {
					augStatusToSend=AppConstant.AUG_STATS_OCCUPIED;
				}
				else if (augerStatus==2) {
					augStatusToSend=AppConstant.AUG_STATS_DOWN;
				}
				else {
					augStatusToSend=AppConstant.AUG_STATS_EMPTY;
				}
				
				
				return augStatusToSend;
			}
			
			String getAugerSampStatusFromNum(int augerStatus)
			{
				String augStatusToSend="";
				
				if(augerStatus==0) {
					augStatusToSend=AppConstant.AUG_SAMP_STATS_COMPLETED;
				}
				else if (augerStatus==1) {
					augStatusToSend=AppConstant.AUG_SAMP_STATS_STARTED;
				}
				else if (augerStatus==2) {
					augStatusToSend=AppConstant.AUG_SAMP_STATS_ABORTED;
				}
								
				
				
				
				return augStatusToSend;
			}
			
			String getAugerModeFromNum(int augerMode)
			{
				String augModeToSend="";
				
				if(augerMode==0) {
					augModeToSend=AppConstant.AUG_MODE_MANUAL;
				}
				else if(augerMode==1) {
					augModeToSend=AppConstant.AUG_MODE_AUTO;
				}
				else if (augerMode==2) {
					augModeToSend=AppConstant.AUG_MODE_SEMI;
				}
				return augModeToSend;
				
				
			}
			
			void updateAugerSamplingStatusWithRetry(String currentCSCtsRefNumFull)
			{
				
				ResponseEntity response=null;
	    		int retryCount=0;
	    		boolean flag=false;
	    		do
	    		{
	    			 retryCount++;
	    			 System.out.println("Updating Auger Sampling Status- attempt="+retryCount);
	    			 String reasonCode=getSamplingReasonCode();
	    			 response=updateAugSampStatus(AppConstant.AUG_ID,currentCSCtsRefNumFull,reasonCode,AppConstant.PLANT_CODE);
	    			 if((null==response)&&(retryCount<4))
	    			 {
	    				 continue;
	    			 }else if((null==response)&&(retryCount==4)) {
	    				 flag =true;
	    				 break;
	    			 }
	    			 else  
	    			 {
	    				 if(response.getStatusCode()==HttpStatus.OK)
	    				 {
	    					 flag =true;
	    				 }
	    			 }
	    				 
	    			
	    		}while(!flag);
	    		
				
			}
			
			public ResponseEntity<ResponseLims> sendLimsDataFromJsonString(String jSonString,int recordId) {
				 
				log.info("--Inside sendLimsDataFromJsonString--");
				ResponseEntity<ResponseLims> response=null;
				log.info("limsData="+jSonString);
				try {
					HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					HttpEntity<String> entity = new HttpEntity<String>(jSonString, headers);
					String url=AppConstant.API_LIMS_URL;
					log.info("-limsData-Before Call--");
					response= restTemplate.exchange(url, HttpMethod.POST, entity, ResponseLims.class);
					log.info("-limsData-After Call--");	 
					if(null!=response)
			 	 	{
						
					log.info(response.getBody().getStatusCode());
			    	if(response.getBody().getStatusCode().equals("200"))
			    	{
					 log.info("--deleteing record from Lims JSon Table");
					 binService.deleteLimsJsonData(recordId);
			    	}
			    		writeToCellWithRetry(AppConstant.COL_HTTP_STATUS_CODE,Integer.parseInt(response.getBody().getStatusCode()));
			    		writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ZERO);
			 	 	}
			 	 	else
			 	 	{
			 	 		writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ONE);
			 	 	}
			    }
			    catch(HttpClientErrorException  ex)
			    {
			    	log.info("--Exception Inside sendLimsDataFromJsonString-HttpClientErrorException");
			    	ex.printStackTrace();
			     			    
			    }
			    catch(UnknownHttpStatusCodeException  ex) {
			      log.info("--Exception Inside sendLimsDataFromJsonString-UnknownHttpStatusCodeException");
			      log.info(ex.getRawStatusCode());
			      writeToCellWithRetry(AppConstant.COL_HTTP_STATUS_CODE,ex.getRawStatusCode());
			      
	 			    
			    }
			    catch(Exception ex)
			    {
			    	log.info("--Exception Inside sendLimsDataFromJsonString-"+ex.getMessage());
	 		    	
			    }
				return response;
				
			}
	
//			@Scheduled(cron = "* * * * * *")
//			void testBoomStatus()
//			{
//				updateBoomStatus(AppConstant.AUG_ID,AppConstant.BOOM_GATE_OPEN,AppConstant.BOOM_GATE_EXIT,AppConstant.PLANT_CODE);
//			}
			
			
			
			@Scheduled(cron = "* 30 * * * *")	
			void checkAndProcessUnsentLimsData()
			{
				log.info("--checkAndProcessUnsentLimsData--");
				List<LimsDataEntityJson> listLimsDataJson= binService.findAllByIsProcessed(false);
				for(LimsDataEntityJson limsDataEntityJson:listLimsDataJson)
				{
					sendLimsDataFromJsonString(limsDataEntityJson.getLimsDataJson(),limsDataEntityJson.getId());
					
					
				}
				
				
			}
			
			String  getSamplingReasonCode()
			{
				 
				     String reasonCode=null;
				 
					 try {
					  Thread.sleep(12000);
					  String fileName=AppConstant.PLC_BASE_FILE_PATH+File.separator+AppConstant.PLC_FILE_NAME;
					
					  FileInputStream file = new FileInputStream(new File(fileName));
					  XSSFWorkbook workbook = new XSSFWorkbook(file);
					  XSSFSheet sheet = workbook.getSheetAt(0);
					  
					  XSSFRow rowi= sheet.getRow(0);
					  CellReference cellReference = new CellReference(AppConstant.COL_REASON_SAMP_STATUS); 
					  Row row = sheet.getRow(cellReference.getRow());
					  Cell cell = row.getCell(cellReference.getCol()); 
					  reasonCode= cell.getStringCellValue();
					  workbook.close();
					  
					
					   
					  
					  //getStringDatadFromCell();
					  log.info("auger sampling reason code="+reasonCode);
					  workbook.close();
					   
					  
					 }
					 catch(Exception ex)
					 {
						 ex.printStackTrace();
						 
						 
					 }
					
				
				return reasonCode;
			}
		 
}