package com.rtms.augersys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.GetMapping;

import com.rtms.augersys.main.MainApp;
import com.rtms.augersys.service.BinService;
import com.rtms.augersys.util.AppConfig;
import com.rtms.augersys.util.AppConstant;
@EnableScheduling
@SpringBootApplication
@Configuration
 
public class AugersysApplication {
	@Autowired
	static BinService binService;
	public static volatile boolean startFlagNew =false;  
	static Thread mapp=null;
	@Autowired
	public static AppConfig appConfig;
	//public static final Logger log = LoggerFactory.getLogger(this.getClass());
	public static void main(String[] args) {
		
		SpringApplication.run(AugersysApplication.class, args);
		//newstart();
	
	}
	 public static void initializeConfig()
	 {
		 AppConstant.BASE_URL=appConfig.getBaseUrl();
		 AppConstant.BASE_PORT=appConfig.getBasePort();
		 AppConstant.API_LIMS_URL=appConfig.getLimsURL();
		 AppConstant.AUG_ID=appConfig.getAugerCode();
		 AppConstant.BIN_MAX_NUM_TIMES_FILLED=appConfig.getMaxFillBin();
		 AppConstant.AUGER_CALL_BYPASS=appConfig.getAugCallBypass();
		 AppConstant.API_GET_SAMP_RATE=appConfig.getApiGetSampRefUrl();
		 AppConstant.API_UPDATE_AUG_STATUS=appConfig.getApiUpdateAugUrl();
		 AppConstant.API_UPDATE_BOOM_STATUS=appConfig.getApiUpdateBoomStatusUrl();
		 AppConstant.PLC_BASE_FILE_PATH=appConfig.getPlcFilePath();
		 AppConstant.PLC_FILE_NAME=appConfig.getPlcFileName();
		 AppConstant.PLANT_CODE=appConfig.getPlantCode();
		 AppConstant.AUG_ID=appConfig.getAugerCode(); 
		 AppConstant.AUGER_NUMERIC_CODE=appConfig.getAugerNumCode();
		 AppConstant.PLANT_NUMERIC_CODE=appConfig.getPlantNumCode();
		 AppConstant.SPECIAL_BIN_MAX_NUM_TIMES_FILLED=appConfig.getMaxFillSpecialBin();
		 AppConstant.STEP1_CALL_BYPASS=appConfig.getStep1Bypass();
		 AppConstant.NUM_OF_BINS=appConfig.getNumBin();
		 AppConstant.SAMP_STATUS_UPDATE_BYPASS=appConfig.getSampStatusBypass();
		 AppConstant.LIMS_PLANT_CODE=appConfig.getLimsPlantCode();
//		 log.info("----Loading initialization parameter----");
//		 
//		 
//		 log.info("BASE_URL="+AppConstant.BASE_URL);
//		 log.info("BASE_PORT="+AppConstant.BASE_PORT);
//		 log.info("API_LIMS_URL="+AppConstant.API_LIMS_URL);
//		 log.info("LIMS_PLANT_CODE="+AppConstant.LIMS_PLANT_CODE);
//		 log.info("AUG_ID="+AppConstant.AUG_ID);
//		 log.info("BIN_MAX_NUM_TIMES_FILLED="+AppConstant.BIN_MAX_NUM_TIMES_FILLED);
//		 log.info("AUGER_CALL_BYPASS="+AppConstant.AUGER_CALL_BYPASS);
//		 log.info("API_GET_SAMP_RATE="+AppConstant.API_GET_SAMP_RATE);
//		 log.info("API_UPDATE_AUG_STATUS="+AppConstant.API_UPDATE_AUG_STATUS);
//		 log.info("API_UPDATE_BOOM_STATUS="+AppConstant.API_UPDATE_BOOM_STATUS);
//		 log.info("PLC_BASE_FILE_PATH="+AppConstant.PLC_BASE_FILE_PATH);
//		 log.info("PLC_FILE_NAME="+AppConstant.PLC_FILE_NAME);
//		 log.info("PLANT_CODE="+AppConstant.PLANT_CODE);
//		 log.info("AUG_ID="+AppConstant.AUG_ID);
//		 log.info("AUGER_NUMERIC_CODE="+AppConstant.AUGER_NUMERIC_CODE);
//		 log.info("SPECIAL_BIN_MAX_NUM_TIMES_FILLED="+AppConstant.SPECIAL_BIN_MAX_NUM_TIMES_FILLED);	
//		 log.info("SAMP_STATUS_UPDATE_BYPASS="+AppConstant.SAMP_STATUS_UPDATE_BYPASS);	
//		 log.info("---initialization parameter ends----");
	 }
	 
	  
	
	public static void  newstart() {
//		 log.info("****Here---");
		 //initializeConfig();
		 
	     System.out.println("New Start");
	     if(!startFlagNew)
	     {
	    	 startFlagNew=true;
	    	 mapp  = new Thread(new MainApp(binService));	     
	    	// log.info("****Started---&&&&&&");
	    	 mapp.start();
	     }
		 return ;
	 }
 
}
