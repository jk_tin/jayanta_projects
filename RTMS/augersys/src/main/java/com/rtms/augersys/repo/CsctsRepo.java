package com.rtms.augersys.repo;

import java.util.List;
import java.util.Optional;

import javax.persistence.Entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rtms.augersys.entity.CSctsListEntity;

@Repository 
public interface CsctsRepo extends JpaRepository <CSctsListEntity, Integer> {
 
	List<CSctsListEntity>  findByBinNum(int binNum);
	
	List<CSctsListEntity>  findByBinNumOrderByIdDesc(int binNum);
}
