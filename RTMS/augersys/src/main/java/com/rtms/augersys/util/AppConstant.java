package com.rtms.augersys.util;

public class AppConstant {

	private AppConstant() {
	
	
	}
	
	/*status_code -  “empty”, “ready”,”down”,”occupied”
mode_value -  “auto”,”semi”,”manual” 
*/
	public static   String AUG_SAMP_STATS_STARTED = "started";
	public static   String AUG_SAMP_STATS_COMPLETED = "completed";
	public static   String AUG_SAMP_STATS_ABORTED = "aborted";
	public static   String AUG_STATS_EMPTY = "empty";
	public static   String AUG_STATS_READY = "ready";
	public static   String AUG_STATS_DOWN = "down";
	public static   String AUG_STATS_OCCUPIED = "occupied";
	public static   String AUG_MODE_AUTO = "auto";
	public static   String AUG_MODE_SEMI = "semi";
	public static   String AUG_MODE_MANUAL = "manual";
	public static   String BASE_URL = "";
	public static   String BASE_PORT = "";
	public static   String PLANT_CODE="RENUSAGAR";
	public static   String AUG_ID="R1";
	public static   String API_UPDATE_AUG_STATUS="augurctl/updateAugurStatus/";
	public static   String API_UPDATE_AUG_SAMP_STATUS="augurctl/updateSamplingStatus/";
	public static   String API_GET_SAMP_RATE="augurctl/getSampleReference/";
	public static   String API_UPDATE_BOOM_STATUS="augurctl/boomStatus/";	
	//public static   String API_LIMS_URL="http://limshilstg.vvsindia.com/api/api/RunningAugerData/PostRunningAugerData";
	public static   String API_LIMS_URL="http://10.36.121.102:9005/api/RunningAugerData/PostRunningAugerData";
	public static   String LIMS_PLANT_CODE="HK";
	public static   String PLC_BASE_FILE_PATH = "d:\\projects\\RTMS";
	public static   String BARCODE_IMAGE_FILE_PATH = "imagefolder";
	public static   String PLC_FILE_NAME = "rtmsplc.xlsx";
	//public static final String PLC_BASE_FILE_PATH = "c:/apitest/";
	//public static final String PLC_FILE_NAME = "AugerPLC_level1_Interfce.xlsx";
	public static   int BIN_MAX_NUM_TIMES_FILLED=2;
	public static   int SPECIAL_BIN_MAX_NUM_TIMES_FILLED=3;
	public static   int NUM_OF_BINS=3;	
	public static   int AUGER_CALL_BYPASS=0;
	public static   int STEP1_CALL_BYPASS=1;
	public static   int SAMP_STATUS_UPDATE_BYPASS=1;
	public static   String BOOM_GATE_OPEN="open";
	public static   String BOOM_GATE_CLOSE="close";
	public static   String BOOM_GATE_ENTRY="IN";
	public static   String BOOM_GATE_EXIT="OUT";
	public static   String PLANT_NUMERIC_CODE="1"; //this is required to generate collection ref number-1 for Renusgar
	public static   String AUGER_NUMERIC_CODE="1"; //this is required to generate collection ref number-1 for Renusgar
	public static final String COL_AUG_STATUS="B5";
	public static final String COL_AUG_MODE="B6";
	public static final String COL_BOOM_ENTRY_STATUS="B9";
	public static final String COL_BOOM_EXIT_STATUS="B10";
	public static final String COL_SAMP_BIN_NUM="B20";
	public static final String COL_START_SAMP_COMM="B21";
	public static final String COL_AUG_SAMP_STATUS="B7";
	public static final String COL_VEH_ARRVD="B15";
	public static final String COL_VEH_REG_ENTERED="B17";
	public static final String COL_MASTER_RESET="B18";
	public static final String COL_VEH_REG_NUM="B16";
	public static final String COL_COMM_ERROR="B25";
	public static final String COL_RFID_ERROR="B26"; 
	public static final String COL_DATA_ERROR="B27";	
	public static final String COL_BIN_EMPTIED_PRINTING_DONE="B11";
	public static final String COL_EMPTY_BIN_NUM="B22";
	public static final String COL_EMPTY_BIN_NUM_COMM="B23";
	public static final String COL_BIN1_NUM_TIMES_FILL="B28";
	public static final String COL_BIN2_NUM_TIMES_FILL="B29";
	public static final String COL_BIN3_NUM_TIMES_FILL="B30";
	public static final String COL_BIN4_NUM_TIMES_FILL="B31";
	public static final String COL_BIN5_NUM_TIMES_FILL="B32";
	public static final String COL_BIN6_NUM_TIMES_FILL="B33";	
	public static final String COL_BIN7_NUM_TIMES_FILL="B34";
	public static final String COL_BIN8_NUM_TIMES_FILL="B35";
	public static final String COL_DO_PRINTING="B13";
	public static final String COL_STATS_MOD_CHANGE="B12";
	public static final String COL_HEART_BEAT="B41";
	public static final String COL_HTTP_STATUS_CODE="B42";
	public static final String COL_TRIGG_OUT_BOOM="B43";
	public static final String COL_REASON_SAMP_STATUS="B44";
	public static final int AUG_SAMP_COMPLETED=0;
	public static final int AUG_SAMP_ABORTED=2;
	public static final int AUG_SAMP_STARTED=1;	
	public static final int BIN_EMPTIED=1;
	public static final int CSCTS_PART_REF_NUM_START_INDEX=6;
	public static final int CSCTS_PART_REF_NUM_END_INDEX=12;
	public static final int CSCTS_SAMPLING_TYPE_START_INDEX=2;
	public static final int CSCTS_SAMPLING_TYPE_END_INDEX=4;
	public static final int COMM_ERROR_HEALTHY=0;
	public static final int COMM_ERROR_LEVEL1=1;
	public static final String AUG_SAMP_NUM_1="1";
	public static final String AUG_SAMP_NUM_2="2"; 
	public static final int AUG_SAMP_PARTSIZE_1=1;
	public static final int AUG_SAMP_PARTSIZE_2=2;
	public static final int NUMBER_ZERO=0;
	public static final int NUMBER_ONE=1;
	public static final int COMM_DELAY_TIME=1000;
	public static final String EMPTY_VALUE="";
	public static final int LIMS_RETRY_ATTEMPT_LIMIT=3;
	
	
}
