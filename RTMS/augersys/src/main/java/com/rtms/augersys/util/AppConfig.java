package com.rtms.augersys.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Value;
 

@Component
public class AppConfig {
		 
    @Value("${plant.base_url}")
    private String baseUrl;    
    @Value("${plant.base_port}")
    private String basePort;	 
    @Value("${plant.lims_url}")
    private String limsURL;    
    @Value("${plant.plant_code}")
    private String plantCode;    
    @Value("${plant.auger_code}")
    private String augerCode;
    @Value("${plant.num_bin}")
    private int numBin;
    @Value("${plant.max_fill_bin}")
    private int maxFillBin;    
    @Value("${plant.aug_call_bypass}")
    private int augCallBypass;        
    @Value("${api.update_aug_url}")
    private String apiUpdateAugUrl;    
    @Value("${api.get_samp_ref_url}")
    private String apiGetSampRefUrl;
    @Value("${api.update_boom_status_url}")
    private String apiUpdateBoomStatusUrl;    
    @Value("${plc.filepath}")
    private String plcFilePath;
    @Value("${plant.plant_numeric_code}")  
    private String plantNumCode;
    @Value("${plant.auger_numeric_code}") 
    private String augerNumCode;      
    @Value("${plc.filename}")
    private String plcFileName;
    @Value("${plant.max_fill_special_bin}")
    private int maxFillSpecialBin;    
    @Value("${plant.step1_call_bypass}")
    private int step1Bypass;
    
    @Value("${plant.lims_plant_code}")
    private String limsPlantCode;
    
    public String getLimsPlantCode() {
		return limsPlantCode;
	}
	public void setLimsPlantCode(String limsPlantCode) {
		this.limsPlantCode = limsPlantCode;
	}
	@Value("${plant.samp_status_update_bypass}")
    private int sampStatusBypass;
    
	public int getSampStatusBypass() {
		return sampStatusBypass;
	}
	public void setSampStatusBypass(int sampStatusBypass) {
		this.sampStatusBypass = sampStatusBypass;
	}
	public int getMaxFillSpecialBin() {
		return maxFillSpecialBin;
	}
	public int getStep1Bypass() {
		return step1Bypass;
	}
	public void setStep1Bypass(int step1Bypass) {
		this.step1Bypass = step1Bypass;
	}
	public void setMaxFillSpecialBin(int maxFillSpecialBin) {
		this.maxFillSpecialBin = maxFillSpecialBin;
	}
	public String getPlantNumCode() {
		return plantNumCode;
	}
	public void setPlantNumCode(String plantNumCode) {
		this.plantNumCode = plantNumCode;
	}
	public String getAugerNumCode() {
		return augerNumCode;
	}
	public void setAugerNumCode(String augerNumCode) {
		this.augerNumCode = augerNumCode;
	}
	public String getBaseUrl() {
		return baseUrl;
	}
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	public String getBasePort() {
		return basePort;
	}
	public void setBasePort(String basePort) {
		this.basePort = basePort;
	}
	public String getLimsURL() {
		return limsURL;
	}
	public void setLimsURL(String limsURL) {
		this.limsURL = limsURL;
	}
	public String getPlantCode() {
		return plantCode;
	}
	public void setPlantCode(String plantCode) {
		this.plantCode = plantCode;
	}
	public String getAugerCode() {
		return augerCode;
	}
	public void setAugerCode(String augerCode) {
		this.augerCode = augerCode;
	}
	public int getNumBin() {
		return numBin;
	}
	public void setNumBin(int numBin) {
		this.numBin = numBin;
	}
	public int getMaxFillBin() {
		return maxFillBin;
	}
	public void setMaxFillBin(int maxFillBin) {
		this.maxFillBin = maxFillBin;
	}
	public int getAugCallBypass() {
		return augCallBypass;
	}
	public void setAugCallBypass(int augCallBypass) {
		this.augCallBypass = augCallBypass;
	}
	public String getApiUpdateAugUrl() {
		return apiUpdateAugUrl;
	}
	public void setApiUpdateAugUrl(String apiUpdateAugUrl) {
		this.apiUpdateAugUrl = apiUpdateAugUrl;
	}
	public String getApiGetSampRefUrl() {
		return apiGetSampRefUrl;
	}
	public void setApiGetSampRefUrl(String apiGetSampRefUrl) {
		this.apiGetSampRefUrl = apiGetSampRefUrl;
	}
	public String getApiUpdateBoomStatusUrl() {
		return apiUpdateBoomStatusUrl;
	}
	public void setApiUpdateBoomStatusUrl(String apiUpdateBoomStatusUrl) {
		this.apiUpdateBoomStatusUrl = apiUpdateBoomStatusUrl;
	}
	public String getPlcFilePath() {
		return plcFilePath;
	}
	public void setPlcFilePath(String plcFilePath) {
		this.plcFilePath = plcFilePath;
	}
	public String getPlcFileName() {
		return plcFileName;
	}
	public void setPlcFileName(String plcFileName) {
		this.plcFileName = plcFileName;
	}
    
    
    
    
	
	
}
