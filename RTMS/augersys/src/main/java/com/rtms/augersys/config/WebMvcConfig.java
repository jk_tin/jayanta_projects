package com.rtms.augersys.config;

import java.util.Locale;

import javax.sql.DataSource;

import org.apache.commons.configuration2.DatabaseConfiguration;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

@Configuration

@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.rtms.augersys.repo")
public class WebMvcConfig extends WebMvcConfigurationSupport {

 
	private static DataSource datasource;
	@Value("${driverclassname}")
	private String driverClassName;
	@Value("${dburl}")
	private String dburl;
	@Value("${dbusername}")
	private String dbusername;
	@Value("${dbpassword}")
	private String dbpassword;

	 
	@Bean
	public DataSource dataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(driverClassName);
		dataSource.setUrl(dburl);
		 
		dataSource.setUsername(dbusername);
		dataSource.setPassword(dbpassword);
		// dataSource.setDefaultAutoCommit(false);
		dataSource.setDefaultAutoCommit(true);
		datasource = dataSource;
		return dataSource;
	}

	@Bean
	public DatabaseConfiguration databaseConfiguration() {
		DatabaseConfiguration databaseConfiguration = new DatabaseConfiguration();
		databaseConfiguration.setDataSource(dataSource());
		databaseConfiguration.setTable("application_configuration");
		databaseConfiguration.setKeyColumn("property_key");
		databaseConfiguration.setValueColumn("property_value");
		return databaseConfiguration;
	}

}