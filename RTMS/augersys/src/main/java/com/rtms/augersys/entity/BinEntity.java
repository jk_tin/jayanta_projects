package com.rtms.augersys.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="bin")
public class BinEntity {
	
	@Id
	int id;
	
	@Column (name="is_full")
	Boolean isfull;

	@Column (name="cscts_ref_num")
	String csctsRefNum;
	

	@Column(name="num_times_filled")
	int numTimesFilled;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Boolean getIsfull() {
		return isfull;
	}

	public void setIsfull(Boolean isfull) {
		this.isfull = isfull;
	}

	public String getCsctsRefNum() {
		return csctsRefNum;
	}

	public void setCsctsRefNum(String csctsRefNum) {
		this.csctsRefNum = csctsRefNum;
	}

	public int getNumTimesFilled() {
		return numTimesFilled;
	}

	public void setNumTimesFilled(int numTimesFilled) {
		this.numTimesFilled = numTimesFilled;
	}

	@Override
	public String toString() {
		return "BinEntity [id=" + id + ", isfull=" + isfull + ", csctsRefNum=" + csctsRefNum + ", numTimesFilled="
				+ numTimesFilled + "]";
	}

	 

}
