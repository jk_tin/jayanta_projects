package com.rtms.augersys.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rtms.augersys.entity.LimsDataEntityJson;

public interface LimsJsonRepo extends JpaRepository<LimsDataEntityJson, Integer> {

	List<LimsDataEntityJson> findByBinNumAndIsProcessed(int binNum,boolean isProcessed);
	List<LimsDataEntityJson> findByIsProcessed(boolean isProcessed);
}
