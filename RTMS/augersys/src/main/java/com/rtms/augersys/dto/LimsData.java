package com.rtms.augersys.dto;

import java.util.Date;

public class LimsData {
	
	String plantCode;
	String csCTSRefNumber;
	String collRefNumber;
	String sampleRefNumber;
	int partSize;
	String dateOfSampling;
	public String getPlantCode() {
		return plantCode;
	}
	public void setPlantCode(String plantCode) {
		this.plantCode = plantCode;
	}
	public String getCsCTSRefNumber() {
		return csCTSRefNumber;
	}
	public void setCsCTSRefNumber(String csCTSRefNumber) {
		this.csCTSRefNumber = csCTSRefNumber;
	}
	public String getCollRefNumber() {
		return collRefNumber;
	}
	public void setCollRefNumber(String collRefNumber) {
		this.collRefNumber = collRefNumber;
	}
	public String getSampleRefNumber() {
		return sampleRefNumber;
	}
	public void setSampleRefNumber(String sampleRefNumber) {
		this.sampleRefNumber = sampleRefNumber;
	}
	public int getPartSize() {
		return partSize;
	}
	public void setPartSize(int partSize) {
		this.partSize = partSize;
	}
	public String getDateOfSampling() {
		return dateOfSampling;
	}
	public void setDateOfSampling(String dateOfSampling) {
		this.dateOfSampling = dateOfSampling;
	}
	
	

}
