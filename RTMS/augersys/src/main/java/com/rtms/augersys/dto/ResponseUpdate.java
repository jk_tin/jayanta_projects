package com.rtms.augersys.dto;
import com.fasterxml.jackson.annotation.JsonProperty; 


public class ResponseUpdate {
	
	@JsonProperty
	String statusCode ;
	
	@JsonProperty
	String errorMessage ;

	 
	 

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	

}
