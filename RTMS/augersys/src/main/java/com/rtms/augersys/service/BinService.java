package com.rtms.augersys.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.rtms.augersys.dto.Bin;
import com.rtms.augersys.dto.LimsData;
import com.rtms.augersys.entity.CSctsListEntity;
import com.rtms.augersys.entity.LimsDataEntity;
import com.rtms.augersys.entity.LimsDataEntityJson;


public interface BinService {

	public List<Bin> findAllBin();
	public boolean updateBinCSCTsNum(int binNum,String csctsRefNum, String csctsRefNumFull);
	public boolean updateFillStatus(int binNum,boolean fullStatus);
	public boolean updateNumTimesFilled(int binNum, int fillCount);
	public boolean cleanBin(int binNum);
	public boolean saveLimsDataJson(int binNum,String limsJson);
	public List<LimsDataEntityJson> findByBinNumAndIsProcessed(int binNum,boolean isProcessed);
	public boolean updateIsProcessedFlag(int limsDataId,boolean flag);
	public boolean saveLimsData(int binNum,LimsData limsData);
	public ArrayList<LimsData> findLimsDataByBinNum(int binNum);
	public boolean deleteLimsData(int binNum);
	public boolean deleteLimsJsonData(int recordNum);
	public List<LimsDataEntityJson> findAllByIsProcessed(boolean isProcessed);
	public boolean deleteLastCSTCTSListEntry(int binNum);
	 
	
}
