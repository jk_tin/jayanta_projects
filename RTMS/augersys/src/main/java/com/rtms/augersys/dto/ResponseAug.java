package com.rtms.augersys.dto;
import com.fasterxml.jackson.annotation.JsonProperty; 


public class ResponseAug {
	
	@JsonProperty
	String statusCode ;
	
	@JsonProperty
	String errorMessage ;

	@JsonProperty
	String sampleReferenceID;
	
	public String getSampleReferenceID() {
		return sampleReferenceID;
	}

	public void setSampleReferenceID(String sampleReferenceID) {
		this.sampleReferenceID = sampleReferenceID;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	

}
