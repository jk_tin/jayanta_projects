package com.rtms.augersys.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="ctscts_list")
public class CSctsListEntity {
 
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	int id;
	
	@Column(name="bin_num")
	int binNum;
	
	@Column(name="cscts_ref_num")
	String csctsRefNum;

	
	
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public int getBinNum() {
		return binNum;
	}



	public void setBinNum(int binNum) {
		this.binNum = binNum;
	}



	public String getCsctsRefNum() {
		return csctsRefNum;
	}



	public void setCsctsRefNum(String csctsRefNum) {
		this.csctsRefNum = csctsRefNum;
	}



	@Override
	public String toString() {
		return "CSctsListEntity [id=" + id + ", binNum=" + binNum + ", csctsRefNum=" + csctsRefNum + "]";
	}
	
	
	
}
