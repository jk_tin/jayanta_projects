package com.rtms.augersys.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rtms.augersys.dto.Bin;
import com.rtms.augersys.dto.LimsData;
import com.rtms.augersys.entity.BinEntity;
import com.rtms.augersys.entity.CSctsListEntity;
import com.rtms.augersys.entity.LimsDataEntity;
import com.rtms.augersys.entity.LimsDataEntityJson;
import com.rtms.augersys.main.MainApp;
import com.rtms.augersys.repo.BinRepo;
import com.rtms.augersys.repo.CsctsRepo;
import com.rtms.augersys.repo.LimsDataRepo;
import com.rtms.augersys.repo.LimsJsonRepo;
import com.rtms.augersys.service.BinService;


@Service
public class BinServiceImpl implements BinService {

	@Autowired
	BinRepo binRepo;
	@Autowired
	CsctsRepo csctsRepo;
	@Autowired
	LimsJsonRepo limsJsonRepo;
	@Autowired
	LimsDataRepo limsDataRepo;
	private static final Logger log = LogManager.getLogger(BinServiceImpl.class);
	
	public BinServiceImpl() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public List<Bin> findAllBin() {
		System.out.println("--findAllBin--");
		// TODO Auto-generated method stub
		List<BinEntity> listBinEntity= binRepo.findAll();
		List<CSctsListEntity>  listCsctsListEntity= csctsRepo.findAll();
		List<Bin> binList= new ArrayList<>();
		
		for(BinEntity binEnt:listBinEntity)
		{
			ArrayList<String> csctsRefNumFullList=new ArrayList<String>();
			Bin bin = new Bin();
			bin.setBinNum(binEnt.getId()-1);
			bin.setBinFull(binEnt.getIsfull());
			bin.setNumOfTimesFilled(binEnt.getNumTimesFilled());
			bin.setCsctsRefNum(binEnt.getCsctsRefNum());
			List<CSctsListEntity> csctsList=csctsRepo.findByBinNum(binEnt.getId());
			if(null!=csctsList)
			{
				for (CSctsListEntity csctsRef:csctsList)
				{
					if(csctsRef.getBinNum()==binEnt.getId())
					{
						csctsRefNumFullList.add(csctsRef.getCsctsRefNum());
					}
				}
			}
			if(null!=csctsRefNumFullList)
			{
				bin.setCsCTSrefNumFullList(csctsRefNumFullList);
			}
				
			binList.add(bin);
			
		}
		
		
		return binList;
	}
	@Override
	public boolean updateBinCSCTsNum(int binNum,String csctsRefNum, String csctsRefNumFull)
	{
		log.info("--updateBinCSCTsNum--binNum="+binNum);
		boolean flag= true;
		try {
				BinEntity  binEnt= binRepo.findById(binNum).get();
				if(null!=binEnt)
				{
					System.out.println ("--Not Null--");
					 
					binEnt.setCsctsRefNum(csctsRefNum);					
					CSctsListEntity csCtsListEntity = new CSctsListEntity();
					csCtsListEntity.setBinNum(binNum);
					csCtsListEntity.setCsctsRefNum(csctsRefNumFull);
					binRepo.save(binEnt);
					csctsRepo.save(csCtsListEntity);
				
				}else
				{
					System.out.println ("-- Null--");
				}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			flag=false;
			
		}
	
		return flag;
	}
	@Override
	public boolean updateFillStatus(int binNum, boolean fullStatus) {
		boolean flag= true;
		System.out.println("--updateFillStatus--"+fullStatus);
		try {
			BinEntity  binEnt= binRepo.findById(binNum).get();
			if(null!=binEnt)
			{
				System.out.println("--updateFillStatus--Not Null");
				 
				binEnt.setIsfull(fullStatus);
				binRepo.save(binEnt);
			}
			
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			flag=false;
			
		}
	
		return flag;
	}
	
	@Override
	public boolean updateNumTimesFilled(int binNum, int fillCount) {
		boolean flag= true;
		System.out.println("--updateNumTimesFilled--");
		
		try {
			BinEntity binEnt= binRepo.findById(binNum).get();
			if(null!=binEnt)
			{
				System.out.println("--updateNumTimesFilled--Not Null");
				binEnt.setNumTimesFilled(fillCount);
				binRepo.save(binEnt);
			}
			
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			flag=false;
			
		}
	
		return flag;
	}
	@Override
	public boolean cleanBin(int binNum) {
		boolean flag= true;
		try {
			
			BinEntity  binEnt= binRepo.findById(binNum).get();
			if(null!=binEnt)
			{
				 
				binEnt.setIsfull(false);
				binEnt.setNumTimesFilled(0);
				binEnt.setCsctsRefNum("0");
				binRepo.save(binEnt);
			}
			 List<CSctsListEntity> listCstCtsNumber= csctsRepo.findByBinNum(binNum);
			 if((null!=listCstCtsNumber)&&(listCstCtsNumber.size()!=0))
			 {
				 for (CSctsListEntity listEntity:listCstCtsNumber)
				 {
					 csctsRepo.delete(listEntity);
					 
				 }
				 
			 }
			
			
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			flag=false;
			
		}
		return flag;
	}
	@Override
	public boolean saveLimsDataJson(int binNum, String limsJson) {
		// TODO Auto-generated method stub
		boolean flag= true;
		try {
			log.info("--saveLimsDataJson--");
			LimsDataEntityJson limsJDataEntityJson= new LimsDataEntityJson();
			limsJDataEntityJson.setBinNum(binNum);
			limsJDataEntityJson.setLimsDataJson(limsJson);
			limsJDataEntityJson.setProcessed(false);
			limsJsonRepo.save(limsJDataEntityJson);
						
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			flag=false;
			
		}
		
		
		return flag;
	}
	@Override
	public List<LimsDataEntityJson> findByBinNumAndIsProcessed(int binNum, boolean isProcessed) {
		// TODO Auto-generated method stub
		
		try {
			
			
			return limsJsonRepo.findByBinNumAndIsProcessed(binNum,isProcessed);
			
		}catch(Exception ex)
		{
			
			ex.printStackTrace();
		}
		
		return null;
	}
	@Override
	public boolean updateIsProcessedFlag(int limsDataId, boolean flagVal) {
		// TODO Auto-generated method stub
		boolean flag= true;
		try {
			
			Optional<LimsDataEntityJson> limsDataEntity = Optional.empty();
			limsDataEntity = limsJsonRepo.findById(limsDataId);
			if(limsDataEntity.isPresent())
			{
				limsDataEntity.get().setProcessed(flagVal);
			}
			else
			{
				flag=false;
			}
 
			
		}catch(Exception ex)
		{
			flag=false;
			ex.printStackTrace();
		}
		return flag;
	}
	@Override
	public boolean saveLimsData(int binNum, LimsData limsData) {
		 
		boolean flag= true;
		try {
			
			LimsDataEntity limsDataEntity = new LimsDataEntity();
			limsDataEntity.setBinNum(binNum);
			limsDataEntity.setCollRefNumber(limsData.getCollRefNumber());
			limsDataEntity.setCsCTSRefNumber(limsData.getCsCTSRefNumber());
			limsDataEntity.setDateOfSampling(limsData.getDateOfSampling());
			limsDataRepo.save(limsDataEntity);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			flag=false;
		}
		
		
		return flag;
	}
	@Override
	public ArrayList<LimsData> findLimsDataByBinNum(int binNum) {
		ArrayList<LimsData> listLimsData= new ArrayList<>();
		
		List <LimsDataEntity> listLimsDataEntity= limsDataRepo.findByBinNum(binNum);
		if((null!=listLimsDataEntity)&&(listLimsDataEntity.size()!=0))
		{
			for(LimsDataEntity limsDataEntity:listLimsDataEntity)
			{
				LimsData limsData = new LimsData();
				limsData.setCollRefNumber(limsDataEntity.getCollRefNumber());
				limsData.setCsCTSRefNumber(limsDataEntity.getCsCTSRefNumber());
				limsData.setDateOfSampling(limsDataEntity.getDateOfSampling());
				limsData.setPartSize(limsDataEntity.getPartSize());
				limsData.setPlantCode(limsDataEntity.getPlantCode());
				limsData.setSampleRefNumber(limsDataEntity.getSampleRefNumber());
				listLimsData.add(limsData);
				
			}
			
			
		}
		
				
		return listLimsData;
	}
	@Override
	public boolean deleteLimsData(int binNum) {
		boolean flag= true;
		try {
			List<LimsDataEntity> listLimsDataEntity=limsDataRepo.findByBinNum(binNum);  
			for(LimsDataEntity limsDataEntity :listLimsDataEntity )
			{
			
				limsDataRepo.delete(limsDataEntity);
			
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			flag=false;
		}
		
		return flag;
	}
	@Override
	public boolean deleteLimsJsonData(int recordNum) {
		boolean flag= true;
		try {
			 
			
				limsJsonRepo.deleteById(recordNum);
			
		}
		 
		catch (Exception ex)
		{
			ex.printStackTrace();
			flag=false;
		}
		
		return flag;
	}
	@Override
	public List<LimsDataEntityJson> findAllByIsProcessed(boolean isProcessed) {
		try {
			
			
			return limsJsonRepo.findByIsProcessed(isProcessed);
			
		}catch(Exception ex)
		{
			
			ex.printStackTrace();
		}
		
		return null;
	}
	@Override
	public boolean deleteLastCSTCTSListEntry(int binNum) {
		List<CSctsListEntity> csCtsListEntity = csctsRepo.findByBinNumOrderByIdDesc(binNum) ;
	   
	   try {
		   if(null!=csCtsListEntity)
		   {
			   csctsRepo.delete(csCtsListEntity.get(0));
			   return true;
			   
		   }
	   }catch (Exception ex)
	   {
		   ex.printStackTrace();
	   }
	   return false;
		
	}
	 
	
	
}
