package com.rtms.augersys.service;

import java.awt.print.PrinterException;
import java.io.IOException;
import java.util.ArrayList;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.rtms.augersys.dto.Bin;
import com.rtms.augersys.dto.ResponseDTO;

public interface RTMSService {
 
	 
	
    ResponseEntity<ResponseDTO> updateAugStatus(String augurID,String status,String mode,String reasonCode,String plantCode);

    public ResponseEntity mainLoop(ArrayList<Bin> binMap); 
    
    String getSampRateWithoutRefId(String plantCode,String augId);

    public String getSampRateWithVehiclRegNum(String plantCode,String augId,String vehicleRegNum);

	ResponseEntity mainLoopStop();
	
	
	ResponseEntity writeValCell(String cell,String value);

	ResponseEntity printBin(int binNum);

	ResponseEntity updateBoomStatus(String augurID, String status, String boomId, String plantCode);

	 

	ResponseEntity sendLimsData();

	String getCollRefNum();

	String getSampRefNum1();

	String getSampRefNum2();

	String getDate();
	
	//ResponseEntity printTestBarcode();

	ResponseEntity printTestBarcode(String barcodeFileName,String barcode);

	void print(String imagefile) throws IOException, PrinterException;
	void checkStatusChangeTest();
	public ResponseEntity updateAugStatus();
 
}
