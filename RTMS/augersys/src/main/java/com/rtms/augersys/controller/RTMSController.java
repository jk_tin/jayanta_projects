package com.rtms.augersys.controller;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.UnknownHttpStatusCodeException;
import org.springframework.web.servlet.ModelAndView;

import com.rtms.augersys.util.AppConstant;
import com.rtms.augersys.util.PropertiesLoader;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.rtms.augersys.dto.Bin;
import com.rtms.augersys.dto.LimsData;
import com.rtms.augersys.dto.ResponseLims;
import com.rtms.augersys.main.MainApp;
import com.rtms.augersys.service.BinService;
import com.rtms.augersys.service.RTMSService;
import com.rtms.augersys.service.impl.RTMSServiceImpl;
import com.rtms.augersys.util.AppConfig;

 
@RestController
@RequestMapping("/augersys")
public class RTMSController {
	public final Logger log = LoggerFactory.getLogger(this.getClass());
	public static volatile boolean startFlagNew =false;   
	Thread mapp=null;
	@Autowired
	RTMSService rtmsService;
	@Autowired
	BinService binService;
	private  RestTemplate restTemplate; 
	 
	 @Autowired
	 public AppConfig appConfig;
	
	 public void initializeConfig()
	 {
		 AppConstant.BASE_URL=appConfig.getBaseUrl();
		 AppConstant.BASE_PORT=appConfig.getBasePort();
		 AppConstant.API_LIMS_URL=appConfig.getLimsURL();
		 AppConstant.AUG_ID=appConfig.getAugerCode();
		 AppConstant.BIN_MAX_NUM_TIMES_FILLED=appConfig.getMaxFillBin();
		 AppConstant.AUGER_CALL_BYPASS=appConfig.getAugCallBypass();
		 AppConstant.API_GET_SAMP_RATE=appConfig.getApiGetSampRefUrl();
		 AppConstant.API_UPDATE_AUG_STATUS=appConfig.getApiUpdateAugUrl();
		 AppConstant.API_UPDATE_BOOM_STATUS=appConfig.getApiUpdateBoomStatusUrl();
		 AppConstant.PLC_BASE_FILE_PATH=appConfig.getPlcFilePath();
		 AppConstant.PLC_FILE_NAME=appConfig.getPlcFileName();
		 AppConstant.PLANT_CODE=appConfig.getPlantCode();
		 AppConstant.AUG_ID=appConfig.getAugerCode(); 
		 AppConstant.AUGER_NUMERIC_CODE=appConfig.getAugerNumCode();
		 AppConstant.PLANT_NUMERIC_CODE=appConfig.getPlantNumCode();
		 AppConstant.SPECIAL_BIN_MAX_NUM_TIMES_FILLED=appConfig.getMaxFillSpecialBin();
		 AppConstant.STEP1_CALL_BYPASS=appConfig.getStep1Bypass();
		 AppConstant.NUM_OF_BINS=appConfig.getNumBin();
		 AppConstant.SAMP_STATUS_UPDATE_BYPASS=appConfig.getSampStatusBypass();
		 AppConstant.LIMS_PLANT_CODE=appConfig.getLimsPlantCode();
		 log.info("----Loading initialization parameter----");
		 
		 
		 log.info("BASE_URL="+AppConstant.BASE_URL);
		 log.info("BASE_PORT="+AppConstant.BASE_PORT);
		 log.info("API_LIMS_URL="+AppConstant.API_LIMS_URL);
		 log.info("LIMS_PLANT_CODE="+AppConstant.LIMS_PLANT_CODE);
		 log.info("AUG_ID="+AppConstant.AUG_ID);
		 log.info("BIN_MAX_NUM_TIMES_FILLED="+AppConstant.BIN_MAX_NUM_TIMES_FILLED);
		 log.info("AUGER_CALL_BYPASS="+AppConstant.AUGER_CALL_BYPASS);
		 log.info("API_GET_SAMP_RATE="+AppConstant.API_GET_SAMP_RATE);
		 log.info("API_UPDATE_AUG_STATUS="+AppConstant.API_UPDATE_AUG_STATUS);
		 log.info("API_UPDATE_BOOM_STATUS="+AppConstant.API_UPDATE_BOOM_STATUS);
		 log.info("PLC_BASE_FILE_PATH="+AppConstant.PLC_BASE_FILE_PATH);
		 log.info("PLC_FILE_NAME="+AppConstant.PLC_FILE_NAME);
		 log.info("PLANT_CODE="+AppConstant.PLANT_CODE);
		 log.info("AUG_ID="+AppConstant.AUG_ID);
		 log.info("AUGER_NUMERIC_CODE="+AppConstant.AUGER_NUMERIC_CODE);
		 log.info("SPECIAL_BIN_MAX_NUM_TIMES_FILLED="+AppConstant.SPECIAL_BIN_MAX_NUM_TIMES_FILLED);	
		 log.info("SAMP_STATUS_UPDATE_BYPASS="+AppConstant.SAMP_STATUS_UPDATE_BYPASS);	
		 log.info("---initialization parameter ends----");
	 }
	 
	 @GetMapping(value = "/newstart")
	    public ResponseEntity<?>  newstart() {
		 log.info("****Here---");
		 initializeConfig();
		 
	     System.out.println("New Start");
	     if(!startFlagNew)
	     {
	    	 startFlagNew=true;
	    	 mapp  = new Thread(new MainApp(binService));	     
	    	 log.info("****Started---&&&&&&");
	    	 mapp.start();
	     }
		 return null;
	 }
	 
	 @GetMapping(value = "/newstop")
	    public ResponseEntity<?>  newstop() {
	    try {
		// mapp.join(1000);
	    	startFlagNew=false;
		  
	    }
	    catch(Exception ex)
	    {
	    	ex.printStackTrace();
	    	
	    }
	    
		 
		 return null;
	 }
	 
	 
	 
	@GetMapping(value = "/start")
    public ResponseEntity<?>  mainLoop() {
		
		 
		try{
			System.out.println("mainLoop called - " + new Date());
			
			//create Bin Map
			ArrayList<Bin> binMap=new ArrayList<Bin>();
			
			for(int i=0;i<AppConstant.NUM_OF_BINS;i++)
			{
				
				Bin bin=new Bin();
				bin.setBinNum(i);
				binMap.add(bin);			
				
				
			}
			rtmsService.mainLoop(binMap);
			 
			return null;
			
			//return rtmsService.checRfidResponse();
		}catch(Exception e){
			log.error("Error occured", e);
		}
		return null;
	} 
    
  
	@GetMapping(value = "/stop")	  
	   public ResponseEntity<?>  mainLoopStop() {
		return rtmsService.mainLoopStop();
	   }
	
	
	@GetMapping("/cellval/{cell}/{value}") 
	public ResponseEntity<?> writeValToCell(@PathVariable String cell, @PathVariable String value) {
	   
		
		return rtmsService.writeValCell(cell, value);
		//return "ID: " + id + ", name: " + name;
	}
	
	 

	@GetMapping(value = "/printprop")
    public ResponseEntity<?>  printprop() {
		 System.out.println("BASE & URL="+AppConstant.BASE_URL);
		 return null;
	 }	
 
	 

@GetMapping("/printbin/{binnum}") 
public ResponseEntity<?> printbin(@PathVariable int binnum) {
   
	return rtmsService.printBin(binnum);
	//return rtmsService.writeValCell(cell, value);
	//return "ID: " + id + ", name: " + name;
}


@GetMapping(value = "/sendlims")
public ResponseEntity<?>  sendlims() {
	 //System.out.println("Base URL="+AppConstant.BASE_URL);
	return rtmsService.sendLimsData();
	
	  
 }	

@GetMapping(value = "/getdate")
public String  getdate() {
	 //System.out.println("Base URL="+AppConstant.BASE_URL);
	return rtmsService.getDate();
	
	  
 }	

@GetMapping(value = "/printtestbarcode/{barcodefilename}/{barcode}")
public ResponseEntity  printTestBarcode(@PathVariable("barcode") String barcode,@PathVariable("barcodefilename") String barcodefilename) {
	 //System.out.println("Base URL="+AppConstant.BASE_URL);
	return rtmsService.printTestBarcode(barcodefilename,barcode);
	
	  
 }	
@GetMapping("/jsontest")
public ResponseEntity jsonTest()
{
	 RestTemplateBuilder restTemplateBuilder=new RestTemplateBuilder();
	 
	 this.restTemplate = restTemplateBuilder.build(); 

	 
		// TODO Auto-generated method stub
		log.info("--Inside sendLimsData--");
		//ResponseEntity<ResponseLims> response=null;
		ResponseLims response=null;
		JsonArray jsonArray = new JsonArray();
	 
		 
		//ArrayList<LimsData> limsDataList=bin.getLimsData();
		 
		 
			 
			JsonObject  formObj= new JsonObject();
			//log.info("limsData CSCTSRefNo=="+limsData.getCsCTSRefNumber());
			formObj.addProperty("PlantCode","RS");
			formObj.addProperty("CSCTSRefNo","R100RS628557184042"); 
			formObj.addProperty("CollRefNo","112793490470"); 
			formObj.addProperty("SampleRefNo","1112793490"); 
			formObj.addProperty("PartSize",1); 
			formObj.addProperty("DateOfSampling","12/12/2023"); 
			
			 
			jsonArray.add(formObj);
		
 		     
		
		 
		
		  
		 
		 
		log.info("limsData="+jsonArray);
		
	    try {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		//headers.add("plantCode", plantCode);
		
		 
		HttpEntity<String> entity = new HttpEntity<String>(jsonArray.toString(), headers);
		
		 String url=AppConstant.API_LIMS_URL;
		// response = restTemplate.postForEntity( url, entity , ResponseAug.class );
		 log.info("--Before Call--");
		 response= restTemplate.postForObject(url, entity,ResponseLims.class);
		 //response= restTemplate.postForObject(url, HttpMethod.POST, entity, ResponseLims.class);
		 response.getStatusCode();
		 // response.getBody().getStatusCode();
		 log.info("--After Call--");
		log.info(response.getStatusCode());
		log.info(response.getMessage());	
//		log.info(response.getBody().getStatusCode());
//		log.info(response.getBody().getMessage());	
		
		
		
	    }
	    catch(HttpClientErrorException  ex)
	    {
	    	log.info("--Exception Inside sendLimsData-HttpClientErrorException");
	    	ex.printStackTrace();
	    }
	    catch(UnknownHttpStatusCodeException  ex) {
	      log.info("--Exception Inside sendLimsData-UnknownHttpStatusCodeException");
	     // log.info(ex.getRawStatusCode());
	       
	    }
	    
	 	
	 
	 
	 
	
	return null;
}

@GetMapping("/printtest")
public void Print()
{
	try {
		rtmsService.print("Test");
	}
	catch (Exception ex)
	{
		ex.printStackTrace();
	}

}

@GetMapping("/barcode")
public void printForBarCode(ArrayList<LimsData> limsData) {
	log.info("--- Inside printForBarCode--");
	 	String imageName="Jayanta"+"."+"png";
		
		 LocalDateTime myDateObj = LocalDateTime.now();			  
		 DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("ddMMyyyy");
		 String formattedDate = myDateObj.format(myFormatObj);
		 //String imageFilePath=AppConstant.PLC_BASE_FILE_PATH+"\\"+AppConstant.BARCODE_IMAGE_FILE_PATH+"\\"+formattedDate+"\\"+imageName;
		 String imageFilePath=AppConstant.PLC_BASE_FILE_PATH+"\\"+AppConstant.BARCODE_IMAGE_FILE_PATH+"\\"+formattedDate;
		 //log.info(imageFilePath);				
		 //createImage(imageName,"Jayanta");
		 //String fileName = AppConstant.PLC_BASE_FILE_PATH+"\\"+AppConstant.BARCODE_IMAGE_FILE_PATH+"\\"+formattedDate+"\\"+"newFile.txt";
		
	      
	        // default, create, truncate and write to it.
	        try {
	         Files.createDirectories(Paths.get(imageFilePath));
	         String fileName=imageFilePath+"//"+"newFile.txt";
	         log.info("File Name="+fileName);
		        Path path = Paths.get(fileName);
	         BufferedWriter writer =
	                     Files.newBufferedWriter(path, StandardCharsets.UTF_8);  

	            writer.write("Hello World !!");

	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        catch(Exception ex)
	        {
	        	
	        	ex.printStackTrace();
	        }
		
	}
	
	
 

public  boolean createImage(String imageName,String myString)  {
	boolean retVal=false;
	try {
    
	Code128Bean code128 = new Code128Bean();
	code128.setHeight(7f);
	code128.setModuleWidth(0.3);
	code128.setQuietZone(10);
	code128.doQuietZone(true);
	ByteArrayOutputStream baos = new ByteArrayOutputStream();
	BitmapCanvasProvider canvas = new BitmapCanvasProvider(baos, "image/x-png", 300, BufferedImage.TYPE_BYTE_BINARY, false, 0);
	code128.generateBarcode(canvas, myString);
	canvas.finish();
	//write to png file

	FileOutputStream fos = FileUtils.openOutputStream(new File(imageName)); 
	fos.write(baos.toByteArray());
	fos.flush();
	fos.close();
	retVal=true;
	} catch (Exception e) {
		// TODO: handle exception
		log.info(e.toString());
	}
	finally {
		
		return retVal;
	}
}

@GetMapping("/boomStatus")
public void boomStatus()
{
	try {
		rtmsService.updateBoomStatus("R1","open","out","RENUSAGAR");
	}
	catch (Exception ex)
	{
		ex.printStackTrace();
	}

}

 
@GetMapping("/augerStatus")
public void augerStatus()
{
	try {
		rtmsService.updateAugStatus();
	}
	catch (Exception ex)
	{
		ex.printStackTrace();
	}

}

 

}
 
