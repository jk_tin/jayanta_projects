package com.rtms.augersys.repo;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rtms.augersys.entity.LimsDataEntity;
public interface LimsDataRepo extends JpaRepository <LimsDataEntity,Integer> {

	List<LimsDataEntity> findByBinNum(int binNum);
}
