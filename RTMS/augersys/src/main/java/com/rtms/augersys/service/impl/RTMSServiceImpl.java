package com.rtms.augersys.service.impl;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.net.ssl.SSLContext;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.MediaSize;
import javax.print.attribute.standard.OrientationRequested;
import javax.print.attribute.standard.PrinterResolution;
 

import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.UnknownHttpStatusCodeException;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

import com.google.api.client.util.Base64;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.itextpdf.text.log.SysoLogger;
 
import  com.rtms.augersys.util.AppConstant;
import com.rtms.augersys.dto.Bin;
import com.rtms.augersys.dto.ResponseLims;
import com.rtms.augersys.dto.ResponseUpdate;
import com.rtms.augersys.service.AbstractService;
import com.rtms.augersys.service.RTMSService;
import com.rtms.augersys.dto.ResponseAug;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpHost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.TrustStrategy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;  
@Service
public class RTMSServiceImpl extends AbstractService implements RTMSService
{
	
	private final RestTemplate restTemplate;
	public static boolean startFlag;
	public static boolean allBinsEmpty;
	public static ArrayList<Bin> binMapRef;
	private static final Logger log = LogManager.getLogger(RTMSServiceImpl.class);
	
	public RTMSServiceImpl(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
        this.startFlag=false;
        this.allBinsEmpty=true;
	}
	
	 
	
	
	@Override
	public ResponseEntity updateBoomStatus(String augurID,String status,String boomId,String plantCode) {
		// TODO Auto-generated method stub
		log.info("Inside updateBoomStatus--Service");
		ResponseEntity<ResponseAug> response=null;
	    try {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("plantCode", "RENUSAGAR");
		
		//MultiValueMap<String, Object> map= new LinkedMultiValueMap<String, Object>();
		Map<String, Object> map = new HashMap<>();
		map.put("augurID", "R1");
		map.put("boomid", "out");		
		map.put("status", "open");
	 
		//HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		HttpEntity<?> request = new HttpEntity<>(map.toString(), headers);
		
		String url=AppConstant.BASE_URL+":"+AppConstant.BASE_PORT+"/"+AppConstant.API_UPDATE_BOOM_STATUS;
		log.info("--updateBoomStatusTest URL="+url);
		log.info("--updateBoomStatusTest request="+request);
		//response = restTemplate.postForEntity( url, request , ResponseAug.class );
		//response.getBody().getStatusCode();
		//restTemplate.exchange(url, HttpMethod.POST, request, ResponseUpdate.class);
		response = restTemplate.postForEntity( url, request , ResponseAug.class );
		log.info("updateBoomStatus-"+ response.getBody().getStatusCode());
		log.info("updateBoomStatus-"+ response.getBody().getErrorMessage());	
		
		
		
	    }
	    catch(HttpClientErrorException  ex)
	    {
	    	
	    	ex.printStackTrace();
	    }
	    catch(UnknownHttpStatusCodeException  ex) {
	    
	      log.info(ex.getRawStatusCode());
	       
	    }
	    
		return response;
		
	}
	
	@Override
	public ResponseEntity updateAugStatus(String augurID,String status,String mode,String reasonCode,String plantCode) {
		// TODO Auto-generated method stub
		log.info("Inside updateAugStatus-");
		ResponseEntity<ResponseAug> response=null;
	    try {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("plantCode", plantCode);
		
		//MultiValueMap<String, Object> map= new LinkedMultiValueMap<String, Object>();
		Map<String, Object> map = new HashMap<>();
		map.put("augurID", augurID);
		map.put("status", status);		
		map.put("mode", mode);
		map.put("reason_code", reasonCode);
		//HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		HttpEntity<Map<String, Object>> request = new HttpEntity<>(map, headers);
		
		String url=AppConstant.BASE_URL+":"+AppConstant.BASE_PORT+"/"+AppConstant.API_UPDATE_AUG_STATUS;
		 response = restTemplate.postForEntity( url, request , ResponseAug.class );
		 response.getBody().getStatusCode();
		 
		log.info("updateAugStatus-"+ response.getBody().getStatusCode());
		log.info("updateAugStatus-"+response.getBody().getErrorMessage());	
		
		
		
	    }
	    catch(HttpClientErrorException  ex)
	    {
	    	
	    	ex.printStackTrace();
	    }
	    catch(UnknownHttpStatusCodeException  ex) {
	    
	      System.out.println(ex.getRawStatusCode());
	       
	    }
	    
		return response;
		
	}
	
	
	@Override
	public ResponseEntity mainLoop(ArrayList<Bin> binMap) {
	
		try {
	   
	    String csctsRefNumFull=null;
	    String csctsRefNum=null;	
	    String sampleType=null;
			
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		startFlag=true;
		binMapRef=binMap;
		log.debug("Debug Message Logged !!!");
		          
		  while(startFlag)
		  {
		      int allocatedBin=-1;
			  if(checkVehicleArrived()) 
			  {
				 
				  if(AppConstant.AUGER_CALL_BYPASS==1)
				  {
					  //csctsRefNumFull =getSampRateWithoutRefId(AppConstant.PLANT_CODE,AppConstant.AUG_ID);
					  csctsRefNumFull =getSampRateWithVehiclRegNum(AppConstant.PLANT_CODE,AppConstant.AUG_ID,"");
				  }
				  else
				  {
					  
					  System.out.println("Please eneter CSCTS Ref number in full--:");
					  Scanner in = new Scanner(System.in);
					  
					  csctsRefNumFull=in.nextLine();
					  System.out.println("Entered ref number is="+csctsRefNumFull);
					  
				  }
				  
				  
				  
			  }
			  
			  if((csctsRefNumFull==null)||(csctsRefNumFull.isEmpty()))
			  {
				  
				  writeToCell(AppConstant.COL_COMM_ERROR, AppConstant.COMM_ERROR_LEVEL1);
				  
				  if(checkVehicleRegNumberEntered()) {
					  
					  
					   String vehicleRegNum=getVehicleRegNumber();
					  		  
					   csctsRefNumFull=getSampRateWithVehiclRegNum(AppConstant.PLANT_CODE,AppConstant.AUG_ID,vehicleRegNum);
				       
					   if(csctsRefNumFull==null)				            				    
					   {
						   
						   writeToCell(AppConstant.COL_COMM_ERROR, AppConstant.COMM_ERROR_LEVEL1);
						   
					   }
				  }
				  
			  }
			  
			  if(csctsRefNumFull!=null)
			  {
				  
				        csctsRefNum=csctsRefNumFull.substring(AppConstant.CSCTS_PART_REF_NUM_START_INDEX,AppConstant.CSCTS_PART_REF_NUM_END_INDEX);
				        sampleType = csctsRefNumFull.substring(AppConstant.CSCTS_SAMPLING_TYPE_START_INDEX,AppConstant.CSCTS_SAMPLING_TYPE_END_INDEX);
					    allocatedBin=alloCateBin(csctsRefNum,binMap,csctsRefNumFull,sampleType);
					    
					    System.out.println("Allocated Bin="+(allocatedBin+1));
					    if(allocatedBin==-1)
					    {
					    	System.out.println("Bin Allocation Error");
					    	
					    	
					    }
					    else {
					        
					    	int startSampStatus=-1;
					    	binMap.get(allocatedBin).setCsctsRefNum(csctsRefNum);
					    	//binMap.get(allocatedBin).getCsCTSrefNumList().add(csctsRefNumFull);
					    	
					    	if((allocatedBin==0)&&(allBinsEmpty))
					    	{
					    		
					    		allBinsEmpty=false;
					    	}
					    	
					    	//start sampling 
					    	
					    	writeToCell(AppConstant.COL_SAMP_BIN_NUM,allocatedBin+1);
						    writeToCell(AppConstant.COL_START_SAMP_COMM,1);
					    	
					    	do //wait for sampling to completed or aborted
					    	{
					    		
					    	   
					    	   System.out.println("Waiting for Sampling to begin");	
					    	   Thread.sleep(5000);
					    	   startSampStatus=getNumericDatadFromCell(AppConstant.COL_AUG_SAMP_STATUS);
					    							    		
					    	}while((startSampStatus!=AppConstant.AUG_SAMP_STARTED)&&(startFlag));
					    	
					    	System.out.println("Sampling started");
					    	
					    	do //wait for sampling to completed or aborted
					    	{
					    		
					    	   
					    	   System.out.println("Waiting for Sampling to end");
					    	   Thread.sleep(5000);
					    	   startSampStatus=getNumericDatadFromCell(AppConstant.COL_AUG_SAMP_STATUS);
					    							    		
					    	}while((startSampStatus!=AppConstant.AUG_SAMP_COMPLETED)&&(startSampStatus!=AppConstant.AUG_SAMP_ABORTED)&&(startFlag));
					    	
					    	if(startSampStatus==AppConstant.AUG_SAMP_COMPLETED) //graecfully completed
					    	{
					    		//increase count
					    		binMap.get(allocatedBin).setNumOfTimesFilled(binMap.get(allocatedBin).getNumOfTimesFilled()+1);
					    		
					    		if(binMap.get(allocatedBin).getNumOfTimesFilled()==AppConstant.BIN_MAX_NUM_TIMES_FILLED) 
					    		{
					    			clearBin(binMap,allocatedBin);
					    		}
					    		 
					    		
					    		 printBinMap(binMap);
					    	}
					    	
					    	
					         
					    }
			  }
			  
			 
			   
		  }
			  
		  System.out.println("Stopped");
		  
		  
		 //
		  return new ResponseEntity<>(
			      "All OK", 
			      HttpStatus.OK);
		 
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
		
	}
	
	
	@Override
	public String  getSampRateWithoutRefId(String plantCode,String augId) {
		// TODO Auto-generated method stub
		
		ResponseEntity<ResponseAug> response=null;
		String csctsReferNum=null;
		
		
	    try {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("plantCode", plantCode);
		
		//MultiValueMap<String, Object> map= new LinkedMultiValueMap<String, Object>();
		Map<String, Object> map = new HashMap<>();
		map.put("augurID",augId);
		map.put("rfid","");
		//HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		HttpEntity<Map<String, Object>> request = new HttpEntity<>(map, headers);
		
		String url=AppConstant.BASE_URL+":"+AppConstant.BASE_PORT+"/"+AppConstant.API_GET_SAMP_RATE;
		
		 
		 response = restTemplate.postForEntity( url, request , ResponseAug.class );
		 response.getBody().getStatusCode();
		 
		System.out.println(response.getBody().getStatusCode());
		System.out.println(response.getBody().getErrorMessage());	
		
		
		
		
	    }
	    catch(HttpClientErrorException  ex)
	    {
	    	
	    	ex.printStackTrace();
	    	log.info(ex.toString());
	    }
	    catch(UnknownHttpStatusCodeException  ex) {
	    	
	    
	        log.info(ex.getResponseBodyAsString());
	        log.info(ex.getRawStatusCode());
	       
	    }
	    catch (Exception ex)
	    {
	    	log.info(ex.toString());
	    	
	    }
	    
		return csctsReferNum;
		
	}
	
	@Override
	public  String  getSampRateWithVehiclRegNum(String plantCode,String augId,String vehicleRegNum) {
		// TODO Auto-generated method stub
		System.out.println("plantCode="+plantCode);
		System.out.println("augId="+augId);
		
		ResponseEntity<ResponseAug> response=null;
		String csctsReferNum=null;
	    try {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("plantCode", plantCode);
		
		//MultiValueMap<String, Object> map= new LinkedMultiValueMap<String, Object>();
		Map<String, Object> map = new HashMap<>();
		map.put("augurID",augId);
		map.put("vehRegNo",vehicleRegNum);
	 
		//HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		HttpEntity<Map<String, Object>> request = new HttpEntity<>(map, headers);
		
		String url=AppConstant.BASE_URL+":"+AppConstant.BASE_PORT+"/"+AppConstant.API_GET_SAMP_RATE;
		 response = restTemplate.postForEntity( url, request , ResponseAug.class );
		String responseCode= response.getBody().getStatusCode();
		 
		System.out.println(response.getBody().getStatusCode());
		System.out.println(response.getBody().getErrorMessage());	
		
		if((responseCode!=null)&&(responseCode.isEmpty()&&(responseCode.equalsIgnoreCase("200"))))
		{
			csctsReferNum="1111";
			
	 			
		}
		
		
		
	    }
	    catch(HttpClientErrorException  ex)
	    {
	    	
	    	ex.printStackTrace();
	    }
	    catch(UnknownHttpStatusCodeException  ex) {
	    
	      System.out.println(ex.getRawStatusCode());
	       
	    }
	    csctsReferNum="1111";
	    
		return csctsReferNum;
		
	}
	
	
	boolean checkVehicleArrived()
	{
		boolean flag=true;
		
		while((flag)&&(startFlag))
		{
			 try {
			  Thread.sleep(5000);		  
		      int d=getNumericDatadFromCell(AppConstant.COL_VEH_ARRVD);     
		     
			   
			 
			  if(d==1)
			  {
				   
				  break;
				  
			  }
			  else
			  {
				  System.out.println("Vehicle not arrived yet");
				  
			  }
			  
			  
			  
			  Thread.sleep(1000);
			  
			  
			  
			 }
			 catch(Exception ex)
			 {
				 ex.printStackTrace();
				 
				 
			 }
			
			
			
		}
		
		
		
		
		
		return flag;
	}
	
	
	boolean checkVehicleRegNumberEntered()
	{
		boolean flag=true;
		
		while((flag)&&(startFlag))
		{
			 try {
			
			    
		       
		     int d= getNumericDatadFromCell(AppConstant.COL_VEH_REG_ENTERED);
		     
			  //System.out.println(d);
			  
			  
			  
			  if(d==1)
			  {
				   //reset 
				
				  
				  break;
				  
			  }
			  
			  Thread.sleep(1000);
			  
			  
			  
			 }
			 catch(Exception ex)
			 {
				 ex.printStackTrace();
				 
				 
			 }
			
			
			
		}
		
				
		return flag;
	}
	
	
	String  getVehicleRegNumber()
	{
		 
		     String vehicleRegNumber=null;
		 
			 try {
			
			  String fileName=AppConstant.PLC_BASE_FILE_PATH+File.separator+AppConstant.PLC_FILE_NAME;
			
			  FileInputStream file = new FileInputStream(new File(fileName));
			  XSSFWorkbook workbook = new XSSFWorkbook(file);
			  XSSFSheet sheet = workbook.getSheetAt(0);
			  
			  XSSFRow rowi= sheet.getRow(0);
			  CellReference cellReference = new CellReference(AppConstant.COL_VEH_REG_NUM); 
			  Row row = sheet.getRow(cellReference.getRow());
			  Cell cell = row.getCell(cellReference.getCol()); 
			  vehicleRegNumber= cell.getStringCellValue();
			  workbook.close();
			  
			
			  //reset 
			  FileInputStream fileveh = new FileInputStream(new File(fileName));
			  XSSFWorkbook workbookveh = new XSSFWorkbook(fileveh); 
			  XSSFSheet sheetveh = workbook.getSheetAt(0);
			  
			  XSSFRow rowii= sheet.getRow(0); CellReference cellReferenceVeh = new
			  CellReference(AppConstant.COL_VEH_REG_ENTERED); 
			  Row rowVeh = sheetveh.getRow(cellReferenceVeh.getRow()); 
			  Cell cellVeh =row.getCell(cellReferenceVeh.getCol()); 
			  cellVeh.setCellValue(1);
			  
			  //getStringDatadFromCell();
			  System.out.println("vehicleRegNumber="+vehicleRegNumber);
			  workbook.close();
			   
			  
			 }
			 catch(Exception ex)
			 {
				 ex.printStackTrace();
				 
				 
			 }
			
		
		return vehicleRegNumber;
	}
	
	
  int alloCateBin(String csctsRefrNum,ArrayList<Bin>binMap,String fullcsCTSRefNum,String sampleType)
  {
	   
	  int retBinNum=-1;
	
	  if(sampleType==null)
	  {
		  return retBinNum;
		  
	  }
	  try {
	  
	 if(sampleType.equalsIgnoreCase("00"))
	 {
		 
		  System.out.println("Normal Sampling");
	 
		  //case 1 - first time all bins empty choose the first one
		  
		  if(allBinsEmpty)
		  {
			  
			  retBinNum=0;
			  
		  }
		  else
		  {
		  
		    
		  
		   //look out for sample matching ref number
		    for(int i=0;i<binMap.size()-1;i++) { //dont consider the special BIN
			   
				      Bin bin= binMap.get(i);
				      
				      if(bin.getCsctsRefNum()!=null && (bin.getCsctsRefNum().equalsIgnoreCase(csctsRefrNum))) {
						  //what is the filling status
						  if(bin.getNumOfTimesFilled()<AppConstant.BIN_MAX_NUM_TIMES_FILLED)//this bin can still take sample
						  {
							  retBinNum=i;
							  return retBinNum;
							  
						  }
			   	   
				       
				      }
		    }
				      
		  //NO match found
	      //find an empty bin
		      for(int j=0;j<binMap.size()-1;j++)
		      {
		    	  
		    	  Bin binempty= binMap.get(j);
		    	  if(binempty.getNumOfTimesFilled()==0)
		    	  {
		    		  retBinNum=j;
		    		  return retBinNum;
		    		  
		    	  }
		   	  
		      }
		      //All bins have some material find out the bin which has been filled max number of times
		      
		      int maxBinSizeIndex=0;//assume first 1 has the max value
		      Bin binFirst=binMap.get(0);
		      int maxNum=binFirst.getNumOfTimesFilled();
		      
		      for(int j=1;j<binMap.size()-1;j++) {
		    	  
		    	   Bin binnew=binMap.get(j);
		    	   if(binnew.getNumOfTimesFilled()>maxNum)
		    	   {
		    		   maxNum=binnew.getNumOfTimesFilled();
		    		   maxBinSizeIndex=j;
		    	   }
		    	  
		    	  
		      }
		      if(clearBin(binMap,maxBinSizeIndex))
		      {
		    	  printForBarCode(maxBinSizeIndex);
		    	  retBinNum= maxBinSizeIndex;
		      }
	  
		    }
	  
		  }
	 
	  }catch(Exception ex)
	  {
		  
		  ex.printStackTrace();
		  
	  }
	  finally {
		  return retBinNum;
	  }
  }
  
  void printBinMap(ArrayList<Bin>binMap)
  {
	  System.out.println("Printing BIN MAP");
	  for(Bin b:binMap)
	  {
		  
		  System.out.println("Bin Num= "+b.getBinNum()+1);
		  System.out.println("CSCTS Ref Num= "+b.getCsctsRefNum());
		  System.out.println("NumOfTimesFilled= "+b.getNumOfTimesFilled());
		  
		  
		  
	  }
	  
	  
  }
  
  @Override
	public ResponseEntity mainLoopStop()
	{
	  
	  startFlag=false;
	  
	  return new ResponseEntity<>(
		      "Stopped-- All OK", 
		      HttpStatus.OK);
	  
	}
  
  
  int getNumericDatadFromCell(String cellNumber)
  {
	  int ret=-1;
	  
	  try
	  {
		  
		  String fileName=AppConstant.PLC_BASE_FILE_PATH+File.separator+AppConstant.PLC_FILE_NAME;			
		  FileInputStream file = new FileInputStream(new File(fileName));
		  XSSFWorkbook workbook = new XSSFWorkbook(file);
		  XSSFSheet sheet = workbook.getSheetAt(0);		  
		  XSSFRow rowi= sheet.getRow(0);
		  CellReference cellReference = new CellReference(cellNumber); 
		  Row row = sheet.getRow(cellReference.getRow());
		  Cell cell = row.getCell(cellReference.getCol()); 
		  ret=(int) cell.getNumericCellValue();
		  workbook.close();	  
		  
	  }catch(Exception ex)
	  {
	    ex.printStackTrace();
		  
	  }	  
	  finally
	  {
		  
		  return ret;	  
		  
	  }
	
  }
  
  String getStringDataFromCell(String cellNumber)
  {
	  String  ret= null;
	  
	  try
	  {
		  
		  String fileName=AppConstant.PLC_BASE_FILE_PATH+File.separator+AppConstant.PLC_FILE_NAME;			
		  FileInputStream file = new FileInputStream(new File(fileName));
		  XSSFWorkbook workbook = new XSSFWorkbook(file);
		  XSSFSheet sheet = workbook.getSheetAt(0);		  
		  XSSFRow rowi= sheet.getRow(0);
		  CellReference cellReference = new CellReference(cellNumber); 
		  Row row = sheet.getRow(cellReference.getRow());
		  Cell cell = row.getCell(cellReference.getCol()); 
		  ret=  cell.getStringCellValue();
		  workbook.close();	  
		  
	  }catch(Exception ex)
	  {
	    ex.printStackTrace();
		  
	  }	  
	  finally
	  {
		  
		  return ret;	  
		  
	  }
	
  }
  
  void writeToCell(String cellNumber,int value)
  {
	  
	  try {
		 
		  
		  String fileName=AppConstant.PLC_BASE_FILE_PATH+File.separator+AppConstant.PLC_FILE_NAME;			
		  FileInputStream file = new FileInputStream(new File(fileName));
		  XSSFWorkbook workbook = new XSSFWorkbook(file);
		  XSSFSheet sheet = workbook.getSheetAt(0);		  
		  XSSFRow rowi= sheet.getRow(0);
		  CellReference cellReference = new CellReference(cellNumber); 
		  Row row = sheet.getRow(cellReference.getRow());
		  Cell cell = row.getCell(cellReference.getCol()); 
		  int ret=(int) cell.getNumericCellValue();
		  
		  System.out.println(ret);
		  
		  cell.setCellValue(value);
		  file.close();
		  FileOutputStream outputStream = new FileOutputStream(fileName);
		  workbook.write(outputStream);
		  outputStream.close();
		  workbook.close();
		  
		 /* cellVeh.setCellValue(value);;
		  fileveh.close();
		  FileOutputStream outputStream = new FileOutputStream(fileName);
		  workbookveh.write(outputStream);
		  outputStream.close();
		  workbookveh.close();*/
		 
		  
		  
		  
	  }
	  catch(Exception ex)
	  {
		  ex.printStackTrace();
		  
		  
	  }
	  
	 
  }
  
  
  void writeToCellStr(String cellNumber,String value)
  {
	  
	  try {
		 
		  
		  String fileName=AppConstant.PLC_BASE_FILE_PATH+File.separator+AppConstant.PLC_FILE_NAME;			
		  FileInputStream file = new FileInputStream(new File(fileName));
		  XSSFWorkbook workbook = new XSSFWorkbook(file);
		  XSSFSheet sheet = workbook.getSheetAt(0);		  
		  XSSFRow rowi= sheet.getRow(0);
		  CellReference cellReference = new CellReference(cellNumber); 
		  Row row = sheet.getRow(cellReference.getRow());
		  Cell cell = row.getCell(cellReference.getCol()); 
		  int ret=(int) cell.getNumericCellValue();
		  
		  System.out.println(ret);
		  
		  cell.setCellValue(value);
		  file.close();
		  FileOutputStream outputStream = new FileOutputStream(fileName);
		  workbook.write(outputStream);
		  outputStream.close();
		  workbook.close();
		  
		 /* cellVeh.setCellValue(value);;
		  fileveh.close();
		  FileOutputStream outputStream = new FileOutputStream(fileName);
		  workbookveh.write(outputStream);
		  outputStream.close();
		  workbookveh.close();*/
		 
		  
		  
		  
	  }
	  catch(Exception ex)
	  {
		  ex.printStackTrace();
		  
		  
	  }
	  
	 
  }
  
  
  boolean clearBin(ArrayList<Bin>binMap,int binNumIndex) throws InterruptedException
  {
	  System.out.println("Bin Clear command set for Bin Index="+binNumIndex+1);
	  
	  
	  boolean retval=false;
	  
	  binMap.get(binNumIndex).setBinFull(true);
	  writeToCell(AppConstant.COL_EMPTY_BIN_NUM,binNumIndex+1);
	  writeToCell(AppConstant.COL_EMPTY_BIN_NUM_COMM,1);
	
	//wait for BIN empty response
	   
	  while((true)&&(startFlag))
	  {
		  Thread.sleep(5000);
		  int response=getNumericDatadFromCell(AppConstant.COL_BIN_EMPTIED_PRINTING_DONE);
		  System.out.println("---Bin Emptied completion response awaited--");
		  if(response==AppConstant.BIN_EMPTIED)
		  {
			   printForBarCode(binNumIndex);
			   Thread.sleep(1000);
			   binMap.get(binNumIndex).setCsctsRefNum("");
			   binMap.get(binNumIndex).setNumOfTimesFilled(0);
			   binMap.get(binNumIndex).setBinFull(false);
			  // binMap.get(binNumIndex).getCsCTSrefNumList().clear();
			   System.out.println("Bin Clear command   for Bin ="+(binNumIndex+1)+ " Completed Successfully");
			   retval=true;
			   break;
		  }
			  						  
	  }
	
	  
	  
	  for(Bin bin:binMap)
	  {
		  
		  if(bin.getNumOfTimesFilled()>0)
		  {
			  allBinsEmpty=false;
			  break;
			  
		  }
		  
		  
	  }
	  
	  
	  
	  
	  return retval;
	  
  }



@Override
public ResponseEntity writeValCell(String cell, String value) {
	// TODO Auto-generated method stub
	
	try {
		
		writeToCell(cell,Integer.parseInt(value));
		
		
		
	}catch(Exception ex)
	{
		
		ex.printStackTrace();
		
	}
	
	
	  return new ResponseEntity<>(
		      "WRITTEN-- All OK", 
		      HttpStatus.OK);
}

 
 
@Override
public ResponseEntity printBin(int binNum) {
	
	Bin b=binMapRef.get(binNum);
	 
	  System.out.println("Bin Num= "+(b.getBinNum()+1));
	  
	  System.out.println("CSCTS Ref Num= "+b.getCsctsRefNum());
	  System.out.println("NumOfTimesFilled= "+b.getNumOfTimesFilled());
	  
	return new ResponseEntity<>(
		      "WRITTEN-- All OK", 
		      HttpStatus.OK);
}

 
public void printForBarCode(int binNum) {
	
	Bin b=binMapRef.get(binNum);
	//ArrayList<String>strList= b.getCsCTSrefNumList();
//	for(String str:strList)
//	{
//		System.out.println("Barcode="+str);
//		
//		
//	}
//	
	
}


@Override
public ResponseEntity sendLimsData() {
	// TODO Auto-generated method stub
	log.info("Inside sendLimsData-");
	ResponseEntity<ResponseLims> response=null;
	JSONArray jsonArray = new JSONArray();
	
	JSONObject  formObj= new JSONObject();
	
	formObj.put("PlantCode","RS");
	formObj.put("CSCTSRefNo","R100RS628557184042"); 
	formObj.put("CollRefNo","381896874793"); 
	formObj.put("SampleRefNo","7037159759"); 
	formObj.put("PartSize",1); 
	formObj.put("DateOfSampling","23/08/2021"); 
	
	jsonArray.add(formObj);
	
	formObj.put("PlantCode","RS");
	formObj.put("CSCTSRefNo","R100RS628557184042"); 
	formObj.put("CollRefNo","381896834799"); 
	formObj.put("SampleRefNo","7037159770"); 
	formObj.put("PartSize",1); 
	formObj.put("DateOfSampling","23/02/2023"); 
	
	jsonArray.add(formObj);
	formObj.put("PlantCode","RS");
	formObj.put("CSCTSRefNo","R100RS628557184042"); 
	formObj.put("CollRefNo","381896834599"); 
	formObj.put("SampleRefNo","7037159570"); 
	formObj.put("PartSize",1); 
	formObj.put("DateOfSampling","23/08/2021"); 
	jsonArray.add(formObj);
	
    try {
	HttpHeaders headers = new HttpHeaders();
	headers.setContentType(MediaType.APPLICATION_JSON);
	//headers.add("plantCode", plantCode);
	
	 
	HttpEntity<JSONArray> entity = new HttpEntity<JSONArray>(jsonArray, headers);
	
	 String url=AppConstant.API_LIMS_URL;
	// response = restTemplate.postForEntity( url, entity , ResponseAug.class );
	 response= restTemplate.exchange(url, HttpMethod.POST, entity, ResponseLims.class);
	 response.getBody().getStatusCode();
	 
	log.info("sendLimsData-" +response.getBody().getStatusCode());
	log.info("sendLimsData-"+response.getBody().getMessage());	
	
 
    }
    catch(HttpClientErrorException  ex)
    {
    	
    	ex.printStackTrace();
    }
    catch(UnknownHttpStatusCodeException  ex) {
    
      System.out.println(ex.getRawStatusCode());
       
    }
    
	return response;
	
}

@Override
public String getCollRefNum()
{
	String colRefNum="Date";
	try
	{
		LocalDate ld=LocalDate.now();
		System.out.println(ld.getYear());
		System.out.println(ld.getMonthValue());
		System.out.println(ld.getDayOfMonth());
		System.out.println(System.currentTimeMillis()/600);
		Long lMin=(System.currentTimeMillis()/600);
		String currMin=lMin.toString();
		
		colRefNum=AppConstant.PLANT_NUMERIC_CODE+AppConstant.AUGER_NUMERIC_CODE+currMin;
		
		//colRefNum= 
		
	}catch(Exception ex)
	{
		ex.printStackTrace();
	}
	
	finally
	{
		return colRefNum;
	}


}


@Override
public String getSampRefNum1()
{
	String colRefNum="Date";
	try
	{
		 
		//System.out.println(System.currentTimeMillis()/60000);
		Long lMin=(System.currentTimeMillis()/60000);
		String currMin=lMin.toString();
		
		colRefNum=AppConstant.AUG_SAMP_NUM_1+AppConstant.PLANT_NUMERIC_CODE+AppConstant.AUGER_NUMERIC_CODE+currMin;
		
		//colRefNum= 
		
	}catch(Exception ex)
	{
		ex.printStackTrace();
	}
	
	finally
	{
		return colRefNum;
	}


}
 

@Override
public String getSampRefNum2()
{
	String colRefNum="Date";
	try
	{
		 
		//System.out.println(System.currentTimeMillis()/60000);
		Long lMin=(System.currentTimeMillis()/60000);
		String currMin=lMin.toString();
		
		colRefNum=AppConstant.AUG_SAMP_NUM_2+AppConstant.PLANT_NUMERIC_CODE+AppConstant.AUGER_NUMERIC_CODE+currMin;
		
		//colRefNum= 
		
	}catch(Exception ex)
	{
		ex.printStackTrace();
	}
	
	finally
	{
		return colRefNum;
	}


}


@Override
public String getDate()
{
	LocalDate localDate = LocalDate.now();
	String pattern = "dd/MM/YYYY";
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
	String formatatedDate = localDate.format(formatter);
	//System.out.println("LocalDate.now: ");
	//System.out.println(localDate);
	//System.out.println("Formatted: ");
	//System.out.println(format);
	return formatatedDate;

}

@Override
public ResponseEntity printTestBarcode(String barcodeFileName,String barcode) {
	// TODO Auto-generated method stub
	
	 
	 createImage(barcodeFileName,barcode);
	 //createImage("barcode1.png","1234567890");
	
	
	
	return new ResponseEntity<>(
		      "Printed Barcode-- All OK", 
		      HttpStatus.OK);
}

public  void createImage(String image_name,String myString)  {
	try {
	String PRINTERNAME="Kores ENDURA 2801 - TSPL";	
		
	Code128Bean code128 = new Code128Bean();
	code128.setHeight(7f);
	code128.setModuleWidth(0.3);
	code128.setQuietZone(10);
	code128.doQuietZone(true);
	ByteArrayOutputStream baos = new ByteArrayOutputStream();
	BitmapCanvasProvider canvas = new BitmapCanvasProvider(baos, "image/x-png", 200, BufferedImage.TYPE_BYTE_BINARY, false, 0);
	 
	code128.generateBarcode(canvas, myString);
	canvas.finish();
	//write to png file
	LocalDateTime myDateObj = LocalDateTime.now();
	  
	 DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("ddMMyyyy");
	 String formattedDate = myDateObj.format(myFormatObj);
	String imageFilePath=AppConstant.PLC_BASE_FILE_PATH+"\\"+AppConstant.BARCODE_IMAGE_FILE_PATH+"\\"+formattedDate+"\\"+image_name;
	log.info(imageFilePath);
	FileOutputStream fos = FileUtils.openOutputStream(new File(imageFilePath)); 
	fos.write(baos.toByteArray());
	fos.flush();
	fos.close();
	//printTest(imageFilePath);
	 print(imageFilePath);
	//doPrint(imageFilePath);
	//PrinterJob printJob = PrinterJob.getPrinterJob();
	/*PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
	pras.add(OrientationRequested.PORTRAIT); 
	pras.add(new Copies(1));
	pras.add(new PrinterResolution(203,203,PrinterResolution.DPI));  
    PrintService pss[] = PrintServiceLookup.lookupPrintServices(DocFlavor.INPUT_STREAM.GIF, pras);*/
   // if (pss.length == 0)
   //   throw new RuntimeException("No printer services available.");
    	/*PageFormat pf = new PageFormat();
    	Paper p = new Paper();
    	pf.setOrientation(PageFormat.LANDSCAPE); 
    	pf.setPaper(p);*/
    
   /* for (int i = 0; i < pss.length; i++) {
    	 System.out.println(pss[i].getName());
    	 
    	 if (pss[i].getName().compareTo(PRINTERNAME) == 0) {
    		 
    		 	PrintService ps = pss[i];
    		 	 
    		 	System.out.println("Printing to " + ps);
    		    DocPrintJob job = ps.createPrintJob();
    		    
    		    FileInputStream fin = new FileInputStream(imageFilePath);
    		    Doc doc = new SimpleDoc(fin, DocFlavor.INPUT_STREAM.GIF, null);
    		     
    		    job.print(doc, pras);
    		    fin.close();
    	 }
    }
    
    */
	
	
	
	} catch (Exception e) {
		// TODO: handle exception
		log.info(e.toString());
	}
}

public void print(String imageFileName) throws IOException, PrinterException {
	  
	  imageFileName="d:\\Projects\\RTMS\\imagefolder\\16102021\\t11.png";
	  final PrintService printService = PrintServiceLookup.lookupDefaultPrintService();
	  final BufferedImage image = ImageIO.read(new File(imageFileName));
	  final PrinterJob printJob = PrinterJob.getPrinterJob();
	  printJob.setJobName("MyApp: " + imageFileName);
	  
	  printJob.setPrintService(printService);
	//  printJob.setPrintable(new Printable());
	  final PageFormat pf = printJob.defaultPage();
	  double pageWidth= pf.getWidth();
	  double pageHeight= pf.getHeight();
	  log.info("pageWidth="+pageWidth);
	  log.info("pageHeight="+pageHeight);
	  
	  printJob.setPrintable(new Printable() {
	    @Override
	    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
	      if (pageIndex == 0) {
	    	
	    	  /*Paper paper= new Paper();
	    	  paper.setSize(pageWidth , pageHeight );
	    	  paper.setImageableArea(0.0, 0.0, paper.getWidth(), paper.getHeight());
	    	  pageFormat.setPaper(paper);*/
	    	  Paper paper= new Paper();
	    	  double width = cmsToPixel(10.41, 200);
	    		double height = cmsToPixel(17.16, 200);
	    		paper.setSize(width, height);
	    		// 10 mm border...
	    		paper.setImageableArea(
	    		                //cmsToPixel(0.1, 72),
	    		                //cmsToPixel(0.1, 72),
	    						0.0,
	    						0.0,
	    		                width - cmsToPixel(0.1, 200),
	    		                height - cmsToPixel(0.1, 200));
	    		// Orientation
	    		//pf.setOrientation(PageFormat.PORTRAIT);
	    		pageFormat.setOrientation(PageFormat.LANDSCAPE);
	    		pageFormat.setPaper(paper);
	    	  
	       // final Paper paper = pageFormat.getPaper();
	       // paper.setImageableArea(0.0, 0.0, pageFormat.getPaper().getWidth(), pageFormat.getPaper().getHeight());
	       
	       //p.setImageableArea(0.0, 0.0, width, height);
	        log.info("Width="+pageFormat.getPaper().getWidth());
	        log.info("Height="+pageFormat.getPaper().getHeight());
	        pageFormat.setPaper(paper);
	        graphics.translate((int) pageFormat.getImageableX(), (int) pageFormat.getImageableY());
	        graphics.drawImage(image, 0, 0, (int) pageFormat.getPaper().getWidth(), (int) pageFormat.getPaper().getHeight(), null);
	        //graphics.drawImage(image, 0, 0, (int) pageFormat.getImageableWidth(), (int) pageFormat.getImageableHeight(), null);
	       // graphics.drawImage(image, 0, 0, (int) image.getWidth(), (int) image.getHeight(), null);
	     /*   Graphics2D g2 = (Graphics2D) graphics;
	        final double xScale = 1.1;
	        final double xTranslate = -55;
	        final double yScale = 1;
	        final double yTranslate = 0;
	        final double widthScale = (pageFormat.getWidth() / image.getWidth()) * xScale;
	        final double heightScale = (pageFormat.getHeight() / image.getHeight()) * yScale;
	        System.out.println("Setting scale to " + widthScale + "x" + heightScale);
	        final AffineTransform at = AffineTransform.getScaleInstance(widthScale, heightScale);
	        System.out.println("Setting translate to " + xTranslate + "x" + yTranslate);
	        at.translate(xTranslate, yTranslate);*/
	        if (pageIndex != 0) {
	            return NO_SUCH_PAGE;
	        }
	       // g2.drawRenderedImage(image, at);
	        
	        
	        
	        return PAGE_EXISTS;
	      } else {
	        return NO_SUCH_PAGE;
	      }
	    }
	  });
	  printJob.print();
	}
 
void doPrint(String imageFileName)
{
	
	try {
	PrinterJob pj = PrinterJob.getPrinterJob();
	PageFormat pf = pj.defaultPage();
	Paper paper = pf.getPaper();
	// 10x15mm
	
	double width = cmsToPixel(10, 72);
	double height = cmsToPixel(15, 72);
	paper.setSize(width, height);
	// 10 mm border...
	paper.setImageableArea(
	                cmsToPixel(0.1, 72),
	                cmsToPixel(0.1, 72),
	                width - cmsToPixel(0.1, 72),
	                height - cmsToPixel(0.1, 72));
	// Orientation
	pf.setOrientation(PageFormat.PORTRAIT);
	pf.setPaper(paper);
	PageFormat validatePage = pj.validatePage(pf);
	final BufferedImage image=ImageIO.read(new File(imageFileName));
	pj.setPrintable(new Printable() {
	    @Override
	    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
	        // Your code here
	        	graphics.translate((int) pageFormat.getImageableX(), (int) pageFormat.getImageableY());
		       // graphics.drawImage(image, 0, 0, (int) pageFormat.getPaper().getWidth(), (int) pageFormat.getPaper().getHeight(), null);
		        graphics.drawImage(image, 0, 0, (int) pageFormat.getImageableWidth(), (int) pageFormat.getImageableHeight(), null);

	        return NO_SUCH_PAGE;
	    }
	
	},  validatePage);
	 pj.print();
	
	}
	catch (Exception ex)
	{
		ex.printStackTrace();
		
	}
		 
	

}

public void printTest(String imageFileName) throws IOException, PrinterException {
	 try { 
	 final PrintService printService = PrintServiceLookup.lookupDefaultPrintService();
	  final BufferedImage image = ImageIO.read(new File(imageFileName));
	  final PrinterJob printJob = PrinterJob.getPrinterJob();
	  printJob.setJobName("MyApp: " + imageFileName);
	  
	  printJob.setPrintService(printService);
 
	  final PageFormat pageFormat = printJob.defaultPage();
	 
	 
     
	  printJob.setPrintable(new Printable() {
		    @Override
		    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
		      if (pageIndex == 0) {
		    	
		    	  
		        graphics.translate((int) pageFormat.getImageableX(), (int) pageFormat.getImageableY());
		        graphics.drawImage(image, 0, 0, (int) pageFormat.getPaper().getWidth(), (int) pageFormat.getPaper().getHeight(), null);
		  
		        if (pageIndex != 0) {
		            return NO_SUCH_PAGE;
		        }
		     
		        
		        
		        return PAGE_EXISTS;
		      } else {
		        return NO_SUCH_PAGE;
		      }
		    }
		  });
	  if (printJob.printDialog()) {
		  
		  printJob.print();
		  
	  	}
	   
	  
	 }
	 catch (Exception ex)
	 {
		 ex.printStackTrace();
		 
		 
	 }
	  
	   
	}

//The number of CMs per Inch
public static final double CM_PER_INCH = 0.393700787d;
//The number of Inches per CMs
public static final double INCH_PER_CM = 2.545d;
//The number of Inches per mm's
public static final double INCH_PER_MM = 25.45d;

/**
* Converts the given pixels to cm's based on the supplied DPI
*
* @param pixels
* @param dpi
* @return
*/
public static double pixelsToCms(double pixels, double dpi) {
return inchesToCms(pixels / dpi);
}

/**
* Converts the given cm's to pixels based on the supplied DPI
*
* @param cms
* @param dpi
* @return
*/
public static double cmsToPixel(double cms, double dpi) {
return cmToInches(cms) * dpi;
}

/**
* Converts the given cm's to inches
*
* @param cms
* @return
*/
public static double cmToInches(double cms) {
return cms * CM_PER_INCH;
}

/**
* Converts the given inches to cm's
*
* @param inch
* @return
*/
public static double inchesToCms(double inch) {
return inch * INCH_PER_CM;
}

@Override
public void checkStatusChangeTest() {
	// TODO Auto-generated method stub
	//binFille
}
@Override
public ResponseEntity updateAugStatus() {
	// TODO Auto-generated method stub
	log.info("Inside updateAugStatus");
	ResponseEntity<ResponseAug> response=null;
    try {
    		//writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ZERO);
	HttpHeaders headers = new HttpHeaders();
	headers.setContentType(MediaType.APPLICATION_JSON);
	headers.add("plantCode", "RENUSAGAR");
	
	//MultiValueMap<String, Object> map= new LinkedMultiValueMap<String, Object>();
	Map<String, Object> map = new HashMap<>();
	map.put("augurID", "R1");
	map.put("status", "Ready");		
	map.put("mode", "Open");
	map.put("reason_code", "");
	//HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
	HttpEntity<Map<String, Object>> request = new HttpEntity<>(map, headers);
	System.out.println(request);
	String url=AppConstant.BASE_URL+":"+AppConstant.BASE_PORT+"/"+AppConstant.API_UPDATE_AUG_STATUS;
	 response = restTemplate.postForEntity( url, request , ResponseAug.class );
	 response.getBody().getStatusCode();
	 
	System.out.println(response.getBody().getStatusCode());
	System.out.println(response.getBody().getErrorMessage());	
	
	 
    }
    catch(UnknownHttpStatusCodeException  ex) {
    	
    	log.info("updateAugStatus - Data error"+ex.getMessage());		    		    
    	log.info(ex.getRawStatusCode());
         
    }
   catch(Exception commError) {
	   
	   log.info("updateAugStatus -Communication Error: "+commError.getMessage());
	//   log.info(commError.getMessage());
	//   writeToCellWithRetry(AppConstant.COL_COMM_ERROR,AppConstant.NUMBER_ONE);
   }
    return response; 
    
}

}

