package com.rtms.augersys.dto;

import java.util.ArrayList;

import com.rtms.augersys.util.AppConstant;

public class Bin {
	
	
	int binNum;
	boolean binFull; //full - not full
	String csctsRefNum;	 
	int numOfTimesFilled;  
	ArrayList<String> csCTSrefNumFullList;
	ArrayList<LimsData> limsData;
	
	public ArrayList<LimsData> getLimsData() {
		return limsData;
	}
	public void setLimsData(ArrayList<LimsData> limsData) {
		this.limsData = limsData;
	}
	 
	public int getBinNum() {
		return binNum;
	}
	public void setBinNum(int binNum) {
		this.binNum = binNum;
	}
	 
	public String getCsctsRefNum() {
		return csctsRefNum;
	}
	public void setCsctsRefNum(String csctsRefNum) {
		this.csctsRefNum = csctsRefNum;
	}
	
	
	public ArrayList<String> getCsCTSrefNumFullList() {
		return csCTSrefNumFullList;
	}
	public void setCsCTSrefNumFullList(ArrayList<String> csCTSrefNumFullList) {
		this.csCTSrefNumFullList = csCTSrefNumFullList;
	}
	public Bin()
	{
		
		binNum=0;
		binFull=false;
		csctsRefNum=null;
		numOfTimesFilled=0;
		
		csCTSrefNumFullList = new ArrayList<String>();
		
		String str="";
		
		/*for(int i=0;i<AppConstant.BIN_MAX_NUM_TIMES_FILLED;i++)
		{
		
			csCTSrefNumList.add(str);
		}*/
		
		
	}
	public boolean isBinFull() {
		return binFull;
	}
	public void setBinFull(boolean binFull) {
		this.binFull = binFull;
	}
	 
	public int getNumOfTimesFilled() {
		return numOfTimesFilled;
	}
	public void setNumOfTimesFilled(int numOfTimesFilled) {
		this.numOfTimesFilled = numOfTimesFilled;
	}
	@Override
	public String toString() {
		return "Bin [binNum=" + binNum + ", binFull=" + binFull + ", csctsRefNum=" + csctsRefNum + ", numOfTimesFilled="
				+ numOfTimesFilled + ", csCTSrefNumFullList=" + csCTSrefNumFullList + ", limsData=" + limsData + "]";
	}
	 
	 
	

}
