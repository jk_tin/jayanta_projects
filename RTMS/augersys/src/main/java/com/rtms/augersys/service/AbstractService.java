package com.rtms.augersys.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import  com.rtms.augersys.dto.ResponseDTO;

public class AbstractService {

	public static ResponseEntity<ResponseDTO> responseEntitySuccess() {
		ResponseDTO response = new ResponseDTO(HttpStatus.OK.value(), HttpStatus.OK.toString(), null);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	public static ResponseEntity<ResponseDTO> responseEntitySuccess(String message) {
		ResponseDTO response = new ResponseDTO(HttpStatus.OK.value(), message, null);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	public static <T> ResponseEntity<ResponseDTO> responseEntitySuccess(String message, T data) {
		ResponseDTO response = new ResponseDTO(HttpStatus.OK.value(), message, data);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	public static ResponseEntity<ResponseDTO> responseEntityError() {
		ResponseDTO response = new ResponseDTO(HttpStatus.INTERNAL_SERVER_ERROR.value(),
				HttpStatus.INTERNAL_SERVER_ERROR.toString(), null);
		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	public static ResponseEntity<ResponseDTO> responseEntityError(String message) {
		ResponseDTO response = new ResponseDTO(HttpStatus.INTERNAL_SERVER_ERROR.value(), message, null);
		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	public static <T> ResponseEntity<ResponseDTO> responseEntityError(String message, T data) {
		ResponseDTO response = new ResponseDTO(HttpStatus.INTERNAL_SERVER_ERROR.value(), message, data);
		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	public static <T> ResponseEntity<ResponseDTO> responseEntityMultipleSuccess(String message, List<T> attrList,
			List<String> keyList) {
		Map<String, T> mapResponse = new HashMap<>();
		for (int i = 0; i < attrList.size(); i++) {
			mapResponse.put(keyList.get(i), attrList.get(i));
		}
		ResponseDTO response = new ResponseDTO(HttpStatus.OK.value(), message, mapResponse);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	 
}
