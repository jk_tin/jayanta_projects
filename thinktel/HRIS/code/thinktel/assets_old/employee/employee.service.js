(function () {
    'use strict';
	angular.module('employeeApp')
    .service('employeeService', ['$q', '$http','thinktelConstant', function($q, $http, thinktelConstant){
        var getEmployeeList = function (filter) {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'/employee/emplist?process_id='+filter.process_id+'&location_id='+filter.location_id+'&emp_code='+filter.emp_code,
                method: 'GET'
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });

            return deferred.promise;
        }


        return {
            getEmployeeList: getEmployeeList
        }
    }])
})();