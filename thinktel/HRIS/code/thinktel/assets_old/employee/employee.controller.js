(function () {
    'use strict';

    angular.module('employeeApp', ['nvd3', 'moment-picker', 'ngFileUpload'])
    .controller('addEditEmployeeController',['$q', 'addEmployeeService', 'commonService','$filter','thinktelConstant','$window', function($q, addEmployeeService, commonService,$filter, thinktelConstant, $window){
        var vm = this;
        vm.title = 'Employee';
        vm.employee = {};
        vm.processMaster = [];
        vm.locationMaster = [];
        vm.designationMaster = [];
        vm.subFunctionList = [];
        vm.empId = 0;


        vm.calculateSalary = function(){
            var data = {
                emp_code: vm.employee.emp_code,
                gross_sal: vm.employee.employee_sal_master_earning.emp_gross_sal,
                pfopt: vm.employee.employee_sal_master_deduction.emp_pf_opted
            }
            if(data.emp_code != 0
                && data.gross_sal != null && data.gross_sal != ''
                && data.pfopt != null && data.pfopt != ''){
                    addEmployeeService.calculateSalary(data).then(function(res){
                        vm.employee.employee_sal_company_contri = res.employee.employee_sal_company_contri;
                        vm.employee.employee_sal_master_computation = res.employee.employee_sal_master_computation;
                        vm.employee.employee_sal_master_deduction = res.employee.employee_sal_master_deduction;
                        vm.employee.employee_sal_master_earning = res.employee.employee_sal_master_earning;
                        bootbox.alert(res.message);

                    }, function(err){
                        console.log(err);
                    });
            }else{
                bootbox.alert("Please save basic info and give \"gross salary\" and \"PF Opted\"!");
            }

        }

        vm.saveEmployee = function(){
        	if(angular.isUndefined(vm.employee.personal_details.emp_dob)){
        		vm.employee.personal_details['emp_dob'] = "";
        	}

        	if(angular.isUndefined(vm.employee.join_info.emp_interview_date)){
        		vm.employee.join_info['emp_interview_date'] = "";
        	}

        	if(angular.isUndefined(vm.employee.join_info.emp_join_date)){
        		vm.employee.join_info['emp_join_date'] = "";
        	}


        	if(angular.isUndefined(vm.employee.join_info.emp_probation_date)){
        		vm.employee.join_info['emp_probation_date'] = "";
        	}


        	if(angular.isUndefined(vm.employee.employee_resig_info.emp_resig_date)){
        		vm.employee.employee_resig_info['emp_resig_date'] = "";
        	}

        	if(angular.isUndefined(vm.employee.employee_resig_info.emp_resig_accpt_date)){
        		vm.employee.employee_resig_info['emp_resig_accpt_date'] = "";
        	}
            var data = {
                        "employee":[angular.copy(vm.employee)]
                        }

            bootbox.confirm({
                message: "Do you want to save employee detail!?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if(result){
                        addEmployeeService.saveOrEditEmployee(data).then(function(res){
                            bootbox.alert(res.message, function() {
                                var form = document.createElement("form");
                                form.method = "POST";
                                form.action =  thinktelConstant.baseUrl+"employees/employeeList";
                                document.body.appendChild(form);
                                form.submit();
                            });

                        }, function(err){
                            console.log(err);
                        });
                    }
                }
            });
        }

        vm.uploadProfilePicture = function(file){
            $('.splash').css('display', 'block');
            var data = {
                emp_code: vm.employee.emp_code,
                userfile: file.files[0]
            }
            var fileType = file.files[0].type;//'image/jpeg'
            var size = file.files[0].size;
            if((fileType == 'image/jpeg' || fileType == 'image/jpg') && size <= 1024000){
                addEmployeeService.uploadProfilePicture(data).then(function(res){
                    bootbox.alert(res.message);
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('.profile-pic').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(file.files[0]);
                    $('.splash').css('display', 'none');
                }, function(err){
                    console.log(err);
                    $('.splash').css('display', 'none');
                });
            }else{
                bootbox.alert("File type should be 'JPG' and file size less than 1 MB");
                $('.splash').css('display', 'none');
            }

        }

        var readURL = function(input) {
            if (input.files && input.files[0]) {
                vm.uploadProfilePicture(input);
            }
        }

        $(".file-upload").on('change', function(){
            readURL(this);
        });

        vm.openSelectUploadDialog = function(){
            $(".file-upload").click();
        }

        vm.uploadDocuments = function(){ //function to call on form submit
            if (vm.parsonalDetailForm.documents.$valid && vm.documents) { //check if from is valid
                addEmployeeService.uploadDocuments(vm.documents, vm.employee.emp_code).then(function(res){
                    bootbox.alert(res.message);
                }, function(err){
                    bootbox.alert(err.message);
                })
            }
        }

        vm.downloadAllDocument = function(){
            addEmployeeService.downloadDocument(vm.employee.emp_code).then(function(res){
                //$('.profile-pic').attr('src', res);
            }, function(err){
                bootbox.alert(err.message);
            });
        }

        vm.getLoationList = function(){
            if(angular.isUndefined(vm.employee.personal_details.process_id) || vm.employee.personal_details.process_id == ''){
                vm.locationMaster = [];
                vm.employee.personal_details.loc_id = '';
                return;
            }
            $('.splash').css('display', 'block');
            commonService.getLocationMaster(vm.employee.personal_details.process_id).then(function(res){
                vm.locationMaster = res;
                $('.splash').css('display', 'none');
            }, function(err){
                console.log(err);
                $('.splash').css('display', 'none');
            });
        }

        vm.init = function(){
            vm.empId = $('#empId').val();
            vm.empLevel = $('#empLevel').val();
            $('.splash').css('display', 'block');
            var promiseArray = [];
            promiseArray.push(commonService.getProcessMaster());
            //promiseArray.push(commonService.getLocationMaster());
            promiseArray.push(commonService.getDesignationMaster());
            promiseArray.push(commonService.getSubFunction());

            if(vm.empId != 0){
                promiseArray.push(addEmployeeService.getEmployeeDetail(vm.empId));
            }else{
                promiseArray.push(addEmployeeService.getEmployeeModel());
            }
            $q.all(promiseArray).then(function(res){
                vm.processMaster = res[0];
                //vm.locationMaster = res[1].responseData;
                vm.designationMaster = res[1];
                vm.subFunctionList = res[2];
                if(vm.empId != 0){
                    vm.employee = res[3].employee;
                    addEmployeeService.getProfilePicture(vm.employee.emp_code).then(function(res){
                        $('.profile-pic').attr('src', res);
                    }, function(err){
                        bootbox.alert(err.message);
                    });
                }else{
                    vm.employee = res[3];
                }
            }, function(err){
                console.log(err);
            });
            // if(vm.empId != 0){
            //     addEmployeeService.getEmployeeDetail(vm.empId).then(function(res){
            //         vm.employee = res.employee;
            //         $('.splash').css('display', 'none');
            //     }, function(err){
            //         alert("Error");
            //         $('.splash').css('display', 'none');
            //     });
            // }else{
            //     addEmployeeService.getEmployeeModel().then(function(res){
            //         vm.employee = res;
            //         $('.splash').css('display', 'none');
            //     }, function(err){
            //         alert("Error");
            //         $('.splash').css('display', 'none');
            //     });
            // }

        }

    }]);
})();
