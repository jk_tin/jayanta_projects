(function () {
    'use strict';
    angular.module('employeeApp', ['nvd3', 'moment-picker'])
    .controller('salarySlipController', salarySlipController);
    function salarySlipController() {
        var vm = this; 
        vm.title = 'Salary Slip';
    }
})();