(function () {
    'use strict';
	angular.module('employeeApp')
    .service('salaryService', ['$q', '$http','thinktelConstant', function($q, $http, thinktelConstant){
        var getEmpoloyeeSalaryDetail = function (filter) {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'salary/payrolldetails?process_id='+filter.process_id+'&location_id='+filter.location_id+'&month='+filter.month+'&year='+filter.year+'&emp_code='+filter.emp_code,
                method: 'GET'
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });

            return deferred.promise;
        }

        var downloadAttendanceExcel = function (filter) {
            var objectUrl = thinktelConstant.restBaseUrl+'salary/saldownload?&month='+filter.month+'&year='+filter.year+"&process_id="+filter.process_id+"&location_id="+filter.location_id
            var link = angular.element('<a/>');
                link.attr({
                    href : objectUrl,
                    target: "_blank"
                })[0].click();
        }

        var generatePayRoll = function (filter) {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'salary/genpayroll',
                method: 'POST',
                data: filter
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });

            return deferred.promise;
        }

        var saveSalary = function (data) {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'salary/savepayrollweb',
                method: 'POST',
                data: data
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });

            return deferred.promise;
        }

        return {
            getEmpoloyeeSalaryDetail: getEmpoloyeeSalaryDetail,
            downloadAttendanceExcel: downloadAttendanceExcel,
            generatePayRoll: generatePayRoll,
            saveSalary: saveSalary
        }
    }])
})();