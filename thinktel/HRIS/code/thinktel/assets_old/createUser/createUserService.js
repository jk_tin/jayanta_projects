(function () {
    'use strict';
	angular.module('employeeApp')
    .service('createUserService', ['$q', '$http','thinktelConstant', function($q, $http, thinktelConstant){
        var createUser = function (data) {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'users/createuser',
                method: 'POST',
                data: data
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });
            return deferred.promise;
        }

        
        return {
            createUser: createUser
        }
    }])
})();