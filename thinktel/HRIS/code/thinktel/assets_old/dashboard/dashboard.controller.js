(function () {
    'use strict';
 
    angular.module('employeeApp', ['nvd3', 'moment-picker'])
    .controller('DashboardController',['thinktelConstant', function(thinktelConstant){
        var vm = this;
        vm.thinktelConstant = thinktelConstant;
 
        vm.title = 'Dashboard';
    }]);
})();