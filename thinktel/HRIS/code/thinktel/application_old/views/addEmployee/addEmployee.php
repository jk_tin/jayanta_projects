<!DOCTYPE html>
<html lang="en" ng-app="employeeApp">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Employee Tracker</title>
      <!-- CSS -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/bootstrap.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/metisMenu.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/main.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/sb-admin-2.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/angular-moment-picker.min.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/font-awesome.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/nv.d3.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/animate.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/loading-bars.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/footable.core.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/avtar.css" />
   </head>
   <body>
      <input id="empId" type ="hidden" id="empid" value ="<?php echo $emp_master_id ?>"/>
      <input id="empLevel" type ="hidden" id="empid" value ="<?php echo $emp_level ?>"/>
      <!-- Simple splash screen-->
      <div class="splash">
         <div class="color-line"></div>
         <div class="splash-title">
            <h1>Employee Tracker</h1>
            <img src="<?php echo base_url(); ?>assets/vendor/images/loading-bars.svg" width="64" height="64" />
         </div>
      </div>
      <div id="wrapper">
         <!-- Start Navigation Area -->
         <?php
            $this->load->view('share/leftHeader');
            ?>
         <!-- End Navigation Area -->
         <!-- Content Area -->
         <!-- ng-class with current state name give you the ability to extended customization your view -->
         <div id="page-wrapper" class="gray-bg {{$state.current.name}}">
            <!-- Nested view  -->
            <div class="template animated slideInRight" ng-controller="addEditEmployeeController as vm" ng-init="vm.init()">
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                     <h2>Add / Edit Employee</h2>
                     <ol class="breadcrumb">
                        <li><a ui-sref="home.dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active"><i class="fa fa-table"></i> {{vm.title}}</li>
                     </ol>
                  </div>
               </div>
               <!-- /.row -->
               <div class="row">
                  <form class="form-horizontal" role="form" name="vm.parsonalDetailForm" novalidate>
                     <fieldset class="the-fieldset">
                        <legend class="the-legend">Personal Detail</legend>
                        <div class="form-group">
                           <div class="col-sm-10">
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Emp. Code:</label>
                                 <div class="col-sm-3 col-xs-10">
                                    <input type="text" ng-model="vm.employee.emp_code" class="form-control" disabled>
                                 </div>
                                 <label ng-if="vm.empLevel == 1" class="col-sm-3 control-label">Emp. Level :</label>
                                 <div ng-if="vm.empLevel == 1" class="col-sm-3">
                                    <select class="form-control" ng-model="vm.employee.emp_level" style="width: 60px;">
                                       <option>1</option>
                                       <option>2</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 col-xs-2 control-label">Name:</label>
                                 <div class="col-sm-2 col-xs-10" ng-class="{'has-error': vm.parsonalDetailForm.salutation.$touched && vm.parsonalDetailForm.salutation.$invalid}">
                                    <select class="form-control" ng-model="vm.employee.personal_details.emp_title" name="salutation" required>
                                       <option>Mr</option>
                                       <option>Ms</option>
                                       <option>Mrs</option>
                                    </select>
                                 </div>
                                 <div class="col-sm-3 col-xs-12" ng-class="{'has-error': vm.parsonalDetailForm.fName.$touched && vm.parsonalDetailForm.fName.$invalid}">
                                    <input type="text" name="fName" ng-model="vm.employee.personal_details.emp_first" class="form-control" placeholder="First Name" required>
                                 </div>
                                 <div class="col-sm-2 col-xs-12" ng-class="{'has-error': vm.parsonalDetailForm.emp_mid.$touched && vm.parsonalDetailForm.emp_mid.$invalid}">
                                    <input type="text" name="emp_mid" ng-model="vm.employee.personal_details.emp_mid" class="form-control" placeholder="Middle Name" required>
                                 </div>
                                 <div class="col-sm-3 col-xs-12" ng-class="{'has-error': vm.parsonalDetailForm.lName.$touched && vm.parsonalDetailForm.lName.$invalid}">
                                    <input type="text" class="form-control" placeholder="Last Name" name="lName" ng-model="vm.employee.personal_details.emp_last" required>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 col-xs-2 control-label">Father`s Name/Husband Name:</label>
                                 <div class="col-sm-4 col-xs-10">
                                    <input type="text" ng-model="vm.employee.personal_details.emp_fa_hus_name" class="form-control" placeholder="Father`s Name/Husband Name">
                                 </div>
                                 <label class="col-sm-2 col-xs-2 control-label">Marital Status:</label>
                                 <div class="col-sm-4 col-xs-10">
                                    <label class="radio-inline">
                                    <input type="radio" name="maritalStatus" value="Married" ng-model="vm.employee.personal_details.emp_marital_status">Married
                                    </label>
                                    <label class="radio-inline">
                                    <input type="radio" name="maritalStatus" value="Unmarried" ng-model="vm.employee.personal_details.emp_marital_status">Single
                                    </label>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 col-xs-2 control-label">Gender:</label>
                                 <div class="col-sm-4 col-xs-10" ng-class="{'has-error': vm.parsonalDetailForm.gender.$touched && vm.parsonalDetailForm.gender.$invalid}">
                                    <label class="radio-inline">
                                    <input type="radio" name="gender" ng-model="vm.employee.personal_details.emp_gender" value="Male" required>Male
                                    </label>
                                    <label class="radio-inline">
                                    <input type="radio" name="gender" ng-model="vm.employee.personal_details.emp_gender" value="Female" required>Female
                                    </label>
                                 </div>
                                 <label class="col-sm-2 col-xs-2 control-label">D.O.B.:</label>
                                 <div class="col-sm-4 col-xs-10">
                                    <div class="input-group"
                                       ng-class="{'has-error': vm.parsonalDetailForm.dob.$touched && vm.parsonalDetailForm.dob.$invalid}"
                                       moment-picker="vm.employee.personal_details.emp_dob"
                                       start-view="month"
                                       format="DD-MM-YYYY">
                                       <span class="input-group-addon">
                                       <i class="fa fa-calendar fa-fw"></i>
                                       </span>
                                       <input class="form-control"
                                          placeholder="Select a date"
                                          name="dob" 
                                          ng-model="vm.employee.personal_details.emp_dob" 
                                          required
                                          ng-model-options="{ updateOn: 'blur' }">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-2 col-xs-10">
                              <div class="avatar-wrapper">
                                 <img class="profile-pic" src="" />
                                 <div class="upload-button" ng-click="vm.openSelectUploadDialog()">
                                    <i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
                                 </div>
                                 <input class="file-upload" type="file" accept="image/jpg"/>
                              </div>
                              <span>* only jpeg allowed and file size should be less then 1 MB</span>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">Blood Group:</label>
                           <div class="col-sm-4 col-xs-10">
                              <select class="form-control" ng-model="vm.employee.personal_details.emp_blood_grp" >
                                 <option>A+</option>
                                 <option>A-</option>
                                 <option>B+</option>
                                 <option>B-</option>
                                 <option>AB+</option>
                                 <option>AB-</option>
                                 <option>O+</option>
                                 <option>O-</option>
                              </select>
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">Designation:</label>
                           <div class="col-sm-4 col-xs-10">
                              <select class="form-control" ng-model="vm.employee.personal_details.emp_desig_id">
                                 <option value="">--Select Designation--</option>
                                 <option ng-repeat="designation in vm.designationMaster" value={{designation.id}}>{{designation.emp_desig_desc}}</option>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">Sub Function:</label>
                           <div class="col-sm-4 col-xs-10">
                              <select class="form-control" ng-model="vm.employee.personal_details.emp_sub_function_id">
                                 <option value="">--Select Sub Fucntion--</option>
                                 <option ng-repeat="subFuncton in vm.subFunctionList" value={{subFuncton.id}}>{{subFuncton.subfunction_name}}</option>
                              </select>
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">Process:</label>
                           <div class="col-sm-4 col-xs-10">
                              <select class="form-control" ng-model="vm.employee.personal_details.process_id">
                                 <option value="">--Select Process--</option>
                                 <option ng-repeat="process in vm.processMaster" value={{process.id}}>{{process.process_name}}</option>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">Location:</label>
                           <div class="col-sm-4 col-xs-10">
                              <select class="form-control" ng-model="vm.employee.personal_details.loc_id">
                                 <option value="">--Select Location--</option>
                                 <option ng-repeat="location in vm.locationMaster" value={{location.id}}>{{location.location_name}}</option>
                              </select>
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">Status:</label>
                           <div class="col-sm-4 col-xs-10">
                              <select class="form-control" ng-model="vm.employee.personal_details.emp_is_active" >
                                 <option value='1'>Active</option>
                                 <option value='2'>Inactive</option>
                              </select>
                           </div>
                        </div>
                     </fieldset>
                     <!--personal detail end-->
                     <!--contact detail Start-->
                     <fieldset class="the-fieldset" style="margin-top: 20px;">
                        <legend class="the-legend">Contact Detail</legend>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label"> Address 1:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.personal_details.emp_add1_id" class="form-control" placeholder="Address 1">
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label"> Address 2:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" class="form-control" ng-model="vm.employee.personal_details.emp_add_2_id" placeholder=" Address 2">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label"> State:</label>
                           <div class="col-sm-4 col-xs-10">
                              <select class="form-control" ng-model="vm.employee.personal_details.emp_state" >
                                 <option>WB</option>
                                 <option>UP</option>
                              </select>
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label"> City/Dist:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.personal_details.emp_city_dist" class="form-control" placeholder="City/Dist">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label"> PIN Code:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.personal_details.emp_pin" class="form-control" placeholder="PIN Code">
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">Phone Number.:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.personal_details.emp_contact_num" class="form-control" placeholder="Phone Number">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">Alternate phone number.:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.personal_details.emp_alt_contact_num" class="form-control" placeholder="Alternate phone number">
                           </div>
                        </div>
                     </fieldset>
                     <!--contact detail END-->
                     <!--Joining Information START-->
                     <fieldset class="the-fieldset" style="margin-top: 20px;">
                        <legend class="the-legend">Joining Information</legend>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label"> Interview Date:</label>
                           <div class="col-sm-4 col-xs-10">
                              <div class="input-group"
                                 moment-picker="vm.employee.join_info.emp_interview_date"
                                 start-view="month"
                                 format="DD-MM-YYYY">
                                 <span class="input-group-addon">
                                 <i class="fa fa-calendar fa-fw"></i>
                                 </span>
                                 <input class="form-control"
                                    placeholder="Select a date"
                                    ng-model="vm.employee.join_info.emp_interview_date"
                                    ng-model-options="{ updateOn: 'blur' }">
                              </div>
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">Joining date:</label>
                           <div class="col-sm-4 col-xs-10">
                              <div class="input-group"
                                 moment-picker="vm.employee.join_info.emp_join_date"
                                 start-view="month"
                                 format="DD-MM-YYYY">
                                 <span class="input-group-addon">
                                 <i class="fa fa-calendar fa-fw"></i>
                                 </span>
                                 <input class="form-control"
                                    placeholder="Select a date"
                                    ng-model="vm.employee.join_info.emp_join_date"
                                    ng-model-options="{ updateOn: 'blur' }">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">Probation Upto:</label>
                           <div class="col-sm-4 col-xs-10">
                              <div class="input-group"
                                 moment-picker="vm.employee.join_info.emp_probation_date"
                                 start-view="month"
                                 format="DD-MM-YYYY">
                                 <span class="input-group-addon">
                                 <i class="fa fa-calendar fa-fw"></i>
                                 </span>
                                 <input class="form-control"
                                    placeholder="Select a date"
                                    ng-model="vm.employee.join_info.emp_probation_date"
                                    ng-model-options="{ updateOn: 'blur' }">
                              </div>
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">Period:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.join_info.emp_probation_month" class="form-control" placeholder="Period">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">Appl Courier Sent:</label>
                           <div class="col-sm-4 col-xs-10">
                              <select class="form-control" ng-model="vm.employee.join_info.emp_appl_courier_sent" >
                                 <option value="Y">Yes</option>
                                 <option value="N">No</option>
                              </select>
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">Appl Courier Receive status:</label>
                           <div class="col-sm-4 col-xs-10">
                              <select class="form-control" ng-model="vm.employee.join_info.emp_appl_courier_recvd" >
                                 <option value="Y">Yes</option>
                                 <option value="N">No</option>
                              </select>
                           </div>
                        </div>
                     </fieldset>
                     <!--Joining Information END-->
                     <fieldset class="the-fieldset" style="margin-top: 20px;">
                        <legend class="the-legend">Identification Number</legend>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">PAN Number:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_id_info.emp_pan_number" class="form-control" placeholder="PAN Number">
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">Aadhar Card Number:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_id_info.emp_aadhar_number" class="form-control" placeholder="Aadhar Card Number">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">Voter Id Card Number:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_id_info.emp_voter_number" class="form-control" placeholder="Voter Id Card Number">
                           </div>
                        </div>
                     </fieldset>
                     <!--Bank Detail start-->
                     <fieldset class="the-fieldset" style="margin-top: 20px;">
                        <legend class="the-legend">Bank Detail</legend>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">Bank Name:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_bank_info.emp_bank_name" class="form-control" placeholder="Bank Name">
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">A/C Number:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_bank_info.emp_bank_ac_number" class="form-control" placeholder="A/C Number">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">IFSC Code:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_bank_info.emp_bank_ifsc_code" class="form-control" placeholder="IFSC Code">
                           </div>
                        </div>
                     </fieldset>
                     <!--bank detail den-->
                     <!--PF/ESIC Details START-->
                     <fieldset class="the-fieldset" style="margin-top: 20px;">
                        <legend class="the-legend">PF/ESIC Details</legend>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">PF Account:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_pf_esic_info.emp_pf_number" class="form-control" placeholder="PF Account">
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">ESIC Account Number:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_pf_esic_info.emp_esic_ac_number" class="form-control" placeholder="ESIC Account Number">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">UAN:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_pf_esic_info.emp_uan_number" class="form-control" placeholder="UAN">
                           </div>
                        </div>
                     </fieldset>
                     <!--PF/ESIC Details END-->
                     <!--Resignation Information START-->
                     <fieldset class="the-fieldset" style="margin-top: 20px;">
                        <legend class="the-legend">Resignation Information</legend>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">Resignation Date:</label>
                           <div class="col-sm-4 col-xs-10">
                              <div class="input-group"
                                 moment-picker="vm.employee.employee_resig_info.emp_resig_date"
                                 start-view="month"
                                 format="DD-MM-YYYY">
                                 <span class="input-group-addon">
                                 <i class="fa fa-calendar fa-fw"></i>
                                 </span>
                                 <input class="form-control"
                                    placeholder="Select a date"
                                    ng-model="vm.employee.employee_resig_info.emp_resig_date"
                                    ng-model-options="{ updateOn: 'blur' }">
                              </div>
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">Resignation Acceptance Date:</label>
                           <div class="col-sm-4 col-xs-10">
                              <div class="input-group"
                                 moment-picker="vm.employee.employee_resig_info.emp_resig_accpt_date"
                                 start-view="month"
                                 format="DD-MM-YYYY">
                                 <span class="input-group-addon">
                                 <i class="fa fa-calendar fa-fw"></i>
                                 </span>
                                 <input class="form-control"
                                    placeholder="Select a date"
                                    ng-model="vm.employee.employee_resig_info.emp_resig_accpt_date"
                                    ng-model-options="{ updateOn: 'blur' }">
                              </div>
                           </div>
                        </div>
                     </fieldset>
                     <!--Resignation Information END-->
                     <!--salary detail Salary  -Earning START-->
                     <fieldset class="the-fieldset" style="margin-top: 20px;">
                        <legend class="the-legend">Salary-Earning</legend>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">Gross Salary:</label>
                           <div class="col-sm-3 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_sal_master_earning.emp_gross_sal" class="form-control" placeholder="Gross Salary">
                           </div>
                           <div class="col-sm-1 col-xs-10">
                              <button type="button" class="btn btn-primary  pull-right" ng-click="vm.calculateSalary()">
                              <i class="fa fa-calculator"></i>
                              </button>
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">Basic:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_sal_master_earning.emp_basic_sal"  class="form-control" placeholder="Basic">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">HRA:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_sal_master_earning.emp_hra"  class="form-control" placeholder="HRA">
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">Conv. Allowanc:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_sal_master_earning.emp_conv_allowance"  class="form-control" placeholder="Conv. Allowanc">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">Education Allowance:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_sal_master_earning.emp_edu_allowance"  class="form-control" placeholder="Education Allowance">
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">Spl. Allowance:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_sal_master_earning.emp_spcl_allowance"  class="form-control" placeholder="Spl. Allowance">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">Upkeep Allowance:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_sal_master_earning.emp_upkeep_allowance"  class="form-control" placeholder="Upkeep Allowance">
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">Round Off:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_sal_master_earning.emp_round_off"  class="form-control" placeholder="Round Off">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">Gross Earning:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_sal_master_earning.emp_gross_earn"  class="form-control" placeholder="Gross Earning">
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">Tele/Other Reimbursement:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_sal_master_earning.emp_other_reimb"  class="form-control" placeholder="Tele/Other Reimbursement">
                           </div>
                        </div>
                     </fieldset>
                     <!--salary detail Salary  -Earning END-->
                     <!--slary detail Salary  -Deduction START-->
                     <fieldset class="the-fieldset" style="margin-top: 20px;">
                        <legend class="the-legend">Salary-Deduction</legend>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">PF Opted :</label>
                           <div class="col-sm-4 col-xs-10">
                              <select class="form-control"  ng-model="vm.employee.employee_sal_master_deduction.emp_pf_opted" >
                                 <option value="Y">Yes</option>
                                 <option value="N">No</option>
                              </select>
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">PF Contribution:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text"  ng-model="vm.employee.employee_sal_master_deduction.emp_pf_contri" class="form-control" placeholder="PF Contribution">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">ESIC :</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text"  ng-model="vm.employee.employee_sal_master_deduction.emp_esic_contri" class="form-control" placeholder="ESIC">
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">WB Prof Tax:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text"  ng-model="vm.employee.employee_sal_master_deduction.emp_wb_prof_tax" class="form-control" placeholder="WB Prof Tax">
                           </div>
                        </div>
                     </fieldset>
                     <!--salary detail Salary  -Deduction END-->
                     <!--Salary-computed(pre Tax) START-->
                     <fieldset class="the-fieldset" style="margin-top: 20px;">
                        <legend class="the-legend">Salary-computed(pre Tax)</legend>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">Salary In hand :</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_sal_master_computation.emp_sal_in_hand"  class="form-control" placeholder="Salary In hand">
                           </div>
                        </div>
                     </fieldset>
                     <!--Salary-computed(pre Tax) END-->
                     <!--Company's Contribution START-->
                     <fieldset class="the-fieldset" style="margin-top: 20px;">
                        <legend class="the-legend">Company's Contribution</legend>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">PF Contribution (3.67%) :</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_sal_company_contri.emp_pf_contri" class="form-control" placeholder="PF Contribution (3.67%)">
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">Pension Fund (8.33%) :</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_sal_company_contri.emp_pension_contri" class="form-control" placeholder="Pension Fund (8.33%)">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">EDLI (0.5%) :</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_sal_company_contri.emp_edli_contri" class="form-control" placeholder="EDLI (0.5%)">
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">ADM Charge(0.65%) :</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_sal_company_contri.emp_adm_charge" class="form-control" placeholder="ADM Charge(0.65%)">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">ADM to EDLI (0.01%):</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_sal_company_contri.emp_adm_charge_edli" class="form-control" placeholder="ADM to EDLI (0.01%)">
                           </div>
                           <label class="col-sm-2 col-xs-2 control-label">Total company's contribution to EPF(13.61%) :</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_sal_company_contri.emp_tot_contri_edli" class="form-control" placeholder="Total company's contribution to EDLI(13.61%)">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">Company's contribution to ESI:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text" ng-model="vm.employee.employee_sal_company_contri.emp_esi_contri" class="form-control" placeholder="Company's contribution to ESI">
                           </div>
                        </div>
                     </fieldset>
                     <!--Company's Contribution END-->
                     <fieldset class="the-fieldset" style="margin-top: 20px;">
                        <legend class="the-legend">Total CTC</legend>
                        <div class="form-group">
                           <label class="col-sm-2 col-xs-2 control-label">CTC PM:</label>
                           <div class="col-sm-4 col-xs-10">
                              <input type="text"  ng-model="vm.employee.employee_sal_master_computation.emp_tot_ctc" class="form-control" placeholder="CTC PM">
                           </div>
                        </div>
                     </fieldset>


                     <fieldset class="the-fieldset" style="margin-top: 20px;" ng-if="vm.employee.emp_code != '0'">
                        <legend class="the-legend">Upload Documents</legend>
                        <div class="form-group">                           
                           <div class="col-sm-2 col-sm-offset-8">
                              <div class="upload-btn-wrapper">
                                    <button class="btn btn-primary">Select file</button>
                                    <input type="file" 
                                          ngf-select 
                                          multiple
                                          ng-change="vm.uploadDocuments()"
                                          ng-model="vm.documents" 
                                          name="documents"                                     
                                          ngf-max-size="20MB">                                    
                              </div>
                           </div>
                           <div class="col-sm-2">
                              <button type="button" ng-click="vm.downloadAllDocument()" class="btn btn-primary btn-block">
                                    <i class="fa fa-download" aria-hidden="true"></i> Download
                              </button>
                           </div>
                           
                        </div>
                        <table class="table table-bordered">
                              <thead>
                                    <tr>
                                          <th>Serial No.</th>
                                          <th>File Name</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="file in vm.documents">
                                          <td>{{$index + 1}}</td>
                                          <td>{{file.name}}</td>
                                    </tr>
                              </tbody>
                              </table>
                     </fieldset>

                     <div style="margin-top: 20px;">
                        <button type="submit" class="btn btn-primary  pull-right" ng-click="vm.saveEmployee()">
                        <i class="fa fa-save"></i> Save
                        </button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <!-- End Content Area -->
      </div>
      <script src="<?php echo base_url(); ?>assets/vendor/js/jquery.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/metisMenu.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-ui-router.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/d3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/nv.d3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-nvd3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/moment.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/footable.all.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-footable.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/sb-admin-2.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/moment-with-locales.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-moment-picker.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/loading-bars.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/bootbox.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/ng-file-upload.min.js"></script>


      <script src="<?php echo base_url(); ?>assets/addEmployee/addEmployee.controller.js"></script>
      <script src="<?php echo base_url(); ?>assets/addEmployee/addEmployee.service.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/service.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/sidebarDirective.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/headerController.js"></script>
   </body>
</html>