<nav ng-controller="headerController as vm" ng-init="vm.init()" class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<!-- navbar-header -->
<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="index.html">Employee Tracker</a>
</div>
<!-- /.navbar-header -->
<!-- navbar-top-links -->
<ul class="nav navbar-top-links navbar-right">
    <li><?php $username = $this->session->username; echo $username; ?>
    <input id="userId" type="hidden" value="<?php $userid=$this->session->userdata['user_id']; echo $userid; ?>">
    </li>
        <!-- dropdown user icon -->
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
        </a>
        <!-- dropdown-user -->
        <ul class="dropdown-menu dropdown-user">
            <li><a href="profile"><i class="fa fa-user fa-fw"></i> User Profile</a>
            </li>
            <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
            </li>
            <li class="divider"></li>
            <li><a href="javascript:void(0)" ng-click="vm.logout()"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
            </li>
        </ul>
        <!-- /.dropdown-user -->
    </li>
    <!-- /.dropdown user icon -->
</ul>
<!-- /.navbar-top-links -->
<!-- navbar-static-side -->
<span ng-if="vm.menuList.length > 0">
    <sidebar data="vm.menuList"></sidebar>
</span>
<!-- /.navbar-static-side -->
</nav>