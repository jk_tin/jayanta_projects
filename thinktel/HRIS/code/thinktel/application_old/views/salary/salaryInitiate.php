<!DOCTYPE html>
<html lang="en" ng-app="employeeApp">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Employee Tracker</title>
      <!-- CSS -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/bootstrap.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/metisMenu.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/main.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/sb-admin-2.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/angular-moment-picker.min.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/font-awesome.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/nv.d3.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/animate.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/loading-bars.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/footable.core.min.css" />
   </head>
   <body>
      <!-- Simple splash screen-->
      <div class="splash">
         <div class="color-line"></div>
         <div class="splash-title">
            <img src="<?php echo base_url(); ?>assets/vendor/images/loading-bars.svg" width="64" height="64" />
         </div>
      </div>
      <div id="wrapper">
         <!-- Start Navigation Area -->
         <?php
            $this->load->view('share/leftHeader');
         ?>
         <!-- End Navigation Area -->
         <!-- Content Area -->
         <!-- ng-class with current state name give you the ability to extended customization your view -->
         <div id="page-wrapper" class="gray-bg {{$state.current.name}}" ng-controller="salaryInitiatController as vm" ng-init="vm.init()">
            <!-- Nested view  -->
            <div class="tempLate animated slideInRight">
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                     <h2>{{vm.title}}</h2>
                     <ol class="breadcrumb">
                        <li><a ui-sref="home.dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active"><i class="fa fa-table"></i> {{vm.title}}</li>
                     </ol>
                  </div>
               </div>
               <!--filter start-->
               <div class="row" style="margin-bottom: 10px;">
                  <div class="form-group col-sm-12">
                     <div class="col-sm-4 col-sm-offset-8">
                        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#salaryDownload"><i class="fa fa-download"></i> Download Salary Detail</button>
                     </div>
                  </div>
                  <div class="form-group col-sm-2">
                     <label for="email">Year Month:</label>
                     <div class="input-group"
                        moment-picker="vm.filter.yearMonth"
                        start-view="month"
                        format="MMMM-YYYY">
                        <span class="input-group-addon">
                        <i class="fa fa-calendar fa-fw"></i>
                        </span>
                        <input class="form-control"
                           placeholder="Select a date"
                           ng-model="vm.filter.yearMonth"
                           ng-model-options="{ updateOn: 'blur' }">
                     </div>
                  </div>
                  <div class="form-group col-sm-2">
                     <label for="email">Employee Code:</label>
                     <input type="text" ng-model="vm.filter.emp_code" class="form-control" placeholder="Employee code">
                  </div>
                  <div class="form-group col-sm-2">
                     <label for="email">Process:</label>
                     <select class="form-control" ng-model="vm.filter.process_id">
                        <option value="">--Select Process--</option>
                        <option ng-repeat="process in vm.processMaster" value={{process.id}}>{{process.process_name}}</option>
                     </select>
                  </div>
                  <div class="form-group col-sm-2">
                     <label for="email">Location:</label>
                     <select class="form-control" ng-model="vm.filter.location_id">
                        <option value="">--Select Location--</option>
                        <option ng-repeat="location in vm.locationMaster" value={{location.id}}>{{location.location_name}}</option>
                     </select>
                  </div>
                  <div class="form-group col-sm-4">
                     <label for="email">&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;</label>
                     <button class="btn pull-right" ng-click="vm.generatePayRoll()" style="margin-left: 10px;"><i class="fa fa-calculator"></i> Generate Pay Roll </button>
                     <button class="btn pull-right" ng-click="vm.applyFilter()"><i class="fa fa-search"></i> Search</button>
                    
                  </div>
               </div>
               <!--filter end-->
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                     <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default panel-border-color-custom" ng-repeat="(employeeIndex, empSalDetail) in vm.employeesSalary">
                           <div class="panel-heading panel-header-custom" role="tab" id="heading{{employeeIndex}}">
                              <h4 class="panel-title">
                                 <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{employeeIndex}}" aria-expanded="false" aria-controls="collapseTwo">
                                 <i class="pull-right collapse-icon fa fa-angle-down"></i>
                                 {{empSalDetail.personal_details.emp_title}} 
                                 {{empSalDetail.personal_details.emp_first}} 
                                 {{empSalDetail.personal_details.emp_last}}
                                 ({{empSalDetail.emp_code}})
                                 </a>
                              </h4>
                           </div>
                           <div id="{{employeeIndex}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{employeeIndex}}">
                              <div class="panel-body">
                                 <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                       <label class="col-sm-2 control-label text-align-left">Process:</label>
                                       <div class="col-sm-2">
                                          <span>{{empSalDetail.personal_details.process_id | processFilter: vm.processMaster}}</span>
                                          <!--<input disabled type="text" class="form-control" ng-model="empSalDetail.personal_details.process_id">-->
                                       </div>
                                       <label class="col-sm-2 control-label text-align-left">Location:</label>
                                       <div class="col-sm-2">
                                          <span>{{empSalDetail.personal_details.loc_id | locationFilter: vm.locationMaster}}</span>
                                          <!--<input disabled type="text" class="form-control" ng-model="empSalDetail.personal_details.loc_id">-->
                                       </div>
                                       <label class="col-sm-2 control-label text-align-left">Designation:</label>
                                       <div class="col-sm-2">
                                          <span>{{empSalDetail.personal_details.emp_desig_id | designationFilter: vm.designationMaster}}</span>
                                          <!--<input disabled type="text" class="form-control" ng-model="empSalDetail.personal_details.emp_desig_id">-->
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-sm-2 control-label text-align-left">EPF No:</label>
                                       <div class="col-sm-2">
                                          <input disabled type="text" class="form-control" ng-model="empSalDetail.employee_pf_esic_info.emp_pf_number">
                                       </div>
                                       <label class="col-sm-2 control-label text-align-left">UAN:</label>
                                       <div class="col-sm-2">
                                          <input disabled type="text" class="form-control" ng-model="empSalDetail.employee_pf_esic_info.emp_uan_number">
                                       </div>
                                       <label class="col-sm-2 control-label text-align-left">ESI No.:</label>
                                       <div class="col-sm-2">
                                          <input disabled type="text" class="form-control" ng-model="empSalDetail.employee_pf_esic_info.emp_esic_ac_number">
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-sm-2 control-label text-align-left">Bank Acc. No.:</label>
                                       <div class="col-sm-2">
                                          <input disabled type="text" class="form-control" ng-model="empSalDetail.employee_bank_info.emp_bank_ac_number">
                                       </div>
                                       <label class="col-sm-2 control-label text-align-left">IFSC code:</label>
                                       <div class="col-sm-2">
                                          <input disabled type="text" class="form-control" ng-model="empSalDetail.employee_bank_info.emp_bank_ifsc_code">
                                       </div>
                                       <label class="col-sm-2 control-label text-align-left">Bank name:</label>
                                       <div class="col-sm-2">
                                          <input disabled type="text" class="form-control" ng-model="empSalDetail.employee_bank_info.emp_bank_name">
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-sm-2 control-label text-align-left">Days In Month:</label>
                                       <div class="col-sm-2">
                                          <input disabled type="text" class="form-control" ng-model="empSalDetail.days_in_month">
                                       </div>
                                       <label class="col-sm-2 control-label text-align-left">Attendance:</label>
                                       <div class="col-sm-2">
                                          <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.tot_days_worked_in_month">
                                       </div>
                                       <label class="col-sm-2 control-label text-align-left">Deduct:</label>
                                       <div class="col-sm-2">
                                          <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.daysdeduct">
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="panel panel-default">
                                          <div class="panel-body">
                                             <div class="form-group earning-deduction-header">
                                                <div class="col-sm-6 control-label text-align-center">Earnings</div>
                                                <div class="col-sm-6 control-label text-align-center">Deduction</div>
                                             </div>
                                             <div class="form-group" style="border-bottom: 1px dotted;">
                                                <!--Earning-->
                                                <div class="col-sm-6">
                                                   <div class="form-group">
                                                      <label class="col-sm-6 control-label text-align-left">Basic:</label>
                                                      <div class="col-sm-6">
                                                         <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.monthly_basic">
                                                      </div>
                                                   </div>
                                                   <div class="form-group">
                                                      <label class="col-sm-6 control-label text-align-left">HRA:</label>
                                                      <div class="col-sm-6">
                                                         <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.hra">
                                                      </div>
                                                   </div>
                                                   <div class="form-group">
                                                      <label class="col-sm-6 control-label text-align-left">Conv. Allow:</label>
                                                      <div class="col-sm-6">
                                                         <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.conv_allowance">
                                                      </div>
                                                   </div>
                                                   <div class="form-group">
                                                      <label class="col-sm-6 control-label text-align-left">Edu. Allow:</label>
                                                      <div class="col-sm-6">
                                                         <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.edu_allowance">
                                                      </div>
                                                   </div>
                                                   <div class="form-group">
                                                      <label class="col-sm-6 control-label text-align-left">Spcl Allow:</label>
                                                      <div class="col-sm-6">
                                                         <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.special_allowance">
                                                      </div>
                                                   </div>
                                                   <div class="form-group">
                                                      <label class="col-sm-6 control-label text-align-left">Up-Keep Allow:</label>
                                                      <div class="col-sm-6">
                                                         <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.upkeep_allowance">
                                                      </div>
                                                   </div>
                                                   <div class="form-group">
                                                      <label class="col-sm-6 control-label text-align-left">Others/Roff:</label>
                                                      <div class="col-sm-6">
                                                         <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.round_off">
                                                      </div>
                                                   </div>
                                                </div>
                                                <!--Deduction-->
                                                <div class="col-sm-6">
                                                   <div class="form-group">
                                                      <label class="col-sm-6 control-label text-align-left">PF Contr:</label>
                                                      <div class="col-sm-6">
                                                         <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.pf">
                                                      </div>
                                                   </div>
                                                   <div class="form-group">
                                                      <label class="col-sm-6 control-label text-align-left">ESI Contr:</label>
                                                      <div class="col-sm-6">
                                                         <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.esi">
                                                      </div>
                                                   </div>
                                                   <div class="form-group">
                                                      <label class="col-sm-6 control-label text-align-left">Prof. Tax:</label>
                                                      <div class="col-sm-6">
                                                         <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.prof_tax">
                                                      </div>
                                                   </div>
                                                   <div class="form-group">
                                                      <label class="col-sm-6 control-label text-align-left">Advance:</label>
                                                      <div class="col-sm-6">
                                                         <input type="text" class="form-control" ng-model="empSalDetail.emp_sal_adv_tds.advance">
                                                      </div>
                                                   </div>
                                                   <div class="form-group">
                                                      <label class="col-sm-6 control-label text-align-left">TDS:</label>
                                                      <div class="col-sm-6">
                                                         <input type="text" class="form-control" ng-model="empSalDetail.emp_sal_adv_tds.tds">
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!--total Earning and deduction-->
                                             <div class="form-group">
                                                <label class="col-sm-3 control-label text-align-left">Gross Earnings:</label>
                                                <div class="col-sm-3">
                                                   <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.gross_monthly_sal">
                                                </div>
                                                <label class="col-sm-3 control-label text-align-left">Total Deduction:</label>
                                                <div class="col-sm-3">
                                                   <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.tot_deduction">
                                                </div>
                                             </div>
                                             <!--extra-->
                                             <div class="form-group">
                                                <label class="col-sm-3 control-label text-align-left">Tele/Inc/others rembursement:</label>
                                                <div class="col-sm-3">
                                                   <input type="text" class="form-control" ng-model="empSalDetail.emp_sal_reimbursement.reimbursement">
                                                </div>
                                                <label class="col-sm-3 control-label text-align-left">Salary In Hand:</label>
                                                <div class="col-sm-3">
                                                   <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.sal_hand">
                                                </div>
                                             </div>
                                             <div class="clearfix"></div>
                                             <div class="form-group">
                                                <label class="col-sm-3 control-label text-align-left">Co's Cont. To PF 3.67%:</label>
                                                <div class="col-sm-3">
                                                   <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.tot_company_contri_pf">
                                                </div>
                                                <label class="col-sm-3 control-label text-align-left">Pension Fund 8.33%:</label>
                                                <div class="col-sm-3">
                                                   <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.pension_fund">
                                                </div>
                                             </div>
                                             <div class="form-group">
                                                <label class="col-sm-3 control-label text-align-left">EDLI 0.5%:</label>
                                                <div class="col-sm-3">
                                                   <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.edli">
                                                </div>
                                                <label class="col-sm-3 control-label text-align-left">ADM CH 0.65%:</label>
                                                <div class="col-sm-3">
                                                   <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.adm_charge">
                                                </div>
                                             </div>
                                             <div class="form-group">
                                                <label class="col-sm-3 control-label text-align-left">ADM TO EDLI 0.01%:</label>
                                                <div class="col-sm-3">
                                                   <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.adm_charge_eli">
                                                </div>
                                                <label class="col-sm-3 control-label text-align-left">Total CO's Cont to EPF 13.61%:</label>
                                                <div class="col-sm-3">
                                                   <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.tot_company_contri_pf">
                                                </div>
                                             </div>
                                             <div class="form-group">
                                                <label class="col-sm-3 control-label text-align-left">CO'S CONT. TO ESI:</label>
                                                <div class="col-sm-3">
                                                   <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.company_contri_esi">
                                                </div>
                                             </div>
                                             <div class="clearfix"></div>
                                             <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-6 control-label text-align-left">CTC:</label>
                                                <div class="col-sm-3">
                                                   <input disabled type="text" class="form-control" ng-model="empSalDetail.sal_month_info.ctc_per_month">
                                                </div>
                                             </div>
                                             <div class="clearfix"></div>
                                             <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-10">
                                                   <button type="button" ng-click="vm.saveSalary(empSalDetail)" class="btn btn-primary  pull-right">
                                                   <i class="fa fa-save"></i> Save
                                                   </button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--attendance Download modal-->
            <div class="modal fade" id="salaryDownload" role="dialog">
               <div class="modal-dialog modal-lg">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Attendance Download</h4>
                     </div>
                     <div class="modal-body" style="height: 100px">
                        <div class="col-sm-12">
                           <div class="form-group col-sm-4">
                              <label>Date:</label>
                              <div class="input-group"
                                 moment-picker="vm.downloadParameter.currentDate" 
                                 start-view="day" 
                                 format="MMMM-YYYY">
                                 <span class="input-group-addon">
                                 <i class="fa fa-calendar fa-fw"></i>
                                 </span>
                                 <input class="form-control" ng-model="vm.downloadParameter.currentDate" ng-model-options="{ updateOn: 'blur' }">
                              </div>
                           </div>
                           <div class="form-group col-sm-3">
                              <label>process:</label>
                              <select class="form-control" ng-model="vm.downloadParameter.process_id">
                                <option value="">--Select Process--</option>
                                <option ng-repeat="process in vm.processMaster" value={{process.id}}>{{process.process_name}}</option>
                             </select>
                           </div>
                           <div class="form-group col-sm-3">
                              <label>Location:</label>
                              <select class="form-control" ng-model="vm.downloadParameter.location_id">
                                <option value="">--Select Location--</option>
                                <option ng-repeat="location in vm.locationMaster" value={{location.id}}>{{location.location_name}}</option>
                             </select>
                              <!--<input type="text" class="form-control" ng-model="vm.downloadParameter.location">-->
                           </div>
                           <div class="form-group col-sm-2">
                              <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                              <button type="button" class="btn btn-primary pull-right" ng-click="vm.downloadSalaryExcel()"><i class="fa fa-download"></i> &nbsp; Download</button>
                           </div>
                        </div>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- End Content Area -->
      </div>
      <script src="<?php echo base_url(); ?>assets/vendor/js/jquery.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/metisMenu.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-ui-router.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/d3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/nv.d3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-nvd3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/moment.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/footable.all.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-footable.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/sb-admin-2.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/moment-with-locales.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-moment-picker.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/loading-bars.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/bootbox.min.js"></script>

      <script src="<?php echo base_url(); ?>assets/salaryInitiat/salaryInitiatcontroller.js"></script>
      <script src="<?php echo base_url(); ?>assets/salaryInitiat/salaryInitiatService.js"></script>
      
      <script src="<?php echo base_url(); ?>assets/common/service.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/filter.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/sidebarDirective.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/headerController.js"></script>
      <script>
         function toggleIcon(e) {
             $(e.target)
                     .prev('.panel-heading')
                     .find(".collapse-icon")
                     .toggleClass('fa-angle-down fa-angle-up');
             }
             $('.panel-group').on('hidden.bs.collapse', toggleIcon);
             $('.panel-group').on('shown.bs.collapse', toggleIcon);
      </script>
   </body>
</html>