<!DOCTYPE html>
<html lang="en" ng-app="employeeApp">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Employee Tracker</title>
      <!-- CSS -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/bootstrap.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/metisMenu.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/main.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/sb-admin-2.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/angular-moment-picker.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/font-awesome.min.css" />-->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/nv.d3.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/animate.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/loading-bars.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/footable.core.min.css" />
   </head>
   <body>
      <!-- Simple splash screen-->
      <div class="splash">
         <div class="color-line"></div>
         <div class="splash-title">
            <img src="<?php echo base_url(); ?>assets/vendor/images/loading-bars.svg" width="64" height="64" />
         </div>
      </div>
      <div id="wrapper">
         <!-- Start Navigation Area -->
         <?php
            $this->load->view('share/leftHeader');
         ?>
         <!-- End Navigation Area -->
         <!-- Content Area -->
         <!-- ng-class with current state name give you the ability to extended customization your view -->
         <div id="page-wrapper" class="gray-bg {{$state.current.name}}" ng-controller="attendanceController as vm" ng-init="vm.init(1)">
            <!-- Nested view  -->
            <div class="tempLate animated slideInRight">
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                     <h2>Attendance</h2>
                     <ol class="breadcrumb">
                        <li><a ui-sref="home.dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active"><i class="fa fa-table"></i> {{vm.title}}</li>
                     </ol>
                  </div>
               </div>
               <!-- /.row -->
               <div class="row" style="margin-bottom: 10px;">
                  <div class="col-sm-6 col-sm-offset-6 grid-padding-0">
                     <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#attendanceUpload" style="margin-left: 15px;"><i class="fa fa-upload" aria-hidden="true"></i>&nbsp;Attendance  Upload</button>
                     <!-- <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#attendanceDownload"><i class="fa fa-download"></i> Attendance Download</button> -->
                  </div>                 
               </div>
               <div class="row" style="margin-bottom: 10px;">
                  <div class="row">
                     <div class="col-sm-3 col-xs-10" style="padding-right: 0px">
                        <div class="input-group"
                                moment-picker="vm.filter.yearMonth" 
                                start-view="day" 
                                format="MMMM-YYYY">
                            <span class="input-group-addon">
                            <i class="fa fa-calendar fa-fw"></i>
                            </span>
                            <input class="form-control" ng-model="vm.filter.yearMonth" ng-model-options="{ updateOn: 'blur' }">
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <input type="text" ng-model="vm.filter.empId" class="form-control" placeholder="employee Id">
                     </div>
                     <div class="col-sm-2">
                        <select class="form-control" ng-model="vm.filter.process">
                            <option value="">--Select Process--</option>
                            <option ng-repeat="process in vm.processMaster" value={{process.id}}>{{process.process_name}}</option>
                        </select>
                     </div>
                     <div class="col-sm-2">
                        <select class="form-control" ng-model="vm.filter.location">
                            <option value="">--Select Location--</option>
                            <option ng-repeat="location in vm.locationMaster" value={{location.id}}>{{location.location_name}}</option>
                        </select>
                        <!--<input type="text" ng-model="vm.filter.location" class="form-control" placeholder="Location">-->
                     </div>
                     <div class="col-sm-2">
                        <button class="btn pull-left" ng-click="vm.applyFilter()"><i class="fa fa-search"></i> Search</button>
                     </div>
                  </div>
                  <div class="row" style="padding-top: 15px;">
                     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row row-margin panel-heading bgOrg">
                            <div class="col-sm-3 col-xs-12">Emp. Code</div>
                           <div class="col-sm-3 col-xs-12">Name</div>
                           <div class="col-sm-3 col-xs-12">Designation</div>
                           <!--<div class="col-sm-2 col-xs-12">Status</div>
                           <div class="col-sm-3 col-xs-12">DOJ</div>-->
                        </div>
                        <div class="panel-group" id="accordion">
                           <div class="panel panel-default" ng-repeat="(employeeIndex, attendance) in vm.employees">
                              <div class="panel-heading">
                                 <h4 class="panel-title" style="height: 45px;">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#_{{$index}}">
                                       <i class="pull-right collapse-icon fa fa-angle-down"></i>
                                       <div class="col-sm-3 col-xs-12" data-ng-bind="attendance.emp_code"></div>
                                       <div class="col-sm-3 col-xs-12" data-ng-bind="attendance.emp_name"></div>
                                       <div class="col-sm-3 col-xs-12" data-ng-bind="attendance.emp_location"></div>
                                       <!--<div class="col-sm-2 col-xs-12" data-ng-bind="attendance.empStatus"></div>
                                       <div class="col-sm-3 col-xs-12" data-ng-bind="attendance.empDoj | date: 'dd/MM/yyyy'"></div>-->
                                    </a>
                                 </h4>
                              </div>
                              <div id="_{{$index}}" class="panel-collapse collapse">
                                 <div class="panel-body">
                                    <!--<div class="col-sm-11 grid-padding-0">
                                       <div class="col-sm-12 grid-padding-0">
                                          <div class="col-sm-2 grid-padding-0">
                                             <label>Present: </label> {{attendance.numPresentDay}}
                                          </div>
                                          <div class="col-sm-2">
                                             <label>Absent: </label> {{attendance.numAbsentDay}}
                                          </div>
                                          <div class="col-sm-2 grid-padding-0">
                                             <label>Holiday: </label> {{attendance.numHoliday}}
                                          </div>
                                          <div class="col-sm-2">
                                             <label>Leave: </label> {{attendance.numLeave}}
                                          </div>
                                          <div class="col-sm-2 grid-padding-0">
                                             <label>Late: </label> {{attendance.numLate}}
                                          </div>
                                          <div class="col-sm-2">
                                             <label>Week Off: </label> {{attendance.numWeeklyOff}}
                                          </div>
                                       </div>
                                       <div class="col-sm-12 grid-padding-0">
                                          <div class="col-sm-2 grid-padding-0">
                                             <label>Sunday </label> {{attendance.numSunday}}
                                          </div>
                                          <div class="col-sm-2">
                                             <label>Auto Deduc.: </label> {{attendance.numAutoDeduct | number: 1}}
                                          </div>
                                          <div class="col-sm-2 grid-padding-0">
                                             <label>Excep. Grant: </label> {{attendance.numExcpeGrant}}
                                          </div>
                                          <div class="col-sm-2">
                                             <label>Final Deduc.: </label> {{attendance.numFinalDeduct | number: 1}}
                                          </div>
                                          <div class="col-sm-2 grid-padding-0">
                                             <label>Total: </label> {{attendance.numTotalDayInMonth}}
                                          </div>
                                          <div class="col-sm-2">
                                             <label>Final Attend.: </label> {{attendance.numFinalAttendance | number: 1}}
                                          </div>
                                       </div>
                                    </div>-->
                                    <div class="col-sm-11 grid-padding-0">
                                        <div class="col-sm-2">
                                            Exception Grant:
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="number" ng-model="attendance.emp_excep_grant" class="form-control" placeholder="Exception Grant">
                                        </div>
                                        <div class="col-sm-2">
                                            Remark:
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" ng-model="attendance.emp_excep_grant_reason" class="form-control" placeholder="Remark">
                                        </div>
                                    </div>
                                    <div class="col-sm-1 grid-padding-0" style="padding-bottom: 5px;">
                                       <!--<img class="pull-right" ng-src="../content/images/saveRefresh-color.png" height="40" width="40">-->
                                       <button type="button" class="btn btn-primary  pull-right" ng-click="vm.saveAttendanceForEmnployee(attendance)" style="font-size: 12px;"> Save <br> Refresh
                                       </button>
                                    </div>
                                    <div class="col-sm-12 grid-padding-0" style="margin-bottom: 20px;">
                                       <div class="table-responsive">
                                          <table class="table table-striped table-bordered table-hover" style="float: left">
                                             <thead>
                                                <tr>
                                                   <th scope="col" ng-repeat="date in vm.dates" style="background-color:#bedee5; text-align: center"> {{date.shortDay}} </th>
                                                </tr>
                                                <tr>
                                                   <th scope="col" ng-repeat="date in vm.dates" style="background-color: #5c5e5e; color: white;text-align: center"> {{date.shortDate}} </th>
                                                </tr>
                                             </thead>
                                             <tbody>
                                                <tr>
                                                   <td ng-repeat="date in attendance.attendance">
                                                      <select class="custom-selectbox" ng-model="date.emp_attendance_code_id" ng-change="vm.calculateStatus(employeeIndex, date.emp_attendance_code_id)">
                                                         <option value="0">--Select--</option>
                                                         <option ng-repeat="status in vm.attendanceStatusMaster" value={{status.id}}>{{status.att_desc}}</option>
                                                      </select>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--attendance upload modal-->
            <div class="modal fade" id="attendanceUpload" role="dialog">
               <div class="modal-dialog modal-lg">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Attendance Upload</h4>
                     </div>
                     <div class="modal-body">
                         <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-sm-2 control-label text-align-left">Date:</label>
                                <div class="col-sm-4">
                                    <div class="input-group"
                                            moment-picker="vm.uploadParameter.currentDate" 
                                            start-view="day" 
                                            format="MMMM-YYYY">
                                        <span class="input-group-addon">
                                        <i class="fa fa-calendar fa-fw"></i>
                                        </span>
                                        <input class="form-control" 
                                                ng-model="vm.uploadParameter.currentDate" 
                                                ng-model-options="{ updateOn: 'blur' }">
                                    </div>
                                </div>
                                <label class="col-sm-2 control-label text-align-left">Location:</label>
                                <div class="col-sm-4">
                                    <select class="form-control" ng-model="vm.uploadParameter.location_id">
                                        <option value="">--Select Location--</option>
                                        <option ng-repeat="location in vm.locationMaster" value={{location.id}}>{{location.location_name}}</option>
                                    </select>
                                </div>                                
                            </div>

                            <div class="form-group">                                
                                <label class="col-sm-2 control-label text-align-left">Process:</label>
                                <div class="col-sm-4">
                                    <select class="form-control" ng-model="vm.uploadParameter.process_id">
                                        <option value="">--Select Process--</option>
                                        <option ng-repeat="process in vm.processMaster" value={{process.id}}>{{process.process_name}}</option>
                                    </select>                                    
                                </div>  
                                <label class="col-sm-2 control-label text-align-left">Attachment:</label>
                                <div class="col-sm-4">
                                    <div class="col-sm-4 grid-padding-0">
                                        <label class="btn-bs-file btn btn-primary">
                                        Browse
                                        <input type="file" id="uploadFileInput" fileinput="vm.uploadParameter.userfile"/>
                                        </label>
                                        </div>
                                        <div class="col-sm-8 grid-padding-0">
                                        <input class="form-control" disabled ng-model="vm.uploadParameter.userfile.name">
                                    </div>
                                </div>                              
                            </div>
                         </form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary pull-right" data-dismiss="modal" ng-click="vm.uploadAttendanceExcel()">Upload</button>
                     </div>
                  </div>
               </div>
            </div>


            <!--attendance Download modal-->
            <div class="modal fade" id="attendanceDownload" role="dialog">
               <div class="modal-dialog modal-lg">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Attendance Download</h4>
                     </div>
                     <div class="modal-body" style="height: 100px">
                         <div class="col-sm-12">
                         <div class="form-group col-sm-4">
                            <label>Date:</label>
                            <div class="input-group"
                                    moment-picker="vm.downloadParameter.currentDate" 
                                    start-view="day" 
                                    format="DD-MM-YYYY">
                                <span class="input-group-addon">
                                <i class="fa fa-calendar fa-fw"></i>
                                </span>
                                <input class="form-control" ng-model="vm.downloadParameter.currentDate" ng-model-options="{ updateOn: 'blur' }">
                            </div>
                         </div>
                         <div class="form-group col-sm-3">
                            <label>process:</label>
                            <select class="form-control" ng-model="vm.downloadParameter.process">
                                <option value="">--Select Process--</option>
                                <option ng-repeat="process in vm.processMaster" value={{process.id}}>{{process.process_name}}</option>
                            </select>
                            <!--<input type="text" class="form-control" ng-model="vm.downloadParameter.process">-->
                         </div>
                         <div class="form-group col-sm-3">
                            <label>Location:</label>
                            <select class="form-control" ng-model="vm.downloadParameter.location">
                                <option value="">--Select Location--</option>
                                <option ng-repeat="location in vm.locationMaster" value={{location.id}}>{{location.location_name}}</option>
                            </select>
                            <!--<input type="text" class="form-control" ng-model="vm.downloadParameter.location">-->
                         </div>
                         <div class="form-group col-sm-2">
                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <button type="button" class="btn btn-primary pull-right" ng-click="vm.downloadAttendanceExcel()"><i class="fa fa-download"></i> &nbsp; Download</button>
                         </div>
                     </div>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- End Content Area -->
      </div>
      <script src="<?php echo base_url(); ?>assets/vendor/js/jquery.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/metisMenu.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-ui-router.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/d3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/nv.d3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-nvd3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/moment.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/footable.all.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-footable.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/sb-admin-2.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/moment-with-locales.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-moment-picker.min.js"></script>      
      <script src="<?php echo base_url(); ?>assets/vendor/js/loading-bars.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/lodash.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/bootbox.min.js"></script>

      <script src="<?php echo base_url(); ?>assets/attendance/attendance.controller.js"></script>
      <script src="<?php echo base_url(); ?>assets/attendance/attendance.service.js"></script>
      <script src="<?php echo base_url(); ?>assets/attendance/directive.js"></script>

      <script src="<?php echo base_url(); ?>assets/common/service.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/sidebarDirective.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/headerController.js"></script>
      <script>
         function toggleIcon(e) {
             $(e.target)
                     .prev('.panel-heading')
                     .find(".collapse-icon")
                     .toggleClass('fa-angle-down fa-angle-up');
             }
             $('.panel-group').on('hidden.bs.collapse', toggleIcon);
             $('.panel-group').on('shown.bs.collapse', toggleIcon);
      </script>
   </body>
</html>