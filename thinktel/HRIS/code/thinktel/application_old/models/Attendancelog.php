<?PHP

class Attendancelog extends CI_Model {

    function __construct () {
        parent::__construct ();
    }

      function create_log($data)
	  {
	  	if($this->db->replace('emp_attendance_log', $data))
	  	{

	  		//var_dump( $this->db->error());
	  	}
	  	else
	  	{
	  		//var_dump( $this->db->error());
	  	}


	  	return 1;

	  }

	   function update_log($emp_code,$month,$year,$modified_by,$data)
	   {

			foreach ($data as $datarow)
			{




   					$this->db->update('emp_attendance_log', array('emp_attendance_code_id'=>$datarow['emp_attendance_code_id']),array('emp_code' => $emp_code, 'year' => $year,'day'=>$datarow['day']));




			}




	   }





	   function insert_att_log_summary($emp_code,$month,$year,$created_by,$modified_by,$data,$empexptgrant,$empexptgrantreason)
 	{
			$this->load->model("Atttendancemonthlysummary");

			$this->db->trans_start();

			//Absent code=2
			$emp_attendance_code_id=2;

			$data_attedance= array(
				'emp_code' =>$emp_code ,
				'month'=> $month,
				'year'=> $year,
				'is_active'=>1,
				'created_by' =>$created_by,
				'modified_by'=>$modified_by,
			    'emp_excep_grant'=>$empexptgrant,
			    'emp_excep_grant_reason'=>$empexptgrantreason
			);

			$this->Atttendancemonthlysummary->create_monthly_summary($data_attedance);

			//insert into attendance_log table
			//how many days in month ?
			$d=cal_days_in_month(CAL_GREGORIAN,$month,$year);
		    //now for each day insert a record in emp_attendance_log table for this employee

		    for ($i=1;$i<=$d;$i++)
		    {



		   		 $data_attedance_log= array(
							'emp_code' =>$emp_code ,
							'month'=> $month,
							'year'=> $year,
							'day'=> $i,
							'emp_attendance_code_id'=>$emp_attendance_code_id,
							'is_active'=>1,
							'created_by' =>$created_by,
							'modified_by'=>$modified_by
				);

				$this->create_log($data_attedance_log);

			}
			//update the attendance data as passed from the front end

			$this->update_log($emp_code,$month,$year,$modified_by,$data);


			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{
				return 1;
			}
			else
			{
				return -1;
			}



 	}

 		function get_emp_att_log($emp_code,$day,$month,$year,$process_id)
		{

			//$sql="select * from emp_attendance_log where emp_code='".$emp_code."' and "." year=".$year." and month='".$month."'";
			$sql="select * from emp_attendance_log  where  year= ".$year;

			$sql=$sql." order by emp_code asc";

			$query=$this->db->query($sql);

			$data= $query->result_array();



			return $data;


	 	 }
	 	 function get_emp_att_log_by_empcode($emp_code,$day,$month,$year)
		 {


		 			$sql="select * from emp_attendance_log  where  month= ".$month . " and year=  ".$year ;
		 			if(!($day==0))
					{
						$sql = $sql ." and day=".$day."";


					}

					if(!($emp_code==""))
					{
							$sql = $sql ." and emp_code='".$emp_code."'";

					}

					//var_dump($sql);
					//exit;
		 			$query=$this->db->query($sql);

		 			$data= $query->result_array();


		 			return $data;


	 	 }

	 	





		 //this will create a hash with emp_code vs number of records
	 	 function get_emp_att_log_hash($emp_code,$day,$month,$year,$process_id)
	 	 {


			$sql="SELECT  emp_code , count(*) as countval FROM  emp_attendance_log where year= ". $year;
			if($emp_code!="")
			{
				$sql = $sql ." and emp_code='".$emp_code."'";

			}

			if(!($day==0))
			{
				$sql = $sql ." and day=".$day."";


			}

			if(!($month==0))
			{
				$sql = $sql ." and month=".$month."";

			}
			//return $sql;
			/*if($process_id=!0)
			{
				$sql = $sql ." and process_id=".$process_id."";

			}*/

			$sql =$sql . " group by emp_code  ";



			//var_dump($sql);
			//exit;

			$query=$this->db->query($sql);

			$data= $query->result_array();

			return $data;





	 	 }

	 	 function get_emp_code_by_proc_id($process_id)
	 	 {

	 	   $sql="select distinct emp_code from employee_master where process_id=".$process_id;

	 	   $query=$this->db->query($sql);

	 	   return $query->result_array();


	 	 }

	 	 function get_emp_code_all()
		 {

		 	 	   $sql="select distinct emp_code from employee_master";

		 	 	   $query=$this->db->query($sql);

		 	 	   return $query->result_array();


	 	 }

	 	 function get_emp_code_by_param($emp_code,$process_id,$loc_id)
	 	 {
                    
	 	        // echo "emp_code".$emp_code;
				 
			     if(($process_id==0)&& ($loc_id==0)&&($emp_code==""))
			     {
			         
			         $sql="select distinct emp_code from employee_master"; 
			         
			     }
			     else if ($emp_code!=""){
			         
			         $sql="select distinct emp_code from employee_master where emp_code='".$emp_code."'";
			         
			     }
			     else if(($emp_code=="") && ($process_id!=0))
			     {
			         $sql="select distinct emp_code from employee_master where process_id=".$process_id;
			         
			         
			     }
			     else if(($emp_code=="") && ($loc_id!=0))
			     {
			         $sql="select distinct emp_code from employee_master where loc_id=".$loc_id;
			         
			         
			     }
			    
			     
				 $query=$this->db->query($sql);
				 
				 return $query->result_array();


	 	 }




}
