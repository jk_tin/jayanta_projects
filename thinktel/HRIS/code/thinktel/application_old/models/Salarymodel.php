<?PHP

class Salarymodel extends CI_Model {

    function __construct () {
        parent::__construct ();
    }


	   function get_sal_adv_tds_data($emp_id,$month,$year)
	   {


	    	$sql=" select * from emp_sal_adv_tds where emp_master_id='".$emp_id."'";
	    	$sql=$sql." and month=".$month . " and year=".$year;

	    	//return $sql;
	    	$query = $this->db->query($sql);
	    	$row = $query->row_array();

	    	return $row;

	    }
	    function merge_adv_tds_data($data)
	    {
	        
	        
	        return $this->db->replace('emp_sal_adv_tds', $data);
	        
	     
	        
	    }
	    function merge_emp_sal_reimbursement($data)
	    {
	        
	        
	        return $this->db->replace('emp_sal_reimbursement', $data);
	        
	        
	        
	    }
	    function get_reimbursement_data($emp_id,$month,$year)
	    {
			$sql=" select * from emp_sal_reimbursement where emp_master_id='".$emp_id."'";
				    	$sql=$sql." and month=".$month . " and year=".$year;

			 //echo $sql;
			//return $sql;
			$query = $this->db->query($sql);
			$row = $query->row_array();
		    //var_dump($row->is_active);
	    	return $row;

	    }

	    function insert_emp_sal_month($data)
	    {
		 $this->db->replace('emp_sal_month', $data);
		// var_dump( $this->db->error());
	    }

	    
	    function get_sal_download_data($process_id,$loc_id,$month,$year)
	    {
	        
	        $sql="SELECT m.emp_code,concat(COALESCE(emp_first,''),\" \",COALESCE(emp_mid,''),\" \",COALESCE(emp_last,'')) as emp_name, s.gross_earning ,b.emp_bank_name ,b.emp_bank_ifsc_code,b.emp_bank_ac_number
                 from	 emp_sal_month s, employee_master m ,process_master p,location_master l ,employee_bank_info b
                 where  s.emp_master_id=m.id 
                 and m.process_id=p.id 
                 and m.loc_id=l.id 
                 and b.emp_master_id=m.id and p.id=".$process_id." and l.id= ".$loc_id;
	        $sql=$sql." and month=".$month." and year=".$year;
	        $query = $this->db->query($sql);
	      //  return $sql;
	        $row = $query->result_array();
	        return $row;    
                  
	    }
	    
	    function get_sal_month_data($emp_master_id,$month,$year)
	    {
	        
	        $sql="SELECT  * from emp_sal_month where ";
	        $sql=$sql." month=".$month." and year=".$year;
	        $query = $this->db->query($sql);
	        //return $sql;
	        $row = $query->row_array();
	        return $row;
	        
	    }


  }

