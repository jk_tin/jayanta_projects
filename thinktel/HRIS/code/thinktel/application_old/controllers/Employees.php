<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employees extends CI_Controller {
	public function index()
	{
	    $this->load->library(array('ion_auth', 'form_validation'));
	    if (!$this->ion_auth->logged_in())
	    {
	        // redirect them to the login page
	        redirect('mainctrl');
	    }
		$this->load->view('employee/employee');
	}
	
	public function addemployee()
	{
	    
	    
	    $this->load->library(array('ion_auth', 'form_validation'));
	    if (!$this->ion_auth->logged_in())
	    {
	        // redirect them to the login page
	        redirect('mainctrl');
	    }
	    $data=array();
	    $data['emp_master_id']="";
	    $group=array("srhr","admin");
	    $level="2"; //this determines if the entire list to be shown or only selected ones depending on user access righgts
	    
	    if ($this->ion_auth->in_group($group))
	    {
	        $level="1";
	        
	    }
	    $data['emp_level']=$level;
	    $this->load->view("addEmployee/addEmployee",$data);
	    
	}
	public function editemployee()
	{
	    //model get data
	    $this->load->library(array('ion_auth', 'form_validation'));
	    if (!$this->ion_auth->logged_in())
	    {
	        // redirect them to the login page
	        redirect('mainctrl');
	    }
	    $empid=$this->input->post("empid");
	    $data=array(); 
	    $data['emp_master_id']=$empid;
	    $group=array("srhr","admin");
	    $level=2; //this determines if the entire list to be shown or only selected ones depending on user access righgts
	    
	    if ($this->ion_auth->in_group($group))
	    {
	        $level=1;
	        
	    }
	    $data['emp_level']=$level;
 
	    $this->load->view("addEmployee/addEmployee",$data);	    
	}

	public function employeeList()
	{
	    $this->load->library(array('ion_auth', 'form_validation'));
	    if (!$this->ion_auth->logged_in())
	    {
	        // redirect them to the login page
	        redirect('mainctrl');
	    }
	    //model get data
	    $data=array(); 
 
	    $this->load->view("employee/employee");	    
 
 
	}
	public function downloadzip(){
	    $this->load->library('zip');
	    $this->load->helper('file');
	    $path="d:/mailtest/";
	    
	    $this->zip->add_data('d:/1.jpg', 'd:/2.jpg');
	    $this->zip->download('my_spreadsheet.zip');
	    //$path = './uploads/';
	  /*  $files = get_filenames($path);
	    foreach($files as $f){
	       
	       $r=$this->zip->read_file($path.$f, true);
	       var_dump("here".$r);
	    }
	   header("Content-type: application/zip;\n");
	   header("Content-Transfer-Encoding: Binary");
	  $this->zip->download('Download_all_files');*/
	}
	
	
	
	
}
