<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries\REST_Controller.php');
use Restserver\Libraries\REST_Controller;

class Employee extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

		function __construct()
		{
			parent::__construct();

			$this->load->library('form_validation');
			$this->load->library(array('ion_auth', 'form_validation'));
			$this->load->helper(array('url', 'language'));



		}
		public function empsalcalc_get()
		{
		    $this->load->model("Employeemodel");
		    $emp_code=$this->get("emp_code");
		    $gross_sal=$this->get("gross_sal");
		    $pf_opted=$this->get("pfopt");
		    $retdata=$this->Employeemodel->get_empmasterid_from_code($emp_code);
		    $emp_id=$retdata['id'];
		    $datarow=array();
		    //get PF details
		    $saldedctdata=$this->Employeemodel->get_employee_salmaster_deduct_info($emp_id);
		   // var_dump($saldedctdata['emp_pf_opted']);
		    $datarow["emp_code"]=$emp_code;
		    
		    if(empty($pf_opted))
		    {
		       $pf_opted= $saldedctdata['emp_pf_opted'];
		    }
		    $dataval=$this->Employeemodel->get_employee_details($emp_id);
		    //earning
		    $monthly_basic=round(($gross_sal * 35)/100);
		    $hra=round(($monthly_basic * 30	)/100);
		    $conv_allowance=round(($gross_sal * 18)/100);
		    $edu_allowance=round(($gross_sal * 15)/100);
		    $special_allowance=round(($gross_sal * 9.5)/100);
		    $upkeep_allowance=round(($gross_sal * 12)/100);
		    $round_off=$gross_sal-($monthly_basic+$hra+$conv_allowance+$edu_allowance+$special_allowance+$upkeep_allowance);
		    $gross_earning=($monthly_basic+$hra+$conv_allowance+$edu_allowance+$special_allowance+$upkeep_allowance)+$round_off;
		    
		    
		    $salmasterearnval=$this->Employeemodel->get_employee_salmaster_earn_info($emp_id);
		    
		    $employee_sal_earn_info=array(
		        "id" =>$salmasterearnval['id'],//change
		        "emp_master_id" =>$emp_id,
		        "emp_gross_sal" =>$gross_sal,
		        "emp_basic_sal" =>$monthly_basic,
		        "emp_hra" =>$hra,
		        "emp_spcl_allowance" =>$special_allowance,
		        "emp_conv_allowance" =>$conv_allowance,
		        "emp_edu_allowance" =>$edu_allowance,
		        "emp_upkeep_allowance" =>$upkeep_allowance,
		        "emp_round_off" =>$round_off,
		        "emp_gross_earn" =>$gross_earning,
		        "emp_other_reimb" =>"0.0"
		    );
		    
		    $datarow["employee_sal_master_earning"]= $employee_sal_earn_info;
		    
		    //deductioin
		    if($pf_opted=="N")
		    {
		        
		        $pf=0.0;
		    }
		    //$pf=0.0;
		    if($pf_opted=="Y")
		    {
		      $pf=round(($monthly_basic * 12	)/100);
		    }
		    if($gross_earning > 21000)
		    {
		        $esi=0;
		        
		    }
		    else
		    {
		        $esi=round(($gross_earning * 12	)/100);
		    }
		    
		    //professional tax starts
		    $prof_tax= 0.0;
		    if($gross_earning<=10000.00)
		    {
		        $prof_tax=0.0;
		    }
		    elseif($gross_earning<=25000)
		    {
		        $prof_tax=130.00;
		        
		    }
		    elseif($gross_earning<=40000)
		    {
		        $prof_tax=150.00;
		    }
		    else
		    {
		        $prof_tax=200.00;
		        
		    }
		    //professional tax ends
		    
		    $salmasterdeduction=$this->Employeemodel->get_employee_salmaster_deduct_info($emp_id);
		    
		    $employee_sal_deduct_info=array(
		        "id" =>$salmasterdeduction['id'],
		        "emp_master_id" =>$emp_id,
		        "emp_pf_opted" =>$pf_opted,
		        "emp_esic_contri" =>$esi,
		        "emp_pf_contri" =>$pf,
		        "emp_wb_prof_tax" =>$prof_tax
		        
		    );
		    
		   
		    
		    if($pf==0.0)
		    {
		        $pension_fund=0.0;
		    }
		    elseif($monthly_basic>15000)
		    {
		        $pension_fund=(15000* 8.33)/100;
		        
		    }
		    else
		    {
		        $pension_fund=round(($monthly_basic* 8.33)/100);
		        
		    }
		    
		    $pf_comp_contri=$pf-$pension_fund;
		    
		    
		    //EDLI
		    
		    if($pf==0.0)
		    {
		        $edli=0.0;
		    }
		    elseif($monthly_basic>15000)
		    {
		        $edli=(15000.0*0.5)/100;
		        
		        
		        
		    }
		    else
		    {
		        $edli=round(($monthly_basic*0.5)/100);
		        
		        
		    }
		    //adm charge
		    
		    
		    
		    if($pf==0.0)
		    {
		        $adm_charge=0.0;
		    }
		    elseif($monthly_basic>15000)
		    {
		        $adm_charge=(15000.0*0.65)/100;
		        
		    }
		    else
		    {
		        $adm_charge=round(($monthly_basic*0.65)/100);
		    }
		    //adm_charge_eli
		    
		    if($pf==0)
		    {
		        $adm_charge_eli=0.0;
		    }
		    else
		    {
		        
		        
		        $rounded_val=round(($monthly_basic*0.01)/100);
		        
		        //echo "$rounded_val=" .$rounded_val;
		        $adm_charge_eli=max(array(1,$rounded_val));
		        
		        
		        
		    }
		    
		    $tot_company_contri_pf=$pf_comp_contri+$pension_fund+$edli+$adm_charge+$adm_charge_eli;
		    
		    //company's contribution to esi
		    
		    if($gross_earning>21000)
		    {
		        $company_contri_esi=0.0;
		        
		    }
		    else{
		        
		        $company_contri_esi=round(($gross_earning*4.75)/100);
		    }
		    
		    $ctc_per_month=$gross_earning+$tot_company_contri_pf+$company_contri_esi;
		    
		    $tds=0.0;
		    $advance=0.0;
		    $reimbursement=0.0;
		    $tot_deduction=$pf+$esi+$prof_tax+$advance+$tds;
		    $sal_hand=$gross_earning+$reimbursement-$tot_deduction;
		    $datarow["employee_sal_master_deduction"]= $employee_sal_deduct_info;
		    
		    $salmastercomputation=$this->Employeemodel->get_employee_salmaster_compute_info($emp_id);
		    
		   
		    $employee_sal_compute_info=array(
		        "id" =>$salmastercomputation['id'],
		        "emp_master_id" =>$emp_id,
		        "emp_sal_in_hand" =>$sal_hand,
		        "emp_tot_ctc" =>$ctc_per_month//needs to change
		    );
		    $datarow["employee_sal_master_computation"]= $employee_sal_compute_info;
		    
		    $salmastercompanycontri=$this->Employeemodel->get_employee_salmaster_company_contri_info($emp_id);
		    
		    //employee_sal_company_contri
		    $employee_sal_comnay_contri_info=array(
		        "id" =>$salmastercompanycontri['id'],
		        "emp_master_id" =>$emp_id,
		        "emp_pf_contri" =>$pf_comp_contri,
		        "emp_pension_contri" =>$pension_fund,
		        "emp_edli_contri" =>$edli,
		        "emp_adm_charge" =>$adm_charge,
		        "emp_adm_charge_edli" =>$adm_charge_eli,
		        "emp_tot_contri_edli"=>$tot_company_contri_pf,
		        "emp_esi_contri" =>$company_contri_esi
		    );
		    $datarow["employee_sal_company_contri"]= $employee_sal_comnay_contri_info;
		    
		       
		    
		  
		    $data['employee']=$datarow;	
		    $data['message']="Calulated Successfully";
		    $data['code']=200;
		    
		    $this->response($data);
		    
		
		}
		
		public function emplist_get()
		{
		    $this->load->model("Employeemodel");
		    $emp_code=$this->get("emp_code");
		    $process_id=$this->get("process_id");
		    $loc_id=$this->get("location_id");
		   
		    $group=array("srhr","admin");
		    $level=2; //this determines if the entire list to be shown or only selected ones depending on user access righgts
		    
		    if ($this->ion_auth->in_group($group))
		    {
		         $level=1;
		         
		    }
		 
		    
		    $data=$this->Employeemodel->get_employee_list($process_id,$loc_id,$emp_code,$level);
		   
		    $this->response($data);
		   


		}
		
		public function empdetails_get()
		{
		    $this->load->model("Employeemodel");
		    $emp_master_id=$this->get("emp_master_id");


		    $data=array();
		    $datarow=array();



		    $dataval=$this->Employeemodel->get_employee_details($emp_master_id);
		   // var_dump($dataval);
		   //exit;
		   if(!empty($dataval))
		    {

		       $datarow["created_by"]=$dataval['created_by'];
		       $datarow["modified_by"]=$dataval['modified_by'];
		       $datarow["emp_code"]=$dataval['emp_code'];
		       $datarow["emp_level"]=$dataval['emp_level'];
		       $emp_dob=$dataval['emp_dob']; 
		       $ymd = !empty($emp_dob) ? DateTime::createFromFormat('Y-m-d', $emp_dob)->format('d-m-Y')  : null;
		       
		       $personal_details=array(

		         "emp_title"=>$dataval['emp_title'],
		         "emp_desig_id"=>$dataval['emp_desig_id'],
		         "process_id"=>$dataval['process_id'],
		         "loc_id"=>$dataval['loc_id'],
		         "emp_fa_hus_name"=>$dataval['emp_fa_hus_name'],
		         "emp_marital_status"=>$dataval['emp_marital_status'],
		         "emp_blood_grp"=>$dataval['emp_blood_grp'],
		         "emp_sub_function_id"=>$dataval['emp_sub_function_id'],
		           "emp_dob"=>$ymd,
		         "emp_gender"=>$dataval['emp_gender'],
		         "emp_first"=>$dataval['emp_first'],
		         "emp_mid"=>$dataval['emp_mid'],
		         "emp_last"=>$dataval['emp_last'],
		         "emp_add1_id"=>$dataval['emp_add1_id'],
		         "emp_add_2_id"=>$dataval['emp_add_2_id'],
		         "emp_city_dist"=>$dataval['emp_city_dist'],
		         "emp_state"=>$dataval['emp_state'],
		         "emp_pin"=>$dataval['emp_pin'],
		         "emp_contact_num"=>$dataval['emp_contact_num'],
		         "emp_alt_contact_num"=>$dataval['emp_alt_contact_num'],
		         "is_active"=>$dataval['is_active'],
		         "emp_is_active"=>$dataval['emp_is_active']


		    );
		       
		    $datarow["personal_details"]= $personal_details;
		    $joininfoval=$this->Employeemodel->get_employee_join_info($emp_master_id);
		    $empjd=$joininfoval['emp_join_date'];
		    $empjdval = !empty($empjd) ? DateTime::createFromFormat('Y-m-d', $empjd)->format('d-m-Y')  : null;
		    
		    $empid=$joininfoval['emp_interview_date'];
		    $empidval = !empty($empid) ? DateTime::createFromFormat('Y-m-d', $empid)->format('d-m-Y')  : null;
		    
		    $emppd=$joininfoval['emp_probation_date'];
		    $emppdval = !empty($emppd) ? DateTime::createFromFormat('Y-m-d', $emppd)->format('d-m-Y')  : null;
		    
		    $join_info=array(

		        "id"  =>$joininfoval['id'],
		        "emp_master_id" =>$joininfoval['emp_master_id'],
		        "emp_interview_date" =>$empidval,
		        "emp_join_date"=>$empjdval,
		        "emp_probation_month"=>$joininfoval['emp_probation_month'],
		        "emp_appl_courier_sent" =>$joininfoval['emp_appl_courier_sent'],
		        "emp_appl_courier_recvd" =>$joininfoval['emp_appl_courier_recvd'],
		        "emp_probation_date"=>$emppdval

		    );


		    $datarow["join_info"]= $join_info;


		    $idnfoval=$this->Employeemodel->get_employee_id_info($emp_master_id);
		    $id_info=array(
		        "id" =>$idnfoval['id'],
		        "emp_master_id" =>$idnfoval['emp_master_id'],
		        "emp_pan_number" =>$idnfoval['emp_pan_number'],
		        "emp_voter_number" =>$idnfoval['emp_voter_number'],
		        "emp_aadhar_number"=>$idnfoval['emp_aadhar_number']

		        );

		    $datarow["employee_id_info"]= $id_info;


		    $bankinfoval=$this->Employeemodel->get_employee_bank_info($emp_master_id);


		    $bank_info=array(
		        "id" =>$bankinfoval['id'],
		        "emp_master_id" =>$bankinfoval['emp_master_id'],
		        "emp_bank_name" =>$bankinfoval['emp_bank_name'],
		        "emp_bank_ac_number" =>$bankinfoval['emp_bank_ac_number'],
		        "emp_bank_ifsc_code" =>$bankinfoval['emp_bank_ifsc_code']
		    );
		    $datarow["employee_bank_info"]= $bank_info;

		    $pfesicval=$this->Employeemodel->get_employee_pfesic_info($emp_master_id);
		    $employee_pf_esic_info=array(
		        "id" =>$pfesicval['id'],
		        "emp_master_id" =>$pfesicval['emp_master_id'],
		        "emp_pf_number" =>$pfesicval['emp_pf_number'],
		        "emp_esic_ac_number" =>$pfesicval['emp_esic_ac_number'],
		        "emp_uan_number" =>$pfesicval['emp_uan_number']
		    );
		    $datarow["employee_pf_esic_info"]= $employee_pf_esic_info;

		    $resigval=$this->Employeemodel->get_employee_resig_info($emp_master_id);
		    $employee_resig_info=array(
		        "id" =>$resigval['id'],
		        "emp_master_id" =>$resigval['emp_master_id'],
		        "emp_resig_reasons" =>$resigval['emp_resig_reasons'],
		        "emp_resig_date" =>$resigval['emp_resig_date'],
		        "emp_resig_accpt_date" =>$resigval['emp_resig_accpt_date']
		    );

		    $datarow["employee_resig_info"]= $employee_resig_info;
		    $resigval=$this->Employeemodel->get_employee_resig_info($emp_master_id);
		    //employee_sal_master_earning

		    $salmasterearnval=$this->Employeemodel->get_employee_salmaster_earn_info($emp_master_id);

		    $employee_sal_earn_info=array(
		        "id" =>$salmasterearnval['id'],
		        "emp_master_id" =>$salmasterearnval['emp_master_id'],
		        "emp_gross_sal" =>$salmasterearnval['emp_gross_sal'],
		        "emp_basic_sal" =>$salmasterearnval['emp_basic_sal'],
		        "emp_hra" =>$salmasterearnval['emp_hra'],
				"emp_spcl_allowance" =>$salmasterearnval['emp_spcl_allowance'],
		        "emp_conv_allowance" =>$salmasterearnval['emp_conv_allowance'],
		        "emp_edu_allowance" =>$salmasterearnval['emp_edu_allowance'],
		        "emp_upkeep_allowance" =>$salmasterearnval['emp_upkeep_allowance'],
		        "emp_round_off" =>$salmasterearnval['emp_round_off'],
		        "emp_gross_earn" =>$salmasterearnval['emp_gross_earn'],
		        "emp_other_reimb" =>$salmasterearnval['emp_other_reimb']
		    );

		    $datarow["employee_sal_master_earning"]= $employee_sal_earn_info;

		    $salmasterdeduction=$this->Employeemodel->get_employee_salmaster_deduct_info($emp_master_id);

		    $employee_sal_deduct_info=array(
		        "id" =>$salmasterdeduction['id'],
		        "emp_master_id" =>$salmasterdeduction['emp_master_id'],
		        "emp_pf_opted" =>$salmasterdeduction['emp_pf_opted'],
		        "emp_esic_contri" =>$salmasterdeduction['emp_esic_contri'],
		        "emp_pf_contri" =>$salmasterdeduction['emp_pf_contri'],
		        "emp_wb_prof_tax" =>$salmasterdeduction['emp_wb_prof_tax']

		    );

		    $datarow["employee_sal_master_deduction"]= $employee_sal_deduct_info;
		    //employee_sal_master_computation
		    $salmastercomputation=$this->Employeemodel->get_employee_salmaster_compute_info($emp_master_id);

		    $employee_sal_compute_info=array(
		        "id" =>$salmastercomputation['id'],
		        "emp_master_id" =>$salmastercomputation['emp_master_id'],
		        "emp_sal_in_hand" =>$salmastercomputation['emp_sal_in_hand'],
		        "emp_tot_ctc" =>$salmastercomputation['emp_tot_ctc']
		    );
		    $datarow["employee_sal_master_computation"]= $employee_sal_compute_info;

		    $salmastercompanycontri=$this->Employeemodel->get_employee_salmaster_company_contri_info($emp_master_id);
		    //employee_sal_company_contri
		    $employee_sal_comnay_contri_info=array(
		        "id" =>$salmastercompanycontri['id'],
		        "emp_master_id" =>$salmastercompanycontri['emp_master_id'],
		        "emp_pf_contri" =>$salmastercompanycontri['emp_pf_contri'],
		        "emp_pension_contri" =>$salmastercompanycontri['emp_pension_contri'],
		        "emp_edli_contri" =>$salmastercompanycontri['emp_edli_contri'],
		        "emp_adm_charge" =>$salmastercompanycontri['emp_adm_charge'],
		        "emp_adm_charge_edli" =>$salmastercompanycontri['emp_adm_charge_edli'],
		        "emp_tot_contri_edli" =>$salmastercompanycontri['emp_tot_contri_edli'],
		        "emp_esi_contri" =>$salmastercompanycontri['emp_esi_contri']
		    );
		    $datarow["employee_sal_company_contri"]= $employee_sal_comnay_contri_info;


		    $data['employee']=$datarow;

		    }
		    $this->response($data);

		}
		public function add_post()
		{
		    $this->load->model("Employeemodel");
		    $qdata=$this->post('employee');
		    $personal_details=$qdata[0]['personal_details'];
           
		    

		    $emp_code=$qdata[0]['emp_code'];
		    $join_info=$qdata[0]['join_info'];
		    $id_info=$qdata[0]['employee_id_info'];
		    $bank_info=$qdata[0]['employee_bank_info'];

		    $pf_info=$qdata[0]['employee_pf_esic_info'];
		    $resig_info=$qdata[0]['employee_resig_info'];

		    $salmaster_info=$qdata[0]['employee_sal_master_earning'];
		    $salmaster_deduct_info=$qdata[0]['employee_sal_master_deduction'];

		    $salmaster_comp_info=$qdata[0]['employee_sal_master_computation'];
		    $salmaster_company_contri_info=$qdata[0]['employee_sal_company_contri'];

		    $created_by=intval($qdata[0]['created_by']);
		    $modified_by=intval($qdata[0]['modified_by']);
		    $returndata=array();
		    $responsedata=array();
			//var_dump($personal_details);
			//exit;
		    if(empty($emp_code))//new employee
		    {
		        $emp_master_id=$this->insert_employee($personal_details,$join_info,$bank_info,$id_info,$pf_info,$resig_info,$salmaster_info,$salmaster_deduct_info,$salmaster_comp_info,$salmaster_company_contri_info,$created_by,$modified_by);
		       // var_dump("HEtre");
		       // exit;
		        $returndata['message']="Employee Data Created Succsessfully";
		    }
		    else {//existing employee

		        $emp_master_id=$emp_code;
		       // var_dump($join_info);
		        $this->update_employee($emp_code,$personal_details,$join_info,$bank_info,$id_info,$pf_info,$resig_info,$salmaster_info,$salmaster_deduct_info,$salmaster_comp_info,$salmaster_company_contri_info,$created_by,$modified_by);


		        $returndata['message']="Employee Data Updated Succsessfully";

		    }




		    $responsedata['emp_master_id']= $emp_master_id;
		    $returndata['responsedata']=$responsedata;
		    $returndata['code']=200;

		    $this->response( $returndata);



		}

		function update_employee($emp_code,$personal_details,$join_info,$bank_info,$id_info,$pf_info,$resig_info,$salmaster_info,$salmaster_deduct_info,$salmaster_comp_info,$salmaster_company_contri_info,$created_by,$modified_by)
		{


		    $this->load->model("Employeemodel");
		    // $personal_details_re=$personal_details;
		  // var_dump($personal_details);
		  //  exit;
		    $emp_dob=$personal_details['emp_dob'];
		    $ymd = !empty($emp_dob) ? DateTime::createFromFormat('d-m-Y', $emp_dob)->format('Y-m-d')  : null;
		    
		   // $ymd = (DateTime::createFromFormat('d-m-Y',$emp_dob))->format('Y-m-d');
		    //$ymd = DateTime::createFromFormat('m-d-Y', $emp_dob)->format('Y-m-d');

		    $retdata=$this->Employeemodel->get_empmasterid_from_code($emp_code);
		    $emp_master_id=$retdata['id'];
		     
		    $data=array(

		        "process_id"=>intval($personal_details['process_id']),
		        "loc_id"=>intval($personal_details['loc_id']),
		        'emp_fa_hus_name'=>$personal_details['emp_fa_hus_name'],
		        'emp_marital_status'=>$personal_details['emp_marital_status'],
		        'emp_blood_grp'=>$personal_details['emp_blood_grp'],
		        'emp_sub_function_id'=>intval($personal_details['emp_sub_function_id']),
		        'emp_dob'=>$ymd,
		        'emp_gender'=>$personal_details['emp_gender'],
		        'emp_first'=>$personal_details['emp_first'],
		        'emp_mid'=>$personal_details['emp_mid'],
		        'emp_last'=>$personal_details['emp_last'],
		        'emp_add1_id'=>$personal_details['emp_add1_id'],
		        'emp_add_2_id'=>$personal_details['emp_add_2_id'],
		        'emp_city_dist'=>$personal_details['emp_city_dist'],
		        'emp_state'=>$personal_details['emp_state'],
		        'emp_pin'=>$personal_details['emp_pin'],
		        'emp_contact_num'=>$personal_details['emp_contact_num'],
		        'emp_alt_contact_num'=>$personal_details['emp_alt_contact_num'],
		        "emp_is_active"=>intval($personal_details['emp_is_active']),
		        "created_by"=>$created_by,
		        "modified_by"=>$modified_by


		    );

		    $this->Employeemodel->update_employee_master($data,$emp_master_id);

		   $join_id=$join_info['id'];


		    //update joininfo
		   if(!empty($join_id))
		    {

		        $this->update_join_data($join_info,$modified_by);


		    }
		    else
		    {
		        
		        $this->insert_join_info($emp_master_id, $join_info, $created_by, $modified_by);
		        
		        
		    }
		     
		    $id_info_id=$id_info['id'];
		    if(!empty($id_info_id))
		    {

		        $this->update_id_info_data($id_info,$modified_by);


		    }
		    else
		    {
		        
		        $this->insert_id_info($emp_master_id, $id_info, $created_by, $modified_by);
		        
		        
		    }
		    $bank_info_id=$bank_info['id'];
		    if(!empty($bank_info_id))
		    {

		        $this->update_bank_info_data($bank_info,$modified_by);


		    }else 
		    {
		        
		        $this->insert_bank_info($emp_master_id, $bank_info, $created_by, $modified_by);
		        
		    }
		    
		    $pf_id=$pf_info['id'];
		    if(!empty($pf_id))
		    {

		        $this->update_pf_info_data($pf_info,$modified_by);


		    }else
		    {
		        $this->insert_pf_info($emp_master_id, $pf_info, $created_by, $modified_by);
		        
		    }

		    
		    
		    $resig_id=$resig_info['id'];
		    if(!empty($resig_id))
		    {

		        $this->update_resig_info_data($resig_info,$modified_by);


		    }else
		    {
		       // $this->insert_resig_info($emp_master_id, $resig_info_rec, $created_by, $modified_by)
		        
		        
		    }

		    $sal_master_id=$salmaster_info['id'];
		    if(!empty($sal_master_id))
		    {

		        $this->update_sal_master_info_data($salmaster_info,$modified_by);


		    }

		    $sal_master_deduct_id=$salmaster_deduct_info['id'];
		    if(!empty($sal_master_deduct_id))
		    {

		        $this->update_sal_master_deduct_info_data($salmaster_deduct_info,$modified_by);


		    }

		    $sal_master_comp_id=$salmaster_comp_info['id'];
		    if(!empty($sal_master_comp_id))
		    {

		        $this->update_sal_master_comp_info_data($salmaster_comp_info,$modified_by);


		    }


		    $sal_master_company_conti_id=$salmaster_company_contri_info['id'];
		    if(!empty($sal_master_company_conti_id))
		    {

		        $this->update_sal_company_contri_info_data($salmaster_company_contri_info,$modified_by);


		    }
		}

		function update_join_data($joininfo,$modified_by)
		{
		    $id=$joininfo['id'];

		    if(!empty($joininfo['emp_interview_date']))
		    {
		        $emp_interview_date=(DateTime::createFromFormat('d-m-Y',$joininfo['emp_interview_date']))->format('Y-m-d');

		    }
		    else {

		        $emp_interview_date=NULL;
		    }
		    if(!empty($joininfo['emp_join_date']))
		    {
		        $emp_join_date=(DateTime::createFromFormat('d-m-Y',$joininfo['emp_join_date']))->format('Y-m-d');

		    }
		    else {

		        $emp_join_date=NULL;
		    }
		    if(!empty($joininfo['emp_probation_date']))
		    {
		        $emp_probation_date=(DateTime::createFromFormat('d-m-Y',$joininfo['emp_probation_date']))->format('Y-m-d');

		    }
		    else {

		        $emp_probation_date=NULL;
		    }
		    $emp_probation_month=$joininfo['emp_probation_month'];
		    $emp_appl_courier_sent=$joininfo['emp_appl_courier_sent'];
		    $emp_appl_courier_recvd=$joininfo['emp_appl_courier_recvd'];
		    $modified_at=date('Y-m-d H:i:s');

		    //"process_id"=>intval($personal_details['process_id'])

		    $data=array(
		        "emp_interview_date"=>$emp_interview_date,
		        "emp_join_date"=>$emp_join_date,
		        "emp_probation_month"=>$emp_probation_month,
		        "emp_appl_courier_sent"=>$emp_appl_courier_sent,
		        "emp_appl_courier_recvd"=>$emp_appl_courier_recvd,
		        "emp_probation_date"=>$emp_probation_date,
		        "modified_at"=>$modified_at,
		        "modified_by"=>$modified_by
		        );
		   // var_dump($data);
		   // exit;
		    $this->Employeemodel->update_employee_data("employee_join_info",$data,$id);

		}

		function update_id_info_data($id_info,$modified_by)
		{

		    $id=$id_info['id'];
		    $modified_at=date('Y-m-d H:i:s');
		    //var_dump($id_info);
		    //exit;
		    $data=array(
		        "emp_pan_number"=>$id_info['emp_pan_number'],
		        "emp_voter_number"=>$id_info['emp_voter_number'],
		        "emp_aadhar_number"=>$id_info['emp_aadhar_number'],
		        "modified_at"=>$modified_at,
		        "modified_by"=>$modified_by
		    );

		    $this->Employeemodel->update_employee_data("employee_id_info",$data,$id);


		}

		function update_bank_info_data($bank_info,$modified_by)
		{


		    $id=$bank_info['id'];
		    $modified_at=date('Y-m-d H:i:s');

		    $data=array(
		        "emp_bank_name"=>$bank_info['emp_bank_name'],
		        "emp_bank_ifsc_code"=>$bank_info['emp_bank_ifsc_code'],
		        "emp_bank_ac_number"=>$bank_info['emp_bank_ac_number'],
		        "modified_at"=>$modified_at,
		        "modified_by"=>$modified_by
		    );

		    $this->Employeemodel->update_employee_data("employee_bank_info",$data,$id);


		}
		function update_pf_info_data($pf_info,$modified_by)
		{


		    $id=$pf_info['id'];
		    $modified_at=date('Y-m-d H:i:s');

		    $data=array(
		        "emp_pf_number"=>$pf_info['emp_pf_number'],
		        "emp_uan_number"=>$pf_info['emp_uan_number'],
		        "emp_esic_ac_number"=>$pf_info['emp_esic_ac_number'],
		        "modified_at"=>$modified_at,
		        "modified_by"=>$modified_by
		    );

		    $this->Employeemodel->update_employee_data("employee_pf_esic_info",$data,$id);


		}

		function update_resig_info_data($resig_info,$modified_by)
		{


		    $id=$resig_info['id'];
		    $modified_at=date('Y-m-d H:i:s');

		    if(!empty($resig_info['emp_resig_date']))
		    {

		        $emp_resig_date=(DateTime::createFromFormat('d-m-Y',$resig_info['emp_resig_date']))->format('Y-m-d');
		       // var_dump($emp_resig_date);
		       // exit;
		    }
		    else {

		        $emp_resig_date=NULL;
		    }
		    if(!empty($resig_info['emp_resig_accpt_date']))
		    {
		        $emp_resig_accpt_date=(DateTime::createFromFormat('d-m-Y',$resig_info['emp_resig_accpt_date']))->format('Y-m-d');

		    }
		    else {

		        $emp_resig_accpt_date=NULL;
		    }

		    $data=array(

		        "emp_resig_reasons"=>$resig_info['emp_resig_reasons'],
		        "emp_resig_date"=>$emp_resig_date,
		        "emp_resig_accpt_date"=>$emp_resig_accpt_date,
		        "modified_at"=>$modified_at,
		        "modified_by"=>$modified_by
		    );

		    $this->Employeemodel->update_employee_data("employee_resig_info",$data,$id);


		}

		function update_sal_master_info_data($sal_info,$modified_by)
		{

		    $id=$sal_info['id'];
		    $modified_at=date('Y-m-d H:i:s');


		    $data=array(
		        "emp_gross_sal"=>$sal_info['emp_gross_sal'],
		        "emp_basic_sal"=>$sal_info['emp_basic_sal'],
		        "emp_hra"=>$sal_info['emp_hra'],
		        "emp_conv_allowance"=>$sal_info['emp_conv_allowance'],
		        "emp_spcl_allowance"=>$sal_info['emp_spcl_allowance'],
		        "emp_edu_allowance"=>$sal_info['emp_edu_allowance'],
		        "emp_upkeep_allowance"=>$sal_info['emp_upkeep_allowance'],
		        "emp_round_off"=>$sal_info['emp_round_off'],
		        "emp_gross_earn"=>$sal_info['emp_gross_earn'],
		        "emp_other_reimb"=>$sal_info['emp_other_reimb'],
		        "modified_at"=>$modified_at,
		        "modified_by"=>$modified_by
		    );

		    $this->Employeemodel->update_employee_data("employee_sal_master_earning",$data,$id);


		}

		function update_sal_master_deduct_info_data($sal_deduct_info,$modified_by)
		{



		    $id=$sal_deduct_info['id'];
		    $modified_at=date('Y-m-d H:i:s');

		    $data=array(
		        "emp_pf_opted"=>$sal_deduct_info['emp_pf_opted'],
		        "emp_pf_contri"=>$sal_deduct_info['emp_pf_contri'],
		        "emp_esic_contri"=>$sal_deduct_info['emp_esic_contri'],
		        "emp_wb_prof_tax"=>$sal_deduct_info['emp_wb_prof_tax'],
		        "modified_at"=>$modified_at,
		        "modified_by"=>$modified_by
		    );

		    $this->Employeemodel->update_employee_data("employee_sal_master_deduction",$data,$id);


		}

		function update_sal_master_comp_info_data($sal_comp_info,$modified_by)
		{


		    $id=$sal_comp_info['id'];
		    $modified_at=date('Y-m-d H:i:s');

		    $data=array(
		        "emp_sal_in_hand"=>$sal_comp_info['emp_sal_in_hand'],
		        "emp_tot_ctc"=>$sal_comp_info['emp_tot_ctc'],
		        "modified_at"=>$modified_at,
		        "modified_by"=>$modified_by
		    );

		    $this->Employeemodel->update_employee_data("employee_sal_master_computation",$data,$id);


		}

		function update_sal_company_contri_info_data($sal_comp_contri_info,$modified_by)
		{

		    $id=$sal_comp_contri_info['id'];
		    $modified_at=date('Y-m-d H:i:s');

		    $data=array(
		        "emp_pf_contri"=>$sal_comp_contri_info['emp_pf_contri'],
		        "emp_pension_contri"=>$sal_comp_contri_info['emp_pension_contri'],
		        "emp_edli_contri"=>$sal_comp_contri_info['emp_edli_contri'],
		        "emp_adm_charge"=>$sal_comp_contri_info['emp_adm_charge'],
		        "emp_adm_charge_edli"=>$sal_comp_contri_info['emp_adm_charge_edli'],
		        "emp_esi_contri"=>$sal_comp_contri_info['emp_esi_contri'],
		        "modified_at"=>$modified_at,
		        "modified_by"=>$modified_by
		    );

		    $this->Employeemodel->update_employee_data("employee_sal_master_company_contri",$data,$id);


		}





		function insert_employee($personal_details,$join_info,$bank_info,$id_info,$pf_info,$resig_info,$salmaster_info,$salmaster_deduct_info,$salmaster_comp_info,$salmaster_company_contri_info,$created_by,$modified_by)
		{

		    $this->load->model("Employeemodel");
		    // $personal_details_re=$personal_details;
		    $emp_dob=$personal_details['emp_dob'];
		    $ymd = !empty($emp_dob) ? DateTime::createFromFormat('d-m-Y', $emp_dob)->format('Y-m-d')  : null;

		    //$ymd = (DateTime::createFromFormat('d-m-Y',$emp_dob))->format('Y-m-d');
		    //$ymd = DateTime::createFromFormat('m-d-Y', $emp_dob)->format('Y-m-d');



		    $newempcode=$this->getnextemcode();

		    $data=array(
		        "emp_code"=>$newempcode,
		        "process_id"=>intval($personal_details['process_id']),
		        "emp_title"=>$personal_details['emp_title'],
				"emp_desig_id"=>intval($personal_details['emp_desig_id']),
		        "loc_id"=>intval($personal_details['loc_id']),
		        'emp_fa_hus_name'=>$personal_details['emp_fa_hus_name'],
		        'emp_marital_status'=>$personal_details['emp_marital_status'],
		        'emp_blood_grp'=>$personal_details['emp_blood_grp'],
		        'emp_sub_function_id'=>intval($personal_details['emp_sub_function_id']),
		        'emp_dob'=>$ymd,
		        'emp_gender'=>$personal_details['emp_gender'],
		        'emp_first'=>$personal_details['emp_first'],
		        'emp_mid'=>$personal_details['emp_mid'],
		        'emp_last'=>$personal_details['emp_last'],
		        'emp_add1_id'=>$personal_details['emp_add1_id'],
		        'emp_add_2_id'=>$personal_details['emp_add_2_id'],
		        'emp_city_dist'=>$personal_details['emp_city_dist'],
		        'emp_state'=>$personal_details['emp_state'],
		        'emp_pin'=>$personal_details['emp_pin'],
		        'emp_contact_num'=>$personal_details['emp_contact_num'],
		        'emp_contact_num'=>$personal_details['emp_contact_num'],
		        "emp_is_active"=>intval($personal_details['emp_is_active']),
		        "created_by"=>$created_by,
		        "modified_by"=>$modified_by


		    );
		    $emp_master_id=$this->Employeemodel->create_emp_master($data);


		    //join_info
		    //$join_info_rec=$join_info[0];
		    $this->insert_join_info($emp_master_id,$join_info,$created_by,$modified_by);
		    //join_info

		    //id_info
		    //$id_info_rec=$id_info[0];
		    $this->insert_id_info($emp_master_id,$id_info,$created_by,$modified_by);
		    //id_info

		    //bank_info
		    //$bank_info_rec=$bank_info[0];
		    $this->insert_bank_info($emp_master_id,$bank_info,$created_by,$modified_by);
		    //bank_info

		    //pf info
		    //$pf_info_rec=$pf_info[0];
		    $this->insert_pf_info($emp_master_id,$pf_info,$created_by,$modified_by);
		    //pf info


		    //resig info
		   // $resig_info_rec=$resig_info;
		   if(!empty($resig_info))
		   {

		       $this->insert_resig_info($emp_master_id, $resig_info, $created_by, $modified_by);

		    //resig info
		   }
		    //salmaster
		    //$salmaster_info_rec=$salmaster_info[0];
		    $this->insert_sal_master($emp_master_id, $salmaster_info, $created_by, $modified_by);
		    //salmaster

		    //salmasterdeduct
		    // $salmaster_deduct_info_rec=$salmaster_deduct_info[0];
		    $this->insert_sal_master_deduct($emp_master_id, $salmaster_deduct_info, $created_by, $modified_by);
		    //salmasterdeduct

		    //salmastercomp
		    // $salmaster_comp_info_rec=$salmaster_comp_info[0];
		    $this->insert_sal_master_computation($emp_master_id, $salmaster_comp_info, $created_by, $modified_by);
		    //salmastercomp

		    //compamy contribution
		    //  $salmaster_company_contri_info_rec=$salmaster_company_contri_info[0];
		    $this->insert_sal_master_company_contri($emp_master_id, $salmaster_company_contri_info, $created_by, $modified_by);
		    //compamy contribution
		    return $emp_master_id;


		}


		function getnextemcode()
		{

		    $this->load->model("Employeemodel");
		    $latest_emp_code=$this->Employeemodel->get_latest_emp_code();

		    $pieces = explode("/", $latest_emp_code);
		    $piece1= $pieces[0];
		    $piece2= $pieces[1];
		    $newpiece2=$piece2+1;
		    $newpeice=strval($piece1)."/".strval($newpiece2);
		    return $newpeice;
		}

		function insert_join_info($emp_master_id,$join_info_rec,$created_by,$modified_by)
		{


		    $emp_interview_date=$join_info_rec['emp_interview_date'];
		    $emp_join_dt=$join_info_rec['emp_join_date'];
		    $emp_probation_month=$join_info_rec['emp_probation_month'];
		    $emp_probation_date=$join_info_rec['emp_probation_date'];

		    $emp_interview_dt = !empty($emp_interview_dt) ? DateTime::createFromFormat('d-m-Y', $emp_interview_dt)->format('Y-m-d')  : null;
		    $emp_join_dt = !empty($emp_join_date) ? DateTime::createFromFormat('d-m-Y', $emp_join_date)->format('Y-m-d')  : null;
		    $emp_probation_date = !empty($emp_probation_date) ? DateTime::createFromFormat('d-m-Y', $emp_probation_date)->format('Y-m-d')  : null;



		    $data=array(

		        "emp_master_id"=>$emp_master_id,
		        "emp_interview_date"=>$emp_interview_dt,
		        "emp_join_date"=>$emp_join_dt,
		        "emp_probation_month"=>$emp_probation_month ,
		        "emp_probation_date"=>$emp_probation_date,
		        "emp_appl_courier_sent"=> $join_info_rec['emp_appl_courier_sent'],
		        "emp_appl_courier_recvd"=> $join_info_rec['emp_appl_courier_recvd'],
		        "created_by"=>$created_by,
		        "modified_by"=>$modified_by

		    );

		    $emp_join_id=$this->Employeemodel->create_join_info($data);
		     
		}

		function insert_id_info($emp_master_id,$id_info_rec,$created_by,$modified_by)
		{



		    $data=array(

		        "emp_master_id"=>$emp_master_id,
		        "emp_pan_number"=>$id_info_rec['emp_pan_number'],
		        "emp_voter_number"=>$id_info_rec['emp_voter_number'],
		        "emp_aadhar_number"=>$id_info_rec['emp_aadhar_number'],
		        "created_by"=>$created_by,
		        "modified_by"=>$modified_by

		    );

		    $emp_info_id=$this->Employeemodel->create_id_info($data);
		}
		function insert_bank_info($emp_master_id,$bank_info_rec,$created_by,$modified_by)
		{




		    $data=array(

		        "emp_master_id"=>$emp_master_id,
		        "emp_bank_name"=>$bank_info_rec['emp_bank_name'],
		        "emp_bank_ac_number"=>$bank_info_rec['emp_bank_name'],
		        "emp_bank_ac_number"=>$bank_info_rec['emp_bank_ac_number'],

		        "created_by"=>$created_by,
		        "modified_by"=>$modified_by

		    );
		    $emp_bank_id=$this->Employeemodel->create_bank_info($data);
		}

		function insert_pf_info($emp_master_id,$pf_info_rec,$created_by,$modified_by)
		{

		    $data=array(

		        "emp_master_id"=>$emp_master_id,
		        "emp_pf_number"=>$pf_info_rec['emp_pf_number'],
		        "emp_esic_ac_number"=>$pf_info_rec['emp_esic_ac_number'],
		        "emp_uan_number"=>$pf_info_rec['emp_uan_number'],
		        "created_by"=>$created_by,
		        "modified_by"=>$modified_by

		    );
		    $emp_pf_id=$this->Employeemodel->create_pf_info($data);
		}
		function insert_resig_info($emp_master_id,$resig_info_rec,$created_by,$modified_by)
		{




		    $emp_resig_date=$resig_info_rec['emp_resig_date'];
		    $emp_resig_accpt_date=$resig_info_rec['emp_resig_accpt_date'];

		    $emp_resig_dt = !empty($emp_resig_dt) ? DateTime::createFromFormat('d-m-Y', $emp_resig_dt)->format('Y-m-d')  : null;

		    $emp_resig_accpt_date=$resig_info_rec['emp_resig_accpt_date'];
		    $emp_resig_accpt_dt = !empty($emp_resig_accpt_dt) ? DateTime::createFromFormat('d-m-Y', $emp_resig_accpt_dt)->format('Y-m-d')  : null;


		    $data=array(

		        "emp_master_id"=>$emp_master_id,
		        "emp_resig_reasons"=>$resig_info_rec['emp_resig_reasons'],
		        "emp_resig_date"=>$emp_resig_dt,
		        "emp_resig_accpt_date"=>$emp_resig_accpt_dt,
		        "created_by"=>$created_by,
		        "modified_by"=>$modified_by

		    );
		    $emp_pf_id=$this->Employeemodel->create_resig_info($data);
		}


		function insert_sal_master($emp_master_id,$salmaster_info_rec,$created_by,$modified_by)
		{


		        $data=array(

		            "emp_master_id"=>$emp_master_id,
		            "emp_gross_sal"=>doubleval($salmaster_info_rec['emp_gross_sal']),
		            "emp_basic_sal"=>doubleval($salmaster_info_rec['emp_basic_sal']),
		            "emp_hra"=>doubleval($salmaster_info_rec['emp_hra']),
		            "emp_conv_allowance"=>doubleval($salmaster_info_rec['emp_conv_allowance']),
		            "emp_spcl_allowance"=>doubleval($salmaster_info_rec['emp_spcl_allowance']),
		            "emp_edu_allowance"=>doubleval($salmaster_info_rec['emp_edu_allowance']),
		            "emp_upkeep_allowance"=>doubleval($salmaster_info_rec['emp_upkeep_allowance']),
		            "emp_round_off"=>doubleval($salmaster_info_rec['emp_round_off']),
		            "emp_gross_earn"=>doubleval($salmaster_info_rec['emp_gross_earn']),
		            "emp_other_reimb"=>doubleval($salmaster_info_rec['emp_other_reimb']),
		            "created_by"=>$created_by,
		            "modified_by"=>$modified_by
		            );
		        $emp_sal_id=$this->Employeemodel->create_salmaster_info($data);

		}

		function insert_sal_master_deduct($emp_master_id,$salmaster_deduct_info_rec,$created_by,$modified_by)
		{

		    $data=array(

		        "emp_master_id"=>$emp_master_id,
		        "emp_pf_contri"=>doubleval($salmaster_deduct_info_rec['emp_pf_contri']),
		        "emp_esic_contri"=>doubleval($salmaster_deduct_info_rec['emp_esic_contri']),
		        "emp_esic_contri"=>doubleval($salmaster_deduct_info_rec['emp_esic_contri']),
		        "emp_pf_opted"=>$salmaster_deduct_info_rec['emp_pf_opted'],
		        "created_by"=>$created_by,
		        "modified_by"=>$modified_by
		    );
		    $emp_sal_id=$this->Employeemodel->create_salmaster_deduct_info($data);

		}

		function insert_sal_master_computation($emp_master_id,$salmaster_deduct_info_rec,$created_by,$modified_by)
		{

		$data=array(

		    "emp_master_id"=>$emp_master_id,
		    "emp_sal_in_hand"=>doubleval($salmaster_deduct_info_rec['emp_sal_in_hand']),
		    "emp_tot_ctc"=>doubleval($salmaster_deduct_info_rec['emp_tot_ctc']),
		    "created_by"=>$created_by,
		    "modified_by"=>$modified_by
		);
		$emp_sal_id=$this->Employeemodel->create_salmaster_comp_info($data);

		}

		function insert_sal_master_company_contri($emp_master_id,$salmaster_company_contri_info_rec,$created_by,$modified_by)
		{

		    $data=array(
		        "emp_master_id"=>$emp_master_id,
		        "emp_pf_contri"=>doubleval($salmaster_company_contri_info_rec['emp_pf_contri']),
		        "emp_pension_contri"=>doubleval($salmaster_company_contri_info_rec['emp_pension_contri']),
		        "emp_edli_contri"=>doubleval($salmaster_company_contri_info_rec['emp_edli_contri']),
		        "emp_adm_charge"=>doubleval($salmaster_company_contri_info_rec['emp_adm_charge']),
		        "emp_adm_charge_edli"=>doubleval($salmaster_company_contri_info_rec['emp_adm_charge_edli']),
		        "emp_esi_contri"=>doubleval($salmaster_company_contri_info_rec['emp_esi_contri']),
		        "created_by"=>$created_by,
		        "modified_by"=>$modified_by
		    );
		    $emp_sal_id=$this->Employeemodel->create_salmaster_company_contri_info($data);

		}

	 
		
		
		function is_dir_empty($dir) {
		    if (!is_readable($dir)) return NULL;
		    return (count(scandir($dir)) == 2);
		}
		public function uploademdoc_post()
		{
		    
		    
		    $config['upload_path']          = './';
		    $config['allowed_types']        = 'xls|csv|xlsx|doc|docx|pdf';
		    $config['max_size']             = 2048;
		    $config['max_width']            = 1024;
		    $config['max_height']           = 768;
		    $emp_code=$this->input->post("emp_code");
		    $emp_code_copy=$emp_code;
		    $returndata=array();
		    $this->load->library('upload');
		    
		    if(!empty($_FILES['upl_files']['name'])){
		    
		       // $emp_code_copy=str_replace("//","_",$emp_code_copy);//replace the / in employee code with _
		       $emp_code_copy=preg_replace('/[\W\s\/]+/', '-', $emp_code_copy);
		      //var_dump($emp_code_copy);exit;
		        
		    $fileuploaddir=$_SERVER['DOCUMENT_ROOT']."/thinktel/uploads/empfiles/".$emp_code_copy;
		    
		    if (!file_exists($fileuploaddir)) {
		        mkdir($fileuploaddir, 0777, true);
		    }
		        
		        
		        
		    $number_of_files_uploaded = count($_FILES['upl_files']['name']);
		    // Faking upload calls to $_FILE
		    for ($i = 0; $i < $number_of_files_uploaded; $i++) :
		    $_FILES['userfile']['name']     = $_FILES['upl_files']['name'][$i];
		    $_FILES['userfile']['type']     = $_FILES['upl_files']['type'][$i];
		    $_FILES['userfile']['tmp_name'] = $_FILES['upl_files']['tmp_name'][$i];
		    $_FILES['userfile']['error']    = $_FILES['upl_files']['error'][$i];
		    $_FILES['userfile']['size']     = $_FILES['upl_files']['size'][$i];
		    $config = array(
		       // 'file_name'     => <your ouw function to generate random names>,
		        'allowed_types' => 'jpg|jpeg|png|gif|xls|csv|xlsx|doc|docx|pdf',
		        'max_size'      => 3000,
		        'overwrite'     => FALSE,
		         /* real path to upload folder ALWAYS */
		        'upload_path'
		        => $fileuploaddir);
		    $this->upload->initialize($config);
		    if ( ! $this->upload->do_upload()) :
		    $error = array('error' => $this->upload->display_errors());
		    $returndata['message']="Error uploading files".$this->upload->display_errors();
		    $returndata['code']="200";
		    //var_dump($error);
		    //$this->load->view('upload_form', $error);
		    else :
		    $returndata['message']="File(s) uploaded successfully";
		    
		    $returndata['code']="200";
		    
		    $final_files_data[] = $this->upload->data();
		    // Continue processing the uploaded data
		    endif;
		    endfor;
		    }
		    else{
		        
		        $returndata['message']="No files supplied";
		        $returndata['code']="200";
		    }
		    
		    $this->response($returndata);
		}
		public function employeeimage_get()
		{
		   
		    $emp_code=$this->get("emp_code");		   
		    $emp_code_copy=$emp_code;
		    $emp_code_copy=preg_replace('/[\W\s\/]+/', '-', $emp_code_copy);		    
		    $imagedir=$_SERVER['DOCUMENT_ROOT']."/thinktel/uploads/empimages/".$emp_code_copy;
		    
		    
		    $filename = $emp_code_copy.".jpg";
		    $filename=$imagedir."/".$filename;
		    //var_dump($filename);
		   // exit;
		   
		    if((file_exists($filename)))
		    {
		   
		    $handle = fopen($filename, "rb");
		    $contents = fread($handle, filesize($filename));
		    fclose($handle);
		    header("content-type: image/jpg  ");
		    echo $contents;
		    //header("content-type: image/png");
		    $this->response($filename);
		    }
		    else {
		        $data['message']="Calulated Successfully";
		        $data['code']=200;
		        echo "file  does not exist";
		        $this->response($data);
		    }
		    
		       
		    
		}
		public function employeeimage_post()
		{
		    
		    $emp_code=$this->post("emp_code");
		    $emp_code_copy=$emp_code;
		    $emp_code_copy=preg_replace('/[\W\s\/]+/', '-', $emp_code_copy);
		    
		    $imagedir=$_SERVER['DOCUMENT_ROOT']."/thinktel/uploads/empimages/".$emp_code_copy;
		    
		   // var_dump($emp_code_copy);
		   // exit;
		    
		   // $emp_code=$this->input->post("emp_code");
		   //$emp_code_copy=$emp_code;
		    $returndata=array();
		    $this->load->library('upload');
		    		     
		    if (!file_exists($imagedir)) {
		        mkdir($imagedir, 0777, true);
		        }
		        $new_name=$emp_code_copy.".jpg";
		        
		        $config = array(
		            // 'file_name'     => <your ouw function to generate random names>,
		            'allowed_types' => 'jpg|jpeg|png|gif',
		            'max_size'      => 3000,
		            'overwrite'     => TRUE,
		            'max_width' =>1024,
		            'max_height'=>768,
		            'file_name' =>$new_name,
		            /* real path to upload folder ALWAYS */
		            'upload_path' => $imagedir);
		        $this->upload->initialize($config);
		        if ( ! $this->upload->do_upload()) {
		            $error = array('error' => $this->upload->display_errors());
		            
		            
		            $returndata['message']="Error uploading files".$this->upload->display_errors();
		            $returndata['code']="200";
		        }
		        else
		        {
		            
		            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
		            $file_name = $upload_data['file_name'];
		            
		            
		            $returndata['message']="File ".$file_name." Uploaded Successfully";		            
		            $returndata['code']="200";
		            
		            
		        }
		        $this->response($returndata);
		    
		}
		
		/*public function  ziptest_get()
		{
		    $this->load->helper('file');
		    $zipname='basket1.zip';
		    $zip = new ZipArchive;
		    $zip->open($zipname,ZipArchive::CREATE);
		    $emp_code=$this->get("emp_code");
		    $emp_code_copy=$emp_code;
		    // $emp_code_copy=str_replace("//","_",$emp_code_copy);//replace the / in employee code with _
		    $emp_code_copy=preg_replace('/[\W\s\/]+/', '-', $emp_code_copy);
		    //var_dump($emp_code_copy);exit;
		    
		    //$fileuploaddir=$_SERVER['DOCUMENT_ROOT']."/thinktel/uploads/empfiles/".$emp_code_copy;
		    //$fileuploaddir= "./uploads/empfiles/".$emp_code_copy;
		    $fileuploaddir= "./".$emp_code_copy;
		   // $fileuploaddir= "d:/test/".$emp_code_copy;
		    $path = $fileuploaddir;
		    $files =  get_filenames($path);
		    if(!empty($files))
		    {
		     
    		    foreach($files as $f){
    		        $file=$path."/".$f;
    		     //  var_dump( $file);
    		        
    		        $zip->addFile($file);
    		    }
		        // exit;
		        $status=$zip->getStatusString();
		      //  var_dump( $status);
		     //   exit;
		        $zip->close();
		    
		        //$zipname= $zip->filename;
    		    header('Content-Description: File Transfer');
    		    header('Content-Type: application/zip');
    		    header('Content-Disposition:attachment; filename='.basename($zipname));
    		    header('Content-Length:'.filesize($zipname));
    		    readfile($zipname);
		    }
		}*/
		
		public function  downloadempfiles_get()
		{
		    $this->load->helper('file');
		    $zipname='basket1.zip';
		    $zip = new ZipArchive;
		    $zip->open($zipname,ZipArchive::CREATE);
		    $emp_code=$this->get("emp_code");
		    $emp_code_copy=$emp_code;
		    // $emp_code_copy=str_replace("//","_",$emp_code_copy);//replace the / in employee code with _
		    $emp_code_copy=preg_replace('/[\W\s\/]+/', '-', $emp_code_copy);
		  
		    $zipname=$emp_code_copy.".zip";
		    $zip = new ZipArchive;
		    $dirval='uploads/empfiles/'.$emp_code_copy;
		     
		    if ($zip->open($zipname, ZipArchive::CREATE) === TRUE)
		    {
		        if ($handle = opendir( $dirval))
		        {
		            // Add all files inside the directory
		          
		            while (false !== ($entry = readdir($handle)))
		            {
		                if ($entry != "." && $entry != ".." && !is_dir( $dirval."/" . $entry))
		                {
		                    $zip->addFile($dirval."/". $entry);
		                   // var_dump($entry);
		                   // $zip->addFile($entry);
		                }
		            }
		            closedir($handle);
		        }
		       
		        $zip->close();
		    }
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/zip');
		    header('Content-Disposition:attachment; filename='.basename($zipname));
		    header('Content-Length:'.filesize($zipname));
		    readfile($zipname);
		    $this->response();
		}
}

