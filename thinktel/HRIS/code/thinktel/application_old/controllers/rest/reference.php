<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'libraries\REST_Controller.php');
use Restserver\Libraries\REST_Controller;

class Reference extends REST_Controller
{

    public function __construct() {
        parent::__construct();
         $this->load->model('Referencemodel');
         $this->load->library('utility');

    }
    public function processes_get(){


        $data=$this->Referencemodel->get_process_list();
        if($data)
        {
            $this->response($data, 200);
        }

        else
        {
            $this->response(NULL, 404);
        }

    }
    public function usergroup_get(){
        
        
        $data=$this->Referencemodel->get_users_group();
        if($data)
        {
            $this->response($data, 200);
        }
        
        else
        {
            $this->response(NULL, 404);
        }
        
    }

    public function locations_get(){

		$data=array();
		$message=array();

        $data=$this->Referencemodel->get_location_list();
        if($data)
        {

            array_push($message,"message1","message2");

			$responseval=$this->utility->prepareresponse($data,$message);



            $this->response($responseval, 200);
        }

        else
        {
            $this->response(NULL, 404);
        }



    }

        public function designations_get(){

        $data=$this->Referencemodel->get_designation_list();
        if($data)
        {


           $this->response($data, 200);
        }

        else
        {
            $this->response(NULL, 404);
        }
      }
      public function subfunction_get(){
          
          $data=$this->Referencemodel->get_sub_function_list();
          if($data)
          {
              
              
              $this->response($data, 200);
          }
          
          else
          {
              $this->response(NULL, 404);
          }
      }
      
      
      
        public function attndnccode_get(){


            $data=$this->Referencemodel->get_attendance_code();
            $message=array ('status' => FALSE,
                'message' => 'User could not be found');

            if($data)
            {

                $datasend= array_push($data,$message);
                $this->response($data, 200);
            }

            else
            {
                $this->response(NULL, 404);
            }

        }





    }


