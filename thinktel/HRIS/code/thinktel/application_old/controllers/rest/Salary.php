<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries\REST_Controller.php');
require 'vendor/autoload.php';
use Restserver\Libraries\REST_Controller;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Salary extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));

	}

	public function index()
	{
		//$this->load->view('salary/salaryInitiate');
	}

 
	
	public function saldownload_get() {
	   
	    $this->load->model("Employeemodel");
	    $this->load->model("Referencemodel");
	    $this->load->model("Salarymodel");
	    $month=$this->input->get("month");
	    $year=$this->input->get("year");
	    $process_id=$this->input->get("process_id");
	    $location_id=$this->input->get("location_id");
	    
	    
	    
	    
	    
	    $spreadsheet = new Spreadsheet();
	    $sheet = $spreadsheet->getActiveSheet();
	   
	    
	    $writer = new Xlsx($spreadsheet);
	    
	    //Process	Location	Employee Code	Employe Name	Gross Amount	Bank Name	IFSC Code
	    
	    $sheet->setCellValue('A1', 'Process');
	    $sheet->setCellValue('B1', 'Location');
	    $sheet->setCellValue('C1', 'Employee Code');
	    $sheet->setCellValue('D1', 'Employe Name');
	    $sheet->setCellValue('E1', 'Gross Amount');
	    $sheet->setCellValue('F1', 'Bank Name');
	    $sheet->setCellValue('G1', 'Bank Account Number');
	    $sheet->setCellValue('H1', 'IFSC Code');
	    $sheet->getColumnDimension('A')->setWidth(20);
	    $sheet->getColumnDimension('B')->setWidth(20);
	    $sheet->getColumnDimension('C')->setWidth(20);
	    $sheet->getColumnDimension('D')->setWidth(40);
	    $sheet->getColumnDimension('E')->setWidth(20);
	    $sheet->getColumnDimension('F')->setWidth(30);
	    $sheet->getColumnDimension('G')->setWidth(30);
	    $sheet->getColumnDimension('H')->setWidth(30);
	    
	   // var_dump($process_id);
	   /// exit;
	    $process_name=($this->Referencemodel->get_process_info_by_code($process_id))->process_name;
	    $location_name=($this->Referencemodel->get_location_info_by_code($location_id))->location_name;
	    
	    $filename = 'SalaryDump_'.$process_name."_".$location_name."_".$month."_".$year;
	     	    
	    $data=$this->Salarymodel->get_sal_download_data($process_id,$location_id,$month,$year);
	  // var_dump($data);
	   // exit;
	    $row=2;  
	    foreach($data as $datarow)
	    {
	        
	       /* $sheet->setCellValue('A1', 'Process');
	        $sheet->setCellValue('B1', 'Location');
	        $sheet->setCellValue('C1', 'Employee Code');
	        $sheet->setCellValue('D1', 'Employe Name');
	        $sheet->setCellValue('E1', 'Gross Amount');
	        $sheet->setCellValue('F1', 'Bank Name');
	        $sheet->setCellValue('G1', 'Bank Account Number');
	        $sheet->setCellValue('H1', 'IFSC Code');*/
	        
	        
	        
	        $emp_code=$datarow['emp_code'];
	        $emp_name=$datarow['emp_name'];
	        $gross_earning=$datarow['gross_earning'];
	        $emp_bank_name=$datarow['emp_bank_name'];
	        $emp_bank_ifsc_code=$datarow['emp_bank_ifsc_code'];
	        $emp_bank_ac_number=$datarow['emp_bank_ac_number'];
	        
	        $cellno="A".$row;
	        $sheet->setCellValue($cellno, $process_name);
	        $cellno="B".$row;	        
	        $sheet->setCellValue($cellno, $location_name);
	        $cellno="C".$row;
	        $sheet->setCellValue($cellno, $emp_code);
	        $cellno="D".$row;
	        $sheet->setCellValue($cellno, $emp_name);
	        $cellno="E".$row;
	        $sheet->setCellValue($cellno, $gross_earning);
	        $cellno="F".$row;
	        $sheet->setCellValue($cellno, $emp_bank_name);
	        $cellno="G".$row;
	        $sheet->setCellValue($cellno, $emp_bank_ac_number);
	        $cellno="H".$row;
	        $sheet->setCellValue($cellno, $emp_bank_ifsc_code);
	        
	        $row=$row+1;
	        
	    }
	   // exit;
	    header('Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
	    header('Cache-Control: max-age=0');
	    //var_dump("Heere");
	    $writer->save('php://output'); // download file */
	} 

	public function getsaltdsadvdata($empid,$month,$year)
	{
	    
	    $this->load->model('Salarymodel');
	    
	    //var_model($month);
	   // var_model($date);
	   
	    $data=$this->Salarymodel->get_sal_adv_tds_data($empid,$month,$year);
	    return $data;
	    
	    //var_dump($data);
	    
	  
	    
	}
	
	public function sal_tds_adv_reimb_post()
	{
	    
	    $this->load->model("Employeemodel");
	    $this->load->model("Referencemodel");
	    $this->load->model("Salarymodel");
	    
	    $tdsval=$this->post("emp_sal_adv_tds");
	    $reimbval=$this->post("emp_sal_reimbursement");
	    
	    if(empty($tdsval))
	    {
	        $this->Salarymodel->merge_adv_tds_data($tdsval);
	        
	    }
	    if(empty($reimbval))
	    {
	        $this->Salarymodel->merge_emp_sal_reimbursement($reimbval);
	        
	    }
	    
	    $returndata['message']="Data Saved";
	    $returndata['code']="200";
	    
	    $this->response( $returndata);
	   
	    
	    
	}
	
	
	public function getsal_tds_adv_reimb_get()
	{
	    
	    $this->load->model("Employeemodel");
	    $this->load->model("Referencemodel");
	    $this->load->model("Salarymodel");
	    
	    $emp_code=$this->input->get("emp_code");
	    $month=$this->input->get("month");
	    $year=$this->input->get("year");
	    $datarow=array();
	    //convert emp_code to master code
	    if(!empty($emp_code)){
	        
	        $retdata= $this->Employeemodel->get_empmasterid_from_code($emp_code);
	        
	         
	    
	       
	        if(!empty($retdata))
	        {
	        $emp_master_id=$retdata['id'];
	        $dataval=$this->Employeemodel->get_employee_details($emp_master_id);
	        $advtdsval=$this->Salarymodel->get_sal_adv_tds_data($emp_master_id,$month,$year);
	        
	        $emp_name=$dataval['emp_first']." ".$dataval['emp_mid']." ".$dataval['emp_last'];
	        $datarow["emp_name"]= $emp_name;
	        $datarow["emp_code"]= $emp_code;
	        if(!empty($advtdsval))
	        {
	            
	            $emp_sal_adv_tds=array(
	                "id"=>$advtdsval['id'],
	                "emp_master_id"=>$advtdsval['emp_master_id'],
	                "month"=>$advtdsval['month'],
	                "year"=>$advtdsval['year'],
	                "advance"=>doubleval($advtdsval['advance']),
	                "tds"=>doubleval($advtdsval['tds']),
	                "is_active"=>$advtdsval['is_active']
	                
	            );
	        }else
	        {
	            
	            
	            $emp_sal_adv_tds=array(
	                "id"=>null,
	                "emp_master_id"=>$emp_master_id,
	                "month"=>$month,
	                "year"=>$year,
	                "advance"=>0.0,
	                "tds"=>0.0,
	                "is_active"=>0
	                
	            );
	            
	            
	            
	        }
	        
	        $datarow["emp_sal_adv_tds"]= $emp_sal_adv_tds;
	        
	        $reimbval=$this->Salarymodel->get_reimbursement_data($emp_master_id,$month,$year);
	        
	        if(!empty($reimbval))
	        {
	            
	            
	            $emp_sal_reimbursement=array(
	                "id"=>$reimbval['id'],
	                "emp_master_id"=>$reimbval['emp_master_id'],
	                "month"=>$reimbval['month'],
	                "year"=>$reimbval['year'],
	                "reimbursement"=>doubleval($reimbval['reimbursement']),
	                "is_active"=>$reimbval['is_active'] );
	            
	        }else{
	            
	            $emp_sal_reimbursement=array(
	                "id"=>null,
	                "emp_master_id"=>$emp_master_id,
	                "month"=>$month,
	                "year"=>$year,
	                "reimbursement"=>0.0,
	                "is_active"=>0
	            );
	            
	        }
	        
	        $datarow["emp_sal_reimbursement"]= $emp_sal_reimbursement;
	        }
	        
	    }
	    
	    $this->response($datarow);
	}
	
	
	
	 
	
	
	public function savesaltdsadvdata($empid,$month,$year,$gross,$days_in_month,$days_worked,$created_by,$modified_by)
	{
	    
	    $this->load->model('Salarymodel');
	    
	   
	    $basic=0.0;
	    /***Calculation begins*/
	    
	    $tot_days_worked_in_month=0;
	    $gross_monthly_sal=0;
	    $monthly_basic=0;
	    $prorata_gross=0;
	    $hra=0;
	    $conv_allowance=0;
	    $edu_allowance=0;
	    $special_allowance=0;
	    $upkeep_allowance=0;
	    $round_off=0;
	    $gross_earning=0;
	    $pf=0;
	    $esi=0;
	    $prof_tax=0;
	    $tot_deduction=0;
	    $sal_hand=0;
	    $pf_comp_contri=0;
	    $pension_fund=0;
	    $edli=0;
	    $adm_charge=0;
	    $adm_charge_eli=0;
	    $tot_company_contri_pf=0;
	    $company_contri_esi=0;
	    $ctc_per_month=0;
	    $is_active=1;
	   /* $created_at=0;
	    $created_by=0;
	    $modified_at="";
	    $modified_by="";*/
	    
	    $emp_deduct_info= $this->Employeemodel->get_employee_salmaster_deduct_info($empid);
	    $pf_opted=$emp_deduct_info['emp_pf_opted'];
	    $pf=1.0;
	    if(empty($pf_opted))
	    {
	        $pf=0.0;
	    }
	    else if($pf_opted=="N")
	    {
	        $pf=0.0;
	        
	    }
	    
	    $emp_id=$empid;
	    $tot_days_worked_in_month=$days_worked;
	    $gross_monthly_sal=$gross;
	    
	    $prorata_gross=$gross_monthly_sal *($days_worked/$days_in_month);
	    //var_dump($prorata_gross);
	    
	    $monthly_basic=round(($prorata_gross * 35)/100);
	    $hra=round(($monthly_basic * 30	)/100);
	    $conv_allowance=round(($prorata_gross * 18)/100);
	    $edu_allowance=round(($prorata_gross * 15)/100);
	    $special_allowance=round(($prorata_gross * 9.5)/100);
	    $upkeep_allowance=round(($prorata_gross * 12)/100);
	    $round_off=$prorata_gross-($monthly_basic+$hra+$conv_allowance+$edu_allowance+$special_allowance+$upkeep_allowance);
	    $gross_earning=($monthly_basic+$hra+$conv_allowance+$edu_allowance+$special_allowance+$upkeep_allowance)+$round_off;
	    
	    //PF
	    if($pf!=0.0)
	    {
	       $pf=round(($monthly_basic * 12	)/100);
	    }
	    if($gross_monthly_sal > 21000)
	    {
	        $esi=0;
	        
	    }
	    else
	    {
	        $esi=round(($gross_earning * 12	)/100);
	    }
	    
	    //professional tax starts
	    $prof_tax= 0.0;
	    if($gross_earning<=10000.00)
	    {
	        $prof_tax=0.0;
	    }
	    elseif($gross_earning<=25000)
	    {
	        $prof_tax=130.00;
	        
	    }
	    elseif($gross_earning<=40000)
	    {
	        $prof_tax=150.00;
	    }
	    else
	    {
	        $prof_tax=200.00;
	        
	    }
	    //professional tax ends
	    
	    
	    $adv_tds_val=$this->getsaltdsadvdata($empid,$month,$year,$created_by,$modified_by);
	    
	   // var_dump($adv_tds_val);
	   // exit;
	    
	    if(empty($adv_tds_val))
	    {
	        $advance=0.0;
	        $tds=0.0;
	        
	    }
	    else
	    {
	        $advance=$adv_tds_val['advance'];
	        $tds=$adv_tds_val['tds'];
	        //$tds=0.0;
	    }
	    
	    $tot_deduction=$pf+$esi+$prof_tax+$advance+$tds;
	    
	    $reimbursement=0.0;
	    $reimbursementval=array();
	    $reimbursementval=$this->Salarymodel->get_reimbursement_data($empid,$month,$year);
//	    //var_dump($reimbursementval);
	    if(!empty($reimbursementval))
	    {
	        $reimbursement=$reimbursementval['reimbursement'];
	        //$reimbursement=0;
	    }
	    
	   // echo  "reimbursement=".$reimbursement;
	    $sal_hand=$gross_earning+$reimbursement-$tot_deduction;
	    
	    //$tot_company_contri_pf=
	    
	    
	    if($pf==0.0)
	    {
	        $pension_fund=0.0;
	    }
	    elseif($monthly_basic>15000)
	    {
	        $pension_fund=(15000* 8.33)/100;
	        
	    }
	    else
	    {
	        $pension_fund=round(($monthly_basic* 8.33)/100);
	        
	    }
	    
	    $pf_comp_contri=$pf-$pension_fund;
	    
	    
	    //EDLI
	    
	    if($pf==0.0)
	    {
	        $edli=0.0;
	    }
	    elseif($monthly_basic>15000)
	    {
	        $edli=(15000.0*0.5)/100;
	        
	        
	        
	    }
	    else
	    {
	        $edli=round(($monthly_basic*0.5)/100);
	        
	        
	    }
	    //adm charge
	    
	    
	    
	    if($pf==0.0)
	    {
	        $adm_charge=0.0;
	    }
	    elseif($monthly_basic>15000)
	    {
	        $adm_charge=(15000.0*0.65)/100;
	        
	    }
	    else
	    {
	        $adm_charge=round(($monthly_basic*0.65)/100);
	    }
	    //adm_charge_eli
	    
	    if($monthly_basic==0)
	    {
	        $adm_charge_eli=0.0;
	    }
	    else
	    {
	        
	        
	        $rounded_val=round(($monthly_basic*0.01)/100);
	        
	        //echo "$rounded_val=" .$rounded_val;
	        $adm_charge_eli=max(array(1,$rounded_val));
	        
	        
	        
	    }
	    
	    $tot_company_contri_pf=$pf_comp_contri+$pension_fund+$edli+$adm_charge+$adm_charge_eli;
	    
	    //company's contribution to esi
	    
	    if($gross_monthly_sal>21000)
	    {
	        $company_contri_esi=0.0;
	        
	    }
	    else{
	        
	        $company_contri_esi=round(($prorata_gross*4.75)/100);
	    }
	    
	    $ctc_per_month=$gross_monthly_sal+$tot_company_contri_pf+$company_contri_esi;
	    
	   
	    
	    
	    $data_salary_details=array(
	        'emp_master_id' 			=>$emp_id,
	        'month' 					=>$month ,
	        'year' 						=>$year ,
	        'monthly_basic'         			=>$basic ,
	        'tot_days_worked_in_month'	=>$tot_days_worked_in_month,
	        'gross_monthly_sal' 		=>$gross_monthly_sal,
	        'monthly_basic' 			=>$monthly_basic,
	        'prorata_gross' 			=>$prorata_gross,
	        'hra' 						=>$hra,
	        'conv_allowance' 			=>$conv_allowance,
	        'edu_allowance' 			=>$edu_allowance,
	        'special_allowance' 		=>$special_allowance,
	        'upkeep_allowance' 			=>$upkeep_allowance,
	        'round_off' 				=>$round_off,
	        'gross_earning' 			=>$gross_earning,
	        'pf' 						=>$pf,
	        'esi' 						=>$esi,
	        'prof_tax' 					=>$prof_tax,
	        'tot_deduction' 			=>$tot_deduction,
	        'sal_hand' 					=>$sal_hand,
	        'pf_comp_contri'			=>$pf_comp_contri,
	        'pension_fund' 				=>$pension_fund,
	        'edli' 						=>$edli,
	        'adm_charge' 				=>$adm_charge,
	        'adm_charge_eli' 			=>$adm_charge_eli,
	        'tot_company_contri_pf' 	=>$tot_company_contri_pf,
	        'company_contri_esi' 		=>$company_contri_esi,
	        'ctc_per_month' 			=>$ctc_per_month,
	        'is_active' 				=>$is_active,
	        'created_by' 				=>$created_by,
	        'modified_by' 				=>$modified_by
	    );
	    $this->Salarymodel->insert_emp_sal_month($data_salary_details);
	    
	    
	    
	    
	}
	
	public function payrolldetails_get() {
	    
	    $this->load->model("Employeemodel");
	    $this->load->model("Referencemodel");
	    $this->load->model("Salarymodel");
	    $this->load->model("Attendancelog");
	    $this->load->model("Atttendancemonthlysummary");
	    $deduct=0;
	    
	    $month=$this->input->get("month");
	    $year=$this->input->get("year");
	    $process_id=$this->input->get("process_id");
	    $location_id=$this->input->get("location_id");
	    $emp_code=$this->input->get("emp_code");
	    $data['empsalinfo']=array();
	    if(!empty($emp_code))//get the details for the specific emp_code
	    {
	        $retdata= $this->Employeemodel->get_empmasterid_from_code($emp_code);
	        
	        $summary=$this->Atttendancemonthlysummary->get_summary_data($emp_code,$month,$year);
	        if(!empty($deduct))
	        {
	           $deduct=$summary[0]['emp_final_deduct'];
	           
	        }
	        
	        
	        $emp_master_id=$retdata['id'];
	        $datarow=$this->getpayrolldata($emp_master_id,$deduct,$month,$year);
	        //$data['employee']=array($datarow);
	        array_push($data['empsalinfo'],$datarow);
	    
	    }else{
	        //get employee list for process_id and location_id
	        
	        $group=array("srhr","admin");
	        $level=2; //this determines if the entire list to be shown or only selected ones depending on user access righgts
	        
	        if ($this->ion_auth->in_group($group))
	        {
	            $level=1;
	            
	        }
	        
	        $retdata= $this->Employeemodel->get_employee_list($process_id,$location_id,"",$level);
	        //var_dump($retdata);
	        $deduct=0; 
	        foreach ($retdata as $retdatarow)
	        {
	            $emp_code=$retdatarow['emp_code'];
	            $summary=$this->Atttendancemonthlysummary->get_summary_data($emp_code,$month,$year);
	            if(!empty($deduct))
	            {
	                $deduct=$summary[0]['emp_final_deduct'];
	                
	            }
	            $emp_master_id= $retdatarow['id'];
	           $datarow=$this->getpayrolldata($emp_master_id,$deduct,$month,$year);
	            array_push($data['empsalinfo'],$datarow);
	            
	        }
	    }
	    
	    
	   $this->response($data);
	    
	}
	
	public function genpayroll_post() {
	    
	    $this->load->model("Employeemodel");
	    $this->load->model("Referencemodel");
	    $this->load->model("Salarymodel");
	    $this->load->model("Attendancelog");
	    $this->load->model("Atttendancemonthlysummary");
	    
	    $month=$this->post("month");
	    $year=$this->post("year");
	    $process_id=$this->post("process_id");
	    $location_id=$this->post("location_id");
	    $emp_code=$this->post("emp_code");
	    $created_by=$this->post("created_by");
	    $modified_by=$this->post("modified_by");
	    $returndata=array();
	    if(!empty($emp_code))//get the details for the specific emp_code
	    {
	       $this->savepayrolldata($emp_code,$month,$year,$created_by,$modified_by);
	       $returndata['message']="Payroll generated successfully";
	       $returndata['code']="200";
	        
	    }
	    else{
	        //get employee list for process_id and location_id
	        
	        $group=array("srhr","admin");
	        $level=2; //this determines if the entire list to be shown or only selected ones depending on user access righgts
	        
	        if ($this->ion_auth->in_group($group))
	        {
	            $level=1;
	            
	        }
	        
	        $retdata= $this->Employeemodel->get_employee_list($process_id,$location_id,"",$level);
	        foreach ($retdata as $retdatarow)
	        {
	            
	            $emp_code= $retdatarow['emp_code'];
	            $this->savepayrolldata($emp_code,$month,$year,$created_by,$modified_by);
	            
	            
	        }
	        $returndata['message']="Payroll generated successfully";
	        $returndata['code']="200";   
	        
	              
	    }
	    
	    
	    $this->response($returndata);
	    
	}
     
	public function savepayrolldata($emp_code,$month,$year,$created_by,$modified_by)
	{
	    
	    $this->load->model("Employeemodel");
	    $this->load->model("Referencemodel");
	    $this->load->model("Salarymodel");
	    $this->load->model("Attendancelog");
	    $this->load->model("Atttendancemonthlysummary");
	    
	    $retdata= $this->Employeemodel->get_empmasterid_from_code($emp_code);
	    $days_worked=0;
	    $emp_master_id=$retdata['id'];
	    $days_in_month=cal_days_in_month(CAL_GREGORIAN,$month,$year);
	    //$empid,$month,$year,$gross,$days_in_month,$days_worked
	    // var_dump($numd);
	    $emp_earn_info= $this->Employeemodel->get_employee_salmaster_earn_info($emp_master_id);
	    $gross=$emp_earn_info['emp_gross_sal'];
	     
	    $monthsum=$this->Atttendancemonthlysummary->get_summary_data($emp_code,$month,$year);
	    if(!empty($monthsum))
	    {
	      $days_worked=$monthsum[0]['emp_final_attendance'];
	    }
	    $this->savesaltdsadvdata($emp_master_id,$month,$year,$gross,$days_in_month,$days_worked,$created_by,$modified_by);
	    
	    
	    
	}
	
	public function getpayrolldata($emp_master_id,$deduct,$month,$year)
	{
	    
	    $datarow=array();
	    $this->load->model("Employeemodel");
	    $this->load->model("Referencemodel");
	    $this->load->model("Salarymodel");
	    $this->load->model("Attendancelog");
	    $this->load->model("Atttendancemonthlysummary");
	        
	    $dataval=$this->Employeemodel->get_employee_details($emp_master_id);
	    
	    if(!empty($dataval))
	    {
	        
	        $datarow["created_by"]=$dataval['created_by'];
	        $datarow["modified_by"]=$dataval['modified_by'];
	        $datarow["emp_code"]=$dataval['emp_code'];
	        $days_in_month=cal_days_in_month(CAL_GREGORIAN,$month,$year);
	        $datarow["days_in_month"]=$days_in_month;
	        $personal_details=array(
	            
	            "emp_title"=>$dataval['emp_title'],
	            "emp_desig_id"=>$dataval['emp_desig_id'],
	            "process_id"=>$dataval['process_id'],
	            "loc_id"=>$dataval['loc_id'],
	            "emp_fa_hus_name"=>$dataval['emp_fa_hus_name'],
	            "emp_marital_status"=>$dataval['emp_marital_status'],
	            "emp_blood_grp"=>$dataval['emp_blood_grp'],
	            "emp_sub_function_id"=>$dataval['emp_sub_function_id'],
	            "emp_dob"=>$dataval['emp_dob'],
	            "emp_gender"=>$dataval['emp_gender'],
	            "emp_first"=>$dataval['emp_first'],
	            "emp_mid"=>$dataval['emp_mid'],
	            "emp_last"=>$dataval['emp_last'],
	            "emp_add1_id"=>$dataval['emp_add1_id'],
	            "emp_add_2_id"=>$dataval['emp_add_2_id'],
	            "emp_city_dist"=>$dataval['emp_city_dist'],
	            "emp_state"=>$dataval['emp_state'],
	            "emp_pin"=>$dataval['emp_pin'],
	            "emp_contact_num"=>$dataval['emp_contact_num'],
	            "emp_alt_contact_num"=>$dataval['emp_alt_contact_num'],
	            "is_active"=>$dataval['is_active'],
	            "emp_is_active"=>$dataval['emp_is_active']
	            
	            
	        );
	        $datarow["personal_details"]= $personal_details;
	        
	        $bankinfoval=$this->Employeemodel->get_employee_bank_info($emp_master_id);
	        
	        
	        $bank_info=array(
	            "id" =>$bankinfoval['id'],
	            "emp_master_id" =>$bankinfoval['emp_master_id'],
	            "emp_bank_name" =>$bankinfoval['emp_bank_name'],
	            "emp_bank_ac_number" =>$bankinfoval['emp_bank_ac_number'],
	            "emp_bank_ifsc_code" =>$bankinfoval['emp_bank_ifsc_code']
	        );
	        $datarow["employee_bank_info"]= $bank_info;
	        
	        
	        $pfesicval=$this->Employeemodel->get_employee_pfesic_info($emp_master_id);
	        $employee_pf_esic_info=array(
	            "id" =>$pfesicval['id'],
	            "emp_master_id" =>$pfesicval['emp_master_id'],
	            "emp_pf_number" =>$pfesicval['emp_pf_number'],
	            "emp_esic_ac_number" =>$pfesicval['emp_esic_ac_number'],
	            "emp_uan_number" =>$pfesicval['emp_uan_number']
	        );
	        $datarow["employee_pf_esic_info"]= $employee_pf_esic_info;
	       
	        
	        $salmonthval=$this->Salarymodel->get_sal_month_data($emp_master_id,$month,$year);
	        //var_dump($salmonthval);
	        //exit;
	        
	        $sal_month_info=array(	            
	            "id"=>$salmonthval['id'],
	            "emp_master_id"=>$salmonthval['emp_master_id'],
	            "month"=>$salmonthval['month'],
	            "year"=>$salmonthval['year'],
	            "tot_days_worked_in_month"=>$salmonthval['tot_days_worked_in_month'],
	            "gross_monthly_sal"=>$salmonthval['gross_monthly_sal'],
	            "monthly_basic"=>$salmonthval['monthly_basic'],
	            "prorata_gross"=>$salmonthval['prorata_gross'],
	            "hra"=>$salmonthval['hra'],
	            "conv_allowance"=>$salmonthval['conv_allowance'],
	            "edu_allowance"=>$salmonthval['edu_allowance'],
	            "special_allowance"=>$salmonthval['special_allowance'],
	            "upkeep_allowance"=>$salmonthval['upkeep_allowance'],
	            "round_off"=>$salmonthval['round_off'],
	            "gross_earning"=>$salmonthval['gross_earning'],
	            "pf"=>$salmonthval['pf'],
	            "esi"=>$salmonthval['esi'],
	            "prof_tax"=>$salmonthval['prof_tax'],
	            "tot_deduction"=>$salmonthval['tot_deduction'],
	            "sal_hand"=>$salmonthval['sal_hand'],
	            "pf_comp_contri"=>$salmonthval['pf_comp_contri'],
	            "pension_fund"=>$salmonthval['pension_fund'],
	            "edli"=>$salmonthval['edli'],
	            "adm_charge"=>$salmonthval['adm_charge'],
	            "adm_charge_eli"=>$salmonthval['adm_charge_eli'],
	            "tot_company_contri_pf"=>$salmonthval['tot_company_contri_pf'],
	            "company_contri_esi"=>$salmonthval['company_contri_esi'],
	            "ctc_per_month"=>$salmonthval['ctc_per_month'],
	            "is_active"=>$salmonthval['is_active'],
	            "daysdeduct"=>$deduct
	            );
	        
	        $datarow["sal_month_info"]= $sal_month_info;
	        
	        
	        $advtdsval=$this->Salarymodel->get_sal_adv_tds_data($emp_master_id,$month,$year);
	     
	        if(!empty($advtdsval))
	        {
	        
	          $emp_sal_adv_tds=array(
	            "id"=>$advtdsval['id'],
	            "emp_master_id"=>$advtdsval['emp_master_id'],
	            "month"=>$advtdsval['month'],
	            "year"=>$advtdsval['year'],
	            "advance"=>$advtdsval['advance'],
	            "tds"=>$advtdsval['tds'],
	            "is_active"=>$advtdsval['is_active']
	            
	        );       
	        }else 
	        {
	          
	            
	            $emp_sal_adv_tds=array(
	                "id"=>null,
	                "emp_master_id"=>$emp_master_id,
	                "month"=>$month,
	                "year"=>$year,
	                "advance"=>0.0,
	                "tds"=>0.0,
	                "is_active"=>0
	                
	            );       
	            
	            
	            
	        }
	        $datarow["emp_sal_adv_tds"]= $emp_sal_adv_tds;
	        
	        $reimbval=$this->Salarymodel->get_reimbursement_data($emp_master_id,$month,$year);
	        
	        if(!empty($reimbval))
	        {
	            
	            
	           $emp_sal_reimbursement=array(
	               "id"=>$reimbval['id'],
	               "emp_master_id"=>$reimbval['emp_master_id'],
	               "month"=>$reimbval['month'],
	               "year"=>$reimbval['year'],
	               "reimbursement"=>$reimbval['reimbursement'],	           
	               "is_active"=>$reimbval['is_active'] ); 
	        
	        }else{
	            
	            $emp_sal_reimbursement=array(
	                "id"=>null,
	                "emp_master_id"=>$emp_master_id,
	                "month"=>$month,
	                "year"=>$year,
	                "reimbursement"=>0.0,
	                "is_active"=>0	                
	            );       
	            
	        }
	        $datarow["emp_sal_reimbursement"]= $emp_sal_reimbursement;
	        return $datarow;
	
	
	}
	}
	
	public function savepayrollweb_post() {
	    
	    $this->load->model("Employeemodel");
	    $this->load->model("Referencemodel");
	    $this->load->model("Salarymodel");
	    
	    
	    $data=$this->post("empsalinfo");
	    $emp_sal_adv_tdsdata=$data['emp_sal_adv_tds'];
	    $emp_sal_reimbursement_data=$data['emp_sal_reimbursement'];
	    if(!empty($emp_sal_adv_tdsdata))
	    {
	        
	        $datatosave=array(
	            "emp_master_id"=>$emp_sal_adv_tdsdata['emp_master_id'],
	            "month"=> $emp_sal_adv_tdsdata['month'],
	            "year"=> $emp_sal_adv_tdsdata['year'],
	            "advance"=> $emp_sal_adv_tdsdata['advance'],
	            "tds"=> $emp_sal_adv_tdsdata['tds'],
	            "is_active"=> 1	            
	        );
	        $this->Salarymodel->merge_adv_tds_data($datatosave);
	        
	    }
	    
	    if(!empty($emp_sal_reimbursement_data)){
	    $reimbdatatosave=array(
	        "emp_master_id"=>$emp_sal_reimbursement_data['emp_master_id'],
	        "month"=> $emp_sal_reimbursement_data['month'],
	        "year"=> $emp_sal_reimbursement_data['year'],
	        "reimbursement"=> $emp_sal_reimbursement_data['reimbursement'],	         
	        "is_active"=> 1
	    );
	    
	    $this->Salarymodel-> merge_emp_sal_reimbursement($reimbdatatosave);
	    
	    }
	    
	    $returndata['message']="Data Saved Successfully";
	    $returndata['code']="200";
	    
	    $this->response($returndata);
	    
	}
}
?>