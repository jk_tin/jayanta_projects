<div class="navbar-default sidebar" role="navigation" style="margin-top: 71px!important;">
   <!-- sidebar-collapse -->
   <div class="sidebar-nav navbar-collapse">
      <ul class="nav" id="side-menu">
      	<li ng-repeat="menu in menuList">
      		<a ng-href="<?php echo base_url(); ?>{{menu.url}}">
                  <i class="fa" ng-class="menu.image"></i>
                  <span class="nav-label">{{menu.menu_desc}}</span>
                  </a>
      	</li>
      
      
      
         <!-- <li ui-sref-active="active" class="active">
            <a href="<?php echo base_url(); ?>index.php/Dashboard/index">
            <i class="fa fa-dashboard"></i>
            <span class="nav-label">Dashboard</span>
            </a>
         </li>
         <li ui-sref-active="active">
            <a href="<?php echo base_url(); ?>index.php/Employees/index">
            <i class="fa fa-table"></i>
            <span class="nav-label">Employees</span>
            </a>
         </li>
         <li ui-sref-active="active">
            <a href="<?php echo base_url(); ?>index.php/Attendance/index">
            <i class="fa fa-check"></i>
            <span class="nav-label">Attendance</span>
            </a>
         </li>
         <li ui-sref-active="active">
            <a href="<?php echo base_url(); ?>index.php/Salary/index">
            <i class="fa fa-inr"></i>
            <span class="nav-label">Salary Initiation</span>
            </a>
         </li>
         <li ui-sref-active="active">
            <a href="<?php echo base_url(); ?>index.php/CreateUser/index">
            <i class="fa fa-user"></i>
            <span class="nav-label">Create User</span>
            </a>
         </li>
         <li ui-sref-active="active">
            <a href="<?php echo base_url(); ?>index.php/UpdateAdvance/index">
            <i class="fa fa-edit"></i>
            <span class="nav-label">Update Advance / TDS / Reimbursement</span>
            </a>
         </li> -->
      </ul>
   </div>
   <!-- /.sidebar-collapse -->
</div>