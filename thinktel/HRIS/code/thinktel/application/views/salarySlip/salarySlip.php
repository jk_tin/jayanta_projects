<!DOCTYPE html>
<html lang="en" ng-app="employeeApp">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Thinktel</title>
      <!-- CSS -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/bootstrap.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/metisMenu.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/main.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/sb-admin-2.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/angular-moment-picker.min.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/font-awesome.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/nv.d3.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/animate.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/loading-bars.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/footable.core.min.css" />
   </head>
   <body>
      <!-- Simple splash screen-->
      <input id="empLevel" type ="hidden" id="empid" value ="<?php echo $emp_level ?>"/>
      <input id="empCode" type ="hidden" id="empid" value ="<?php echo $emp_code ?>"/>

      <div class="splash">
         <div class="color-line"></div>
         <div class="splash-title">
            <img src="<?php echo base_url(); ?>assets/vendor/images/loading-bars.svg" width="64" height="64" />
         </div>
      </div>
      <div id="wrapper">
         <!-- Simple splash screen-->
      <div class="splash">
         <div class="color-line"></div>
         <div class="splash-title">
            <h1>Thinktel</h1>
            <img src="<?php echo base_url(); ?>assets/vendor/images/loading-bars.svg" width="64" height="64" />
         </div>
      </div>
      <div id="wrapper">
         <!-- Start Navigation Area -->
         <?php
            $this->load->view('share/leftHeader');
            ?>
         <!-- End Navigation Area -->
         <!-- Content Area -->
         <!-- ng-class with current state name give you the ability to extended customization your view -->
         <div id="page-wrapper" class="gray-bg {{$state.current.name}}">
            <!-- Nested view  -->
            <div class="template animated slideInRight" ng-controller="salarySlipController as vm" ng-init="vm.init()">
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                     <h2>Payslip</h2>
                     <ol class="breadcrumb">
                        <li><a ui-sref="home.dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active"><i class="fa fa-table"></i> {{vm.title}}</li>
                     </ol>
                  </div>
               </div>

               <div class="row" style="margin-bottom: 10px;">
                  <div class="col-sm-12">
                      <div class="form-group col-sm-3">
                        <label>Date:</label>
                        <div class="input-group"
                            moment-picker="vm.downloadParameter.currentDate"
                            start-view="day"
                            format="MMMM-YYYY">
                            <span class="input-group-addon">
                            <i class="fa fa-calendar fa-fw"></i>
                            </span>
                            <input class="form-control" ng-model="vm.downloadParameter.currentDate" ng-model-options="{ updateOn: 'blur' }">
                        </div>
                      </div>
                      <div class="form-group col-sm-2">
                        <label for="email">Employee Code:</label>
                        <input type="text" ng-model="vm.downloadParameter.emp_code" class="form-control" placeholder="Employee code" ng-disabled="vm.empLevel != 1">
                      </div>
                      <div class="form-group col-sm-2" ng-if="vm.empLevel == 1">
                        <label>process:</label>
                        <select class="form-control" ng-model="vm.downloadParameter.process_id" ng-change="vm.getLoationList(vm.downloadParameter.process_id)">
                          <option value="">--Select Process--</option>
                          <option ng-repeat="process in vm.processMaster" value={{process.id}}>{{process.process_name}}</option>
                        </select>
                      </div>
                      <div class="form-group col-sm-2" ng-if="vm.empLevel == 1">
                        <label>Location:</label>
                        <select class="form-control" ng-model="vm.downloadParameter.location_id" ng-disabled="vm.downloadParameter.process_id == ''">
                          <option value="">--Select Location--</option>
                          <option ng-repeat="location in vm.locationMaster" value={{location.id}}>{{location.location_name}}</option>
                        </select>
                      </div>
                      <div class="form-group col-sm-2">
                        <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <button type="button" class="btn btn-primary pull-right" ng-click="vm.downloadPayslipForAdmin()"><i class="fa fa-download"></i> &nbsp; Download</button>
                      </div>
                  </div>
               </div>
               <!-- /.row -->

            </div>
         </div>
      </div>
      <!-- End Content Area -->
      </div>
        <script src="<?php echo base_url(); ?>assets/vendor/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/js/metisMenu.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/js/angular.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/js/angular-ui-router.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/js/d3.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/js/nv.d3.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/js/angular-nvd3.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/js/moment.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/js/footable.all.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/js/angular-footable.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/js/sb-admin-2.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/js/moment-with-locales.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/js/angular-moment-picker.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/js/loading-bars.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/js/bootbox.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/js/ng-file-upload.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/salarySlip/salarySlipController.js"></script>
        <script src="<?php echo base_url(); ?>assets/salarySlip/salarySlipService.js"></script>

        <script src="<?php echo base_url(); ?>assets/common/service.js"></script>
        <script src="<?php echo base_url(); ?>assets/common/sidebarDirective.js"></script>
        <script src="<?php echo base_url(); ?>assets/common/headerController.js"></script>
   </body>
</html>