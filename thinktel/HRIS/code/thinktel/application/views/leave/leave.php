<!DOCTYPE html>
<html lang="en" ng-app="employeeApp">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Thinktel</title>
      <!-- CSS -->
      <link rel="stylesheet" href="./assets/vendor/vendor-css/bootstrap.min.css" />
      <link rel="stylesheet" href="./assets/vendor/vendor-css/metisMenu.min.css" />
      <link rel="stylesheet" href="./assets/vendor/css/main.css">
      <link rel="stylesheet" href="./assets/vendor/css/sb-admin-2.css">
      <link rel="stylesheet" href="./assets/vendor/css/angular-moment-picker.min.css">
      <link rel="stylesheet" href="./assets/vendor/vendor-css/font-awesome.min.css" />
      <link rel="stylesheet" href="./assets/vendor/vendor-css/nv.d3.min.css" />
      <link rel="stylesheet" href="./assets/vendor/css/animate.css" />
      <link rel="stylesheet" href="./assets/vendor/css/loading-bars.css" />
      <link rel="stylesheet" href="./assets/vendor/vendor-css/footable.core.min.css" />
   </head>
   <body>
      <!-- Simple splash screen-->
      <div class="splash">
         <div class="color-line"></div>
         <div class="splash-title">
            <img src="./assets/vendor/images/loading-bars.svg" width="64" height="64" />
         </div>
      </div>
      <div id="wrapper">
         <!-- Navigation Area -->
         <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <!-- navbar-header -->
            <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="index.html">Thinktel</a>
            </div>
            <!-- /.navbar-header -->
            <!-- navbar-top-links -->
            <ul class="nav navbar-top-links navbar-right">
               <!-- dropdown envelope icon -->
               <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
                  </a>
                  <!-- dropdown-messages -->
                  
                  <!-- /.dropdown-messages -->
               </li>
               <!-- /.dropdown envelope icon -->
               <!-- dropdown user icon -->
               <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                  </a>
                  <!-- dropdown-user -->
                  <ul class="dropdown-menu dropdown-user">
                     <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                     </li>
                     <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                     </li>
                     <li class="divider"></li>
                     <li><a href="login"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                     </li>
                  </ul>
                  <!-- /.dropdown-user -->
               </li>
               <!-- /.dropdown user icon -->
            </ul>
            <!-- /.navbar-top-links -->
            <!-- navbar-static-side -->
            <div class="navbar-default sidebar" role="navigation">
               <!-- sidebar-collapse -->
               <div class="sidebar-nav navbar-collapse">
                  <ul class="nav" id="side-menu">
                     <li ui-sref-active="active">
                        <a href="dashboard">
                        <i class="fa fa-dashboard"></i>
                        <span class="nav-label">Dashboard</span>
                        </a>
                     </li>
                     <li ui-sref-active="active">
                        <a href="employees">
                        <i class="fa fa-table"></i>
                        <span class="nav-label">Employees</span>
                        </a>
                     </li>
                     <li ui-sref-active="active">
                        <a href="attendance">
                        <i class="fa fa-check"></i>
                        <span class="nav-label">Attendance</span>
                        </a>
                     </li>
                     <li ui-sref-active="active" class="active">
                        <a href="#salary-initiate">
                        <i class="fa fa-inr"></i>
                        <span class="nav-label">Salary Initiation</span>
                        </a>
                     </li>
                  </ul>
               </div>
               <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
         </nav>
         <!-- End Navigation Area -->
         <!-- Content Area -->
         <!-- ng-class with current state name give you the ability to extended customization your view -->
         <div id="page-wrapper" class="gray-bg {{$state.current.name}}" ng-controller="leaveDetailController as vm" ng-init="vm.init()">
            <!-- Nested view  -->
            <div class="tempLate animated slideInRight">
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                     <h2>{{vm.title}}</h2>
                     <ol class="breadcrumb">
                        <li><a ui-sref="home.dashboard"><i class="fa fa-user"></i> profile</a></li>
                     </ol>
                  </div>
               </div>
               <div class="teacher-name" style="padding-top:20px;">
                    <div class="row" style="margin-top:0px;">
                    <div class="col-md-9">
                    <h2 style="font-size:38px"><strong>Aloke Kantal</strong></h2>
                    </div>                    
                    </div>
                </div>
               <div class="row" style="margin-top:20px;">
                    <div class="col-md-3"> <!-- Image -->
                    <a href="#"> <img class="rounded-circle" src="./assets/vendor/images/profile.jpg" alt="Kamal" style="width:200px;height:200px"></a>
                    </div>

                    <div class="col-md-6"> <!-- Rank & Qualifications -->
                    <h5 style="color:#3AAA64">Associate Professor, <small>Dept. of CSE, Jatiya Kabi Kazi Nazrul Islam University</small></h5>
                    <p>PhD (On study at BUET), M.Sc. in research on ICT(UPC, Spain), M.Sc. in research on ICT(UCL, Belgium).</p>
                    <p>Address: Namapara, Trishal, Mymensingh</p>
                    </div>

                    <div class="col-md-3 text-center"> <!-- Phone & Social -->
                    <span class="number" style="font-size:18px">Phone:<strong>+919903321885</strong></span>
                    <span class="number" style="font-size:14px">Email:<strong>alokekantalcu@gmail.com</strong></span>
                    </div>
                </div>               
            </div>            
         </div>
         <!-- End Content Area -->
      </div>
      <script src="./assets/vendor/js/jquery.min.js"></script>
      <script src="./assets/vendor/js/bootstrap.min.js"></script>
      <script src="./assets/vendor/js/metisMenu.min.js"></script>
      <script src="./assets/vendor/js/angular.min.js"></script>
      <script src="./assets/vendor/js/angular-ui-router.min.js"></script>
      <script src="./assets/vendor/js/d3.min.js"></script>
      <script src="./assets/vendor/js/nv.d3.min.js"></script>
      <script src="./assets/vendor/js/angular-nvd3.min.js"></script>
      <script src="./assets/vendor/js/moment.min.js"></script>
      <script src="./assets/vendor/js/footable.all.min.js"></script>
      <script src="./assets/vendor/js/angular-footable.min.js"></script>
      <script src="./assets/vendor/js/sb-admin-2.js"></script>
      <script src="./assets/vendor/js/moment-with-locales.js"></script>
      <script src="./assets/vendor/js/angular-moment-picker.min.js"></script>


      <script src="./assets/vendor/js/loading-bars.js"></script>
      <script src="./assets/profile/leaveDetailController.js"></script>
      <script src="./assets/profile/leaveDetailService.js"></script>
   </body>
</html>