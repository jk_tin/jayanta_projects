<!DOCTYPE html>
<html lang="en" ng-app="employeeApp">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Thinktel</title>
      <!-- CSS -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/bootstrap.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/metisMenu.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/main.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/sb-admin-2.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/angular-moment-picker.min.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/font-awesome.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/nv.d3.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/animate.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/loading-bars.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/footable.core.min.css" />
   </head>
   <body>
      <!-- Simple splash screen-->
      <div class="splash">
         <div class="color-line"></div>
         <div class="splash-title">
            <h1>Thinktel</h1>
            <img src="<?php echo base_url(); ?>assets/vendor/images/loading-bars.svg" width="64" height="64" />
         </div>
      </div>
      <div id="wrapper">
         <!-- Start Navigation Area -->
         <?php
            $this->load->view('share/leftHeader');
         ?>
         <!-- End Navigation Area -->
         <!-- Content Area -->
         <!-- ng-class with current state name give you the ability to extended customization your view -->
         <div id="page-wrapper" class="gray-bg {{$state.current.name}}">
            <!-- Nested view  -->
            <div class="template animated slideInRight" ng-controller="EmployeeController as vm" ng-init="vm.init()">
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                     <h2>Employees</h2>
                     <ol class="breadcrumb">
                        <li><a ui-sref="home.dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active"><i class="fa fa-table"></i> {{vm.title}}</li>
                     </ol>
                  </div>
               </div>
               <!-- /.row -->
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 5px;margin-top: -15px;">
                     <button ng-click="vm.addEmployee()" type="button" class="btn btn-primary pull-right">Add Employee</button>
                  </div>
               </div>
               <div class="row" style="padding-bottom: 10px;">
                     <div class="col-sm-2">
                        <input type="text" ng-model="vm.filter.emp_code" class="form-control" placeholder="employee Id">
                     </div>
                     <div class="col-sm-3">
                        <select class="form-control" ng-model="vm.filter.process_id" ng-change="vm.getLoationList()">
                            <option value="">--Select Process--</option>
                            <option ng-repeat="process in vm.processMaster" value={{process.id}}>{{process.process_name}}</option>
                        </select>
                     </div>
                     <div class="col-sm-3">
                        <select class="form-control" ng-model="vm.filter.location_id" ng-disabled="vm.locationMaster.length == 0">
                            <option value="">--Select Location--</option>
                            <option ng-repeat="location in vm.locationMaster" value={{location.id}}>{{location.location_name}}</option>
                        </select>
                     </div>
                     <div class="col-sm-2">
                        <button class="btn pull-left" ng-click="vm.applyFilter()"><i class="fa fa-search"></i> Search</button>
                     </div>
                  </div>
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                     <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                           <thead>
                              <tr>
                                 <th>Emp Code</th>
                                 <th>Name</th>
                                 <th>Designation</th>
                                 <th>D.O.B.</th>
                                 <th>Process</th>
                                 <th>Status</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr class="odd gradeX" ng-repeat="employee in vm.employeeList track by $index">
                                 <td>{{employee.emp_code}}</td>
                                 <td>{{employee.emp_name}}</td>
                                 <td>{{employee.emp_desig_desc}}</td>
                                 <td>{{employee.emp_dob | date: 'dd-MM-yyyy'}}</td>
                                 <td>{{employee.process_name}}</td>
                                 <td><span ng-if="employee.status == 1">Active</span><span ng-if="employee.status == 0">Inactive</span></td>
                                 <td>
                                    <!--<a ui-sref="#" data-toggle="modal" data-target="#employeePreview" class="pointer"><i class="fa fa-eye fa-fw"></i></a>-->
                                    <a href="javascript:void(0)" ng-click="vm.editEmployee(employee.id)" class="btn btn-primary btn-sm pointer"><i class="fa fa-pencil fa-fw"></i></a>
                                    <!--<a ui-sref="#" class="pointer"><i class="fa fa-trash-o fa-fw"></i></a>-->
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            
            	<div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 5px;margin-top: -15px;">
                     <button ng-click="vm.addEmployee()" type="button" class="btn btn-primary pull-right">Add Employee</button>
                  </div>
               </div>
            </div>            
            <!-- Modal -->
            <div id="employeePreview" class="modal fade" role="dialog">
               <div class="modal-dialog modal-lg">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Employee Preview</h4>
                     </div>
                     <div class="modal-body">
                        <p>Some text in the modal.</p>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- End Content Area -->
      </div>
      <script src="<?php echo base_url(); ?>assets/vendor/js/jquery.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/metisMenu.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-ui-router.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/d3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/nv.d3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-nvd3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/moment.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/footable.all.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-footable.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/sb-admin-2.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/moment-with-locales.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-moment-picker.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/loading-bars.js"></script>
      <script src="<?php echo base_url(); ?>assets/employee/employee.controller.js"></script>
      <script src="<?php echo base_url(); ?>assets/employee/employee.service.js"></script>

      <script src="<?php echo base_url(); ?>assets/common/service.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/sidebarDirective.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/headerController.js"></script>

   </body>
</html>