<!DOCTYPE html>
<html lang="en" ng-app="employeeApp">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Thinktel</title>
      <!-- CSS -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/bootstrap.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/metisMenu.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/main.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/sb-admin-2.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/angular-moment-picker.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/font-awesome.min.css" />-->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/nv.d3.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/animate.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/loading-bars.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/footable.core.min.css" />
   </head>
   <body>
      <!-- Simple splash screen-->
      <div class="splash">
         <div class="color-line"></div>
         <div class="splash-title">
            <img src="<?php echo base_url(); ?>assets/vendor/images/loading-bars.svg" width="64" height="64" />
         </div>
      </div>
      <div id="wrapper">
         <!-- Start Navigation Area -->
         <?php
            $this->load->view('share/leftHeader');
            ?>
         <!-- End Navigation Area -->
         <!-- Content Area -->
         <!-- ng-class with current state name give you the ability to extended customization your view -->
         <div id="page-wrapper" class="gray-bg {{$state.current.name}}" ng-controller="updateAdvanceController as vm" ng-init="vm.init(1)">
            <!-- Nested view  -->
            <div class="tempLate animated slideInRight">
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                     <h2>Update Advance and TDS</h2>
                     <ol class="breadcrumb">
                        <li><a ui-sref="home.dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active"><i class="fa fa-table"></i> {{vm.title}}</li>
                     </ol>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                     <form class="form-horizontal" role="form" name="parsonalDetailForm" novalidate>
                        <fieldset class="the-fieldset" style="margin-top: 20px;">
                           <legend class="the-legend">Detail</legend>
                           <div class="form-group">
                              <label class="col-sm-2 col-xs-2 control-label">Year Month: :</label>
                              <div class="col-sm-3 col-xs-10">
                                 <div class="input-group"
                                        moment-picker="vm.filter.yearMonth"
                                        start-view="month"
                                        format="MMMM-YYYY">
                                        <span class="input-group-addon">
                                        <i class="fa fa-calendar fa-fw"></i>
                                        </span>
                                        <input class="form-control"
                                        placeholder="Select a date"
                                        ng-model="vm.filter.yearMonth"
                                        ng-model-options="{ updateOn: 'blur' }">
                                </div>
                              </div>
                              <label class="col-sm-2 col-xs-2 control-label">Employee Code :</label>
                              <div class="col-sm-3 col-xs-10">
                                 <input type="text" ng-model="vm.filter.emp_code" class="form-control" placeholder="Employee code">
                              </div>
                              <div class="col-sm-2 col-xs-10">
                                 <button class="btn pull-right" ng-click="vm.search()"><i class="fa fa-search"></i> Search</button>
                              </div>
                           </div>


                           <div class="form-group">
                              <label class="col-sm-2 col-xs-2 control-label">Emp. Code :</label>
                              <div class="col-sm-4 col-xs-10">
                                <input type="text" ng-model="vm.advanceDetail.emp_code" class="form-control" disabled placeholder="Emp. Code">
                              </div>
                              <label class="col-sm-2 col-xs-2 control-label">Emp. Name :</label>
                              <div class="col-sm-4 col-xs-10">
                                <input type="text" ng-model="vm.advanceDetail.emp_name" class="form-control" disabled placeholder="Emp. Name">
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 col-xs-2 control-label">Advance :</label>
                              <div class="col-sm-4 col-xs-10">
                                 <input type="number" ng-model="vm.advanceDetail.emp_sal_adv_tds.advance" class="form-control" placeholder="Advance">
                              </div>
                              <label class="col-sm-2 col-xs-2 control-label">TDS :</label>
                              <div class="col-sm-4 col-xs-10">
                                 <input type="number" ng-model="vm.advanceDetail.emp_sal_adv_tds.tds" class="form-control" placeholder="TDS">
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 col-xs-2 control-label">Reimbursement:</label>
                              <div class="col-sm-4 col-xs-10">
                                 <input type="number" ng-model="vm.advanceDetail.emp_sal_reimbursement.reimbursement" class="form-control" placeholder="Reimbursement">
                              </div>
                           </div>
                           <div style="margin-top: 20px;">
                              <button type="submit" class="btn btn-primary  pull-right" ng-click="vm.saveAdvance()">
                              <i class="fa fa-save"></i> Save
                              </button>
                           </div>
                        </fieldset>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- End Content Area -->
      </div>
      <script src="<?php echo base_url(); ?>assets/vendor/js/jquery.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/metisMenu.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-ui-router.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/d3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/nv.d3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-nvd3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/moment.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/footable.all.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-footable.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/sb-admin-2.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/moment-with-locales.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-moment-picker.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/loading-bars.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/lodash.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/bootbox.min.js"></script>

      <script src="<?php echo base_url(); ?>assets/updateAdvance/updateAdvanceController.js"></script>
      <script src="<?php echo base_url(); ?>assets/updateAdvance/updateAdvanceService.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/service.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/sidebarDirective.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/headerController.js"></script>
      <script>
         function toggleIcon(e) {
             $(e.target)
                     .prev('.panel-heading')
                     .find(".collapse-icon")
                     .toggleClass('fa-angle-down fa-angle-up');
             }
             $('.panel-group').on('hidden.bs.collapse', toggleIcon);
             $('.panel-group').on('shown.bs.collapse', toggleIcon);
      </script>
   </body>
</html>