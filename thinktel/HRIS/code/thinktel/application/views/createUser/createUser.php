<!DOCTYPE html>
<html lang="en" ng-app="employeeApp">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Thinktel</title>
      <!-- CSS -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/bootstrap.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/metisMenu.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/main.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/sb-admin-2.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/angular-moment-picker.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/font-awesome.min.css" />-->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/nv.d3.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/animate.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/loading-bars.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/footable.core.min.css" />
   </head>
   <body>
      <!-- Simple splash screen-->
      <div class="splash">
         <div class="color-line"></div>
         <div class="splash-title">
            <img src="<?php echo base_url(); ?>assets/vendor/images/loading-bars.svg" width="64" height="64" />
         </div>
      </div>
      <div id="wrapper">
         <!-- Start Navigation Area -->
         <?php
            $this->load->view('share/leftHeader');
            ?>
         <!-- End Navigation Area -->
         <!-- Content Area -->
         <!-- ng-class with current state name give you the ability to extended customization your view -->
         <div id="page-wrapper" class="gray-bg {{$state.current.name}}" ng-controller="createUserController as vm" ng-init="vm.init(1)">
            <!-- Nested view  -->
            <div class="tempLate animated slideInRight">
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                     <h2>Create User</h2>
                     <ol class="breadcrumb">
                        <li><a ui-sref="home.dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active"><i class="fa fa-table"></i> {{vm.title}}</li>
                     </ol>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                     <form class="form-horizontal" role="form" name="parsonalDetailForm" novalidate>
                        <fieldset class="the-fieldset" style="margin-top: 20px;">
                           <legend class="the-legend">User Detail</legend>
                           <div class="form-group">
                              <label class="col-sm-2 col-xs-2 control-label">First Name :</label>
                              <div class="col-sm-4 col-xs-10">
                                 <input type="text" ng-model="vm.creteUserModel.first_name" class="form-control" placeholder="First Name">
                              </div>
                              <label class="col-sm-2 col-xs-2 control-label">Last Name :</label>
                              <div class="col-sm-4 col-xs-10">
                                 <input type="text" ng-model="vm.creteUserModel.last_name" class="form-control" placeholder="Last Name">
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 col-xs-2 control-label">User ID :</label>
                              <div class="col-sm-4 col-xs-10">
                                 <input type="text" ng-model="vm.creteUserModel.identity" class="form-control" placeholder="User ID">
                              </div>
                              <label class="col-sm-2 col-xs-2 control-label">Email :</label>
                              <div class="col-sm-4 col-xs-10">
                                 <input type="text" ng-model="vm.creteUserModel.email" class="form-control" placeholder="Email">
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 col-xs-2 control-label">Password:</label>
                              <div class="col-sm-4 col-xs-10">
                                 <input type="text" ng-model="vm.creteUserModel.password" class="form-control" placeholder="Password">
                              </div>
                              <label class="col-sm-2 col-xs-2 control-label" style="padding-left: 5px;">Confirm Password :</label>
                              <div class="col-sm-4 col-xs-10">
                                 <input type="text" ng-model="vm.creteUserModel.cPassword" class="form-control" placeholder="Confirm Password">
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 col-xs-2 control-label">Group:</label>
                              <div class="col-sm-4 col-xs-10">
                                 <select class="form-control" ng-model="vm.creteUserModel.group">
                                    <option value="">--Select Group--</option>
                                    <option ng-repeat="group in vm.userGroupList" value={{group.id}}>{{group.description}}</option>
                                 </select>
                              </div>
                           </div>
                           <div style="margin-top: 20px;">
                              <button type="submit" class="btn btn-primary  pull-right" ng-click="vm.createUser()">
                              <i class="fa fa-save"></i> Save
                              </button>
                           </div>
                        </fieldset>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- End Content Area -->
      </div>
      <script src="<?php echo base_url(); ?>assets/vendor/js/jquery.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/metisMenu.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-ui-router.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/d3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/nv.d3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-nvd3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/moment.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/footable.all.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-footable.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/sb-admin-2.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/moment-with-locales.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-moment-picker.min.js"></script>      
      <script src="<?php echo base_url(); ?>assets/vendor/js/loading-bars.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/lodash.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/bootbox.min.js"></script>

      <script src="<?php echo base_url(); ?>assets/createUser/createUserController.js"></script>
      <script src="<?php echo base_url(); ?>assets/createUser/createUserService.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/service.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/sidebarDirective.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/headerController.js"></script>
      <script>
         function toggleIcon(e) {
             $(e.target)
                     .prev('.panel-heading')
                     .find(".collapse-icon")
                     .toggleClass('fa-angle-down fa-angle-up');
             }
             $('.panel-group').on('hidden.bs.collapse', toggleIcon);
             $('.panel-group').on('shown.bs.collapse', toggleIcon);
      </script>
   </body>
</html>