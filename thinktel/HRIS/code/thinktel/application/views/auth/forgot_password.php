<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/bootstrap.min.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/font-awesome.min.css" />
<style>
      .form-input {
            height: 33px;
            border-radius: 4px;
            border: 1px solid;
            padding: 5px;
      }

      .submit-btn{
            height: 40px;
            width: 94px;
            border-radius: 4px;
            border: 1px solid #b3a8a8;
            color: #fff;
            background-color: #337ab7;
            border-color: #2e6da4;
      }
</style>

<h1 style="text-align: center;"><?php echo lang('forgot_password_heading');?></h1>
<p style="text-align: center;"><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>

<div id="infoMessage" style="text-align: center;"><?php echo $message;?></div>

<?php echo form_open("auth/forgot_password");?>

      <p style="text-align: center;">
      	<label for="identity"><?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label> <br />
      	   	<?php echo form_input($identity);?>
      </p>
      <p style="text-align: center;"><?php echo " Example: 615191234";?> </p>
      <p style="text-align: center;">
      <a href="<?php echo base_url(); ?>index.php/Mainctrl/index">
            already have an account?
      </a>
            <!-- <a class="txt1" ng-href="<?php echo base_url(); ?>index.php/Mainctrl/index">
                  already have an account?
            </a> -->
      </p>

      <p style="text-align: center;"><?php echo form_submit('submit', lang('forgot_password_submit_btn'));?></p>

<?php echo form_close();?>

<script src="<?php echo base_url(); ?>assets/vendor/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/js/bootstrap.min.js"></script>
<script>
      $( document ).ready(function() {
            $( "#identity" ).addClass( "form-input");
            $( "input[name*='submit']" ).addClass( "submit-btn");
      });
</script>
