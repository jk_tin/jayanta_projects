<!DOCTYPE html>
<html lang="en" ng-app="employeeApp">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Thinktel</title>
      <!-- CSS -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/bootstrap.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/metisMenu.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/main.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/sb-admin-2.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/angular-moment-picker.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/font-awesome.min.css" />-->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/nv.d3.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/animate.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/loading-bars.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/footable.core.min.css" />
   </head>
   <style>
      .form-input {
            height: 33px;
            border-radius: 4px;
            border: 1px solid;
            padding: 5px;
      }

      .submit-btn{
            height: 40px;
            width: 94px;
            border-radius: 4px;
            border: 1px solid #b3a8a8;
            color: #fff;
            background-color: #337ab7;
            border-color: #2e6da4;
      }
</style>
   <body>
      <!-- Simple splash screen-->
      <div class="splash">
         <div class="color-line"></div>
         <div class="splash-title">
            <img src="<?php echo base_url(); ?>assets/vendor/images/loading-bars.svg" width="64" height="64" />
         </div>
      </div>
      <div id="wrapper">
         <!-- Start Navigation Area -->
         <?php
            $this->load->view('share/leftHeader');
            ?>
         <!-- End Navigation Area -->
         <!-- Content Area -->
         <!-- ng-class with current state name give you the ability to extended customization your view -->
         <div id="page-wrapper" class="gray-bg {{$state.current.name}}" ng-controller="createUserController as vm" ng-init="vm.init(1)">
            <!-- Nested view  -->
            <div class="tempLate animated slideInRight">
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                     <h2>Change Password</h2>
                     <ol class="breadcrumb">
                        <li><a ui-sref="home.dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active"><i class="fa fa-table"></i> {{vm.title}}</li>
                     </ol>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">					
					<div id="infoMessage"><?php echo $message;?></div>
                        <fieldset class="the-fieldset" style="margin-top: 20px;">
                           <legend class="the-legend">Detail</legend>
                           <?php echo form_open("auth/change_password");?>

						  <p style="text-align: center;">
								<?php echo lang('change_password_old_password_label', 'old_password');?> <br />
								<?php echo form_input($old_password);?>
						  </p>

						  <p style="text-align: center;">
								<label for="new_password"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label> <br />
								<?php echo form_input($new_password);?>
						  </p>

						  <p style="text-align: center;">
								<?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?> <br />
								<?php echo form_input($new_password_confirm);?>
						  </p>

						  <?php echo form_input($user_id);?>
						  <p style="text-align: center;"><?php echo form_submit('submit', lang('change_password_submit_btn'));?></p>

					<?php echo form_close();?>
                        </fieldset>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- End Content Area -->
      </div>
      <script src="<?php echo base_url(); ?>assets/vendor/js/jquery.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/metisMenu.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-ui-router.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/d3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/nv.d3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-nvd3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/moment.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/footable.all.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-footable.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/sb-admin-2.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/moment-with-locales.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-moment-picker.min.js"></script>      
      <script src="<?php echo base_url(); ?>assets/vendor/js/loading-bars.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/lodash.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/bootbox.min.js"></script>

      <script src="<?php echo base_url(); ?>assets/changePassword/changePasswordController.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/service.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/sidebarDirective.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/headerController.js"></script>
      <script>
            $( document ).ready(function() {
                  $( "#old" ).addClass( "form-input");
                  $( "#new" ).addClass( "form-input");
                  $( "#new_confirm" ).addClass( "form-input");
                  $( "input[name*='submit']" ).addClass( "submit-btn");
            });
      </script>
   </body>
</html>