<!DOCTYPE html>
<html lang="en" ng-app="employeeApp">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Thinktel</title>
      <!-- CSS -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/bootstrap.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/metisMenu.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/main.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/sb-admin-2.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/angular-moment-picker.min.css">
      <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/font-awesome.min.css" />-->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/nv.d3.min.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/animate.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/css/loading-bars.css" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/vendor-css/footable.core.min.css" />
   </head>
   <body>
      <!-- Simple splash screen-->
      <div class="splash">
         <div class="color-line"></div>
         <div class="splash-title">
            <h1>Thinktel</h1>
            <img src="<?php echo base_url(); ?>assets/vendor/images/loading-bars.svg" width="64" height="64" />
         </div>
      </div>
      <div id="wrapper">       
         <!-- Start Navigation Area -->
         <?php
            $this->load->view('share/leftHeader');
         ?>
         <!-- End Navigation Area -->
         <!-- Content Area -->
         <!-- ng-class with current state name give you the ability to extended customization your view -->
         <div id="page-wrapper" class="gray-bg {{$state.current.name}}">
            <!-- Nested view  -->
            <div class="template animated slideInRight" ng-controller="DashboardController as vm">
               <div class="row">
                  <div class="col-lg-12">
                     <h1>{{vm.title}}</h1>
                     <ol class="breadcrumb">
                        <li class="active"><i class="fa fa-dashboard"></i> {{vm.title}}</li>
                     </ol>
                     <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="fa fa-info-circle"></i><strong> Like Thinktel?</strong> Make sure you check out other tutorials in <a class="alert-link" href="http://cc28tech.com" target="_blank">Cathy Wun's Blog</a>!
                     </div>
                  </div>
               </div>
               <!-- /.row -->
               <div class="row">
                  <!-- Total Positions Area -->
                  <div class="col-md-4 col-lg-4">
                     <div class="panel panel-warning">
                        <div class="panel-heading">
                           <div class="row">
                              <div class="col-xs-6">
                                 <i class="fa fa-briefcase fa-5x"></i>
                              </div>
                              <div class="col-xs-6 text-right">
                                 <p class="announcement-heading">{{vm.positions}}</p>
                                 <p class="announcement-text">Total Positions</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- End Total Positions Area -->
                  <!-- Total Offices Area -->
                  <div class="col-md-4 col-lg-4">
                     <div class="panel panel-danger">
                        <div class="panel-heading">
                           <div class="row">
                              <div class="col-xs-6">
                                 <i class="fa fa-university fa-5x"></i>
                              </div>
                              <div class="col-xs-6 text-right">
                                 <p class="announcement-heading">{{vm.offices}}</p>
                                 <p class="announcement-text">Total Offices</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- End Total Offices Area -->
                  <!-- Total Employees Area -->
                  <div class="col-md-4 col-lg-4">
                     <div class="panel panel-info">
                        <div class="panel-heading">
                           <div class="row">
                              <div class="col-xs-6">
                                 <i class="fa fa-users fa-5x"></i>
                              </div>
                              <div class="col-xs-6 text-right">
                                 <p class="announcement-heading">{{vm.employees}}</p>
                                 <p class="announcement-text">Total Employees</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- End Total Employees Area -->
               </div>
               <!-- /.row -->
               <div class="row">
                  <!-- Bar chart of Historical Growth Statistics Area -->
                  <div class="col-lg-6">
                     <div class="panel panel-primary">
                        <div class="panel-heading">
                           <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Historial Growth Statistics</h3>
                        </div>
                        <div class="panel-body">
                           <nvd3 options="vm.lineChartOptions" data="vm.lineChartData"></nvd3>
                        </div>
                     </div>
                  </div>
                  <!-- End Bar chart of Historical Growth Statistics Area -->
                  <!-- Pie chart of Office Statistics Area -->
                  <div class="col-lg-6">
                     <div class="panel panel-primary">
                        <div class="panel-heading">
                           <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Offices Statistics</h3>
                        </div>
                        <div class="panel-body">
                           <nvd3 options="vm.pieChartOptions" data="vm.pieChartData"></nvd3>
                        </div>
                     </div>
                  </div>
                  <!-- End Pie chart of Office Statistics Area -->
               </div>
               <!-- /.row -->
            </div>
         </div>
         <!-- End Content Area -->
      </div>
      <script src="<?php echo base_url(); ?>assets/vendor/js/jquery.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/metisMenu.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-ui-router.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/d3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/nv.d3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-nvd3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/moment.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/footable.all.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-footable.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/sb-admin-2.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/moment-with-locales.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/angular-moment-picker.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/vendor/js/loading-bars.js"></script>
      <script src="<?php echo base_url(); ?>assets/dashboard/dashboard.controller.js"></script>
      <script src="<?php echo base_url(); ?>assets/dashboard/dashboard.service.js"></script>

      <script src="<?php echo base_url(); ?>assets/common/service.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/sidebarDirective.js"></script>
      <script src="<?php echo base_url(); ?>assets/common/headerController.js"></script>
   </body>
</html>