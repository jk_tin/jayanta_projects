<?PHP

class Referencemodel extends CI_Model {

    function __construct () {
        parent::__construct ();
    }


    
    function get_state_list()
    {
        $sql="SELECT *  FROM state_master order by sort_order";
        $query = $this->db->query($sql);
        $row = $query->result_array();
        return $row;
        
        
        
    }
    
    
	   function get_process_list()
	   {

	       //$query = $this->db->get('process_master');

	       //`$this->db->where('is_active', 1);
	       $sql="SELECT *  FROM process_master where is_active=1";
	       $query = $this->db->query($sql);
	       $row = $query->result_array();
	       return $row;

	   }
	   function get_process_info_by_code($process_id)
	   {


	       $sql="SELECT id,process_name from process_master where is_active=1 and id=".$process_id;

	       $query = $this->db->query($sql);
	       $row = $query->row();
	       return $row;

	   }
	   function get_location_info_by_code($location_id)
	   {


	       $sql="SELECT id,location_name from location_master where is_active=1 and id=".$location_id;

	       $query = $this->db->query($sql);
	       $row = $query->row();
	       return $row;

	   }
	   function get_sub_function_list()
	   {

	       //$query = $this->db->get('process_master');

	       //`$this->db->where('is_active', 1);
	       $sql="SELECT *  FROM subfunction_master where is_active=1";
	       $query = $this->db->query($sql);
	       $row = $query->result_array();
	       return $row;

	   }

	    function get_location_list()
	    {


	        $sql="SELECT *  FROM location_master where is_active=1";
	        $query = $this->db->query($sql);
	        $row = $query->result_array();
	        return $row;

	    }

	    function get_location_list_from_process_id($process_id)
	    {

			if(!empty($process_id))
			{
	        $sql="select * from location_master
                  where id in (select location_id from process_loc_mapping where process_id=" . $process_id;
	        $sql=$sql. " and is_active=1) and is_active=1";
	        }
	        else

	        {
	         $sql="select * from location_master where is_active=1";

	        }


	      //  return $sql;
	        $query = $this->db->query($sql);
	        $row = $query->result_array();
	        return $row;

	    }

	    function get_designation_list()
	    {


	        $sql="SELECT *  FROM emp_desig_master where is_active=1";
	        $query = $this->db->query($sql);
	        $row = $query->result_array();
	        return $row;

	    }
	    function get_designationdesc_from_id($id)
	    {
	        
	        
	        $sql="SELECT *  FROM emp_desig_master where is_active=1 and id=".$id;
	        $query = $this->db->query($sql);
	        $row = $query->result_array();
	        return $row;
	        
	    }

	    function get_attendance_code()
	    {


	        $sql="SELECT *  FROM emp_attendance_code where is_active=1";
	        $query = $this->db->query($sql);
	        $row = $query->result_array();
	        return $row;

	    }
	    function get_users_group()
	    {


	        $sql="SELECT *  FROM  groups";
	         $query = $this->db->query($sql);
	        $row = $query->result_array();
	        return $row;

	    }
	    function location_edit($id,$desc)
	    {
	       
	         
	        $this->db->set('location_name', $desc);
	        $this->db->where('id', $id);
	        $this->db->update('location_master');
	        
	        
	    }
	    function location_insert($data)
	    {
	        
	        
	        $this->db->insert('location_master', $data);
	        
	    }
	    function process_insert($data)
	    {
	        
	        
	        $this->db->insert('process_master', $data);
	        
	    }
	    function process_edit($id,$desc)
	    {
	        
	        
	        $this->db->set('process_name', $desc);
	        $this->db->where('id', $id);
	        $this->db->update('process_master');
	         
	        
	    }
	    function process_delete($id)
	    {
	        
	        
	        $this->db->set('is_active', 0);
	        $this->db->where('id', $id);
	        $this->db->update('process_master');
	        
	        
	    }
	    function location_delete($id)
	    {
	      
	        $array = array(
	            'is_active' => 0 
	        );
	       // $this->db->set('is_active', 0);
	        $this->db->where('id', $id);
	        $this->db->update('location_master',$array);
	        $sql = $this->db->last_query();
	        
	       // var_dump($db->last_query());
	      //  exit;
	        
	        
	    }
	    
	    function get_loc_for_process($id)
	    {
	        $sql="SELECT l.id,l.location_name FROM process_loc_mapping m, location_master l
	        where  m.process_id=".$id." and m.location_id=l.id";
	        
	        
	        $query = $this->db->query($sql);
	        $row = $query->result_array();
	        return $row;
	        
	    }
	    function delete_process_loc_map($process_id)
	    {
	        
	       // $sql="delete from process_loc_mapping where process_id=".$process_id;
	        $this->db->delete('process_loc_mapping', array('process_id' => $process_id)); 
	        return;
	        
	    }
	    function insert_process_loc_map($data)
	    {
	        
	        $this->db->insert('process_loc_mapping', $data);
	    }
	    


  }

