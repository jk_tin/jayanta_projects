<?PHP

class Atttendancemonthlysummary extends CI_Model {

    function __construct () {
        parent::__construct ();
    }

      function create_monthly_summary($data)
	  {
	  	 
	  	if($this->db->replace('emp_attendance_month_summary', $data))
	  	{

	  		//var_dump( $this->db->error());
	  	}
	  	else
	  	{
	  		//var_dump( $this->db->error());
	  	}
	 

	  	return 1;

	  }
	  
	  function update_monthly_summary($data,$wherearray)
	  {
	      
	      $this->db->where($wherearray);
	      $this->db->update('emp_attendance_month_summary', $data);
	      
	      
	      return 1;
	      
	  }

	  function check_summary_data_exists($emp_code,$month,$year)
	  {

	  		$wherearray=array(

	  					'emp_code' => $emp_code,
	  					'month'=> $month,
	  					'year'=>$year
	  		);
	  		$query = $this->db->get_where('emp_attendance_month_summary', $wherearray);

	  		return $query->num_rows();

	  }
	  function get_summary_data($emp_code,$month,$year)
	  {
	      
	      $wherearray=array(
	          
	          'emp_code' => $emp_code,
	          'month'=> $month,
	          'year'=>$year
	      );
	      $query = $this->db->get_where('emp_attendance_month_summary', $wherearray);
	      
	      return $query->result_array();
	      
	  }

}
