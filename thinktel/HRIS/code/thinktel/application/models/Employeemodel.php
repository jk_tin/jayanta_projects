<?PHP

class Employeemodel extends CI_Model {

    function __construct () {
        parent::__construct ();
    }

     function create_emp_master($data)
	 {
	      //$this->output->enable_profiler(TRUE);
	      $this->db->insert('employee_master', $data);
	      $insert_id = $this->db->insert_id();

	      return  $insert_id;

	  }
	  function create_join_info($data)
	  {
	      //$this->output->enable_profiler(TRUE);
	      $this->db->insert('employee_join_info', $data);
	      $insert_id = $this->db->insert_id();

	      return  $insert_id;

	  }
	  function create_id_info($data)
	  {
	      //$this->output->enable_profiler(TRUE);
	      $this->db->insert('employee_id_info', $data);
	      $insert_id = $this->db->insert_id();

	      return  $insert_id;

	  }
	  function create_bank_info($data)
	  {
	      //$this->output->enable_profiler(TRUE);
	      $this->db->insert('employee_bank_info', $data);
	      $insert_id = $this->db->insert_id();

	      return  $insert_id;

	  }
	  function create_pf_info($data)
	  {
	      //$this->output->enable_profiler(TRUE);
	      $this->db->insert('employee_pf_esic_info', $data);
	      $insert_id = $this->db->insert_id();

	      return  $insert_id;

	  }

	  function create_resig_info($data)
	  {
	      //$this->output->enable_profiler(TRUE);
	      $this->db->insert('employee_resig_info', $data);
	      $insert_id = $this->db->insert_id();

	      return  $insert_id;

	  }
	  function create_salmaster_info($data)
	  {
	      //$this->output->enable_profiler(TRUE);
	      $this->db->insert('employee_sal_master_earning', $data);
	      $insert_id = $this->db->insert_id();

	      return  $insert_id;

	  }
	  function create_salmaster_deduct_info($data)
	  {
	      //$this->output->enable_profiler(TRUE);
	      $this->db->insert('employee_sal_master_deduction', $data);
	      $insert_id = $this->db->insert_id();

	      return  $insert_id;

	  }
	  function create_salmaster_comp_info($data)
	  {
	      //$this->output->enable_profiler(TRUE);
	      $this->db->insert('employee_sal_master_computation', $data);
	      $insert_id = $this->db->insert_id();
	      return  $insert_id;

	  }
	  function create_salmaster_company_contri_info($data)
	  {
	      //$this->output->enable_profiler(TRUE);
	      $this->db->insert('employee_sal_master_company_contri', $data);
	      $insert_id = $this->db->insert_id();
	      return  $insert_id;

	  }

	  function get_latest_emp_code($series_number)
	  {

	      $sql="select last_emp_master_id from emp_last_id where series_numb=".$series_number;

	      $query=$this->db->query($sql);
	      $data= $query->result_array();
	      //var_dump($data[0]['emp_code']);
	      //exit;
	      return $data[0]['last_emp_master_id'];

	  }

	  function udpate_latest_emp_code($series_number,$next_emp_code)
	  {

	      $modified_at=date('Y-m-d H:i:s');


	      $data = array(
	          'modified_at' => $modified_at,
	          'last_emp_master_id' => $next_emp_code

	      );


	      $this->db->where('series_numb', $series_number);
	      $this->db->update('emp_last_id', $data);





	  }

	  function update_employee_master($data,$emp_master_id)
	  {
	      $this->db->where('id', $emp_master_id);
	      $this->db->update('employee_master', $data);


	  }


	  function get_employee_list($process_id,$location_id,$emp_code,$emp_level=2)
	  {

	      $sql="select e.id as id,emp_code ,e.emp_email, CONCAT(COALESCE(e.emp_title,''),\" \",COALESCE(e.emp_first,'') ,\" \",COALESCE(e.emp_mid,''),\" \",COALESCE(e.emp_last,'')) as emp_name, date_format(e.emp_dob,\"%d-%b-%Y\") as emp_dob,e.emp_is_active
	      as status,d.emp_desig_desc, p.process_name
	      from employee_master e,emp_desig_master d, process_master p,location_master l
	      where e.emp_desig_id=d.id
	      and p.id=e.process_id
          and l.id=e.loc_id";
	      //return $emp_code;
	      if(!empty($process_id))
	      {
	          $sql=$sql." and e.process_id=".$process_id;

	      }
	      if(!empty($location_id))
	      {
	          $sql=$sql." and e.loc_id=".$location_id;

	      }
	      if(!empty($emp_code))
		  {
		  	          $sql=$sql." and e.emp_code='".$emp_code."'";

	      }
	      if(!empty($emp_level)&&($emp_level!=1))//show all for level 1
	      {
	          $sql=$sql." and e.emp_level=".$emp_level;

	      }


	      $query=$this->db->query($sql);
	      $data= $query->result_array();

	      return $data;


	  }
	  function get_employee_details($emp_master_id)
	  {

	      $sql="select * from employee_master where id=".$emp_master_id;
	      $query = $this->db->query($sql);
	      return $query->row_array();


	  }
	  function get_employee_join_info($emp_master_id)
	  {

	      $sql="select * from employee_join_info where emp_master_id=".$emp_master_id;
	      $query = $this->db->query($sql);
	      return $query->row_array();


	  }
	  function get_employee_id_info($emp_master_id)
	  {

	      $sql="select * from  employee_id_info where emp_master_id=".$emp_master_id;
	      $query = $this->db->query($sql);
	      return $query->row_array();


	  }
	  function get_employee_bank_info($emp_master_id)
	  {

	      $sql="select * from  employee_bank_info where emp_master_id=".$emp_master_id;
	      $query = $this->db->query($sql);
	      return $query->row_array();


	  }
	  function get_employee_pfesic_info($emp_master_id)
	  {

	      $sql="select * from  employee_pf_esic_info where emp_master_id=".$emp_master_id;
	      $query = $this->db->query($sql);
	      return $query->row_array();


	  }

	  function get_employee_resig_info($emp_master_id)
	  {

	      $sql="select * from  employee_resig_info where emp_master_id=".$emp_master_id;
	      $query = $this->db->query($sql);
	      return $query->row_array();


	  }
	  function get_employee_salmaster_earn_info($emp_master_id)
	  {



	       $sql="select * from  employee_sal_master_earning where emp_master_id=".$emp_master_id;


	      $query = $this->db->query($sql);
	      return $query->row_array();


	  }

	  function get_employee_salmaster_deduct_info($emp_master_id)
	  {

	      $sql="select * from  employee_sal_master_deduction where emp_master_id=".$emp_master_id;
	      $query = $this->db->query($sql);
	      return $query->row_array();


	  }
	  function get_employee_salmaster_compute_info($emp_master_id)
	  {

	      $sql="select * from  employee_sal_master_computation where emp_master_id=".$emp_master_id;
	      $query = $this->db->query($sql);
	      return $query->row_array();


	  }
	  function get_employee_salmaster_company_contri_info($emp_master_id)
	  {

	      $sql="select * from  employee_sal_master_company_contri where emp_master_id=".$emp_master_id;
	      $query = $this->db->query($sql);
	      return $query->row_array();


	  }

	  function update_employee_data($tablename,$data,$id)
	  {

	      $this->db->where('id', $id);
	      $this->db->update($tablename, $data);
	  }
	  function get_empmasterid_from_code($emp_code)
	  {

	      $sql="SELECT   id from employee_master where emp_code='".$emp_code."'";
	      $query = $this->db->query($sql);
	      return $query->row_array();


	  }
	  function get_emp_details_by_empcode($emp_code)
	  {


	      $sql=" select emp.emp_first as emp_name , desig.emp_desig_desc as emp_desig ,  location_name  as location
			from employee_master emp , emp_desig_master desig,location_master loc
			where emp_code='" .  $emp_code . "' " . " and emp.emp_desig_id=desig.id and emp.loc_id=loc.id";

	      //	echo $sql;
	      //	exit;

	      //return $sql;

	      $query=$this->db->query($sql);

	      $data= $query->result_array();


	      return $data;


	  }

	  function create_user_empmaster_map($data)
	  {

	      $this->db->replace('empmaster_user_mapping', $data);


	  }

}
