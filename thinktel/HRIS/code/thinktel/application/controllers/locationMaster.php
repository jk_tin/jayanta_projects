<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LocationMaster extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->library(array('ion_auth', 'form_validation'));
		

	}

	public function index()
	{
	    
	    if (!$this->ion_auth->logged_in())
	    {
	        // redirect them to the login page
	        redirect('mainctrl');
	    }
		$this->load->view('locationList/locationList');
	}

	public function addlocation()
	{
	    
	    
	    $this->load->library(array('ion_auth', 'form_validation'));
	    if (!$this->ion_auth->logged_in())
	    {
	        // redirect them to the login page
	        redirect('mainctrl');
	    }
		$data=array(); 
	    $data['locationid']=0;
	    $this->load->view("locationMaster/locationMaster",$data);
	    
	}
	public function editlocation()
	{
	    //model get data
	    $this->load->library(array('ion_auth', 'form_validation'));
	    if (!$this->ion_auth->logged_in())
	    {
	        // redirect them to the login page
	        redirect('mainctrl');
	    }
	    $locationid=$this->input->post("locationid");
	    $data=array(); 
	    $data['locationid']=$locationid;
	    
	    $this->load->view("locationMaster/locationMaster",$data);	    
	}
}

?>