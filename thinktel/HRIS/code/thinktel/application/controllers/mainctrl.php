<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Mainctrl extends CI_Controller {

		function __construct () {
				parent::__construct ();
		}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		//$data = $this->Init->initPath ('/mainctrl');
		$data ="";
		$this->load->view('login/login',$data);
		//$this->user();
	}
	public function updateTable()
	{
	$data = $this->init->initPath ('/mainctrl');

	  	//City Table
	  	$cityList = array();
		$cityListArray = array();

		$objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
		$objReader->setReadDataOnly(true);

		//FileName and Sheet Name
		$objPHPExcel = $objReader->load("THINKTEL MASTER APPOINTMENT BASE xxx (3).xlsx");
		$worksheet = $objPHPExcel->getSheetByName('attendance');

			echo "###Here##	";
		$highestRow = $worksheet->getHighestRow(); // e.g. 12
		$highestColumn = $worksheet->getHighestColumn(); // e.g M'

		$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); // e.g. 7

		echo "###highestColumnIndex=".$highestColumnIndex;
		//Ignoring first row (As it contains column name)
		for ($row = 2; $row <= $highestRow; ++$row) {
			//A row selected
		    for ($col = 1; $col <= $highestColumnIndex; ++$col) {
			    // values till $cityList['1'] till $cityList['last_column_no']
		        $cityList[$col] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
		    	}
		    	array_push ($cityListArray, $cityList);
		    	//next row, from top
		}

		//Calling modal function to insert into table
		//$status = $this->data->updateCityTable($cityListArray);

	}

	   public function updateAttendance()
		{
		$data = $this->init->initPath ('/mainctrl');
		$this->load->model('Attendancelog');
		$this->load->model('Atttendancemonthlysummary');
		  	//City Table
		  	$cityList = array();
			$cityListArray = array();

			$objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
			$objReader->setReadDataOnly(true);

			//FileName and Sheet Name
			$objPHPExcel = $objReader->load("think_tel_1.xlsx");
			$worksheet = $objPHPExcel->getSheetByName('testx');


			$highestRow = $worksheet->getHighestRow(); // e.g. 12
			$highestColumn = $worksheet->getHighestColumn(); // e.g M'

			$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); // e.g. 7
			//Ignoring first row (As it contains column name)

			//Initializantion starts
			$num_days=0;
			$startcol_index=7;//start index of day one

			$offset_monthly_summary=12;
			//Initializantion ends

			for ($row = 1; $row <= $highestRow; ++$row) { //each employee one row
				echo "Inside Row";
				//A row selected
				$oldval=$newval=0;
				$flag=0;
				$resetrowflag=0;
				$emp_code=0;
				$startindexformonthdata=0;
				$numberofdayspresent =0;
				$numberofdaysabsent=0;
				$numberofdayholiday=0;
				$numberofdayleave=0;
				$numberofdaylate=0;
				$numberofdayweekoff=0;
				$numberofdaysun=0;
				$numberofdayautodeduct=0;
				$numberofdayexptgrant=0;
				$numberofdayfinaldeduct=0;
				$numberofdaytot=0;
				$numberofdayfinalatt=0;

				$flagcomplete=0;//for completeion of parsing of all columns

			    for ($col = 1; $col <= $highestColumnIndex; ++$col) {

				$newval=$val=$worksheet->getCellByColumnAndRow($col, $row)->getValue();

			       if($row ==1)//try to find number of days in month
			       {
			 		 if($newval=="DOJ")//next value is day value
					 {
							$flag=1;

					 }

					if((is_numeric($newval)==1) AND ($flag==1) )
					{
						$oldval=$newval;
					}

					$num_days=$oldval;


			       }
			       else{

						if(($col==1)AND ($resetrowflag==0))
						{

								$resetrowflag=1;
								$emp_code=$newval;

			    		 }
						//for attendance log table
			       		if (($col>=$startcol_index) AND ($col<$startcol_index+$num_days) )
			       		{



							$emp_att_code=$this->convert_attendancecode_to_id($newval);
							//var_dump( "new value =".$newval."emp_att_code=" .$emp_att_code);

			       			$data_attedance= array(
									'emp_code' =>$emp_code ,
									'day'=> 7,
									'month'=> 7,
									'year'=> 2018,
									'emp_attendance_code_id'=>$emp_att_code,
									'created_by' =>1,
									'modified_by'=>1
								);

							$this->Attendancelog->create_log($data_attedance);


			       		}
			       		//for insertion into emp_attendance_summary
						$startindexformonthdata=$startcol_index+$num_days;
						//echo "startindexformonthdata=".$startindexformonthdata;
			       		if (($col>=$startcol_index+$num_days) AND ($col<$startindexformonthdata+$offset_monthly_summary) )
			       		{

							//echo "@col=".$col;
							$value=$worksheet->getCellByColumnAndRow($col, $row)->getCalculatedValue();

							if(is_null($value) OR  (trim($value)==""))
							{
								$value=0;
							}

							if($col==$startindexformonthdata)
							{
								$numberofdayspresent=$value;
								 echo $value;
								//echo "Here11";
							}
							else if($col==$startindexformonthdata+1)
							{

								$numberofdaysabsent=$value;

							}
							else if($col==$startindexformonthdata+2)
							{

								$numberofdayholiday=$value;
							}
							else if($col==$startindexformonthdata+3)
							{

								$numberofdayleave=$value;
							}
							else if($col==$startindexformonthdata+4)
							{

								$numberofdaylate=$value;
							}
							else if($col==$startindexformonthdata+5)
							{

								 $numberofdayweekoff=$value;
							}
							else if($col==$startindexformonthdata+6)
							{

								$numberofdaysun=$value;
							}
							else if($col==$startindexformonthdata+7)
							{

								$numberofdayautodeduct=$value;
							}
							else if($col==$startindexformonthdata+8)
						    {

						        $numberofdayexptgrant=$value;
							}
							else if($col==$startindexformonthdata+9)
							{

							   $numberofdayfinaldeduct=$value;
							}
							else if($col==$startindexformonthdata+10)
							{
							    $numberofdaytot=$value;
								// echo $value;
							}
							else if($col==$startindexformonthdata+11)
							{
							     $numberofdayfinalatt=$value;
								 //echo $value;
								 $flagcomplete=1;
									//echo "highestColumnIndex=".$highestColumnIndex;

							}
							/*else if($col==$startindexformonthdata+12)
							{
							     $numberofdayfinalatt=$value;
							      echo $value;
							     $flagcomplete=1;

							}*/

							//echo "numberofdays absent=".$numberofdaysabsent;
							if($flagcomplete==1)
							{
								  $data_attedance_month_sum= array(
									'emp_code' =>$emp_code ,
									'month'=> 7,
									'year'=> 2018,
									'emp_tot_present'=>$numberofdayspresent,
									'emp_tot_absent'=> $numberofdaysabsent,
									'emp_tot_holiday'=>$numberofdayholiday,
									'emp_tot_leave'=>$numberofdayleave,
									'emp_tot_late'=>$numberofdaylate,
									'emp_tot_weekly_off'=>$numberofdayweekoff,
									'emp_tot_sunday'=>$numberofdaysun,
									'emp_tot_auto_deduct'=>$numberofdayautodeduct,
									'emp_excep_grant'=>$numberofdayexptgrant,
									'emp_final_deduct'=>$numberofdayfinaldeduct,
									'emp_tot_day_month'=>$numberofdaytot,
									'emp_final_attendance'=>$numberofdayfinalatt,
									'created_by' =>1,
									'modified_by'=>1
								);
								//var_dump($data_attedance_month_sum);
							    $this->Atttendancemonthlysummary->create_monthly_summary($data_attedance_month_sum);


							}

			       		}



			       }

	    	}
			    	//array_push ($cityListArray, $cityList);
			    	//next row, from top
			}
			exit;
			//Calling modal function to insert into table
			//$status = $this->data->updateCityTable($cityListArray);

	}


	function convert_attendancecode_to_id($codeintext)
	{
			$retval=1;
			switch ($codeintext) {
				case "P":

						$retval=1;
						break;
					case "A":

						$retval=2;
						break;
					case "WO":

						$retval=3;
						break;
					case "T":
						$retval=4;
						break;
					case "S":
						$retval=5;
						break;
					case "H":
						$retval=6;
						break;
					default:
						$retval=1;

			}


			return $retval;




	}
	
	
	public function downloadSalaryFile()
	
	{
	   
	    
	    $spreadsheet = new Spreadsheet();
	    $sheet = $spreadsheet->getActiveSheet();
	    
	    
	    $writer = new Xlsx($spreadsheet);
	    
	    $filename = 'Salary-Sheet';
	    
	    header('Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
	    header('Cache-Control: max-age=0');
	    
	    //first generate the headers
	    $sheet->setCellValue('A1', 'Sl No');
	    $sheet->setCellValue('B1', 'Emp Code');
	    $sheet->setCellValue('C1', 'PROCESS');
	    $sheet->setCellValue('D1', 'LOCATION');
	    $sheet->setCellValue('E1', 'Name');
	    $sheet->setCellValue('F1', 'Designation');
	    $sheet->setCellValue('G1', 'EPF NO');
	    $sheet->setCellValue('H1', 'UAN');
	    $sheet->setCellValue('I1', 'ESI NO.');
	    $sheet->setCellValue('J1', 'BANK ACC. NO.');
	    $sheet->setCellValue('K1', 'IFSC CODE');
	    $sheet->setCellValue('L1', 'BANK NAME');
	    $sheet->setCellValue('M1', 'JOINING DATE');
	    
	    $sheet->setCellValue('N1', 'Active/ inactive');
	    $sheet->setCellValue('O1', 'Gross Offered');
	    $sheet->setCellValue('P1', 'Days for the month');
	    $sheet->setCellValue('Q1', 'Attendance');
	    $sheet->setCellValue('R1', 'deduct');
	    $sheet->setCellValue('S1', 'Prorata Gross Salary');
	    $sheet->setCellValue('T1', 'Basic');
	    $sheet->setCellValue('U1', 'HRA');
	    $sheet->setCellValue('V1', 'Conv. Allow');
	    
	    $sheet->setCellValue('W1', 'EDU. Allow');
	    $sheet->setCellValue('X1', 'Spcl Allow');
	    $sheet->setCellValue('Y1', 'UP-KEEP Allow');
	    $sheet->setCellValue('Z1', 'Others/Roff');
	    $sheet->setCellValue('AA1', 'Gross Earnings');
	    
	    
	    //		P.F Contr	ESI Contr	Prof. Tax	Advance 	Tds	Total Deduction	Tele/Inc/others rembursement	Salary in hand	CO'S CONT. TO PF 3.67%	PENSION FUND 8.33%	EDLI 0.5%	ADM CH 0.65%	ADM TO EDLI 0.01%	Total CO's Cont to EPF 13.61%	CO'S CONT. TO ESI	CTC per Month
	    
	    
	    
	    
	    
	    
	    $writer->save('php://output'); // download file
	    
	    
	}

}
