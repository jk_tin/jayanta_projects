<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employees extends CI_Controller {
	public function index()
	{
	    $this->load->library(array('ion_auth', 'form_validation'));
	    if (!$this->ion_auth->logged_in())
	    {
	        // redirect them to the login page
	        redirect('mainctrl');
	    }
		$this->load->view('employee/employee');
	}
	
	public function addemployee()
	{
	    
	    
	    $this->load->library(array('ion_auth', 'form_validation'));
	    if (!$this->ion_auth->logged_in())
	    {
	        // redirect them to the login page
	        redirect('mainctrl');
	    }
	    $data=array();
	    $data['emp_master_id']="";
	    $group=array("srhr","admin");
	    $level="2"; //this determines if the entire list to be shown or only selected ones depending on user access righgts
	    
	    if ($this->ion_auth->in_group($group))
	    {
	        $level="1";
	        
	    }
	    $data['emp_level']=$level;
	    $this->load->view("addEmployee/addEmployee",$data);
	    
	}
	public function editemployee()
	{
	    //model get data
	    $this->load->library(array('ion_auth', 'form_validation'));
	    if (!$this->ion_auth->logged_in())
	    {
	        // redirect them to the login page
	        redirect('mainctrl');
	    }
	    $empid=$this->input->post("empid");
	    $data=array(); 
	    $data['emp_master_id']=$empid;
	    $group=array("srhr","admin");
	    $level=2; //this determines if the entire list to be shown or only selected ones depending on user access righgts
	    
	    if ($this->ion_auth->in_group($group))
	    {
	        $level=1;
	        
	    }
	    $data['emp_level']=$level;
 
	    $this->load->view("addEmployee/addEmployee",$data);	    
	}

	public function employeeList()
	{
	    $this->load->library(array('ion_auth', 'form_validation'));
	    if (!$this->ion_auth->logged_in())
	    {
	        // redirect them to the login page
	        redirect('mainctrl');
	    }
	    //model get data
	    $data=array(); 
 
	    $this->load->view("employee/employee");	    
 
 
	}
	public function downloadzip(){
	    $this->load->library('zip');
	    $this->load->helper('file');
	    $path="d:/mailtest/";
	    
	    $this->zip->add_data('d:/1.jpg', 'd:/2.jpg');
	    $this->zip->download('my_spreadsheet.zip');
	    //$path = './uploads/';
	  /*  $files = get_filenames($path);
	    foreach($files as $f){
	       
	       $r=$this->zip->read_file($path.$f, true);
	       var_dump("here".$r);
	    }
	   header("Content-type: application/zip;\n");
	   header("Content-Transfer-Encoding: Binary");
	  $this->zip->download('Download_all_files');*/
	}
	
	
	function sendmail()
	{
	  /*  $config['useragent'] = 'CodeIgniter';
	    $config['protocol'] = 'smtp';
	    //$config['mailpath'] = '/usr/sbin/sendmail';
	    $config['smtp_host'] = 'ssl://smtp.googlemail.com';
	    $config['smtp_user'] = 'jayantacal@gmail.com';
	    $config['smtp_pass'] = 'eclipse14';
	    $config['smtp_port'] = 465;
	    $config['smtp_timeout'] = 5;
	    $config['wordwrap'] = TRUE;
	    $config['wrapchars'] = 76;
	    $config['mailtype'] = 'html';
	    $config['charset'] = 'utf-8';
	    $config['validate'] = FALSE;
	    $config['priority'] = 3;
	    $config['crlf'] = "\r\n";
	    $config['newline'] = "\r\n";
	    $config['bcc_batch_mode'] = FALSE;
	    $config['bcc_batch_size'] = 200;
	    $this->email->initialize($config);*/
	    
	    $this->load->library('email');
	    $this->email->from('jayanta.cal@gmail.com', 'Test');
	    $list = array('xxx@gmail.com');
	    $this->email->to($list);
	    $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
	    $this->email->subject('This is an email Testing');
	    $this->email->message('It is working. Great!');
	    $this->email->send();
	}
	
}
