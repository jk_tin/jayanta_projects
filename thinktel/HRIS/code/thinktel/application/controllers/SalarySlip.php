<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SalarySlip extends CI_Controller {
	function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->library(array('ion_auth', 'form_validation'));
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in())
	    {
	        // redirect them to the login page
	        redirect('mainctrl');
		}
		$group=array("srhr","admin","hr","accounts");
		$data=array();
		$level=2; //this determines if the entire list to be shown or only selected ones depending on user access righgts
		$emp_code="";
		
	    if ($this->ion_auth->in_group($group))
	    {
	        $level=1;
	       

	    }else
	    {
	        
	        $emp_code= $this->session->userdata('emp_code');
	    }
		 
		$data['emp_level']=$level;
		$data['emp_code']=$emp_code;


		$this->load->view('salarySlip/salarySlip', $data);
	}
}
