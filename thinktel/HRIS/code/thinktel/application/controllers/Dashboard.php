<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function index()
	{
	    $this->load->library(array('ion_auth', 'form_validation'));
	    if (!$this->ion_auth->logged_in())
	    {
	        // redirect them to the login page
	        redirect('mainctrl');
	    }
	    $this->load->view('dashboard/dashboard');
	}
}
