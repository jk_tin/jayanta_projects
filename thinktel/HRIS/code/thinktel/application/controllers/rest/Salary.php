<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries\REST_Controller.php');
require 'vendor/autoload.php';
use Restserver\Libraries\REST_Controller;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Salary extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));

		$this->load->library("Pdf");
	}

	public function index()
	{
		//$this->load->view('salary/salaryInitiate');
	}



	public function saldownload_get() {

	    $this->load->model("Employeemodel");
	    $this->load->model("Referencemodel");
	    $this->load->model("Salarymodel");
	    
	    $month=$this->input->get("month");
	    $year=$this->input->get("year");
	    $location_id="";
	    $process_id="";
	    $process_name="";
	    $location_name="";
	    $process_id=$this->input->get("process_id");
	    $location_id=$this->input->get("location_id");
	   



	    $spreadsheet = new Spreadsheet();
	    $sheet = $spreadsheet->getActiveSheet();


	    $writer = new Xlsx($spreadsheet);

	    //Process	Location	Employee Code	Employe Name	Gross Amount	Bank Name	IFSC Code

	    $sheet->setCellValue('A1', 'Process');
	    $sheet->setCellValue('B1', 'Location');
	    $sheet->setCellValue('C1', 'Employee Code');
	    $sheet->setCellValue('D1', 'Employee Name');
	    $sheet->setCellValue('E1', 'PF Number');
	    $sheet->setCellValue('F1', 'UAN Number');
	    $sheet->setCellValue('G1', 'ESI Number');
	    $sheet->setCellValue('H1', 'Days worked');
	    $sheet->setCellValue('I1', 'Gross Amount');
	    $sheet->setCellValue('J1', 'Salary in Hand');
	    $sheet->setCellValue('K1', 'Bank Name');
	    $sheet->setCellValue('L1', 'Bank Account Number');
	    $sheet->setCellValue('M1', 'IFSC Code');
	    $sheet->setCellValue('N1', 'PF Employee');
	    $sheet->setCellValue('O1', 'ESIC Employee');
	    $sheet->setCellValue('P1', 'PF Company');
	    $sheet->setCellValue('Q1', 'Pension Fund');
	    $sheet->setCellValue('R1', 'EDLI');
	    $sheet->setCellValue('S1', 'Adm Charge');
	    $sheet->setCellValue('T1', 'Adm Charge EDLI');
	    $sheet->setCellValue('U1', 'PF Total(Company)');
	    $sheet->setCellValue('V1', 'ESI(Company)');
	    
	    $sheet->getColumnDimension('A')->setWidth(20);
	    $sheet->getColumnDimension('B')->setWidth(20);
	    $sheet->getColumnDimension('C')->setWidth(20);
	    $sheet->getColumnDimension('D')->setWidth(40);
	    $sheet->getColumnDimension('E')->setWidth(40);
	    $sheet->getColumnDimension('F')->setWidth(40);
	    $sheet->getColumnDimension('G')->setWidth(40);	    
	    $sheet->getColumnDimension('H')->setWidth(10);
	    $sheet->getColumnDimension('I')->setWidth(30);
	    $sheet->getColumnDimension('J')->setWidth(30);
	    $sheet->getColumnDimension('K')->setWidth(30);
	    $sheet->getColumnDimension('L')->setWidth(30);
	    $sheet->getColumnDimension('M')->setWidth(30);
	    $sheet->getColumnDimension('N')->setWidth(13);
	    $sheet->getColumnDimension('O')->setWidth(13);
	    $sheet->getColumnDimension('P')->setWidth(13);
	    $sheet->getColumnDimension('Q')->setWidth(13);
	    $sheet->getColumnDimension('R')->setWidth(13);
	    $sheet->getColumnDimension('S')->setWidth(15);
	    $sheet->getColumnDimension('T')->setWidth(13);
	    $sheet->getColumnDimension('U')->setWidth(13);
	    $sheet->getColumnDimension('V')->setWidth(15);

	   // var_dump($process_id);
	   /// exit;
	   
	   if(!empty($process_id))
	   {
	     $process_name=($this->Referencemodel->get_process_info_by_code($process_id))->process_name;
	   }
	    if(!empty($location_id))
	    {
	       $location_name=($this->Referencemodel->get_location_info_by_code($location_id))->location_name;
	    }
        
	    $filename = 'SalaryDump_'.$process_name."_".$location_name."_".$month."_".$year;
	     
	    $data=$this->Salarymodel->get_sal_download_data($process_id,$location_id,$month,$year);
	    
	   
	   //var_dump($data);
	   //exit;
	    $row=2;
	    foreach($data as $datarow)
	    {
	        $days_worked="";
            /*
            $sheet->setCellValue('A1', 'Process');
    	    $sheet->setCellValue('B1', 'Location');
    	    $sheet->setCellValue('C1', 'Employee Code');
    	    $sheet->setCellValue('D1', 'Employe Name');
    	    $sheet->setCellValue('E1', 'Days worked');
    	    $sheet->setCellValue('F1', 'Gross Amount');
    	    $sheet->setCellValue('G1', 'Salary in Hand');
    	    $sheet->setCellValue('H1', 'Bank Name');
    	    $sheet->setCellValue('I1', 'Bank Account Number');
    	    $sheet->setCellValue('J1', 'IFSC Code');
    	    */

	        $emp_code=$datarow['emp_code'];
	        $emp_name=$datarow['emp_name'];
	        $gross_earning=$datarow['gross_earning'];
	        $days_worked=$datarow['tot_days_worked_in_month'];

	        $sal_in_hand=$datarow['sal_hand'];
	        $emp_bank_name=$datarow['emp_bank_name'];
	        $emp_bank_ifsc_code=$datarow['emp_bank_ifsc_code'];
	        $emp_bank_ac_number=$datarow['emp_bank_ac_number'];
	        
	        $location_name=$datarow['location_name'];
	        $pf=$datarow['pf'];
	        $esic=$datarow['esi'];
	        $comp_pf=$datarow['pf_comp_contri'];
	        $pension=$datarow['pension_fund'];
	        $edli=$datarow['edli'];
	        $adm_charge=$datarow['adm_charge'];
	        $adm_charge_edli=$datarow['adm_charge_eli'];
	        $tot_company_contri_pf=$datarow['tot_company_contri_pf'];
	        $company_contri_esi=$datarow['company_contri_esi'];
	        
	       // pfinfo.emp_pf_number,pfinfo.emp_esic_ac_number,pfinfo.emp_uan_number
	        $pf_ac_number=$datarow['emp_pf_number'];
	        $esic_ac_number=$datarow['emp_esic_ac_number'];
	        $emp_uan_number=$datarow['emp_uan_number'];
	        
	        $cellno="A".$row;
	        $sheet->setCellValue($cellno, $process_name);
	        $cellno="B".$row;
	        $sheet->setCellValue($cellno, $location_name);
	        $cellno="C".$row;
	        $sheet->setCellValue($cellno, $emp_code);
	        $cellno="D".$row;
	        $sheet->setCellValue($cellno, $emp_name);
            /*    $sheet->setCellValue('E1', 'PF Number');
	              $sheet->setCellValue('F1', 'UAN Number');
	              $sheet->setCellValue('G1', 'ESI Number');
	        */
	        $cellno="E".$row;
	        $sheet->setCellValue($cellno, $pf_ac_number);
	        $cellno="F".$row;
	        $sheet->setCellValue($cellno, $esic_ac_number);
	        $cellno="G".$row;
	        $sheet->setCellValue($cellno, $emp_uan_number);
	        $cellno="H".$row;
	        $sheet->setCellValue($cellno, $days_worked);
	        $cellno="I".$row;
	        $sheet->setCellValue($cellno, $gross_earning);
	        $cellno="J".$row;
	        $sheet->setCellValue($cellno, $sal_in_hand);
	        $cellno="K".$row;
	        $sheet->setCellValue($cellno, $emp_bank_name);
	        $cellno="L".$row;
	        $sheet->setCellValue($cellno, $emp_bank_ac_number);
	        $cellno="M".$row;
	        $sheet->setCellValue($cellno, $emp_bank_ifsc_code);
	        $cellno="N".$row;
	        $sheet->setCellValue($cellno, $pf);
	        $cellno="O".$row;
	        $sheet->setCellValue($cellno, $esic); 
	        $cellno="P".$row;
	        $sheet->setCellValue($cellno, $comp_pf);	        
	        $cellno="Q".$row;
	        $sheet->setCellValue($cellno, $pension);
	        $cellno="R".$row;
	        $sheet->setCellValue($cellno, $edli);
	        $cellno="S".$row;
	        $sheet->setCellValue($cellno, $adm_charge);
	        $cellno="T".$row;
	        $sheet->setCellValue($cellno, $adm_charge_edli);
	        $cellno="U".$row;
	        $sheet->setCellValue($cellno, $tot_company_contri_pf);
	        $cellno="V".$row;
	        $sheet->setCellValue($cellno, $company_contri_esi);
	        $row=$row+1;

	    }
	   // exit;
	    header('Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
	    header('Cache-Control: max-age=0');
	    //var_dump("Heere");
	    $writer->save('php://output'); // download file */
	}

	public function getsaltdsadvdata($empid,$month,$year)
	{

	    $this->load->model('Salarymodel');

	    //var_model($month);
	   // var_model($date);

	    $data=$this->Salarymodel->get_sal_adv_tds_data($empid,$month,$year);
	    return $data;

	    //var_dump($data);



	}

	public function sal_tds_adv_reimb_post()
	{

	    $this->load->model("Employeemodel");
	    $this->load->model("Referencemodel");
	    $this->load->model("Salarymodel");

	    $tdsval=$this->post("emp_sal_adv_tds");
	    $reimbval=$this->post("emp_sal_reimbursement");

	    if(empty($tdsval))
	    {
	        $this->Salarymodel->merge_adv_tds_data($tdsval);

	    }
	    if(empty($reimbval))
	    {
	        $this->Salarymodel->merge_emp_sal_reimbursement($reimbval);

	    }

	    $returndata['message']="Data Saved";
	    $returndata['code']="200";

	    $this->response( $returndata);



	}


	public function getsal_tds_adv_reimb_get()
	{

	    $this->load->model("Employeemodel");
	    $this->load->model("Referencemodel");
	    $this->load->model("Salarymodel");

	    $emp_code=$this->input->get("emp_code");
	    $month=$this->input->get("month");
	    $year=$this->input->get("year");
	    $datarow=array();
	    //convert emp_code to master code
	    if(!empty($emp_code)){

	        $retdata= $this->Employeemodel->get_empmasterid_from_code($emp_code);




	        if(!empty($retdata))
	        {
	        $emp_master_id=$retdata['id'];
	        $dataval=$this->Employeemodel->get_employee_details($emp_master_id);
	        $advtdsval=$this->Salarymodel->get_sal_adv_tds_data($emp_master_id,$month,$year);

	        $emp_name=$dataval['emp_first']." ".$dataval['emp_mid']." ".$dataval['emp_last'];
	        $datarow["emp_name"]= $emp_name;
	        $datarow["emp_code"]= $emp_code;
	        if(!empty($advtdsval))
	        {

	            $emp_sal_adv_tds=array(
	                "id"=>$advtdsval['id'],
	                "emp_master_id"=>$advtdsval['emp_master_id'],
	                "month"=>$advtdsval['month'],
	                "year"=>$advtdsval['year'],
	                "advance"=>doubleval($advtdsval['advance']),
	                "tds"=>doubleval($advtdsval['tds']),
	                "is_active"=>$advtdsval['is_active']

	            );
	        }else
	        {


	            $emp_sal_adv_tds=array(
	                "id"=>null,
	                "emp_master_id"=>$emp_master_id,
	                "month"=>$month,
	                "year"=>$year,
	                "advance"=>0.0,
	                "tds"=>0.0,
	                "is_active"=>0

	            );



	        }

	        $datarow["emp_sal_adv_tds"]= $emp_sal_adv_tds;

	        $reimbval=$this->Salarymodel->get_reimbursement_data($emp_master_id,$month,$year);

	        if(!empty($reimbval))
	        {


	            $emp_sal_reimbursement=array(
	                "id"=>$reimbval['id'],
	                "emp_master_id"=>$reimbval['emp_master_id'],
	                "month"=>$reimbval['month'],
	                "year"=>$reimbval['year'],
	                "reimbursement"=>doubleval($reimbval['reimbursement']),
	                "is_active"=>$reimbval['is_active'] );

	        }else{

	            $emp_sal_reimbursement=array(
	                "id"=>null,
	                "emp_master_id"=>$emp_master_id,
	                "month"=>$month,
	                "year"=>$year,
	                "reimbursement"=>0.0,
	                "is_active"=>0
	            );

	        }

	        $datarow["emp_sal_reimbursement"]= $emp_sal_reimbursement;
	        }

	    }

	    $this->response($datarow);
	}






	public function savesaltdsadvdata($empid,$month,$year,$gross,$days_in_month,$days_worked,$created_by,$modified_by)
	{

	    $this->load->model('Salarymodel');


	    $basic=0.0;
	    /***Calculation begins*/

	    $tot_days_worked_in_month=0;
	    $gross_monthly_sal=0;
	    $monthly_basic=0;
	    $prorata_gross=0;
	    $hra=0;
	    $conv_allowance=0;
	    $edu_allowance=0;
	    $special_allowance=0;
	    $upkeep_allowance=0;
	    $round_off=0;
	    $gross_earning=0;
	    $pf=0;
	    $esi=0;
	    $prof_tax=0;
	    $tot_deduction=0;
	    $sal_hand=0;
	    $pf_comp_contri=0;
	    $pension_fund=0;
	    $edli=0;
	    $adm_charge=0;
	    $adm_charge_eli=0;
	    $tot_company_contri_pf=0;
	    $company_contri_esi=0;
	    $ctc_per_month=0;
	    $is_active=1;
	   /* $created_at=0;
	    $created_by=0;
	    $modified_at="";
	    $modified_by="";*/

	    $emp_deduct_info= $this->Employeemodel->get_employee_salmaster_deduct_info($empid);
	    $pf_opted=$emp_deduct_info['emp_pf_opted'];
	    $pf=1.0;
	    if(empty($pf_opted))
	    {
	        $pf=0.0;
	    }
	    else if($pf_opted=="N")
	    {
	        $pf=0.0;

	    }

	    $emp_id=$empid;
	    $tot_days_worked_in_month=$days_worked;
	    $gross_monthly_sal=$gross;
		//var_dump("here1=".$days_worked);
		//exit;
	    $prorata_gross=$gross_monthly_sal *($days_worked/$days_in_month);
	    $monthly_basic=round(($prorata_gross * 35)/100);
	    $hra=round(($monthly_basic * 30	)/100);
	    $conv_allowance=round(($prorata_gross * 18)/100);
	    $edu_allowance=round(($prorata_gross * 15)/100);
	    $special_allowance=round(($prorata_gross * 9.5)/100);
	    $upkeep_allowance=round(($prorata_gross * 12)/100);
	    $round_off=$prorata_gross-($monthly_basic+$hra+$conv_allowance+$edu_allowance+$special_allowance+$upkeep_allowance);
	    $gross_earning=($monthly_basic+$hra+$conv_allowance+$edu_allowance+$special_allowance+$upkeep_allowance)+$round_off;


	    //PF
	    if($pf!=0.0)
	    {
	       $pf=round(($monthly_basic * 12	)/100);
	    }
	    if($gross_monthly_sal > 21000)
	    {
	        $esi=0;

	    }
	    else
	    {
	        $esi=round(($gross_earning * 1.75	)/100);
	    }

	    //professional tax starts
	    $prof_tax= 0.0;
	    if($tot_days_worked_in_month>0)
	    {
	    if($gross_earning<=15000.00)
	    {
	        $prof_tax=110.0;
	    }
	    elseif($gross_earning<=25000)
	    {
	        $prof_tax=130.00;

	    }
	    elseif($gross_earning<=40000)
	    {
	        $prof_tax=150.00;
	    }
	    else
	    {
	        $prof_tax=200.00;

	    }
	    }
	    //professional tax ends


	    $adv_tds_val=$this->getsaltdsadvdata($empid,$month,$year,$created_by,$modified_by);

	   // var_dump($adv_tds_val);
	   // exit;

	    if(empty($adv_tds_val))
	    {
	        $advance=0.0;
	        $tds=0.0;

	    }
	    else
	    {
	        $advance=$adv_tds_val['advance'];
	        $tds=$adv_tds_val['tds'];
	        //$tds=0.0;
	    }

	    $tot_deduction=$pf+$esi+$prof_tax+$advance+$tds;

	    $reimbursement=0.0;
	    $reimbursementval=array();
	    $reimbursementval=$this->Salarymodel->get_reimbursement_data($empid,$month,$year);
//	    //var_dump($reimbursementval);
	    if(!empty($reimbursementval))
	    {
	        $reimbursement=$reimbursementval['reimbursement'];
	        //$reimbursement=0;
	    }

	   // echo  "reimbursement=".$reimbursement;
	    $sal_hand=$gross_earning+$reimbursement-$tot_deduction;

	    //$tot_company_contri_pf=


	    if($pf==0.0)
	    {
	        $pension_fund=0.0;
	    }
	    elseif($monthly_basic>15000)
	    {
	        $pension_fund=(15000* 8.33)/100;

	    }
	    else
	    {
	        $pension_fund=round(($monthly_basic* 8.33)/100);

	    }

	    $pf_comp_contri=$pf-$pension_fund;


	    //EDLI

	    if($pf==0.0)
	    {
	        $edli=0.0;
	    }
	    elseif($monthly_basic>15000)
	    {
	        $edli=(15000.0*0.5)/100;



	    }
	    else
	    {
	        $edli=round(($monthly_basic*0.5)/100);


	    }
	    //adm charge



	    if($pf==0.0)
	    {
	        $adm_charge=0.0;
	    }
	    elseif($monthly_basic>15000)
	    {
	        $adm_charge=(15000.0*0.5)/100;

	    }
	    else
	    {
	        $adm_charge=round(($monthly_basic*0.5)/100);
	    }
	    //adm_charge_eli

	    if($monthly_basic==0)
	    {
	        $adm_charge_eli=0.0;
	    }
	    else
	    {


	        //$rounded_val=round(($monthly_basic*0.01)/100);
			$adm_charge_eli=0.0;
	        //echo "$rounded_val=" .$rounded_val;
	       // $adm_charge_eli=max(array(1,$rounded_val));



	    }

	    $tot_company_contri_pf=$pf_comp_contri+$pension_fund+$edli+$adm_charge+$adm_charge_eli;
 		/*var_dump($pf_comp_contri);
		var_dump($pension_fund);
		var_dump($edli);

		var_dump($adm_charge);
		var_dump($adm_charge_eli);
 	    var_dump($tot_company_contri_pf);

	    exit;*/

	    //company's contribution to esi

	    if($gross_monthly_sal>21000)
	    {
	        $company_contri_esi=0.0;

	    }
	    else{

	        $company_contri_esi=round(($prorata_gross*4.75)/100);
	    }
    
	    $ctc_per_month=$prorata_gross+$tot_company_contri_pf+$company_contri_esi;




	    $data_salary_details=array(
	        'emp_master_id' 			=>$emp_id,
	        'month' 					=>$month ,
	        'year' 						=>$year ,
	        'monthly_basic'         			=>$basic ,
	        'tot_days_worked_in_month'	=>$tot_days_worked_in_month,
	        'gross_monthly_sal' 		=>$gross_monthly_sal,
	        'monthly_basic' 			=>$monthly_basic,
	        'prorata_gross' 			=>$prorata_gross,
	        'hra' 						=>$hra,
	        'conv_allowance' 			=>$conv_allowance,
	        'edu_allowance' 			=>$edu_allowance,
	        'special_allowance' 		=>$special_allowance,
	        'upkeep_allowance' 			=>$upkeep_allowance,
	        'round_off' 				=>$round_off,
	        'gross_earning' 			=>$gross_earning,
	        'pf' 						=>$pf,
	        'esi' 						=>$esi,
	        'prof_tax' 					=>$prof_tax,
	        'tot_deduction' 			=>$tot_deduction,
	        'sal_hand' 					=>$sal_hand,
	        'pf_comp_contri'			=>$pf_comp_contri,
	        'pension_fund' 				=>$pension_fund,
	        'edli' 						=>$edli,
	        'adm_charge' 				=>$adm_charge,
	        'adm_charge_eli' 			=>$adm_charge_eli,
	        'tot_company_contri_pf' 	=>$tot_company_contri_pf,
	        'company_contri_esi' 		=>$company_contri_esi,
	        'ctc_per_month' 			=>$ctc_per_month,
	        'is_active' 				=>$is_active,
	        'created_by' 				=>$created_by,
	        'modified_by' 				=>$modified_by
	    );
	    //var_dump($data_salary_details);
		//exit;
	    $this->Salarymodel->insert_emp_sal_month($data_salary_details);




	}

	public function payrolldetails_get() {

	    $this->load->model("Employeemodel");
	    $this->load->model("Referencemodel");
	    $this->load->model("Salarymodel");
	    $this->load->model("Attendancelog");
	    $this->load->model("Atttendancemonthlysummary");
	    $deduct=0;

	    $month=$this->input->get("month");
	    $year=$this->input->get("year");
	    $process_id=$this->input->get("process_id");
	    $location_id=$this->input->get("location_id");
	    $emp_code=$this->input->get("emp_code");
	    $data['empsalinfo']=array();
	    if(!empty($emp_code))//get the details for the specific emp_code
	    {
	        $retdata= $this->Employeemodel->get_empmasterid_from_code($emp_code);

	        $summary=$this->Atttendancemonthlysummary->get_summary_data($emp_code,$month,$year);
	        if(!empty($deduct))
	        {
	           $deduct=$summary[0]['emp_final_deduct'];

	        }


	        $emp_master_id=$retdata['id'];
	        $datarow=$this->getpayrolldata($emp_master_id,$deduct,$month,$year);
	        //$data['employee']=array($datarow);
	        array_push($data['empsalinfo'],$datarow);

	    }else{
	        //get employee list for process_id and location_id

	        $group=array("srhr","admin");
	        $level=2; //this determines if the entire list to be shown or only selected ones depending on user access righgts

	        if ($this->ion_auth->in_group($group))
	        {
	            $level=1;

	        }

	        $retdata= $this->Employeemodel->get_employee_list($process_id,$location_id,"",$level);
	        //var_dump($retdata);
	        $deduct=0;
	        foreach ($retdata as $retdatarow)
	        {
	            $emp_code=$retdatarow['emp_code'];
	            $summary=$this->Atttendancemonthlysummary->get_summary_data($emp_code,$month,$year);
	            if(!empty($deduct))
	            {
	                $deduct=$summary[0]['emp_final_deduct'];

	            }
	            $emp_master_id= $retdatarow['id'];
	           $datarow=$this->getpayrolldata($emp_master_id,$deduct,$month,$year);
	            array_push($data['empsalinfo'],$datarow);

	        }
	    }


	   $this->response($data);

	}

	public function genpayroll_post() {

	    $this->load->model("Employeemodel");
	    $this->load->model("Referencemodel");
	    $this->load->model("Salarymodel");
	    $this->load->model("Attendancelog");
	    $this->load->model("Atttendancemonthlysummary");

	    $month=$this->post("month");
	    $year=$this->post("year");
	    $process_id=$this->post("process_id");
	    $location_id=$this->post("location_id");
	    $emp_code=$this->post("emp_code");
	    $created_by=$this->post("created_by");
	    $modified_by=$this->post("modified_by");
	    $returndata=array();
	    if(!empty($emp_code))//get the details for the specific emp_code
	    {
	       $this->savepayrolldata($emp_code,$month,$year,$created_by,$modified_by);
	       $returndata['message']="Payroll generated successfully";
	       $returndata['code']="200";

	    }
	    else{
	        //get employee list for process_id and location_id

	        $group=array("srhr","admin");
	        $level=2; //this determines if the entire list to be shown or only selected ones depending on user access righgts

	        if ($this->ion_auth->in_group($group))
	        {
	            $level=1;

	        }

	        $retdata= $this->Employeemodel->get_employee_list($process_id,$location_id,"",$level);
	        foreach ($retdata as $retdatarow)
	        {

	            $emp_code= $retdatarow['emp_code'];
	            $this->savepayrolldata($emp_code,$month,$year,$created_by,$modified_by);


	        }
	        $returndata['message']="Payroll generated successfully";
	        $returndata['code']="200";


	    }


	    $this->response($returndata);

	}

	public function savepayrolldata($emp_code,$month,$year,$created_by,$modified_by)
	{

	    $this->load->model("Employeemodel");
	    $this->load->model("Referencemodel");
	    $this->load->model("Salarymodel");
	    $this->load->model("Attendancelog");
	    $this->load->model("Atttendancemonthlysummary");

	    $retdata= $this->Employeemodel->get_empmasterid_from_code($emp_code);
	    $days_worked=0;
	    $emp_master_id=$retdata['id'];
	    $days_in_month=cal_days_in_month(CAL_GREGORIAN,$month,$year);
	    //$empid,$month,$year,$gross,$days_in_month,$days_worked
	    // var_dump($numd);
	    $emp_earn_info= $this->Employeemodel->get_employee_salmaster_earn_info($emp_master_id);
	    $gross=$emp_earn_info['emp_gross_sal'];

	    $monthsum=$this->Atttendancemonthlysummary->get_summary_data($emp_code,$month,$year);

	    if(!empty($monthsum))
	    {
	      $days_worked=$monthsum[0]['emp_final_attendance'];
	    }

	    $this->savesaltdsadvdata($emp_master_id,$month,$year,$gross,$days_in_month,$days_worked,$created_by,$modified_by);



	}

	public function getpayrolldata($emp_master_id,$deduct,$month,$year)
	{

	    $datarow=array();
	    $this->load->model("Employeemodel");
	    $this->load->model("Referencemodel");
	    $this->load->model("Salarymodel");
	    $this->load->model("Attendancelog");
	    $this->load->model("Atttendancemonthlysummary");

	    $dataval=$this->Employeemodel->get_employee_details($emp_master_id);

	    if(!empty($dataval))
	    {

	        $datarow["created_by"]=$dataval['created_by'];
	        $datarow["modified_by"]=$dataval['modified_by'];
	        $datarow["emp_code"]=$dataval['emp_code'];
	        $days_in_month=cal_days_in_month(CAL_GREGORIAN,$month,$year);
	        $datarow["days_in_month"]=$days_in_month;
	        $personal_details=array(

	            "emp_title"=>$dataval['emp_title'],
	            "emp_desig_id"=>$dataval['emp_desig_id'],
	            "process_id"=>$dataval['process_id'],
	            "loc_id"=>$dataval['loc_id'],
	            "emp_fa_hus_name"=>$dataval['emp_fa_hus_name'],
	            "emp_marital_status"=>$dataval['emp_marital_status'],
	            "emp_blood_grp"=>$dataval['emp_blood_grp'],
	            "emp_sub_function_id"=>$dataval['emp_sub_function_id'],
	            "emp_dob"=>$dataval['emp_dob'],
	            "emp_gender"=>$dataval['emp_gender'],
	            "emp_first"=>$dataval['emp_first'],
	            "emp_mid"=>$dataval['emp_mid'],
	            "emp_last"=>$dataval['emp_last'],
	            "emp_add1_id"=>$dataval['emp_add1_id'],
	            "emp_add_2_id"=>$dataval['emp_add_2_id'],
	            "emp_city_dist"=>$dataval['emp_city_dist'],
	            "emp_state"=>$dataval['emp_state'],
	            "emp_pin"=>$dataval['emp_pin'],
	            "emp_contact_num"=>$dataval['emp_contact_num'],
	            "emp_alt_contact_num"=>$dataval['emp_alt_contact_num'],
	            "is_active"=>$dataval['is_active'],
	            "emp_is_active"=>$dataval['emp_is_active']


	        );
	        $datarow["personal_details"]= $personal_details;

	        $bankinfoval=$this->Employeemodel->get_employee_bank_info($emp_master_id);


	        $bank_info=array(
	            "id" =>$bankinfoval['id'],
	            "emp_master_id" =>$bankinfoval['emp_master_id'],
	            "emp_bank_name" =>$bankinfoval['emp_bank_name'],
	            "emp_bank_ac_number" =>$bankinfoval['emp_bank_ac_number'],
	            "emp_bank_ifsc_code" =>$bankinfoval['emp_bank_ifsc_code']
	        );
	        $datarow["employee_bank_info"]= $bank_info;


	        $pfesicval=$this->Employeemodel->get_employee_pfesic_info($emp_master_id);
	        $employee_pf_esic_info=array(
	            "id" =>$pfesicval['id'],
	            "emp_master_id" =>$pfesicval['emp_master_id'],
	            "emp_pf_number" =>$pfesicval['emp_pf_number'],
	            "emp_esic_ac_number" =>$pfesicval['emp_esic_ac_number'],
	            "emp_uan_number" =>$pfesicval['emp_uan_number']
	        );
	        $datarow["employee_pf_esic_info"]= $employee_pf_esic_info;


	        $salmonthval=$this->Salarymodel->get_sal_month_data($emp_master_id,$month,$year);
	        //var_dump($salmonthval);
	       // exit;

	        $sal_month_info=array(
	            "id"=>$salmonthval['id'],
	            "emp_master_id"=>$salmonthval['emp_master_id'],
	            "month"=>$salmonthval['month'],
	            "year"=>$salmonthval['year'],
	            "tot_days_worked_in_month"=>$salmonthval['tot_days_worked_in_month'],
	            "gross_monthly_sal"=>$salmonthval['gross_monthly_sal'],
	            "monthly_basic"=>$salmonthval['monthly_basic'],
	            "prorata_gross"=>$salmonthval['prorata_gross'],
	            "hra"=>$salmonthval['hra'],
	            "conv_allowance"=>$salmonthval['conv_allowance'],
	            "edu_allowance"=>$salmonthval['edu_allowance'],
	            "special_allowance"=>$salmonthval['special_allowance'],
	            "upkeep_allowance"=>$salmonthval['upkeep_allowance'],
	            "round_off"=>$salmonthval['round_off'],
	            "gross_earning"=>$salmonthval['gross_earning'],
	            "pf"=>$salmonthval['pf'],
	            "esi"=>$salmonthval['esi'],
	            "prof_tax"=>$salmonthval['prof_tax'],
	            "tot_deduction"=>$salmonthval['tot_deduction'],
	            "sal_hand"=>$salmonthval['sal_hand'],
	            "pf_comp_contri"=>$salmonthval['pf_comp_contri'],
	            "pension_fund"=>$salmonthval['pension_fund'],
	            "edli"=>$salmonthval['edli'],
	            "adm_charge"=>$salmonthval['adm_charge'],
	            "adm_charge_eli"=>$salmonthval['adm_charge_eli'],
	            "tot_company_contri_pf"=>$salmonthval['tot_company_contri_pf'],
	            "company_contri_esi"=>$salmonthval['company_contri_esi'],
	            "ctc_per_month"=>$salmonthval['ctc_per_month'],
	            "is_active"=>$salmonthval['is_active'],
	            "daysdeduct"=>$deduct
	            );

	        $datarow["sal_month_info"]= $sal_month_info;


	        $advtdsval=$this->Salarymodel->get_sal_adv_tds_data($emp_master_id,$month,$year);

	        if(!empty($advtdsval))
	        {

	          $emp_sal_adv_tds=array(
	            "id"=>$advtdsval['id'],
	            "emp_master_id"=>$advtdsval['emp_master_id'],
	            "month"=>$advtdsval['month'],
	            "year"=>$advtdsval['year'],
	            "advance"=>$advtdsval['advance'],
	            "tds"=>$advtdsval['tds'],
	            "is_active"=>$advtdsval['is_active']

	        );
	        }else
	        {


	            $emp_sal_adv_tds=array(
	                "id"=>null,
	                "emp_master_id"=>$emp_master_id,
	                "month"=>$month,
	                "year"=>$year,
	                "advance"=>0.0,
	                "tds"=>0.0,
	                "is_active"=>0

	            );



	        }
	        $datarow["emp_sal_adv_tds"]= $emp_sal_adv_tds;

	        $reimbval=$this->Salarymodel->get_reimbursement_data($emp_master_id,$month,$year);

	        if(!empty($reimbval))
	        {


	           $emp_sal_reimbursement=array(
	               "id"=>$reimbval['id'],
	               "emp_master_id"=>$reimbval['emp_master_id'],
	               "month"=>$reimbval['month'],
	               "year"=>$reimbval['year'],
	               "reimbursement"=>$reimbval['reimbursement'],
	               "is_active"=>$reimbval['is_active'] );

	        }else{

	            $emp_sal_reimbursement=array(
	                "id"=>null,
	                "emp_master_id"=>$emp_master_id,
	                "month"=>$month,
	                "year"=>$year,
	                "reimbursement"=>0.0,
	                "is_active"=>0
	            );

	        }
	        $datarow["emp_sal_reimbursement"]= $emp_sal_reimbursement;
	        return $datarow;


	}
	}

	public function savepayrollweb_post() {

	    $this->load->model("Employeemodel");
	    $this->load->model("Referencemodel");
	    $this->load->model("Salarymodel");


	    $data=$this->post("empsalinfo");
	    $emp_sal_adv_tdsdata=$data['emp_sal_adv_tds'];
	    $emp_sal_reimbursement_data=$data['emp_sal_reimbursement'];
	    if(!empty($emp_sal_adv_tdsdata))
	    {

	        $datatosave=array(
	            "emp_master_id"=>$emp_sal_adv_tdsdata['emp_master_id'],
	            "month"=> $emp_sal_adv_tdsdata['month'],
	            "year"=> $emp_sal_adv_tdsdata['year'],
	            "advance"=> $emp_sal_adv_tdsdata['advance'],
	            "tds"=> $emp_sal_adv_tdsdata['tds'],
	            "is_active"=> 1
	        );
	        $this->Salarymodel->merge_adv_tds_data($datatosave);

	    }

	    if(!empty($emp_sal_reimbursement_data)){
	    $reimbdatatosave=array(
	        "emp_master_id"=>$emp_sal_reimbursement_data['emp_master_id'],
	        "month"=> $emp_sal_reimbursement_data['month'],
	        "year"=> $emp_sal_reimbursement_data['year'],
	        "reimbursement"=> $emp_sal_reimbursement_data['reimbursement'],
	        "is_active"=> 1
	    );

	    $this->Salarymodel-> merge_emp_sal_reimbursement($reimbdatatosave);

	    }

	    $returndata['message']="Data Saved Successfully";
	    $returndata['code']="200";

	    $this->response($returndata);

	}


	public function gen_empsalslip_get()
	{


	    $emp_code=$this->input->get("emp_code");
	    $month=$this->input->get("month");
	    $year=$this->input->get("year");

	    ob_start() ;
	    $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
	    $pdf->SetTitle('Pay Slip');
	    $pdf->SetHeaderMargin(30);
	    $pdf->SetTopMargin(20);
	    $pdf->setFooterMargin(20);
	    $pdf->SetAutoPageBreak(true);
	    $pdf->SetAuthor('Author');
	    $pdf->SetDisplayMode('real', 'default');

	    $pdf->AddPage();
	    //$data =array();
	    $html1=$this->empsalslip_html($emp_code,$month,$year);



	    $pdf->writeHTML($html1, true, false, true, false, '');
	   // header("Content-type:application/pdf");

	    // It will be called downloaded.pdf
	   // header("Content-Disposition:attachment;filename='downloaded.pdf'");

	    // The PDF source is in original.pdf
	   // readfile("original.pdf");
	    //$pdf->Output('My-File-Name.pdf','D');
	    $file_name="payslip_".$emp_code.".pdf";
	    $pdf->Output($file_name);
	    ob_end_flush();
	}
	public function empsalslip_html($emp_code,$month,$year)
	{

	    $this->load->model("Employeemodel");
	    $this->load->model("Referencemodel");
	    $this->load->model("Salarymodel");
	    $this->load->model("Atttendancemonthlysummary");


	    $data=array();

	    if(!empty($emp_code)){

	        $retdata= $this->Employeemodel->get_empmasterid_from_code($emp_code);

	        //var_dump($retdata);
	       // exit;

	        if(!empty($retdata))
	        {
	            $emp_master_id=$retdata['id'];
	            $dataval=$this->Employeemodel->get_employee_details($emp_master_id);


	            $emp_name=$dataval['emp_first']." ".$dataval['emp_mid']." ".$dataval['emp_last'];


	            $desig_data=$this->Referencemodel->get_designationdesc_from_id($dataval['emp_desig_id']);

	            $data["emp_name"]= $emp_name;
	            $data["emp_code"]= $emp_code;
	            $data["desig_descri"]= $desig_data[0]['emp_desig_desc'];

	            //$dateObj   = DateTime::createFromFormat('!m', $month);
	            $monthName = date('F', mktime(0, 0, 0, $month, 10)); // March
	            // $monthName = $dateObj->format('F'); // March
	            $data["year_month"]=$monthName." ". $year;

	            $joininfoval=$this->Employeemodel->get_employee_join_info($emp_master_id);
	            $joindate="";
	            if(!empty($joininfoval))
	            {

	                $empjd=$joininfoval['emp_join_date'];
	                if(!empty($empjd))
	                {
	                    $joindate = date("d-m-Y", strtotime($empjd));
	                }
	            }
	            $data["joindate"]=$joindate;

	        }

	        $bankinfoval=$this->Employeemodel->get_employee_bank_info($emp_master_id);

	        $bank_name="";
	        $bank_ac_numb="";
	        if(!empty($bankinfoval)){

	            $bank_name=$bankinfoval['emp_bank_name'];
	            $bank_ac_numb=$bankinfoval['emp_bank_ac_number'];
	        }

	        $data["bank_name"]=$bank_name;
	        $data["bank_ac_numb"]=$bank_ac_numb;



	        $pfesicval=$this->Employeemodel->get_employee_pfesic_info($emp_master_id);
	        $pf_number="";
	        $esi_number="";
	        if(!empty($pfesicval))
	        {
	            $pf_number=$pfesicval['emp_pf_number'];
	            $esi_number=$pfesicval['emp_esic_ac_number'];

	        }
	        $data["pf_number"]=$pf_number;
	        $data["esi_number"]=$esi_number;


	        //atendance summary
	        $emp_tot_day_month="";
	        $emp_tot_absent="";
	        $summary=$this->Atttendancemonthlysummary->get_summary_data($emp_code,$month,$year);
	        if(!empty($summary))
	        {

	            $emp_tot_day_month=$summary[0]['emp_tot_day_month'];
	            $emp_tot_absent=$summary[0]['emp_tot_absent'];
	        }
	        $data['emp_tot_day_month']=$emp_tot_day_month;
	        $data['emp_tot_absent']=$emp_tot_absent;



	        $salearnval=$this->Salarymodel->get_sal_month_data($emp_master_id,$month,$year);
	        // var_dump($salearnval);
	        // exit;
	        $emp_basic_sal=0.0;
	        $hra=0.0;
	        $conv_allowance=0.0;
	        $edu_allowance=0.0;
	        $upkeep_allowance=0.0;
	        $special_allowance=0.0;
	        $gross_sal=0.0;
	        $gross_earning=0.0;
	        $pf=0;
	        $esi=0.0;
	        $prof_tax=0.0;
	        $reimbursementamt=0.0;
	        if(!empty($salearnval))
	        {
	            $emp_basic_sal=$salearnval['monthly_basic'];
	            $hra=$salearnval['hra'];
	            $conv_allowance=$salearnval['conv_allowance'];
	            $edu_allowance=$salearnval['edu_allowance'];
	            $upkeep_allowance=$salearnval['upkeep_allowance'];
	            $special_allowance=$salearnval['special_allowance'];
	            $gross_earning=$salearnval['gross_earning'];

	            $pf=$salearnval['pf'];
	            $esi=$salearnval['esi'];
	            $prof_tax=$salearnval['prof_tax'];


	        }

	        $data['emp_basic_sal']=$emp_basic_sal;
	        $data['hra']=$hra;
	        $data['conv_allowance']=$conv_allowance;
	        $data['edu_allowance']=$edu_allowance;
	        $data['upkeep_allowance']=$upkeep_allowance;
	        $data['special_allowance']=$special_allowance;
	        $data['gross_earning']=$gross_earning;
	        $data['pf']=$pf;
	        $data['esi']=$esi;
	        $data['prof_tax']=$prof_tax;

	        //// var_dump($data);
	        // exit;

	        $reimbursement=0.0;
	        $reimbdata=$this->Salarymodel->get_reimbursement_data($emp_master_id,$month,$year);

	        if(!empty($reimbdata))
	        {
	            $reimbursementamt=$reimbdata['reimbursement'];

	        }
	        $data['reimbursementamt']=$reimbursementamt;
	        $saladvtds= $this->Salarymodel->get_sal_adv_tds_data($emp_master_id,$month,$year);
	        $tds=0.0;
	        $advance=0.0;
	        if(!empty($saladvtds))
	        {

	            $tds=$saladvtds['tds'];
	            $advance=$saladvtds['advance'];
	        }
	        $data['tds']=$tds;
	        $data['advance']=$advance;
	    }

	    $totgross=$gross_earning+$reimbursementamt;
	    $totdeduction=$pf+$esi+$prof_tax+$advance+$tds;
	    $netpayable=$totgross-$totdeduction;
	    $data['netpayable']=$netpayable;

	    $html1=$this->load->view('template/payslip',$data,true);
	    return $html1;



	}

	public function ziptest_get()
	{
	    $this->load->library('zip');
	    $this->load->model("Referencemodel");
	    $process_id=1;
	 //  $path = 'd:\temp\test1.pdf';
	  //  $data = 'A Data String!';
	  //  $this->zip->add_data($name, $data);
	    //$this->zip->read_file($path);
	 /*  $path="d:\\temp";
	   $this->zip->read_dir($path);
	   $this->zip->download('my_backup.zip');*/
	   //var_dump( realpath("./uploads"));

	   $process_data=$this->Referencemodel->get_process_info_by_code($process_id);


	   $process_name="payslip";
	   if(!empty($process_data))
	   {

	       foreach ($process_data as $row) {
	          var_dump($row);

	       }

	   }
	   $zipname=$process_name.".zip";

	}


	public function  downloadempfiles_get()
	{
	    $this->load->helper('file');
	    $this->load->library('zip');

	    $process_id=$this->get("process_id");
	    $location_id=$this->get("location_id");



	    $emp_code="61519/4";
	    $month=$this->input->get("month");
	    $year=$this->input->get("year");

	   ob_start() ;
	    $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
	    $pdf->SetTitle('Pay Slip');
	    $pdf->SetHeaderMargin(30);
	    $pdf->SetTopMargin(20);
	    $pdf->setFooterMargin(20);
	    $pdf->SetAutoPageBreak(true);
	    $pdf->SetAuthor('Author');
	    $pdf->SetDisplayMode('real', 'default');

	    $pdf->AddPage();
	    //$data =array();
	    $html1=$this->empsalslip_html($emp_code,$month,$year);



	    $pdf->writeHTML($html1, true, false, true, false, '');

	    $file_name="d:\/temp\/"."test1".".pdf";
	    $dirval="d:\/temp";
	    $pdf->Output( $file_name,'F');




	   ob_end_flush();
	    header('Content-Description: File Transfer');
	    header('Content-Type: application/zip');
	    header('Content-Disposition:attachment; filename='.basename($zipname));
	    header('Content-Length:'.filesize($zipname));
	    readfile($zipname);
	    $this->response();
	}



	public function  downloadPayslipForAdmin_get()
	{
	    $this->load->helper('file');
	    $this->load->library('zip');

	    $this->load->model("Employeemodel");
	    $this->load->model("Referencemodel");
	    $this->load->model("Salarymodel");
	    $this->load->model("Atttendancemonthlysummary");



	    $process_id=$this->get("process_id");
	    $location_id=$this->get("location_id");
	    $month=$this->input->get("month");
	    $year=$this->input->get("year");
	    $emp_code=$this->input->get("emp_code");

	    $emplist=$this->Employeemodel->get_employee_list($process_id,$location_id,$emp_code,2);


	    if(!empty($emplist))
	    {
	        $i=0;
	        $path=realpath("./uploads/temp/payslip/");
	        $dirname = uniqid();
	        $fullpath=$path."/".$dirname;
	        mkdir($fullpath);
	    foreach ($emplist as $empdata)
	    {
	       // echo $empdata['emp_code'];
	        $emp_code=$empdata['emp_code'];



	    $month=$this->input->get("month");
	    $year=$this->input->get("year");

	    ob_start() ;
	    $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
	    $pdf->SetTitle('Pay Slip');
	    $pdf->SetHeaderMargin(30);
	    $pdf->SetTopMargin(20);
	    $pdf->setFooterMargin(20);
	    $pdf->SetAutoPageBreak(true);
	    $pdf->SetAuthor('Author');
	    $pdf->SetDisplayMode('real', 'default');

	    $pdf->AddPage();
	    //$data =array();
	    $html1=$this->empsalslip_html($emp_code,$month,$year);



	    $pdf->writeHTML($html1, true, false, true, false, '');
	    $emp_code_copy=$emp_code;
	    $emp_code_copy=preg_replace('/[\W\s\/]+/', '-', $emp_code_copy);//replace the / in employee code with _

	    $file_name=$fullpath."/".$emp_code_copy.".pdf";
	    //$file_name="d://temp//".$i.".pdf";
	    $pdf->Output( $file_name,'F');

	    $this->zip->read_file($file_name);

	    ob_end_flush();
	   // $this->response();

	    }

	    $process_data=$this->Referencemodel->get_process_info_by_code($process_id);
	     

	    $process_name="payslip";
	    if(!empty($process_data))
	    {
	        $process_name =$process_data->process_name;
	       /* foreach ($process_data as $row) {
	           
	            $process_name = $row;
	            break;
	        }*/
	        

	    }
	    $location_name="";
	    $loc_data=$this->Referencemodel->get_location_info_by_code($location_id);

	    if(!empty($loc_data))
	    {
	        $location_name =$loc_data->location_name;
	        /*foreach ($loc_data as $row) {
	            $location_name = $row;
	            break;
	        }*/

	    }
	    if(empty($location_name))
	    {
	        $zipname=$process_name."_".$month."_".$year;
	    }
	    else
	    {
	        $zipname=$process_name."_".$location_name."_".$month."_".$year;


	    }
	  

	    $this->zip->download($zipname);
	    
	    $this->rmdir_recursive($fullpath);
	    /*header('Content-Description: File Transfer');
	    header('Content-Type: application/zip');
	    header('Content-Disposition:attachment; filename='.basename($zipname));
	    header('Content-Length:'.filesize($zipname));

	    $this->response();*/
	    }


	}


	function rmdir_recursive($dir) {
	    foreach(scandir($dir) as $file) {
	        if ('.' === $file || '..' === $file) continue;
	        if (is_dir("$dir/$file")) rmdir_recursive("$dir/$file");
	        else unlink("$dir/$file");
	    }
	    rmdir($dir);
	}


}
?>