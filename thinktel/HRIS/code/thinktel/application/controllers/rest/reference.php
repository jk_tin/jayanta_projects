<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'libraries\REST_Controller.php');
use Restserver\Libraries\REST_Controller;

class Reference extends REST_Controller
{

    public function __construct() {
        parent::__construct();
         $this->load->model('Referencemodel');
         $this->load->library('utility');

    }
    public function state_get()
    {
        $data=$this->Referencemodel->get_state_list();
        if($data)
        {
            $this->response($data, 200);
        }
        
        else
        {
            $this->response(NULL, 404);
        }
    }
    public function state_get_old(){

    $data=array();
    $arrayval=array(
    'id'=>"1",
    'state'=>"West Bengal"
    );

    array_push($data,$arrayval);

	$array1=array(
    'id'=>"2",
    'state'=>"Karnataka"
    );

     array_push($data,$array1);

		$array1=array(
	    'id'=>"2",
	    'state'=>"Maharashtra"
    );

    array_push($data,$array1);



			$array1=array(
		    'id'=>"2",
		    'state'=>"Odisha"
	    );

    array_push($data,$array1);


 			$array1=array(
 		    'id'=>"2",
 		    'state'=>"Assam"
 	    );

    array_push($data,$array1);


 			$array1=array(
 		    'id'=>"2",
 		    'state'=>"Bihar"
 	    );

    array_push($data,$array1);




	 			$array1=array(
	 		    'id'=>"2",
	 		    'state'=>"Andhra Pradesh"
	 	    );

    array_push($data,$array1);

				$array1=array(
			    'id'=>"2",
			    'state'=>"Jharkhand"
		    );

    array_push($data,$array1);

					$array1=array(
				    'id'=>"2",
				    'state'=>"Tamil Nadu"
			    );

    array_push($data,$array1);

    
    
    $this->response($data,200);

    $array1=array(
    'id'=>"2",
    'state'=>"Tamil Nadu"
    );

    array_push($data,$array1);
    $this->response($data,200);

    }
    public function processes_get(){


        $data=$this->Referencemodel->get_process_list();
        if($data)
        {
            $this->response($data, 200);
        }

        else
        {
            $this->response(NULL, 404);
        }

    }
    public function processesbyid_get(){
        
        $id=$this->get("process_id");
        
        $data=$this->Referencemodel->get_process_info_by_code($id);
        if($data)
        {
            $this->response($data, 200);
        }
        
        else
        {
            $this->response(NULL, 404);
        }
        
    }
    public function locationbyid_get(){
        
        $id=$this->get("loc_id");
        
        $data=$this->Referencemodel->get_location_info_by_code($id);
        if($data)
        {
            $this->response($data, 200);
        }
        
        else
        {
            $this->response(NULL, 404);
        }
        
    }
    public function usergroup_get(){


        $data=$this->Referencemodel->get_users_group();
        if($data)
        {
            $this->response($data, 200);
        }

        else
        {
            $this->response(NULL, 404);
        }

    }

    public function locations_get(){

		$data=array();
		$message=array();
        $process_id = $this->get('process_id');
        if(empty($process_id))
        {
         $process_id="";
        }

        $data=$this->Referencemodel->get_location_list_from_process_id($process_id);
       // var_dump($data);
       // exit;
        if($data)
        {

			$this->response($data, 200);
            //array_push($message,"message1","message2");

			//$responseval=$this->utility->prepareresponse($data,$message);



            $this->response($data, 200);
        }

        else
        {
            $this->response(NULL, 404);
        }



    }

        public function designations_get(){

        $data=$this->Referencemodel->get_designation_list();
        if($data)
        {


           $this->response($data, 200);
        }

        else
        {
            $this->response(NULL, 404);
        }
      }
      public function subfunction_get(){

          $data=$this->Referencemodel->get_sub_function_list();
          if($data)
          {


              $this->response($data, 200);
          }

          else
          {
              $this->response(NULL, 404);
          }
      }



        public function attndnccode_get(){


            $data=$this->Referencemodel->get_attendance_code();
            $message=array ('status' => FALSE,
                'message' => 'User could not be found');

            if($data)
            {

                $datasend= array_push($data,$message);
                $this->response($data, 200);
            }

            else
            {
                $this->response(NULL, 404);
            }

        }

        public function locationedit_post()
        {
            
           $loc_id=$this->post("id");
           $loc_desc=$this->post("location_name");
           
        
           if(!empty($loc_id)) //edit
           {
             $this->Referencemodel->location_edit($loc_id,$loc_desc);
           }
           else{
               
               $data=array(
                   "location_name"=>$loc_desc,
                   "created_by"=>-1,
                   "modified_by"=>-1
               );
               $this->Referencemodel->location_insert($data);
           }
           $returndata['responsedata']="Data updated successfully";
           $returndata['code']=200;
           $this->response($returndata);
            
        }

        public function processedit_post()
        {
            
            $proc_id=$this->post("id");
            $proc_desc=$this->post("process_name");
             
            if(!empty($proc_id)) //edit
            {
             $this->Referencemodel->process_edit($proc_id,$proc_desc);
            }
            else
            {
                $data=array(
                    "process_name"=>$proc_desc,
                    "created_by"=>-1,
                    "modified_by"=>-1
                );
                $this->Referencemodel->process_insert($data);
            }
 
            
            $returndata['responsedata']="Data updated successfully";
            $returndata['code']=200;
            $this->response($returndata);
            
            
        }
        public function processdel_post()
        {
            
            $proc_id=$this->post("process_id");
          
            
            $data=$this->Referencemodel->process_delete($proc_id);
            
            
            $returndata['responsedata']="Data deleted successfully";
            $returndata['code']=200;
            $this->response($returndata);
            
            
        }
        public function locationdel_post()
        {
            
            $loc_id=$this->post("loc_id");
            
            
            $data=$this->Referencemodel->location_delete($loc_id);
            
            
            $returndata['responsedata']="Data deleted successfully";
            $returndata['code']=200;
            $this->response($returndata);
            
            
        }
        
        public function test_get(){
            
            $id=$this->get("X");
            var_dump($id);
            exit;
        }
        public function locdetailsforprocess_get(){
            
            $id=$this->get("process_id");
             
            $data=$this->Referencemodel->get_loc_for_process($id);
             
                
                
                $this->response($data, 200);
             
        }
        
        public function linkprocloc_post(){
            $postdata=$this->post("proc_locmap");
            
           
            foreach ($postdata as $val)
            {
                $proc_id=$val['process_id']; 
                $created_by=$val['created_by']; 
                $modified_by=$val['modified_by']; 
                
                $data=$this->Referencemodel->delete_process_loc_map($proc_id);
                
                //exit;
                $loc_ids=$val['location_id'];
                if(!empty($loc_ids))
                {
                    //var_dump("Here");
                    foreach ($loc_ids as $loc_id )
                    {
                        $data=array(
                            "process_id" =>$proc_id,
                            "location_id"=>$loc_id,
                            "created_by"=>$created_by,
                            "modified_by"=>$modified_by,
                            
                        );
                        
                        $this->Referencemodel->insert_process_loc_map($data);
                        
                    }
                    
                    
                }
                
            }
            $returndata['responsedata']="Data updated successfully";
            $returndata['code']=200;
            $this->response($returndata);
            
            
        }
        

    }


