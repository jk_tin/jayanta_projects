<?php
defined('BASEPATH') or exit('No direct script access allowed');
require (APPPATH . 'libraries\REST_Controller.php');
require 'vendor/autoload.php';
use Restserver\Libraries\REST_Controller;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Attendance extends REST_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * http://example.com/index.php/welcome
     * - or -
     * http://example.com/index.php/welcome/index
     * - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     *
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
    }

    public function uploadatt_post()
    {

        $uploadpath= $_SERVER['DOCUMENT_ROOT']."/thinktel/uploads/attendance";

        $config['upload_path'] = $uploadpath;
        $config['allowed_types'] = 'xls|csv|xlsx';
        $config['max_size'] = 2048;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $process_id = $this->input->post("process_id");
        $location_id = $this->input->post("location_id");
        $created_by = $this->input->post("created_by");
        $modified_by = $this->input->post("modified_by");
        $month = $this->input->post("month");
        $year = $this->input->post("year");

        $returndata=array();

        $this->load->library('upload', $config);

        if (! $this->upload->do_upload('userfile')) {
            $error = array(
                'error' => $this->upload->display_errors()
            );
            $returndata['message']=$this->upload->display_errors();
            $returndata['code']=200;

           // var_dump($error);

            // $this->load->view('upload_form', $error);
        } else {
            $data = array(
                'upload_data' => $this->upload->data()
            );

            // var_dump($data);
            // exit;

            if (! empty($data)) {

                $file_name = $data['upload_data']['file_name'];
               // var_dump($uploadpath."/".$file_name);
               // exit;
                $this->updateAttendance($process_id, $location_id, $month, $year, $uploadpath."/".$file_name, $created_by, $modified_by);
               // rename($file_name, 'uploads/attendance/' . $file_name);
                $returndata['message']="Attendance Data Uploaded Succsessfully";
                $returndata['code']=200;
            }
        }

        $this->response( $returndata);

    }

    public function emplogold_get()
    {
        $this->load->model("Attendancelog");
        $this->load->model("Employeemodel");
        $emp_code = $this->get('emp_code');
        $month = $this->get('month');
        $year = $this->get('year');
        $day = $this->get('day');
        $process_id = $this->get('process_id');

        if (empty($month)) {
            $month = 0;
        }

        if (empty($year)) {
            $year = 0;
        }
        if (empty($day)) {
            $day = 0;
        }
        if (empty($process_id)) {
            $process_id = 0;
        }
        if (empty($emp_code)) {
            $emp_code = "";
        }

        // assumption is atleast year and month will be passed

        $dataval = $this->Attendancelog->get_emp_att_log_hash($emp_code, $day, $month, $year, $process_id);

        $returndata = array();
        $returndatarow = array();

        if (! empty($dataval)) // attendance data for this particular selection exists
        {

            foreach ($dataval as $datavalrow) // for this particular month and year get the employee id and record count
            {

                $i = 0;

                $emp_code = $datavalrow['emp_code'];

                $empdetailsdata = $this->Employeemodel->get_emp_details_by_empcode($emp_code);

                $empdata_db = $this->Attendancelog->get_emp_att_log_by_empcode($emp_code);

                $returndatarow['emp_code'] = $emp_code;
                $returndatarow['emp_location'] = $empdetailsdata[$i]['emp_desig'];
                $returndatarow['emp_designation'] = $empdetailsdata[$i]['location'];
                $returndatarow['emp_name'] = $empdetailsdata[$i]['emp_name'];

                $returndatarow['attendance'] = $empdata_db;

                array_push($returndata, $returndatarow);
                $i ++;
                // array_push($returndata,$empdetailsdata[0]['emp_name']);
            }
        } else {

            if ($emp_code != "") {
                $empdetailsdata = $this->Employeemodel->get_emp_details_by_empcode($emp_code);

                $returndatarow['emp_code'] = $emp_code;
                $returndatarow['emp_location'] = $empdetailsdata[0]['emp_desig'];
                $returndatarow['emp_designation'] = $empdetailsdata[0]['location'];
                $returndatarow['emp_name'] = $empdetailsdata[0]['emp_name'];
                $returndatarow['attendance'] = array();
                array_push($returndata, $returndatarow);
                // $returndata['value']="Jayanta";
                // array_push($returndata,"here11");
            } elseif ($process_id != 0) {

                $empcode_set = $this->Attendancelog->get_emp_code_by_proc_id($process_id);
                // array_push($returndata,$empcode_set);

                foreach ($empcode_set as $emp_code_row) {

                    $emp_code = $emp_code_row['emp_code'];
                    $empdetailsdata = $this->Employeemodel->get_emp_details_by_empcode($emp_code);
                    $returndatarow['emp_code'] = $emp_code;
                    $returndatarow['emp_location'] = $empdetailsdata[0]['emp_desig'];
                    $returndatarow['emp_designation'] = $empdetailsdata[0]['location'];
                    $returndatarow['emp_name'] = $empdetailsdata[0]['emp_name'];
                    $returndatarow['attendance'] = array();

                    array_push($returndata, $returndatarow);
                }
            } else // nothing is specified except date
            {
                $empcode_set = $this->Attendancelog->get_emp_code_all();

                foreach ($empcode_set as $emp_code_row) {
                    $emp_code = $emp_code_row['emp_code'];
                    $empdetailsdata = $this->Employeemodel->get_emp_details_by_empcode($emp_code);

                    $returndatarow['emp_code'] = $emp_code;
                    $returndatarow['emp_location'] = $empdetailsdata[0]['emp_desig'];
                    $returndatarow['emp_designation'] = $empdetailsdata[0]['location'];
                    $returndatarow['emp_name'] = $empdetailsdata[0]['emp_name'];
                    $returndatarow['attendance'] = array();

                    array_push($returndata, $returndatarow);
                }
            }
        }

        $this->response($returndata, 200);
    }
    public function attsave_post()
    {
        $this->load->model("Attendancelog");
        $this->load->model("Atttendancemonthlysummary");



        $attendance = $this->post('attendance');
        foreach ($attendance as $attdatarow) {

            $numberofdayspresent = 0;
            $numberofdaysabsent = 0;
            $numberofdayholiday = 0;
            $numberofdayleave = 0;
            $numberofdaylate = 0;
            $numberofdayweekoff = 0;
            $numberofdaysun = 0;
            $numberofdayautodeduct = 0;
            $numberofdayexptgrant = 0;
            $numberofdayfinaldeduct = 0;
            $numberofdaytot = 0;
            $numberofdayfinalatt = 0;
            $day = 0;


            $att_data =$attdatarow['att_data']  ;


            $emp_code = $attdatarow['emp_code'];
            $month = $attdatarow['month'];
            $year = $attdatarow['year'];
            $created_by = $attdatarow['created_by'];
            $modified_by = $attdatarow['modified_by'];
            $att_data = $attdatarow['att_data'];
            $numberofdayexptgrant = $attdatarow['emp_excep_grant'];
            $empexptgrantreason = $attdatarow['emp_excep_grant_reason'];



            foreach ($att_data as $att_data_row) {

                $emp_attendance_code_id = $att_data_row['emp_attendance_code_id'];
                $day= $att_data_row['day'];
                $modified_at = date('Y-m-d H:i:s');
                $data_attedance = array(
                    'emp_code' => $emp_code,
                    'day' => $day,
                    'month' => $month,
                    'year' => $year,
                    'emp_attendance_code_id' => $emp_attendance_code_id,
                    'created_by' => $created_by,
                    'modified_by' => $modified_by,
                    'modified_at' => $modified_at
                );

                $this->Attendancelog->create_log($data_attedance);
                // var_dump($emp_attendance_code_id);
                switch ($emp_attendance_code_id) {
                    case 1:
                        $numberofdayspresent = $numberofdayspresent + 1;
                        break;
                    case 2:
                        $numberofdaysabsent = $numberofdaysabsent + 1;
                        break;
                    case 3:
                        $numberofdayweekoff = $numberofdayweekoff + 1;
                        break;
                    case 4:
                        $numberofdaylate = $numberofdaylate + 1;
                        //var_dump($numberofdaylate);
                        break;
                    case 5:
                        $numberofdaysun = $numberofdaysun + 1;
                        break;
                    case 6:
                        $numberofdayholiday = $numberofdayholiday + 1;
                        break;
                    case 7:
                        $numberofdayleave = $numberofdayleave + 1;
                        break;
                    default: // nothing
                        break;
                }
            }

            $numberofdayautodeduct = $numberofdaysabsent + (int) ($numberofdaylate / 3);
            $numberofdayfinaldeduct = $numberofdayautodeduct - $numberofdayexptgrant;
            $numberofdaytot = count($att_data);
            $numberofdayfinalatt = $numberofdaytot - $numberofdayfinaldeduct;

            $data_attedance_month_sum = array(
                'emp_code' => $emp_code,
                'month' => $month,
                'year' => $year,
                'emp_tot_present' => $numberofdayspresent,
                'emp_tot_absent' => $numberofdaysabsent,
                'emp_tot_holiday' => $numberofdayholiday,
                'emp_tot_leave' => $numberofdayleave,
                'emp_tot_late' => $numberofdaylate,
                'emp_tot_weekly_off' => $numberofdayweekoff,
                'emp_tot_sunday' => $numberofdaysun,
                'emp_tot_auto_deduct' => $numberofdayautodeduct,
                'emp_excep_grant' => $numberofdayexptgrant,
                'emp_excep_grant_reason' => $empexptgrantreason,
                'emp_final_deduct' => $numberofdayfinaldeduct,
                'emp_tot_day_month' => $numberofdaytot,
                'emp_final_attendance' => $numberofdayfinalatt,
                'created_by' => $created_by,
                'modified_by' => $modified_by,
                'modified_at' => $modified_at
            );
            // var_dump($data_attedance_month_sum);
              $this->Atttendancemonthlysummary->create_monthly_summary($data_attedance_month_sum);
            }


            // var_dump($num_rows);
        }


    public function attsave_post_old()
    {
        $this->load->model("Attendancelog");
        $this->load->model("Atttendancemonthlysummary");

        $numberofdayspresent = 0;
        $numberofdaysabsent = 0;
        $numberofdayholiday = 0;
        $numberofdayleave = 0;
        $numberofdaylate = 0;
        $numberofdayweekoff = 0;
        $numberofdaysun = 0;
        $numberofdayautodeduct = 0;
        $numberofdayexptgrant = 0;
        $numberofdayfinaldeduct = 0;
        $numberofdaytot = 0;
        $numberofdayfinalatt = 0;
        $day = 0;

        $attendance = $this->post('attendance');
        foreach ($attendance as $attdatarow) {

            // var_dump($attdatarow);

            $att_data = $this->post('att_data');

            // we need to decide whether to insert or update in the attendance log table
            // we will do this based on a check on whether there is a summary record corresponding to that month for
            // that employee in thesummary table

            /*
             * $emp_code=$this->post('emp_code');
             * $month=$this->post('month');
             * $year=$this->post('year');
             * $created_by=$this->post('created_by');
             * $modified_by=$this->post('modified_by');
             */

            $emp_code = $attdatarow['emp_code'];
            $month = $attdatarow['month'];
            $year = $attdatarow['year'];
            $created_by = $attdatarow['created_by'];
            $modified_by = $attdatarow['modified_by'];
            $att_data = $attdatarow['att_data'];

            $empexptgrant = $attdatarow['emp_excep_grant'];
            $empexptgrantreason = $attdatarow['emp_excep_grant_reason'];


            $num_rows = $this->Atttendancemonthlysummary->check_summary_data_exists($emp_code, $month, $year);

            if ($num_rows <= 0) // insert followed by update
            {

                $this->Attendancelog->insert_att_log_summary($emp_code, $month, $year, $created_by, $modified_by, $att_data, $empexptgrant, $empexptgrantreason);
            } else {
                // echo "Here"; var_dump($att_data);
                // $this->response("reached",200);
                $this->Attendancelog->update_log($emp_code, $month, $year, $modified_by, $att_data);
                $wherearray = array(

                    'emp_code' => $emp_code,
                    'month' => $month,
                    'year' => $year
                );
                $data_attedance = array(
                    'modified_by' => $modified_by,
                    'emp_excep_grant' => $empexptgrant,
                    'emp_excep_grant_reason' => $empexptgrantreason
                );
                $this->Atttendancemonthlysummary->update_monthly_summary($data_attedance, $wherearray);

                $this->response("updated", 200);
            }

            // var_dump($num_rows);
        }
    }

    public function emplog_get()
    {
        $this->load->model("Attendancelog");
        $this->load->model("Employeemodel");
        $this->load->model("Atttendancemonthlysummary");
        $emp_code = $this->get('emp_code');
        $month = $this->get('month');
        $year = $this->get('year');
        $day = $this->get('day');
        $process_id = $this->get('process_id');
        $loc_id = $this->get('loc_id');
        $this->load->library('session');

        if (empty($month)) {
            $month = 0;
        }

        if (empty($year)) {
            $year = 0;
        }
        if (empty($day)) {
            $day = 0;
        }
        if (empty($process_id)) {
            $process_id = 0;
        }
        if (empty($emp_code)) {
            $emp_code = "";
        }
        if (empty($loc_id)) {
            $loc_id = 0;
        }

        // first fetch the employee or employees based on the selection crtierion

        $empdataval = $this->Attendancelog->get_emp_code_by_param($emp_code, $process_id, $loc_id);
        // find two sets for one emp_log for that log exists and for other emp_log does not exist
        // var_dump($empdataval);
        // exit;
        $empcode_emplog = array();
        $empcode_emplog_doesnotexist = array();
        $empsummarydetailsdatanotexist = array();
        $empsummarydetailsdataexist = array();
        $returndata = array();
        $returndatarow = array();

        if (! empty($empdataval)) {

            foreach ($empdataval as $datavalrow) {

                $emp_code = $datavalrow['emp_code'];
                $num_rows = $this->Atttendancemonthlysummary->check_summary_data_exists($emp_code, $month, $year);
                if ($num_rows > 0) // emp log created for that month
                {
                    array_push($empcode_emplog, $emp_code);
                    // var_dump("here1");
                } else {
                    // var_dump("N");
                    array_push($empcode_emplog_doesnotexist, $emp_code);
                    // var_dump("here2");
                }
                // exit;
                // exit;
            }

            if (! empty($empcode_emplog_doesnotexist)) {

                // insert record into the summary table for each emp_code
                foreach ($empcode_emplog_doesnotexist as $doesnotexitdatavalrow) {
                    $temp_emp_code = $doesnotexitdatavalrow;

                    // $emp_code=$emp_code_row['emp_code'];
                    $empdetailsdata = $this->Employeemodel->get_emp_details_by_empcode($temp_emp_code);
                    $empsummarydetailsdatanotexist = $this->Atttendancemonthlysummary->get_summary_data($temp_emp_code, $month, $year);

                    $returndatarow['emp_code'] = $temp_emp_code;
                    $returndatarow['emp_location'] = $empdetailsdata[0]['location'];
                    $returndatarow['emp_designation'] = $empdetailsdata[0]['emp_desig'];
                    $returndatarow['emp_name'] = $empdetailsdata[0]['emp_name'];
                    $returndatarow['emp_excep_grant'] = 0;
                    $returndatarow['emp_excep_grant_reason'] = "";
                    $returndatarow['attendance'] = array();

                    array_push($returndata, $returndatarow);
                }
            }
            if (! empty($empcode_emplog)) {

                // insert record into the summary table for each emp_code
                foreach ($empcode_emplog as $doesexitdatavalrow) {
                    $emp_code = $doesexitdatavalrow;
                    // var_dump($empcode_emplog);
                    // exit;

                    $i = 0;

                    $empdetailsdata = $this->Employeemodel->get_emp_details_by_empcode($emp_code);

                    $empdata_db = $this->Attendancelog->get_emp_att_log_by_empcode($emp_code, $day, $month, $year);
                    // var_dump($empdata_db);
                    // exit;

                    $empsummarydetailsdataexist = $this->Atttendancemonthlysummary->get_summary_data($emp_code, $month, $year);

                    $returndatarow['emp_code'] = $emp_code;
                    $returndatarow['emp_location'] = $empdetailsdata[$i]['location'];
                    $returndatarow['emp_designation'] = $empdetailsdata[$i]['emp_desig'];
                    $returndatarow['emp_name'] = $empdetailsdata[$i]['emp_name'];
                    $returndatarow['emp_excep_grant'] = (int) $empsummarydetailsdataexist[0]['emp_excep_grant'];
                    $returndatarow['emp_excep_grant_reason'] = $empsummarydetailsdataexist[0]['emp_excep_grant_reason'];

                    $returndatarow['attendance'] = $empdata_db;

                    array_push($returndata, $returndatarow);
                    // $i++;
                }
            }
        }

        $this->response($returndata, 200);
    }

    public function empmnthattumcalc_post()
    {
        $this->load->model("Attendancelog");
        $this->load->model("Atttendancemonthlysummary");
        $qdata = $this->post('querydata');
        $emp_code = "";

        if (! empty($qdata[0]['emp_code'])) {
            $emp_code = $qdata[0]['emp_code'];
        }

        $month = $qdata[0]['month'];
        $year = $qdata[0]['year'];
        $process_id = $qdata[0]['process_id'];
        $loc_id = $qdata[0]['loc_id'];
        $created_by = $qdata[0]['created_by'];
        $modified_by = $qdata[0]['modified_by'];

        if (empty($month)) {
            $month = 0;
        }

        if (empty($year)) {
            $year = 0;
        }
        if (empty($day)) {
            $day = 0;
        }
        if (empty($process_id)) {
            $process_id = 0;
        }
        /*
         * if(empty($emp_code))
         * {
         * $emp_code="";
         * }
         */
        if (empty($created_by)) {
            $created_by = - 1; // denoting system user
        }
        if (empty($modified_by_by)) {
            $modified_by = - 1; // denoting system user
        }
        if (empty($loc_id)) {
            $loc_id = 0;
            // var_dump($loc_code);
        }

        if (! empty($emp_code)) // emp_code supplied
        {

            $this->calculaandsaveattdata($emp_code, $month, $year, $created_by, $modified_by);
        } else { // employee code not supplied

            // get employee codes to be updated
            $logdata = $this->Attendancelog->get_emp_att_log_hash($emp_code, $day, $month, $year, $process_id);

            if (! empty($logdata)) {
                foreach ($logdata as $logdatarow) {
                    // var_dump($logdatarow);

                    $this->calculaandsaveattdata($logdatarow['emp_code'], $month, $year, $created_by, $modified_by);
                }
            }
            // function get_emp_att_log_hash($emp_code,$day,$month,$year,$process_id)
        }

        // var_dump($qdata);
        // exit;
    }

    function calculaandsaveattdata($emp_code, $month, $year, $created_by, $modified_by)
    {
        $startindexformonthdata = 0;
        $numberofdayspresent = 0;
        $numberofdaysabsent = 0;
        $numberofdayholiday = 0;
        $numberofdayleave = 0;
        $numberofdaylate = 0;
        $numberofdayweekoff = 0;
        $numberofdaysun = 0;
        $numberofdayautodeduct = 0;
        $numberofdayexptgrant = 0;
        $numberofdayfinaldeduct = 0;
        $numberofdaytot = 0;
        $numberofdayfinalatt = 0;

        // get the empolyee data from emp_attendance_log
        $this->load->model("Attendancelog");
        $this->load->model("Atttendancemonthlysummary");
        // var_dump($emp_code);
        $logdata = $this->Attendancelog->get_emp_att_log_by_empcode($emp_code, 0, $month, $year);
        if (! empty($logdata)) {
            // var_dump(count($logdata));
            foreach ($logdata as $logdatarow) {
                /*
                 * 1=PRESENT
                 * 2 =ABSENT
                 * 3 =WEEKLY OFF
                 * 4= LATE
                 * S=SUNDAY
                 * H=HOLIDAY
                 *
                 */

                $emp_attendance_code_id = $logdatarow['emp_attendance_code_id'];
                // var_dump($emp_attendance_code_id);
                switch ($emp_attendance_code_id) {
                    case 1:
                        $numberofdayspresent = $numberofdayspresent + 1;
                        break;
                    case 2:
                        $numberofdaysabsent = $numberofdaysabsent + 1;
                        break;
                    case 3:
                        $numberofdayweekoff = $numberofdayweekoff + 1;
                        break;
                    case 4:
                        $numberofdaylate = $numberofdaylate + 1;
                        //var_dump($numberofdaylate);
                        break;
                    case 5:
                        $numberofdaysun = $numberofdaysun + 1;
                        break;
                    case 6:
                        $numberofdayholiday = $numberofdayholiday + 1;
                        break;
                    case 7:
                        $numberofdayleave = $numberofdayleave + 1;
                        break;

                    default: // nothing
                        break;
                }
            }

            $numberofdayautodeduct = $numberofdaysabsent + (int) ($numberofdaylate / 3);
            $numberofdayfinaldeduct = $numberofdayautodeduct - $numberofdayexptgrant;
            $numberofdaytot = count($logdata);
            $numberofdayfinalatt = $numberofdaytot - $numberofdayfinaldeduct;

            // create if data does not exist else update
            $num_rows = $this->Atttendancemonthlysummary->check_summary_data_exists($emp_code, $month, $year);

            // var_dump($num_rows);
            // exit;
            if ($num_rows == 0) // no data exists so update else insert
            {
                $modified_at = date('Y-m-d H:i:s');
                $data_attedance_month_sum = array(
                    'emp_code' => $emp_code,
                    'month' => $month,
                    'year' => $year,
                    'emp_tot_present' => $numberofdayspresent,
                    'emp_tot_absent' => $numberofdaysabsent,
                    'emp_tot_holiday' => $numberofdayholiday,
                    'emp_tot_leave' => $numberofdayleave,
                    'emp_tot_late' => $numberofdaylate,
                    'emp_tot_weekly_off' => $numberofdayweekoff,
                    'emp_tot_sunday' => $numberofdaysun,
                    'emp_tot_auto_deduct' => $numberofdayautodeduct,
                    'emp_excep_grant' => $numberofdayexptgrant,
                    'emp_final_deduct' => $numberofdayfinaldeduct,
                    'emp_tot_day_month' => $numberofdaytot,
                    'emp_final_attendance' => $numberofdayfinalatt,
                    'created_by' => $created_by,
                    'modified_by' => $modified_by,
                    'modified_at' => $modified_at
                );

                $this->Atttendancemonthlysummary->create_monthly_summary($data_attedance_month_sum);
            } else { // update

                $wherearray = array(

                    'emp_code' => $emp_code,
                    'month' => $month,
                    'year' => $year,
                    'is_active' => 1
                );

                $data_attedance_month_sum = array(
                    'emp_tot_present' => $numberofdayspresent,
                    'emp_tot_absent' => $numberofdaysabsent,
                    'emp_tot_holiday' => $numberofdayholiday,
                    'emp_tot_leave' => $numberofdayleave,
                    'emp_tot_late' => $numberofdaylate,
                    'emp_tot_weekly_off' => $numberofdayweekoff,
                    'emp_tot_sunday' => $numberofdaysun,
                    'emp_tot_auto_deduct' => $numberofdayautodeduct,
                    'emp_excep_grant' => $numberofdayexptgrant,
                    'emp_final_deduct' => $numberofdayfinaldeduct,
                    'emp_tot_day_month' => $numberofdaytot,
                    'emp_final_attendance' => $numberofdayfinalatt,
                    'modified_by' => $modified_by
                );

                $this->Atttendancemonthlysummary->update_monthly_summary($data_attedance_month_sum, $wherearray);
            }

            // var_dump($numberofdayautodeduct);
        }

        // var_dump($logdata);
    }

    public function updateAttendance($process_id, $location_id, $month, $year, $filename, $created_by, $modified_by)
    {
        $data = $this->init->initPath('/mainctrl');
        $this->load->model('Attendancelog');
        $this->load->model('Atttendancemonthlysummary');
        // City Table
        $cityList = array();
        $cityListArray = array();

        $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
        $objReader->setReadDataOnly(true);

        // FileName and Sheet Nameuploads\attendance
        // $filename="att_samp.xlsx";
        $objPHPExcel = $objReader->load($filename);
        $worksheet = $objPHPExcel->getSheetByName('Sheet1');

        $highestRow = $worksheet->getHighestRow(); // e.g. 12
        $highestColumn = $worksheet->getHighestDataColumn(); // e.g M'

        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); // e.g. 7
                                                                                                                // Ignoring first row (As it contains column name)

        // Initializantion starts
        $num_days = 0;
        $startcol_index = 2; // start index of day one

        $offset_monthly_summary = 0; // may be removed later
                                     // Initializantion ends

        for ($row = 1; $row <= $highestRow; ++ $row) { // each employee one row
                                                       // echo "Inside Row";
                                                       // A row selected
            $oldval = $newval = 0;
            $flag = 0;
            $resetrowflag = 0;
            $emp_code = 0;
            $startindexformonthdata = 0;
            $numberofdayspresent = 0;
            $numberofdaysabsent = 0;
            $numberofdayholiday = 0;
            $numberofdayleave = 0;
            $numberofdaylate = 0;
            $numberofdayweekoff = 0;
            $numberofdaysun = 0;
            $numberofdayautodeduct = 0;
            $numberofdayexptgrant = 0;
            $numberofdayfinaldeduct = 0;
            $numberofdaytot = 0;
            $numberofdayfinalatt = 0;
            $day = 0;

            $flagcomplete = 0; // for completeion of parsing of all columns

            for ($col = 1; $col <= $highestColumnIndex; ++ $col) {

                $newval = $val = $worksheet->getCellByColumnAndRow($col, $row)->getValue();

                if ($row == 1) // try to find number of days in month
                {
                    if ($newval == "Code") // next value is day value
                    {
                        $flag = 1;
                    }

                    if ((is_numeric($newval))) {

                        $num_days = $num_days + 1;
                    }
                } else {

                    if (($col == 1) and ($resetrowflag == 0)) {

                        $resetrowflag = 1;
                        $emp_code = $newval;
                    }

                    if (($col >= $startcol_index) and ($col < $startcol_index + $num_days)) {

                        // echo "num_days=".$num_days;
                        $day = $day + 1;

                        $emp_att_code = $this->convert_attendancecode_to_id($newval);

                        switch ($newval) {
                            case "P":
                                $numberofdayspresent = $numberofdayspresent + 1;
                                break;
                            case "A":
                                $numberofdaysabsent = $numberofdaysabsent + 1;
                                break;
                            case "WO":
                                $numberofdayweekoff = $numberofdayweekoff + 1;
                                break;
                            case "T":
                                //var_dump($newval);
                                $numberofdaylate = $numberofdaylate + 1;
                                break;
                            case "S":
                                $numberofdaysun = $numberofdaysun + 1;
                                break;
                            case "H":
                                $numberofdayholiday = $numberofdayholiday + 1;
                                break;
                            case "L":
                                $numberofdayleave = $numberofdayleave + 1;
                                break;


                        }

                        // for attendance log table
                        $modified_at = date('Y-m-d H:i:s');
                        $data_attedance = array(
                            'emp_code' => $emp_code,
                            'day' => $day,
                            'month' => $month,
                            'year' => $year,
                            'emp_attendance_code_id' => $emp_att_code,
                            'created_by' => $created_by,
                            'modified_by' => $modified_by,
                            'modified_at' => $modified_at
                        );

                        $this->Attendancelog->create_log($data_attedance);
                    }

                    if ($day == $num_days) {


                        $data=$this->Atttendancemonthlysummary->get_summary_data($emp_code,$month,$year);

                        if(!empty($data))
                        {
                           $numberofdayexptgrant= $data[0]['emp_excep_grant'];
                           $excepgrantreason=$data[0]['emp_excep_grant_reason'];
                        }
                        else {

                            $numberofdayexptgrant= 0;
                            $excepgrantreason="";

                        }
                        $numberofdayautodeduct=$numberofdaysabsent+(int)($numberofdaylate/3);
                        $numberofdayfinaldeduct=$numberofdayautodeduct+$numberofdayexptgrant;
                        $numberofdayfinalatt=$num_days-$numberofdayfinaldeduct;
                        $numberofdaytot=$num_days;



                        $modified_at = date('Y-m-d H:i:s');
                        $data_attedance_month_sum = array(
                            'emp_code' => $emp_code,
                            'month' => $month,
                            'year' => $year,
                            'emp_tot_present' => $numberofdayspresent,
                            'emp_tot_absent' => $numberofdaysabsent,
                            'emp_tot_holiday' => $numberofdayholiday,
                            'emp_tot_leave' => $numberofdayleave,
                            'emp_tot_late' => $numberofdaylate,
                            'emp_tot_weekly_off' => $numberofdayweekoff,
                            'emp_tot_sunday' => $numberofdaysun,
                            'emp_tot_auto_deduct' => $numberofdayautodeduct,
                            'emp_excep_grant' => $numberofdayexptgrant,
                            'emp_excep_grant_reason' => $excepgrantreason,
                            'emp_final_deduct' => $numberofdayfinaldeduct,
                            'emp_tot_day_month' => $numberofdaytot,
                            'emp_final_attendance' => $numberofdayfinalatt,
                            'created_by' => $created_by,
                            'modified_by' => $modified_by,
                            'modified_at' => $modified_at
                        );
                        // var_dump($data_attedance_month_sum);
                        $this->Atttendancemonthlysummary->create_monthly_summary($data_attedance_month_sum);
                    }
                }
            }
        }
    }

    // end of updateAttendance
    function convert_attendancecode_to_id($codeintext)
    {
        $retval = 1;
        switch ($codeintext) {
            case "P":
                $retval = 1;
                break;
            case "A":

                $retval = 2;
                break;
            case "WO":

                $retval = 3;
                break;
            case "T":
                $retval = 4;
                break;
            case "S":
                $retval = 5;
                break;
            case "H":
                $retval = 6;
                break;
            case "L":
                $retval = 7;
                break;
            default:
                $retval = 2;
        }

        return $retval;
    }
}

?>