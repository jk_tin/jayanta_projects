<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries\REST_Controller.php');
require 'vendor/autoload.php';
use Restserver\Libraries\REST_Controller;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Users extends REST_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');


    }
    public function checkuser_get()
    {
        if ($this->ion_auth->username_check($this->input->post('identity')))
        {

            $returndata['message']="Username exits";
            $returndata['code']="201";

        }else {

            $returndata['message']="Username does not exit";
            $returndata['code']="202";

        }
        $returndata['code']=200;

        $this->response( $returndata);





    }


    public function createuser_post()
    {

        $tables = $this->config->item('tables', 'ion_auth');
        $identity_column = $this->config->item('identity', 'ion_auth');
        $returndata=array();
        $groupids=array();


        //$groups=$this->input->post('groups');
        $returndata['message']="Username exits";
       // $qdata=$this->post('identity');

        $identity=$this->post('identity');

        $password=$this->post('password');
        $groupval=$this->post('group');
        $email=$this->post('email');
        $first_name=$this->post('first_name');
        $last_name=$this->post('last_name');


        array_push($groupids, $groupval);
       // return;
        /*if(!empty($groups))
        {
            foreach ($groups as $groupval)
            {
                array_push($groupids, $groupval);

            }

        }*/


        if ($this->ion_auth->username_check($identity))
        {

            $returndata['message']="Username exits";
            $returndata['code']="202";

        }else
        {
            $email = strtolower($email);
            $identity = ($identity_column === 'email') ? $email : $identity;
           // $password = $this->input->post('password');

            $additional_data = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'company' => $this->input->post('company'),
                'phone' => $this->input->post('phone'),
            );
            $returndata['message']="User created Succsessfully-";
           // $returndata['password']=$password;
            $returndata['code']="201";

            $val=$this->ion_auth->register($identity, $password, $email, $additional_data,$groupids);

        }
        $this->response( $returndata);

    }
    public function say_get() {

        $returndata['emp_master_id']= "Heello";

        $returndata['code']=200;

        $this->response( $returndata);

    }

    public function usermenu_get(){

        $this->load->model("Usersmodel");
        $user_id=$this->input->get("userid");
        $data=$this->Usersmodel->get_user_menu($user_id);
        //var_dump($data);
        $this->response($data);

    }

}

?>