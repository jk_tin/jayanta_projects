(function () {
    'use strict';
 
    angular.module('employeeApp', ['moment-picker'])
    .controller('updateAdvanceController',['thinktelConstant','commonService', 'updateAdvanceService', '$q', function(thinktelConstant, commonService, updateAdvanceService, $q){
        var vm = this; 
        vm.title = 'Update Advance/TDS/Reimbarsment';
        vm.advanceDetail = null;
         vm.filter = {
            yearMonth: moment(),
            emp_code: ''
        }

        vm.saveAdvance = function(){
            updateAdvanceService.saveAdvance(vm.advanceDetail).then(function(res){
                bootbox.alert(res.message, function() {
                    var form = document.createElement("form"); 
                    form.method = "POST";
                    form.action =  thinktelConstant.baseUrl+"Mainctrl/index";
                    document.body.appendChild(form);
                    form.submit();
                });                
            }, function(err){
                console.log(err);
            });
        }

         vm.search = function(){
            $('.splash').css('display', 'block');
            vm.filter.year = moment(vm.filter.yearMonth).year();
            vm.filter.month = moment(vm.filter.yearMonth).month()+1;
            updateAdvanceService.getDetail(vm.filter).then(function(res){
                vm.advanceDetail = res;
                $('.splash').css('display', 'none');
            }, function(err){
                console.log(err);
                $('.splash').css('display', 'none');
            });
        }

        vm.init = function(){
            
        }
    }]);
})();