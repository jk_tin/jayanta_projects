(function () {
    'use strict';
 
    angular.module('employeeApp', [])
    .controller('createUserController',['thinktelConstant','commonService', 'createUserService', '$q', function(thinktelConstant, commonService, createUserService, $q){
        var vm = this; 
        vm.title = 'Create User';
        vm.userGroupList = [];
        vm.creteUserModel = {};

        vm.createUser = function(){
            createUserService.createUser(vm.creteUserModel).then(function(res){
                bootbox.alert(res.message, function() {
                    var form = document.createElement("form"); 
                    form.method = "POST";
                    form.action =  thinktelConstant.baseUrl+"Mainctrl/index";
                    document.body.appendChild(form);
                    form.submit();
                });                
            }, function(err){
                console.log(err);
            });
        }

        vm.init = function(){
            var promiseArray = [];            
            promiseArray.push(commonService.getUserGroup());
            $q.all(promiseArray).then(function(res){
                vm.userGroupList = res[0];
            }, function(err){
                console.log(err);
            });  
        }
    }]);
})();