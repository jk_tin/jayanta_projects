(function () {
    'use strict';
	angular.module('employeeApp')
    .service('addEmployeeService', ['$q', '$http','thinktelConstant','Upload', function($q, $http, thinktelConstant, Upload){
        var getEmployeeModel = function () {
            var deferred = $q.defer();
            $http({
                url: window.location.origin +'/thinktel/assets/model/employee.json',
                method: 'GET'
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });

            return deferred.promise;
        }

        var getEmployeeDetail = function (empId) {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'/employee/empdetails?emp_master_id='+empId,
                method: 'GET'
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });

            return deferred.promise;
        }

        var saveOrEditEmployee = function(employee){
            var deferred = $q.defer();
            $http({
                url:thinktelConstant.restBaseUrl+'/employee/add',
                method: 'POST',
                data: employee
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });

            return deferred.promise;
        }

        var uploadProfilePicture = function(uploadParameter){
            var deferred = $q.defer();
            var formData = new FormData();
            angular.forEach(uploadParameter, function(value, key) {
                formData.append(key, value);
            });

            $http.post(thinktelConstant.restBaseUrl+'employee/employeeimage', formData, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}

            }).then(function(res){
                 deferred.resolve(res.data);
            }, function(err){
                return deferred.reject(res);
            });
            return deferred.promise;
        }

        var getProfilePicture = function (empId) {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'employee/employeeimage',
                method: 'GET',
                params: {
                    emp_code: empId
                },
                responseType: 'arraybuffer'
            }).then(function(res){
                let image = btoa(
                    new Uint8Array(res.data)
                      .reduce((data, byte) => data + String.fromCharCode(byte), '')
                  );
                  let contentType = 'image/jpg';
                deferred.resolve(`data:${contentType};base64,${image}`);
            }, function(res){
                return deferred.reject(res);
            });

            return deferred.promise;
        }

        var uploadDocuments = function(files, emp_code){
            var deferred = $q.defer();

            Upload.upload({
                url: thinktelConstant.restBaseUrl+'/employee/uploademdoc',
                arrayKey: '',
                data:{"upl_files[]":files, emp_code: emp_code} //pass file as data, should be user ng-model
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });

            return deferred.promise;
        }

        var downloadDocument = function (empId) {
            var objectUrl = thinktelConstant.restBaseUrl+'employee/downloadempfiles?emp_code='+empId
            var link = angular.element('<a/>');
                link.attr({
                    href : objectUrl,
                    target: "_blank"
                })[0].click();


            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'employee/downloadempfiles',
                method: 'GET',
                params: {
                    emp_code: empId
                }
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });

            return deferred.promise;
        }

        var calculateSalary = function(data){
            var deferred = $q.defer();
            $http({
                url:thinktelConstant.restBaseUrl+'employee/empsalcalc?emp_code='+data.emp_code+'&gross_sal='+data.gross_sal+'&pfopt='+data.pfopt,
                method: 'GET'
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });

            return deferred.promise;
        }

        return {
            getEmployeeModel: getEmployeeModel,
            getEmployeeDetail: getEmployeeDetail,
            saveOrEditEmployee: saveOrEditEmployee,
            uploadProfilePicture: uploadProfilePicture,
            getProfilePicture: getProfilePicture,
            uploadDocuments: uploadDocuments,
            downloadDocument: downloadDocument,
            calculateSalary: calculateSalary
        }
    }])
})();