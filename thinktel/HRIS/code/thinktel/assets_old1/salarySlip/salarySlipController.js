(function () {
    'use strict';
    angular.module('employeeApp', ['nvd3', 'moment-picker'])
    .controller('salarySlipController', ['$q', 'payslipService','commonService','thinktelConstant', function($q, payslipService, commonService, thinktelConstant) {
        var vm = this; 
        vm.title = 'Payslip';
        vm.empLevel = null;
        vm.processMaster = [];
        vm.locationMaster = [];

        vm.downloadParameter = {
            currentDate: moment(),
            month: '',
            year: '',
            emp_code: '',
            process_id:'',
            location_id: ''
        } 
        
        vm.getLoationList = function(processId){
            if(processId == ''){
                vm.locationMaster = [];
                vm.filter.location_id = '';
                vm.downloadParameter.location_id = ''
                return;
            }
            $('.splash').css('display', 'block');
            commonService.getLocationMaster(processId).then(function(res){
                vm.locationMaster = res;
                $('.splash').css('display', 'none');
            }, function(err){
                console.log(err);
                $('.splash').css('display', 'none');
            });
        }

        vm.downloadPayslipForAdmin = function(){  
            vm.downloadParameter.year = moment(vm.downloadParameter.currentDate).year();
            vm.downloadParameter.month = moment(vm.downloadParameter.currentDate).month()+1;
            if(vm.empLevel == '1'){
                payslipService.downloadPayslipForAdmin(vm.downloadParameter);
            }else{
                payslipService.downloadPayslipForEmployee(vm.downloadParameter);
            }
            
        }

        vm.init = function(){
            vm.empLevel = $('#empLevel').val();
            var promiseArray = []; 
            vm.downloadParameter.emp_code = $("#empCode").val();
            promiseArray.push(commonService.getProcessMaster());
            //promiseArray.push(commonService.getLocationMaster());
            promiseArray.push(commonService.getDesignationMaster());
            $q.all(promiseArray).then(function(res){
                vm.processMaster = res[0];
                //vm.locationMaster = res[1].responseData;
                vm.designationMaster = res[1];
            }, function(err){
                console.log(err);
            });     
        }
    }]);
})();