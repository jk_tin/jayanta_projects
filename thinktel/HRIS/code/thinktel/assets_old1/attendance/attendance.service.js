(function () {
    'use strict';
	angular.module('employeeApp')
    .service('attendanceService', ['$q', '$http','thinktelConstant', function($q, $http, thinktelConstant){
        var getAttendanceDetailForAllEmpoloyee = function () {
            var deferred = $q.defer();
            $http({
                url: './assets/master/employeeAttendanceDetail.json',
                method: 'GET'
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });

            return deferred.promise;
        }

        var getEmployeeAttendance = function (filter) {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'attendance/emplog?emp_code='+filter.empId+'&month='+filter.month+'&year='+filter.year+"&process_id="+filter.process,
                method: 'GET'
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });

            return deferred.promise;
        }

        var uploadAttendanceExcel = function(uploadParameter){
            var deferred = $q.defer();
            var formData = new FormData();
            angular.forEach(uploadParameter, function(value, key) {
                formData.append(key, value);
            });


            $http.post(thinktelConstant.restBaseUrl+'attendance/uploadatt', formData, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}

            }).then(function(res){
                 deferred.resolve(res.data);
            }, function(err){
                return deferred.reject(res);
            });
            return deferred.promise;
        }

        var saveAttendanceForEmployee = function(data){
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'attendance/attsave',
                method: 'POST',
                data:  data
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });

            return deferred.promise;
        }
        return {
            getAttendanceDetailForAllEmpoloyee: getAttendanceDetailForAllEmpoloyee,
            uploadAttendanceExcel: uploadAttendanceExcel,
            getEmployeeAttendance: getEmployeeAttendance,
            saveAttendanceForEmployee: saveAttendanceForEmployee
        }
    }])
})();