(function () {
    'use strict';
 
    angular.module('employeeApp', ['moment-picker'])
    .controller('locationMasterController',['thinktelConstant','commonService', 'locationMasterService', '$q', function(thinktelConstant, commonService, locationMasterService, $q){
        var vm = this; 
        vm.title = 'Location Master';
        vm.advanceDetail = null;
         vm.filter = {
            yearMonth: moment(),
            emp_code: ''
        }

        vm.saveLocation = function(){
            locationMasterService.saveLocation(vm.advanceDetail).then(function(res){
                bootbox.alert(res.message, function() {
                    var form = document.createElement("form"); 
                    form.method = "POST";
                    form.action =  thinktelConstant.baseUrl+"LocationMaster/index";
                    document.body.appendChild(form);
                    form.submit();
                });                
            }, function(err){
                console.log(err);
            });
        }

        vm.init = function(){
            
        }
    }]);
})();