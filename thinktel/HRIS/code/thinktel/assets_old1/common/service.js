(function () {
    'use strict';
	angular.module('employeeApp')
    .constant('thinktelConstant', {
        baseUrl: window.location.origin +'/thinktel/index.php/',
       restBaseUrl: window.location.origin +'/thinktel/index.php/rest/'
    })
    .service('commonService', ['$q', '$http','thinktelConstant', function($q, $http, thinktelConstant){
        var getAttendanceStatusMaster = function () {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'reference/attndnccode',
                method: 'GET'
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });
            return deferred.promise;
        }

         var getProcessMaster = function () {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'reference/processes',
                method: 'GET'
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });
            return deferred.promise;
        }

        var getLocationMaster = function (process_id) {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'reference/locations?process_id='+process_id,
                method: 'GET'
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });
            return deferred.promise;
        }

        var getDesignationMaster = function () {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'reference/designations',
                method: 'GET'
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });
            return deferred.promise;
        }

        var getSubFunction = function () {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'reference/subfunction',
                method: 'GET'
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });
            return deferred.promise;
        }

        var getUserGroup = function () {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'reference/usergroup',
                method: 'GET'
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });
            return deferred.promise;
        }

        var logout = function () {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.baseUrl+'auth/logout',
                method: 'GET'
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });
            return deferred.promise;
        }

        var getMenu = function () {
        	var userId = $("#userId").val();
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'users/usermenu?userid='+userId,
                method: 'GET'
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });
            return deferred.promise;
        }

        var getStateMaster = function () {
		            var deferred = $q.defer();
		            $http({
		                url: thinktelConstant.restBaseUrl+'reference/state',
		                method: 'GET'
		            }).then(function(res){
		                deferred.resolve(res.data);
		            }, function(res){
		                return deferred.reject(res);
		            });
		            return deferred.promise;
        }

        return {
            getAttendanceStatusMaster: getAttendanceStatusMaster,
            getProcessMaster: getProcessMaster,
            getLocationMaster: getLocationMaster,
            getDesignationMaster: getDesignationMaster,
            getSubFunction: getSubFunction,
            getUserGroup: getUserGroup,
            logout:logout,
            getMenu: getMenu,
            getStateMaster: getStateMaster
        }
    }])
})();