(function () {
    'use strict';
 
    angular.module('employeeApp')
    .controller('headerController',['$q', 'commonService','thinktelConstant', function($q, commonService, thinktelConstant){
        var vm = this; 
        vm.menuList = [];

        vm.logout = function(){
            commonService.logout().then(function(res){               
                var form = document.createElement("form"); 
                form.method = "POST";
                form.action =  thinktelConstant.baseUrl+"Mainctrl/index";
                document.body.appendChild(form);
                form.submit();
            }, function(err){
                console.log(err); 
            });
        }

        vm.init = function(){
            commonService.getMenu().then(function(res){               
            	vm.menuList = res;
            }, function(err){
                console.log(err); 
            });
        }
        
    }]);
})();