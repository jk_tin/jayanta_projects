(function () {
    'use strict';
	angular.module('employeeApp')
    .directive('sidebar', ['thinktelConstant', function(thinktelConstant){
        return {
            templateUrl: thinktelConstant.baseUrl+"Sidebar/index",
            restrict: 'E',
            replace: true,
            scope: {
                "menuList": "=data"
            },
            controller:function($scope){
                console.log($scope.menuList);
            }
        }
    }])
})();