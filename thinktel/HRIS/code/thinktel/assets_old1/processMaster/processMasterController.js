(function () {
    'use strict';
 
    angular.module('employeeApp', ['moment-picker'])
    .controller('processMasterController',['thinktelConstant','commonService', 'processMasterService', '$q', function(thinktelConstant, commonService, locationMasterService, $q){
        var vm = this; 
        vm.title = 'Location Master';
        vm.processDetail = null;

        vm.saveProcess = function(){
            processMasterService.saveProcess(vm.advanceDetail).then(function(res){
                bootbox.alert(res.message, function() {
                    var form = document.createElement("form"); 
                    form.method = "POST";
                    form.action =  thinktelConstant.baseUrl+"ProcessMaster/index";
                    document.body.appendChild(form);
                    form.submit();
                });                
            }, function(err){
                console.log(err);
            });
        }

        vm.init = function(){
            
        }
    }]);
})();