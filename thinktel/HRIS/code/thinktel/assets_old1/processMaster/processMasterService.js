(function () {
    'use strict';
	angular.module('employeeApp')
    .service('processMasterService', ['$q', '$http','thinktelConstant', function($q, $http, thinktelConstant){
        var saveProcess = function (data) {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'salary/sal_tds_adv_reimb',
                method: 'POST',
                data: data
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });
            return deferred.promise;
        }

        var getProcess = function (filter) {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'salary/getsal_tds_adv_reimb?&month='+filter.month+'&year='+filter.year+'&emp_code='+filter.emp_code,
                method: 'GET'
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });

            return deferred.promise;
        }

        
        return {
            saveProcess: saveProcess,
            getProcess:getProcess
        }
    }])
})();