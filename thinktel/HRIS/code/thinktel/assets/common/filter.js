(function () {
    'use strict';
	angular.module('employeeApp')
    .filter('locationFilter', ['$filter',function ($filter) {  
        return function(id, location) {
            var output = [];
            var desc = $filter('filter')(location, {id: id})[0].location_name;
            return desc;
        };
    }])
    .filter('processFilter', ['$filter',function ($filter) {  
        return function(id, process) {
            var output = [];
            var desc = $filter('filter')(process, {id: id})[0].process_name;
            return desc;
        };
    }])
    .filter('designationFilter', ['$filter',function ($filter) {  
        return function(id, designation) {
            var output = [];
            var desc = $filter('filter')(designation, {id: id})[0].emp_desig_desc;
            return desc;
        };
    }]);
})();