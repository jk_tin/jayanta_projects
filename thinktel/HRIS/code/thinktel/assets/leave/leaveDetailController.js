(function () {
    'use strict';
    angular.module('employeeApp', ['nvd3', 'moment-picker'])
    .controller('leaveDetailController', leaveDetailController);
    function leaveDetailController() {
        var vm = this; 
        vm.title = 'Leave Detail';
    }
})();