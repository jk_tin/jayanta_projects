(function () {
    'use strict';
	angular.module('employeeApp')
    .service('payslipService', ['$q', '$http','thinktelConstant', function($q, $http, thinktelConstant){
        var downloadPayslipForAdmin = function (filter) {
            var objectUrl = thinktelConstant.restBaseUrl+'salary/downloadPayslipForAdmin?month='+filter.month+'&year='+filter.year+"&process_id="+filter.process_id+"&location_id="+filter.location_id+"&emp_code="+filter.emp_code
            var link = angular.element('<a/>');
                link.attr({
                    href : objectUrl,
                    target: "_blank"
                })[0].click();
        }

        var downloadPayslipForEmployee = function (filter) {
            var objectUrl = thinktelConstant.restBaseUrl+'salary/gen_empsalslip?&month='+filter.month+'&year='+filter.year+"&emp_code="+filter.emp_code
            var link = angular.element('<a/>');
                link.attr({
                    href : objectUrl,
                    target: "_blank"
                })[0].click();
        }

        return {
            downloadPayslipForAdmin: downloadPayslipForAdmin,
            downloadPayslipForEmployee: downloadPayslipForEmployee
        }
    }])
})();