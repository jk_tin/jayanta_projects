(function () {
    'use strict';
	angular.module('employeeApp')
    .service('processListService', ['$q', '$http','thinktelConstant', function($q, $http, thinktelConstant){
        var removeProcess = function (process_id) {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'reference/processdel',
                method: 'POST',
                data: {process_id: process_id}
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });

            return deferred.promise;
        }


        return {
            removeProcess: removeProcess
        }
    }])
})();