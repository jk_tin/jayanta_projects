(function () {
    'use strict';
    angular.module('employeeApp', ['nvd3', 'moment-picker'])
    .controller('processListController',['thinktelConstant','processListService','$q','commonService', function(thinktelConstant, processListService, $q, commonService){
         var vm = this;
        vm.title = 'Process List';
        vm.processMaster = [];

        vm.addProcess = function(){
            var form = document.createElement("form");
            form.method = "POST";
            form.action =  thinktelConstant.baseUrl+"ProcessMaster/addprocess";
            var i = document.createElement("input"); //input element, text
            i.setAttribute('type',"text");
           i.setAttribute('name',"processid");
           i.setAttribute('value',0);
           form.appendChild(i);
            document.body.appendChild(form);
            form.submit();
       }

       vm.editProcess = function(processId){
         var form = document.createElement("form");
         form.setAttribute('method',"POST");
         form.setAttribute('action',thinktelConstant.baseUrl+"ProcessMaster/editprocess");

           var i = document.createElement("input"); //input element, text
           i.setAttribute('type',"text");
           i.setAttribute('name',"processid");
           i.setAttribute('value',processId);
           form.appendChild(i);
           document.body.appendChild(form);
           form.submit();
       }

       vm.DeleteProcess = function(id, index){
        bootbox.confirm("Do You want to delete the process", function(result) {
            if(result){
                processListService.removeProcess(id).then(function(delRes){
                    bootbox.alert(delRes.responsedata);
                    vm.processMaster.splice(index, 1);
                }, function(err){
                    console.log(err);
                });
            }
        });  
    }

        vm.init = function(){
            $('.splash').css('display', 'block');
            var promiseArray = [];           
            promiseArray.push(commonService.getProcessMaster());
            $q.all(promiseArray).then(function(res){
                vm.processMaster = res[0];
                 $('.splash').css('display', 'none');
            }, function(err){
                console.log(err);
                 $('.splash').css('display', 'none');
            });
        }
    }]);
})();