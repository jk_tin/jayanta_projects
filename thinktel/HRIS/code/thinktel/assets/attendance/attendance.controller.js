(function () {
    'use strict';
	angular.module('employeeApp', ['nvd3', 'moment-picker'])
    .controller('attendanceController', ['$q', 'weekdayAndMonth', 'attendanceService', 'commonService','$filter', function($q, weekdayAndMonth, attendanceService, commonService, $filter){
        var vm = this; 
        vm.title = 'Attendance';         
        vm.weekday = weekdayAndMonth.weekday;
        vm.dates = [];
        vm.employees = [];
        vm.attendanceStatusMaster = [];
        vm.processMaster = [];
        vm.locationMaster = [];
        vm.uploadParameter = {
            currentDate: moment(),            
            userfile: null,
            year: null,
            month: null,
            location_id: '',
            process_id: '',
            created_by: 1,
            modified_by: 1
        }

        vm.filter = {
            yearMonth: moment(),
            process: '',
            location: '',
            empId: ''
        }

        vm.downloadParameter = {
            currentDate: moment(),
            process: '',
            location: ''
        }

        vm.prepareMonthData = function(){
             var y = moment.isMoment(vm.filter.yearMonth) ? vm.filter.yearMonth.toDate().getFullYear() : moment(vm.filter.yearMonth).toDate().getFullYear(),
                    m = moment.isMoment(vm.filter.yearMonth) ? vm.filter.yearMonth.toDate().getMonth() : moment(vm.filter.yearMonth).toDate().getMonth(),
                    firstDay = new Date(y, m, 1),
                    lastDay = new Date(y, m + 1, 0),
                    year = firstDay.getFullYear(),
                    month = firstDay.getMonth(),
                    day = firstDay.getDate(),
                    date = new Date(year, month, day);
                var longDay = new Date(year, month, day).getDay();
                vm.dates = [{
                    date: date.getTime(),
                    shortDate: day,
                    shortDay: weekdayAndMonth.weekday[date.getDay()],
                    day: day,
                    month: month +1,
                    year: year
                }]

            while(vm.dates[vm.dates.length-1].date < lastDay) { 
                day++;  
                var longDay = new Date(year, month, day).getDay();       
                vm.dates.push({
                        date: new Date(year, month, day).getTime(),
                        shortDate: day,
                        shortDay: weekdayAndMonth.weekday[longDay],
                        day: day,
                        month: month + 1,
                        year: year
                    });
            }
            prepareCompleteAttendanceData();
        }

        function prepareCompleteAttendanceData(){
            angular.forEach(vm.employees, function(employee){
                angular.forEach(vm.dates, function(day, index){
                    if(angular.isUndefined(employee.attendance[index])){
                        var attendanceObj = {
                            "id": null,
                            "date": day.date,
                            "emp_attendance_code_id": "2",
                            day: day.day,
                            month: day.month,
                            year: day.year
                        }
                        employee.attendance.push(attendanceObj);
                    }
                }); 
                 employee.month = employee.attendance[0].month;
                 employee.year = employee.attendance[0].year;              
            });
            console.log(vm.employees);
        }
        
        vm.saveAttendanceForEmnployee = function(attendanceForMonth){
            var attendanceDetail = [];
            angular.forEach(attendanceForMonth.attendance, function(attendance){
                attendanceDetail.push({
                    "day": attendance.day,
                    "emp_attendance_code_id": +attendance.emp_attendance_code_id
                });
            });
            var data = {
                attendance: [
                    {
                        "emp_code": ''+attendanceForMonth.emp_code,
                        "month":attendanceForMonth.month,
                        "year": ''+attendanceForMonth.year,
                        "created_by":"1",
                        "modified_by":"1",
                        "emp_excep_grant": angular.isDefined(attendanceForMonth.emp_excep_grant)? attendanceForMonth.emp_excep_grant : '',
                        "emp_excep_grant_reason": angular.isDefined(attendanceForMonth.emp_excep_grant_reason)?attendanceForMonth.emp_excep_grant_reason: '',
                        "att_data": attendanceDetail
                    }
                ]
            };
            
            attendanceService.saveAttendanceForEmployee(data).then(function(res){
                console.log(res);
            }, function(err){
                alert("Error");
            });
        } 

        vm.calculateStatus = function(employeeIndex, attendanceCode){
            //present
            vm.employees[employeeIndex].numPresentDay = (_.filter(vm.employees[employeeIndex].attendance, (attendancePerday) => attendancePerday.attendanceCode == 1)).length;
            
            //Absent
            vm.employees[employeeIndex].numAbsentDay = (_.filter(vm.employees[employeeIndex].attendance, (attendancePerday) => attendancePerday.attendanceCode == 2)).length;
            
            //Holiday
            vm.employees[employeeIndex].numHoliday = (_.filter(vm.employees[employeeIndex].attendance, (attendancePerday) => attendancePerday.attendanceCode == 3)).length;

            //Leave
            vm.employees[employeeIndex].numLeave = (_.filter(vm.employees[employeeIndex].attendance, (attendancePerday) => attendancePerday.attendanceCode == 4)).length;

            //Late
            vm.employees[employeeIndex].numLate = (_.filter(vm.employees[employeeIndex].attendance, (attendancePerday) => attendancePerday.attendanceCode == 5)).length;

            ///Week Off
            vm.employees[employeeIndex].numWeeklyOff = (_.filter(vm.employees[employeeIndex].attendance, (attendancePerday) => attendancePerday.attendanceCode == 6)).length;

            //Sunday
            vm.employees[employeeIndex].numSunday = (_.filter(vm.employees[employeeIndex].attendance, (attendancePerday) => attendancePerday.attendanceCode == 7)).length;

            //Auto Deduct
            vm.employees[employeeIndex].numAutoDeduct =  vm.employees[employeeIndex].numAbsentDay + (vm.employees[employeeIndex].numLate / 3);
            
            //Excpe Grant

            //Final Deduct
            vm.employees[employeeIndex].numFinalDeduct = vm.employees[employeeIndex].numAutoDeduct - vm.employees[employeeIndex].numExcpeGrant

            //Total Day In Month
            vm.employees[employeeIndex].numTotalDayInMonth = (_.filter(vm.employees[employeeIndex].attendance, (attendancePerday) => attendancePerday.attendanceCode != 0)).length;;

            //Final Attendance
            vm.employees[employeeIndex].numFinalAttendance = vm.employees[employeeIndex].numTotalDayInMonth - vm.employees[employeeIndex].numFinalDeduct;
        }

        vm.uploadAttendanceExcel = function(){
            $('.splash').css('display', 'block');
            vm.uploadParameter.year = moment(vm.uploadParameter.currentDate).year();
            vm.uploadParameter.month = moment(vm.uploadParameter.currentDate).month()+1;
            var filter = angular.copy(vm.uploadParameter);
            //delete filter.currentDate;
            attendanceService.uploadAttendanceExcel(vm.uploadParameter).then(function(res){
                vm.uploadParameter.userfile = null;
                bootbox.alert(res.message);
                $('.splash').css('display', 'none');
            }, function(err){
                console.log(err);
                $('.splash').css('display', 'none');
            });
        }

        vm.downloadAttendanceExcel = function(){
            $('.splash').css('display', 'block');
            vm.downloadParameter.year = moment(vm.downloadParameter.currentDate).year();
            vm.downloadParameter.month = moment(vm.downloadParameter.currentDate).month()+1;
            attendanceService.downloadAttendanceExcel(vm.downloadParameter).then(function(res){
                console.log(res);
                $('.splash').css('display', 'none');
            }, function(err){
                console.log(err);
                $('.splash').css('display', 'none');
            });
        }

        vm.applyFilter = function(){
            $('.splash').css('display', 'block');
            vm.filter.year = moment(vm.filter.yearMonth).year();
            vm.filter.month = moment(vm.filter.yearMonth).month()+1;
            attendanceService.getEmployeeAttendance(vm.filter).then(function(res){
                vm.employees = res;
                angular.forEach(vm.employees, function(emp){
                    angular.forEach(emp.attendance, function(attendance){
                        attendance.day = +attendance.day;
                    });
                });
                angular.forEach(vm.employees, function(emp){
                    emp.attendance = $filter('orderBy')(emp.attendance, 'day');
                });
                angular.forEach(vm.employees, function(emp){
                    angular.forEach(emp.attendance, function(attendance){
                        attendance.day = ''+attendance.day;
                    });
                });
                vm.prepareMonthData();
                $('.splash').css('display', 'none');
            }, function(err){
                console.log(err);
                $('.splash').css('display', 'none');
            });
        }

        vm.getLoationList = function(processId){
            if(processId == ''){
                vm.locationMaster = [];
                vm.filter.location = '';
                vm.uploadParameter.location_id = ''
                return;
            }
            $('.splash').css('display', 'block');
            commonService.getLocationMaster(processId).then(function(res){
                vm.locationMaster = res;
                $('.splash').css('display', 'none');
            }, function(err){
                console.log(err);
                $('.splash').css('display', 'none');
            });
        }

        vm.init = function(a){ 
            var promiseArray = [];
            
            promiseArray.push(commonService.getAttendanceStatusMaster());
            promiseArray.push(commonService.getProcessMaster());
            //promiseArray.push(commonService.getLocationMaster());
            $q.all(promiseArray).then(function(res){
                vm.attendanceStatusMaster = res[0];
                vm.processMaster = res[1];
                //vm.locationMaster = res[2].responseData;
            }, function(err){
                console.log(err);
            });           
        }
    }])
    .constant('weekdayAndMonth', {
        weekday: ['Sun', 'Mon', 'Tus', 'Wed', 'Thu', 'Fri', 'Sat'],
        months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    });
})();