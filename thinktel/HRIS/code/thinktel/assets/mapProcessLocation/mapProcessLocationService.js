(function () {
    'use strict';
	angular.module('employeeApp')
    .service('mapProcessLocationService', ['$q', '$http','thinktelConstant', function($q, $http, thinktelConstant){
        var locationDetailForProcess = function (process_id) {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'reference/locdetailsforprocess?process_id='+process_id,
                method: 'GET',
                //data: {process_id: process_id}
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });
            return deferred.promise;
        }

        var save = function (data) {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'reference/linkprocloc',
                method: 'POST',
                data: data
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });
            return deferred.promise;
        }

        
        return {
            locationDetailForProcess: locationDetailForProcess,
            save: save
        }
    }])
})();