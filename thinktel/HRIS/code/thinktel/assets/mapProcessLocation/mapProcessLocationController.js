(function () {
    'use strict';
 
    angular.module('employeeApp', [])
    .controller('mapProcessLocationController',['thinktelConstant','commonService', 'mapProcessLocationService', '$q', function(thinktelConstant, commonService, mapProcessLocationService, $q){
        var vm = this; 
        vm.title = 'Link Process Location';
        vm.processList = [];
        vm.reserveMasterLocation = [];
        vm.locationList = [];
        vm.linkedLocationList = [];
        vm.leftLocation = [];
        vm.rightLocation = [];
        vm.selectedProcess = '';
        vm.save = function(){
            $('.splash').css('display', 'block');
            var data = {
                "proc_locmap": [{
                    "process_id": vm.selectedProcess,
                    "created_by":"1",
                    "modified_by":1,
                    "location_id": vm.linkedLocationList.map(function(location){ return location.id})
                }]
            }
            mapProcessLocationService.save(data).then(function(res){
                bootbox.alert(res.responsedata); 
                vm.leftLocation = [];
                vm.rightLocation = [];   
                $('.splash').css('display', 'none');            
            }, function(err){
                console.log(err);
                $('.splash').css('display', 'none');
            });
        }

        vm.getLocationList = function(){
            $('.splash').css('display', 'block');
            vm.leftLocation = [];
            vm.rightLocation = []; 
            vm.linkedLocationList = [];
            vm.locationList = angular.copy( vm.reserveMasterLocation);
            mapProcessLocationService.locationDetailForProcess(vm.selectedProcess).then(function(res){
                if(res.length > 0){
                    angular.forEach(res, function(loc){
                        var index =  vm.locationList.findIndex(function(location){return location.id == loc.id});
                        var location =  vm.locationList.find(function(location){return location.id == loc.id});
                        if(index !== -1){
                             vm.linkedLocationList.push(location);
                             vm.locationList.splice(index, 1);
                        }
                     });
                     $('.splash').css('display', 'none');
                }else{
                    vm.linkedLocationList = [];
                    $('.splash').css('display', 'none');
                }                
            }, function(err){
                console.log(err);
                $('.splash').css('display', 'none');
            });
        }

        vm.addLocation = function(){
            angular.forEach(vm.leftLocation, function(locationId){
                var index =  vm.locationList.findIndex(function(location){return location.id == locationId});
                var location =  vm.locationList.find(function(location){return location.id == locationId});
                if(index !== -1){
                     vm.linkedLocationList.push(location);
                     vm.locationList.splice(index, 1);
                }
             });
             vm.leftLocation = [];
        }

        vm.deleteLocation = function(){
            angular.forEach(vm.rightLocation, function(locationId){
                var index =  vm.linkedLocationList.findIndex(function(location){return location.id == locationId});
                var location =  vm.linkedLocationList.find(function(location){return location.id == locationId});
                if(index !== -1){
                     vm.locationList.push(location);
                     vm.linkedLocationList.splice(index, 1);
                }
             });
             vm.rightLocation = [];
        }

        vm.init = function(){
            $('.splash').css('display', 'block');
            var promiseArray = [];            
            promiseArray.push(commonService.getLocationMaster(''));
            promiseArray.push(commonService.getProcessMaster());
            $q.all(promiseArray).then(function(res){
                vm.locationList = res[0];
                vm.processList = res[1];
                vm.reserveMasterLocation = angular.copy(vm.locationList);
                $('.splash').css('display', 'none');
            }, function(err){
                console.log(err);
                $('.splash').css('display', 'none');
            });  
        }
    }]);
})();