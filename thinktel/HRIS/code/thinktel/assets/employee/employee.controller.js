(function () {
    'use strict';
    angular.module('employeeApp', ['nvd3', 'moment-picker'])
    .controller('EmployeeController',['thinktelConstant','employeeService','$q','commonService', function(thinktelConstant, employeeService, $q, commonService){
         var vm = this;
        vm.title = 'Employees';
        vm.employeeList = [];
        vm.processMaster = [];
        vm.locationMaster = [];
        vm.filter = {
            process_id: '',
            location_id: '',
            empId: '',
            emp_code: ''
        }

        vm.addEmployee = function(){
        	 var form = document.createElement("form");
             form.method = "POST";
             form.action =  thinktelConstant.baseUrl+"employees/addemployee";
             document.body.appendChild(form);
             form.submit();
        }

        vm.editEmployee = function(empId){
          var form = document.createElement("form");
          form.setAttribute('method',"POST");
          form.setAttribute('action',thinktelConstant.baseUrl+"employees/editemployee");

            var i = document.createElement("input"); //input element, text
            i.setAttribute('type',"text");
            i.setAttribute('name',"empid");
            i.setAttribute('value',empId);
            form.appendChild(i);
            document.body.appendChild(form);
            form.submit();
        }

        vm.applyFilter = function(){
            $('.splash').css('display', 'block');
            employeeService.getEmployeeList(vm.filter).then(function(res){
                vm.employeeList = res;
                $('.splash').css('display', 'none');
            }, function(err){
                console.log(err);
                $('.splash').css('display', 'none');
            });
        }

        vm.getLoationList = function(){
            if(vm.filter.process_id == ''){
                vm.locationMaster = [];
                vm.filter.location_id = '';
                return;
            }
            $('.splash').css('display', 'block');
            commonService.getLocationMaster(vm.filter.process_id).then(function(res){
                vm.locationMaster = res;
                $('.splash').css('display', 'none');
            }, function(err){
                console.log(err);
                $('.splash').css('display', 'none');
            });
        }

        vm.init = function(){
            $('.splash').css('display', 'block');
            var promiseArray = [];
            promiseArray.push(employeeService.getEmployeeList(vm.filter));
            promiseArray.push(commonService.getProcessMaster());
            //promiseArray.push(commonService.getLocationMaster());
            $q.all(promiseArray).then(function(res){
                vm.employeeList = res[0];
                vm.processMaster = res[1];
                //vm.locationMaster = res[2].responseData;
                 $('.splash').css('display', 'none');
            }, function(err){
                console.log(err);
                 $('.splash').css('display', 'none');
            });
        }
    }]);
})();