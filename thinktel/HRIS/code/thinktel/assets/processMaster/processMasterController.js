(function () {
    'use strict';
 
    angular.module('employeeApp', ['moment-picker'])
    .controller('processMasterController',['thinktelConstant','commonService', 'processMasterService', '$q', function(thinktelConstant, commonService, processMasterService, $q){
        var vm = this; 
        vm.processId = null;
        vm.process = {};
        vm.title = 'Process Master';
        vm.processDetail = null;

        vm.saveProcess = function(){
            processMasterService.saveProcess(vm.process).then(function(res){
                bootbox.alert(res.responsedata, function() {
                    var form = document.createElement("form"); 
                    form.method = "POST";
                    form.action =  thinktelConstant.baseUrl+"ProcessMaster/index";
                    document.body.appendChild(form);
                    form.submit();
                });                
            }, function(err){
                console.log(err);
            });
        }

        vm.init = function(){
            $('.splash').css('display', 'block');
            vm.processId = $('#processId').val();            
            if(vm.processId > 0){
                processMasterService.getProcessById(vm.processId).then(function(res){
                    vm.process = res;
                    $('.splash').css('display', 'none');
                }, function(err){
                    $('.splash').css('display', 'none');
                    console.log(err);
                });
            }else{
                $('.splash').css('display', 'none');
            }
        }
    }]);
})();