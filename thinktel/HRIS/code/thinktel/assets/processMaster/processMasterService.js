(function () {
    'use strict';
	angular.module('employeeApp')
    .service('processMasterService', ['$q', '$http','thinktelConstant', function($q, $http, thinktelConstant){
        var saveProcess = function (data) {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'reference/processedit',
                method: 'POST',
                data: data
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });
            return deferred.promise;
        }

        var getProcessById = function (processId) {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'reference/processesbyid?process_id='+processId,
                method: 'GET'
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });

            return deferred.promise;
        }

        
        return {
            saveProcess: saveProcess,
            getProcessById:getProcessById
        }
    }])
})();