(function () {
    'use strict';
	angular.module('employeeApp')
    .service('locationListService', ['$q', '$http','thinktelConstant', function($q, $http, thinktelConstant){
        var removeLocation = function (locationId) {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'reference/locationdel',
                method: 'POST',
                data: {loc_id: locationId}
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });

            return deferred.promise;
        }


        return {
            removeLocation: removeLocation
        }
    }])
})();