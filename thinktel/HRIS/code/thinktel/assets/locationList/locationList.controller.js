(function () {
    'use strict';
    angular.module('employeeApp', ['nvd3', 'moment-picker'])
    .controller('locationListController',['thinktelConstant','locationListService','$q','commonService', function(thinktelConstant, locationListService, $q, commonService){
         var vm = this;
        vm.title = 'Location List';
        vm.locationMaster = [];

        vm.addLocation = function(){
        	 var form = document.createElement("form");
             form.method = "POST";
             form.action =  thinktelConstant.baseUrl+"LocationMaster/addlocation";
             var i = document.createElement("input"); //input element, text
             i.setAttribute('type',"text");
            i.setAttribute('name',"locationid");
            i.setAttribute('value',0);
            form.appendChild(i);
             document.body.appendChild(form);
             form.submit();
        }

        vm.editLocation = function(locationId){
          var form = document.createElement("form");
          form.setAttribute('method',"POST");
          form.setAttribute('action',thinktelConstant.baseUrl+"LocationMaster/editlocation");

            var i = document.createElement("input"); //input element, text
            i.setAttribute('type',"text");
            i.setAttribute('name',"locationid");
            i.setAttribute('value',locationId);
            form.appendChild(i);
            document.body.appendChild(form);
            form.submit();
        }

        vm.DeleteLocation = function(id, index){
            bootbox.confirm("Do You want to delete the location", function(result) {
                if(result){
                    locationListService.removeLocation(id).then(function(delRes){
                        bootbox.alert(delRes.responsedata);
                        vm.locationMaster.splice(index, 1);
                    }, function(err){
                        console.log(err);
                    });
                }
            });  
        }

        vm.init = function(){
            $('.splash').css('display', 'block');
            var promiseArray = [];           
            promiseArray.push(commonService.getLocationMaster(''));
            $q.all(promiseArray).then(function(res){
                vm.locationMaster = res[0];
                 $('.splash').css('display', 'none');
            }, function(err){
                console.log(err);
                 $('.splash').css('display', 'none');
            });
        }
    }]);
})();