(function () {
    'use strict';
	angular.module('employeeApp')
    .service('locationMasterService', ['$q', '$http','thinktelConstant', function($q, $http, thinktelConstant){
        var saveLocation = function (data) {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'reference/locationedit',
                method: 'POST',
                data: data
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });
            return deferred.promise;
        }

        var getLocationById = function (locationId) {
            var deferred = $q.defer();
            $http({
                url: thinktelConstant.restBaseUrl+'reference/locationbyid?loc_id='+locationId,
                method: 'GET'
            }).then(function(res){
                deferred.resolve(res.data);
            }, function(res){
                return deferred.reject(res);
            });

            return deferred.promise;
        }

        
        return {
            getLocationById: getLocationById,
            saveLocation:saveLocation
        }
    }])
})();