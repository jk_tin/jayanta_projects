(function () {
    'use strict';
 
    angular.module('employeeApp', ['moment-picker'])
    .controller('locationMasterController',['thinktelConstant','commonService', 'locationMasterService', '$q', function(thinktelConstant, commonService, locationMasterService, $q){
        var vm = this; 
        vm.locationId = null;
        vm.location = {};
        vm.title = 'Location Master';
        vm.advanceDetail = null;
         vm.filter = {
            yearMonth: moment(),
            emp_code: ''
        }

        vm.saveLocation = function(){
            locationMasterService.saveLocation(vm.location).then(function(res){
                bootbox.alert(res.responsedata, function() {
                    var form = document.createElement("form"); 
                    form.method = "POST";
                    form.action =  thinktelConstant.baseUrl+"LocationMaster/index";
                    document.body.appendChild(form);
                    form.submit();
                });                
            }, function(err){
                console.log(err);
            });
        }

        vm.init = function(){
            $('.splash').css('display', 'block');
            vm.locationId = $('#locationId').val();
            if(vm.locationId > 0){
                locationMasterService.getLocationById(vm.locationId).then(function(res){
                    vm.location = res;
                    $('.splash').css('display', 'none');
                }, function(err){
                    $('.splash').css('display', 'none');
                    console.log(err);
                });
            }else{
                $('.splash').css('display', 'none');
            }
        }
    }]);
})();