(function () {
    'use strict';
	angular.module('employeeApp', ['nvd3', 'moment-picker'])
    .controller('salaryInitiatController', ['$q', 'salaryService','commonService','thinktelConstant', function($q, salaryService, commonService, thinktelConstant) {
        var vm = this;
        vm.title = 'Salary Initiation';
        vm.currentYearMonth = moment();
        vm.processMaster = [];
        vm.locationMaster = [];
        vm.designationMaster = [];
        vm.processMasterList = [];

        vm.filter = {
            yearMonth: moment(),
            process_id: '',
            location_id: '',
            emp_code: '',
            created_by:1,
        	modified_by:1
        }

        vm.updateFilter = {
            yearMonth: moment(),
            process_id: '',
            location_id: '',
            emp_code: '',
            created_by:1,
        	modified_by:1
        }

        vm.downloadParameter = {
            currentDate: moment(),
            month: '',
            year: '',
            process_id:'',
            location_id: ''
        }

        vm.downloadSalaryExcel = function(){
            vm.downloadParameter.year = moment(vm.downloadParameter.currentDate).year();
            vm.downloadParameter.month = moment(vm.downloadParameter.currentDate).month()+1;
            salaryService.downloadAttendanceExcel(vm.downloadParameter);
        }

        vm.applyFilter = function(){
            $('.splash').css('display', 'block');
            vm.filter.year = moment(vm.filter.yearMonth).year();
            vm.filter.month = moment(vm.filter.yearMonth).month()+1;
            salaryService.getEmpoloyeeSalaryDetail(vm.filter).then(function(res){
                vm.employeesSalary = res.empsalinfo;
                $('.splash').css('display', 'none');
            }, function(err){
                console.log(err);
                $('.splash').css('display', 'none');
            });
        }

        vm.generatePayRoll = function(){
            $('.splash').css('display', 'block');
            vm.filter.year = moment(vm.filter.yearMonth).year();
            vm.filter.month = moment(vm.filter.yearMonth).month()+1;
            salaryService.generatePayRoll(vm.filter).then(function(res){
                bootbox.alert(res.message, function() {});
                $('.splash').css('display', 'none');
            }, function(err){
                console.log(err);
                $('.splash').css('display', 'none');
            });
        }

        vm.saveSalary = function(empSalDetail){
            $('.splash').css('display', 'block');
            var data = {
                empsalinfo: empSalDetail
            }
            salaryService.saveSalary(data).then(function(res){
                bootbox.alert(res.message, function() {});
                $('.splash').css('display', 'none');
            }, function(err){
                console.log(err);
                $('.splash').css('display', 'none');
            });
        }

        vm.getLoationList = function(processId){
            if(processId == ''){
                vm.locationMaster = [];
                vm.filter.location_id = '';
                vm.downloadParameter.location_id = ''
                return;
            }
            $('.splash').css('display', 'block');
            commonService.getLocationMaster(processId).then(function(res){
                vm.locationMaster = res;
                $('.splash').css('display', 'none');
            }, function(err){
                console.log(err);
                $('.splash').css('display', 'none');
            });
        }

        vm.init = function(a){
            var promiseArray = [];
            promiseArray.push(commonService.getProcessMaster());
            promiseArray.push(commonService.getLocationMaster(''));
            promiseArray.push(commonService.getDesignationMaster());
            $q.all(promiseArray).then(function(res){
                vm.processMaster = res[0];
                vm.processMasterList = res[1];
                vm.designationMaster = res[2];
            }, function(err){
                console.log(err);
            });
        }

    }]);
})();