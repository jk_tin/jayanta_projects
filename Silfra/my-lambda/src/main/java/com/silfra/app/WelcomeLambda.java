package com.silfra.app;
import java.util.Map;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.LambdaLogger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class WelcomeLambda  implements RequestHandler<Object, Response>
{

	 //latest code as on 15-01-2022
	Gson gson = new GsonBuilder().setPrettyPrinting().create();
	  @Override
	  public Response handleRequest(Object event, Context context)
	  {
	    LambdaLogger logger = context.getLogger();
	    //String response = "200 OK";
	    Response response= new Response();
	    response.setBody("Welcome to Lambda Test!!! Working fine now");
	    response.setStatusCode(200);
	    // log execution details
	  //  logger.log("ENVIRONMENT VARIABLES: " + gson.toJson(System.getenv()));
	  //  logger.log("CONTEXT: " + gson.toJson(context));
	    // process event
	    //   logger.log("EVENT: " + gson.toJson(event));
	    //    logger.log("EVENT TYPE: " + event.getClass());

	    return response;

	  }



}
