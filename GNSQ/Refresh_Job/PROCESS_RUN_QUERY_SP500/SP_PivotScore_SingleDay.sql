
/****** Object:  StoredProcedure [dbo].[SP_PivotScore_SingleDay]    Script Date: 04/03/2019 12:33:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[SP_PivotScore_SingleDay]                                              
(                                                                
@snapdate date                                              
)                                              
as                                              
begin                   
                  
                                     
                                              
DECLARE  @Score_Daily1 TABLE(                      
 [Cover_id] [uniqueidentifier] NULL,                      
 [snapshotdate] [date] NULL,                      
 [1.1.1] [varchar](300) NULL,                      
 [1.1.2] [varchar](300) NULL,                      
 [1.1.3] [varchar](300) NULL,                      
 [1.1.4] [varchar](300) NULL,                      
 [1.2.1] [varchar](300) NULL,                      
 [1.2.2] [varchar](300) NULL,                      
 [1.2.3] [varchar](300) NULL,                      
 [1.2.4] [varchar](300) NULL,                      
 [1.2.5] [varchar](300) NULL,                      
 [1.2.6] [varchar](300) NULL,                      
 [1.3.1] [varchar](300) NULL,                      
 [1.3.10] [varchar](300) NULL,                      
 [1.3.11] [varchar](300) NULL,                      
 [1.3.12] [varchar](300) NULL,                      
 [1.3.13] [varchar](300) NULL,                      
 [1.3.14] [varchar](300) NULL,                      
 [1.3.15] [varchar](300) NULL,                      
 [1.3.2] [varchar](300) NULL,                      
 [1.3.3] [varchar](300) NULL,                      
 [1.3.4] [varchar](300) NULL,                      
 [1.3.5] [varchar](300) NULL,                      
 [1.3.6] [varchar](300) NULL,                      
 [1.3.7] [varchar](300) NULL,                      
 [1.3.8] [varchar](300) NULL,                      
 [1.3.9] [varchar](300) NULL                      
)                      
                                          
DECLARE @Score_Daily2 TABLE(                      
 [Cover_id] [uniqueidentifier] NULL,                      
 [snapshotdate] [date] NULL,                      
 [2.1.1] [varchar](300) NULL,                      
 [2.1.2] [varchar](300) NULL,                      
 [2.1.3] [varchar](300) NULL,                      
 [2.1.4] [varchar](300) NULL,                      
 [2.2.1] [varchar](300) NULL,                      
 [2.2.2.1] [varchar](300) NULL,                  
 [2.2.2.2] [varchar](300) NULL,                      
 [2.2.2.3] [varchar](300) NULL,                      
 [2.2.2.4] [varchar](300) NULL,                      
 [2.2.2.5] [varchar](300) NULL,                      
 [2.2.2.6] [varchar](300) NULL,                      
 [2.2.2.7] [varchar](300) NULL,                      
 [2.2.3] [varchar](300) NULL,                      
 [2.3.1] [varchar](300) NULL,                      
 [2.3.2] [varchar](300) NULL,                      
 [2.4.1] [varchar](300) NULL,                      
 [2.4.2] [varchar](300) NULL                      
)                      
                      
DECLARE @Score_Daily3 TABLE(                      
 [Cover_id] [uniqueidentifier] NULL,                      
 [snapshotdate] [date] NULL,                      
 [3.1.1] [varchar](300) NULL,                      
 [3.1.2] [varchar](300) NULL,                      
 [3.1.3] [varchar](300) NULL,                      
 [3.2.1] [varchar](300) NULL,                      
 [3.2.2] [varchar](300) NULL,                      
 [3.2.3] [varchar](300) NULL,                      
 [3.3.1] [varchar](300) NULL,                      
 [3.3.2] [varchar](300) NULL,                      
 [3.3.3] [varchar](300) NULL,                      
 [3.4.1] [varchar](300) NULL,                      
 [3.4.2] [varchar](300) NULL,                      
 [3.4.3] [varchar](300) NULL,                      
 [3.4.4] [varchar](300) NULL,                      
 [3.5.1] [varchar](300) NULL,                      
 [3.5.2] [varchar](300) NULL,                      
 [3.5.3] [varchar](300) NULL,                      
 [3.5.4] [varchar](300) NULL                      
)                      
                      
DECLARE @Score_Daily4 TABLE(                      
 [Cover_id] [uniqueidentifier] NULL,                      
 [snapshotdate] [date] NULL,                      
 [4.1.1] [varchar](300) NULL,                      
 [4.1.2] [varchar](300) NULL,                      
 [4.1.3] [varchar](300) NULL,                      
 [4.1.4] [varchar](300) NULL,                      
 [4.1.5] [varchar](300) NULL,                      
 [4.1.6] [varchar](300) NULL,                      
 [4.1.7] [varchar](300) NULL,                      
 [4.1.8] [varchar](300) NULL,                      
 [4.2.1] [varchar](300) NULL,                      
 [4.2.2] [varchar](300) NULL,                      
 [4.3.1] [varchar](300) NULL,                      
 [4.3.2] [varchar](300) NULL                      
)                      
                      
DECLARE @Score_Daily5 TABLE(                      
 [Cover_id] [uniqueidentifier] NULL,                      
 [snapshotdate] [date] NULL,                      
 [5.1.1] [varchar](300) NULL,                      
 [5.1.2] [varchar](300) NULL,                      
 [5.2.1] [varchar](300) NULL,                      
 [5.2.2] [varchar](300) NULL,                      
 [5.2.3] [varchar](300) NULL,                      
 [5.2.4] [varchar](300) NULL,                      
 [5.3.1] [varchar](300) NULL,                      
 [5.3.2] [varchar](300) NULL                      
)                      
                       
                      
insert into @Score_Daily1                                              
SELECT                                      
*                                      
FROM                                      
( select                                      
[Cover_id]                                        
,[snapshotdate],                                      
[GsmNo]                                       
,[outcome]                                      
                                      
FROM   [dbo].[AggMedianPreprocessData1_daily]                                              
) AS P                                       
PIVOT                                      
(                                      
max([outcome]) FOR [GsmNo] IN([1.1.1],                              
[1.1.2],                              
[1.1.3],                              
[1.1.4],                              
[1.2.1],                              
[1.2.2],                              
[1.2.3],                              
[1.2.4],                              
[1.2.5],                              
[1.2.6],                              
[1.3.1],                              
[1.3.10],                              
[1.3.11],                              
[1.3.12],                              
[1.3.13],                              
[1.3.14],                              
[1.3.15],                              
[1.3.2],                              
[1.3.3],                              
[1.3.4],                              
[1.3.5],                              
[1.3.6],                              
[1.3.7],                              
[1.3.8],                              
[1.3.9])                                              
) AS P where --Cover_id=@coverid                                               
--and           
snapshotdate is not null and snapshotdate =@snapdate                        
order by snapshotdate                                              
                                                                 
insert into @Score_Daily2                                                
SELECT                           
*                                        
FROM                                        
( select                                        
[Cover_id]                                          
,[snapshotdate],                                        
[GsmNo]                                         
,[outcome]                                        
                            
FROM   [dbo].[AggMedianPreprocessData2_daily]                                                
) AS P                                         
PIVOT                   
(                                        
max([outcome]) FOR [GsmNo] IN([2.1.1],                              
[2.1.2],                              
[2.1.3],                              
[2.1.4],                              
[2.2.1],                              
[2.2.2.1],                              
[2.2.2.2],                              
[2.2.2.3],                              
[2.2.2.4],                              
[2.2.2.5],                              
[2.2.2.6],                              
[2.2.2.7],                              
[2.2.3],                  [2.3.1],                              
[2.3.2],                              
[2.4.1],                              
[2.4.2])                                                
) AS P where --Cover_id=@coverid                                                 
--and          
 snapshotdate is not null and snapshotdate=@snapdate                        
order by snapshotdate                                    
                                              
insert into @Score_Daily3                                                  
SELECT                                          
*                                          
FROM                                          
( select                                          
[Cover_id]                                            
,[snapshotdate],                                          
[GsmNo]                                           
,[outcome]                                          
                                      
FROM   [dbo].[AggMedianPreprocessData3_daily]                                                  
) AS P                                           
PIVOT                                          
(                                          
max([outcome]) FOR [GsmNo] IN([3.1.1],                              
[3.1.2],                           
[3.1.3],                              
[3.2.1],                              
[3.2.2],                              
[3.2.3],                              
[3.3.1],                              
[3.3.2],                              
[3.3.3],                              
[3.4.1],                              
[3.4.2],                              
[3.4.3],                              
[3.4.4],                              
[3.5.1],                              
[3.5.2],                              
[3.5.3],                              
[3.5.4])                                                  
) AS P where --Cover_id=@coverid                                                   
--and           
snapshotdate is not null and snapshotdate =@snapdate                                                            
order by snapshotdate                                               
                      
insert into @Score_Daily4                                                    
SELECT                                            
*                                            
FROM                                   
( select                                            
[Cover_id]                                              
,[snapshotdate],                                            
[GsmNo]                                             
,[outcome]                                            
                                            
FROM   [dbo].[AggMedianPreprocessData4_daily]                                                    
) AS P                                             
PIVOT                                            
(                                            
max([outcome]) FOR [GsmNo] IN([4.1.1],                                  
[4.1.2],                                  
[4.1.3],                                  
[4.1.4],                                  
[4.1.5],                                  
[4.1.6],                                  
[4.1.7],                                  
[4.1.8],                         
[4.2.1],                        
[4.2.2],                                 
[4.3.1],                                  
[4.3.2])                                                    
) AS P where --Cover_id=@coverid                                                     
--and           
snapshotdate is not null and snapshotdate =@snapdate                                                                 
order by snapshotdate                       
               
insert into @Score_Daily5                                 
SELECT                                        
*                                        
FROM                                        
( select                                        
[Cover_id]                                          
,[snapshotdate],                                        
[GsmNo]                                         
,[outcome]                                        
                                        
FROM   [dbo].[AggMedianPreprocessData5_daily]                                                
) AS P                                         
PIVOT                                        
(                                    
max([outcome]) FOR [GsmNo] IN([5.1.1],                                                
[5.1.2],                                                
[5.2.1],                                              
[5.2.2],                                                
[5.2.3],                                                
[5.2.4],                                                
[5.3.1],                                                
[5.3.2])                                                
) AS P where-- Cover_id=@coverid                                                 
--and           
snapshotdate is not null and snapshotdate =@snapdate                                                         
order by snapshotdate                        
                      
                      
insert into @Score_Daily1                                              
SELECT                                      
*                                      
FROM                                      
( select                                      
[Cover_id]                                        
,[snapshotdate],                                      
[GsmNo]                                       
,[outcome]                                      
                                      
FROM   [dbo].[AggMedianPreprocessData1_daily_Second]                                              
) AS P                                       
PIVOT                                      
(                                      
max([outcome]) FOR [GsmNo] IN([1.1.1],                              
[1.1.2],                              
[1.1.3],                              
[1.1.4],                              
[1.2.1],                              
[1.2.2],                              
[1.2.3],                              
[1.2.4],                              
[1.2.5],                              
[1.2.6],                              
[1.3.1],                              
[1.3.10],                              
[1.3.11],                              
[1.3.12],                              
[1.3.13],                              
[1.3.14],                              
[1.3.15],                              
[1.3.2],                              
[1.3.3],                              
[1.3.4],                              
[1.3.5],                              
[1.3.6],                              
[1.3.7],                              
[1.3.8],                              
[1.3.9])                                              
) AS P where --Cover_id=@coverid                                               
--and           
snapshotdate is not null and snapshotdate =@snapdate                        
order by snapshotdate                                              
          
insert into @Score_Daily2                                                
SELECT                           
*                                        
FROM                                        
( select                                        
[Cover_id]                                          
,[snapshotdate],                                        
[GsmNo]                                         
,[outcome]                                        
                                        
FROM   [dbo].[AggMedianPreprocessData2_daily_Second]                                                
) AS P                                         
PIVOT                   
(                         
max([outcome]) FOR [GsmNo] IN([2.1.1],                              
[2.1.2],                              
[2.1.3],                              
[2.1.4],                              
[2.2.1],                              
[2.2.2.1],                              
[2.2.2.2],                              
[2.2.2.3],                              
[2.2.2.4],                              
[2.2.2.5],                              
[2.2.2.6],                              
[2.2.2.7],                              
[2.2.3],                  [2.3.1],                              
[2.3.2],                              
[2.4.1],                              
[2.4.2])                                                
) AS P where --Cover_id=@coverid                                                 
--and          
 snapshotdate is not null and snapshotdate=@snapdate                        
order by snapshotdate                                    
                                              
insert into @Score_Daily3                                                  
SELECT                                          
*                                          
FROM                                          
( select                                          
[Cover_id]                                            
,[snapshotdate],                                          
[GsmNo]                                           
,[outcome]                                          
                                      
FROM   [dbo].[AggMedianPreprocessData3_daily_Second]                                                  
) AS P                                           
PIVOT                                          
(                                          
max([outcome]) FOR [GsmNo] IN([3.1.1],                              
[3.1.2],                           
[3.1.3],                              
[3.2.1],                              
[3.2.2],                              
[3.2.3],                              
[3.3.1],                              
[3.3.2],                              
[3.3.3],                              
[3.4.1],                              
[3.4.2],                              
[3.4.3],                              
[3.4.4],                              
[3.5.1],                              
[3.5.2],                              
[3.5.3],                              
[3.5.4])                                                  
) AS P where --Cover_id=@coverid                                                   
--and           
snapshotdate is not null and snapshotdate =@snapdate                                                            
order by snapshotdate                                               
                      
insert into @Score_Daily4                                                    
SELECT                                            
*                                            
FROM                                   
( select                                            
[Cover_id]                                              
,[snapshotdate],                                            
[GsmNo]                                             
,[outcome]                                            
                                            
FROM   [dbo].[AggMedianPreprocessData4_daily_Second]                                              
) AS P                                             
PIVOT                                            
(                                            
max([outcome]) FOR [GsmNo] IN([4.1.1],                                  
[4.1.2],                                  
[4.1.3],                                  
[4.1.4],                                  
[4.1.5],                                  
[4.1.6],                                  
[4.1.7],                                  
[4.1.8],                         
[4.2.1],                        
[4.2.2],                                 
[4.3.1],                                  
[4.3.2])                                                    
) AS P where --Cover_id=@coverid                                                     
--and           
snapshotdate is not null and snapshotdate =@snapdate                                                                 
order by snapshotdate                       
                      
insert into @Score_Daily5                                 
SELECT                                        
*                                        
FROM                                        
( select                                        
[Cover_id]                                          
,[snapshotdate],                                        
[GsmNo]                                         
,[outcome]                                        
                                        
FROM   [dbo].[AggMedianPreprocessData5_daily_Second]                                                
) AS P                                         
PIVOT                                        
(                                    
max([outcome]) FOR [GsmNo] IN([5.1.1],                                                
[5.1.2],                                                
[5.2.1],                                              
[5.2.2],                                                
[5.2.3],                                                
[5.2.4],                                                
[5.3.1],                                                
[5.3.2])                                                
) AS P where-- Cover_id=@coverid                                                 
--and           
snapshotdate is not null and snapshotdate =@snapdate                                                         
order by snapshotdate       
    
    
                      
insert into @Score_Daily1                                              
SELECT                                      
*                                      
FROM                                      
( select                                      
[Cover_id]                                        
,[snapshotdate],                                      
[GsmNo]                                       
,[outcome]                                      
                                      
FROM   [dbo].[AggMedianPreprocessData1_daily_third]                                              
) AS P                                       
PIVOT                                      
(                                      
max([outcome]) FOR [GsmNo] IN([1.1.1],                              
[1.1.2],                              
[1.1.3],                              
[1.1.4],                              
[1.2.1],                              
[1.2.2],                              
[1.2.3],                              
[1.2.4],                              
[1.2.5],                              
[1.2.6],                              
[1.3.1],                              
[1.3.10],                              
[1.3.11],                              
[1.3.12],                              
[1.3.13],                              
[1.3.14],                              
[1.3.15],                              
[1.3.2],                              
[1.3.3],                              
[1.3.4],                              
[1.3.5],                              
[1.3.6],                              
[1.3.7],                              
[1.3.8],                              
[1.3.9])                                              
) AS P where --Cover_id=@coverid                                               
--and           
snapshotdate is not null and snapshotdate =@snapdate                        
order by snapshotdate                                              
                                                                 
insert into @Score_Daily2                                                
SELECT                           
*                                        
FROM                                        
( select                                        
[Cover_id]                                          
,[snapshotdate],                                        
[GsmNo]                                         
,[outcome]                                        
                                        
FROM   [dbo].[AggMedianPreprocessData2_daily_third]                                      
) AS P                                         
PIVOT                   
(                                        
max([outcome]) FOR [GsmNo] IN([2.1.1],                              
[2.1.2],                              
[2.1.3],                              
[2.1.4],                              
[2.2.1],                              
[2.2.2.1],                              
[2.2.2.2],                              
[2.2.2.3],                              
[2.2.2.4],                              
[2.2.2.5],                              
[2.2.2.6],                              
[2.2.2.7],                              
[2.2.3],                  [2.3.1],                              
[2.3.2],                              
[2.4.1],                              
[2.4.2])                                                
) AS P where --Cover_id=@coverid                                                 
--and          
 snapshotdate is not null and snapshotdate=@snapdate                        
order by snapshotdate                                    
                                              
insert into @Score_Daily3                                                  
SELECT                                          
*                                          
FROM                                          
( select                                          
[Cover_id]                                            
,[snapshotdate],                                          
[GsmNo]                                           
,[outcome]                                          
                                      
FROM   [dbo].[AggMedianPreprocessData3_daily_third]                                                  
) AS P                                           
PIVOT                                          
(                                          
max([outcome]) FOR [GsmNo] IN([3.1.1],                              
[3.1.2],                           
[3.1.3],                              
[3.2.1],                              
[3.2.2],                              
[3.2.3],                              
[3.3.1],                              
[3.3.2],                              
[3.3.3],                              
[3.4.1],                              
[3.4.2],                              
[3.4.3],                              
[3.4.4],                              
[3.5.1],                              
[3.5.2],                              
[3.5.3],                              
[3.5.4])                                                  
) AS P where --Cover_id=@coverid                                                   
--and           
snapshotdate is not null and snapshotdate =@snapdate                                                            
order by snapshotdate                                               
                      
insert into @Score_Daily4                                                    
SELECT                                            
*                                           
FROM                                   
( select                                            
[Cover_id]                                              
,[snapshotdate],                                            
[GsmNo]                                             
,[outcome]                                            
                                            
FROM   [dbo].[AggMedianPreprocessData4_daily_third]                                                    
) AS P                                             
PIVOT                                            
(                                            
max([outcome]) FOR [GsmNo] IN([4.1.1],                                  
[4.1.2],                                  
[4.1.3],                                  
[4.1.4],                                  
[4.1.5],                                  
[4.1.6],                                  
[4.1.7],                                  
[4.1.8],                         
[4.2.1],                        
[4.2.2],                                 
[4.3.1],                                  
[4.3.2])      
) AS P where --Cover_id=@coverid                                                     
--and           
snapshotdate is not null and snapshotdate =@snapdate                                                                 
order by snapshotdate                       
                      
insert into @Score_Daily5                                 
SELECT                                        
*                                        
FROM                                        
( select                                        
[Cover_id]                                          
,[snapshotdate],                                        
[GsmNo]                                         
,[outcome]                                        
                                        
FROM   [dbo].[AggMedianPreprocessData5_daily_third]                                                
) AS P                                         
PIVOT                                        
(                                    
max([outcome]) FOR [GsmNo] IN([5.1.1],                                                
[5.1.2],                                                
[5.2.1],                                              
[5.2.2],                                                
[5.2.3],                                                
[5.2.4],                                                
[5.3.1],                                                
[5.3.2])                                                
) AS P where-- Cover_id=@coverid                                                 
--and           
snapshotdate is not null and snapshotdate =@snapdate                                                         
order by snapshotdate       
    
                     
                                
                                    
 delete from [Dataset1_Outcome_tot] where --ISIN=@isin and           
 [date]=@snapdate                                               
 insert into [Dataset1_Outcome_tot]                                    
 select   cm.SicNo,s1.snapshotdate,s1.[1.1.1],s1.[1.1.2],s1.[1.1.3],s1.[1.1.4],s1.[1.2.1],                                    
 s1.[1.2.2],s1.[1.2.3],s1.[1.2.4],s1.[1.2.5],s1.[1.2.6],s1.[1.3.1],s1.[1.3.2],s1.[1.3.3],s1.[1.3.4],s1.[1.3.5],s1.[1.3.6],s1.[1.3.7],s1.[1.3.8],s1.[1.3.9],s1.[1.3.10],s1.[1.3.11],s1.[1.3.12],s1.[1.3.13],s1.[1.3.14],s1.[1.3.15],                           
  
    
       
       
         
 s2.[2.1.1],s2.[2.1.2],s2.[2.1.3],s2.[2.1.4],s2.[2.2.1],                                    
 s2.[2.2.2.2],s2.[2.2.2.3],s2.[2.2.2.5],s2.[2.2.2.6],s2.[2.2.2.7],--,s2.[2.2.2.6],s2.[2.2.2.7],--**                                    
 s2.[2.2.3],s2.[2.3.1],s2.[2.3.2],s2.[2.4.1],s2.[2.4.2],                                    
 s3.[3.1.1],s3.[3.1.3],s3.[3.2.1],s3.[3.2.2],s3.[3.2.3],s3.[3.3.1],s3.[3.3.2],s3.[3.3.3],s3.[3.4.1],s3.[3.4.2],s3.[3.4.3],s3.[3.4.4],s3.[3.5.1],s3.[3.5.2],s3.[3.5.3],s3.[3.5.4],                                    
 s4.[4.1.1],s4.[4.1.2],s4.[4.1.3],s4.[4.1.4],s4.[4.1.5],s4.[4.1.6],s4.[4.1.7],s4.[4.1.8],s4.[4.2.1],s4.[4.2.2],s4.[4.3.1],s4.[4.3.2],                                    
 s5.[5.1.1],s5.[5.1.2],s5.[5.2.1],s5.[5.2.2],s5.[5.2.3],s5.[5.2.4],s5.[5.3.1],s5.[5.3.2],cv.COVER_ID                                    
 from @Score_Daily1 s1 join @Score_Daily2 s2  on s1.cover_id=s2.Cover_id and s1.snapshotdate=s2.snapshotdate                                    
 join @Score_Daily3 s3  on s3.cover_id=s2.Cover_id and s3.snapshotdate=s2.snapshotdate                                     
 join @Score_Daily4 s4  on s4.cover_id=s3.Cover_id and s4.snapshotdate=s3.snapshotdate                      
 join @Score_Daily5 s5  on s5.cover_id=s4.Cover_id and s5.snapshotdate=s4.snapshotdate                                    
 join cover cv on cv.Cover_id=s1.Cover_id                                    
 join Company_Master cm on cm.Company_id=cv.Company_Id                                    
 --and cm.SicNo=@isin                               
 and s1.snapshotdate=@snapdate                         
                    
                    
                    
                          
end 