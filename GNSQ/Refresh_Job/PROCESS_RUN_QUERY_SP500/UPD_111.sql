
/****** Object:  StoredProcedure [dbo].[UPD_111]    Script Date: 04/03/2019 12:39:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[UPD_111]        
( @snapshotdate date        
)        
AS BEGIN        
        
--set @snapshotdate='15-aug-2017'        
        
declare @n int =((select count(distinct isin) from DATASET2_PERCENTILE_TOT where         
Date=@SnapShotDate )-1)         
        
declare @tbbelow table(ii int identity(1,1), val decimal(18,2))        
        
declare @b decimal(18,3)        
declare @vl decimal(18,2)        
declare @perc decimal(18,2)        
        
insert into @tbbelow        
select distinct [1#1#1] from DATASET1_OUTCOME_TOT where date=@SnapShotDate        
and [1#1#1]!='0.00' and [1#1#1]!='0'        
        
        
declare @st int =1        
declare @en int =(select COUNT(ii) from @tbbelow)        
while(@st<=@en)        
begin        
  set @vl=(select val from @tbbelow where ii=@st)        
        
  set @b=(select count(distinct isin) from DATASET1_OUTCOME_TOT where         
  Date=@SnapShotDate and convert(decimal(18,3),ISNULL([1#1#1],0))<convert(decimal(18,3),ISNULL(@vl,0)))         
        
  set @perc=(@b/@n)*100        
        
  --print @vl          
  --print @b        
  --print @n        
  --print @perc        
          
  UPDATE DATASET2_PERCENTILE_TOT SET [1#1#1]=@perc where date=@SnapShotDate AND ISIN IN        
  (SELECT DISTINCT ISIN  FROM DATASET1_OUTCOME_TOT where         
  Date=@SnapShotDate and convert(decimal(18,2),[1#1#1])=@vl)        
        
  set @st=@st+1        
end        
        
        
END  