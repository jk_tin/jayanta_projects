
/****** Object:  StoredProcedure [dbo].[usp_SSIS_ScoreAggMedian5_daily_SingleDay_third]    Script Date: 04/03/2019 11:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                                                                                                                    
-- =============================================                                                                                                                                                                              
-- Author:  Arindam Mukhopadhyay                                                                                                                                                                              
-- Create date: 23/05/2013                                                                                                                                                                              
-- Description: Get Score outcome for section 5                                                                                                                                                                             
-- =============================================                                                                                                                                        
ALTER Procedure [dbo].[usp_SSIS_ScoreAggMedian5_daily_SingleDay_third]                                                                                                                                                                                      
(                                                                                                                                                                                      
 @CoverID varchar(100) ,        
 @snapdate date                                                                                                                                                                             
)                                                                                                                                                                                      
                                                                                                                                                                                                            
AS                                                                                                                                                                                       
BEGIN                                                                                                                           
                                                                                                                          
                                                                                                                       
                                                                                                                                             
 delete from AggMedianPreprocessData5_Daily_Third where Cover_id=@CoverID and snapshotdate=@snapdate                                                                                                                     
                                                              
   ----------------------------------------------From Date Checking-------------------                                                                                  
  declare @lastUpdateDate date                                                                 
  declare @toDate date                                                                                    
  declare @fromDate date                                                                                  
  declare @ScoreMedianFromDate date                                                                                                                                                        
   declare @OTH_VARIABLE_1 decimal(18,3)          
                                       
    set @OTH_VARIABLE_1=(select MAX(OTH_VARIABLE_1) from preprocess_Measure_Variable_Instance_Details                                                                                         
   where GSM_DESC like '%trailing%')                   
                                                                    
                          
 -- set @FromDate=(select convert(varchar,DATEADD(Day,365*3,MIN(Proxy_FillingDt)),101)  from Proxy_Period where Cover_Id=@CoverID)                               
                              
                                          
 --set @ScoreMedianFromDate= CONVERT(DATETIME, (select top 1 Proxy_FillingDt from Proxy_Period                                                                  
 --where Cover_Id=@CoverID                                                                             
 --order by Proxy_FillingDt asc)) +(365* CAST(ROUND(@OTH_VARIABLE_1,0) AS INT))                                                                             
                                                           
 -- if(@FromDate>=@ScoreMedianFromDate)                                                                                              
 --set @ScoreMedianFromDate=@FromDate                                                                                                   
                                                                          
                                                                                                                     
 --set @minquarterDate=(select MIN(Quarter_EndDt) from Quarter_Period where Cover_Id=@CoverID)                                                                                                                                                  
  set @lastUpdateDate=(select distinct TOP 1 Last_Update_Date from Company_Submission_Details                                                                                                                
where Cover_Id=@CoverID ORDER BY Last_Update_Date DESC)                                                                                                                
------Check for scorable company                                                                                      
--IF @lastUpdateDate>=@ScoreMedianFromDate                                                                                      
  BEGIN                                                                 
                                                                                                                                                                                                                                                  
                                                                                                                                                                                                              
  ---------------------------------------From Date Checking----------------------                                                             
                                                                                                                          
-----From Date and To Date                                                                                                                                                                                                                         
                                                                                                                                                                                      
--declare @ScoreMedianFromDate datetime                                                                                                           
                                                                                                            
declare @MaxDate datetime                                                                                              
DECLARE @dtmax3 DATETIME = NULL                                                                                                                  
DECLARE @NoofRestatement   DECIMAL(18,3)= 0.000            
 DECLARE @dtmax DATETIME = NULL                                                                                               
 declare @var decimal(10,2)                                                                            
 declare @prdt datetime             
declare @ctx int                                                                        
declare @ctxf int                                                                            
                                                                                                                                           
 --declare @minquarterDate date                                                                                      
                                                  
 --set @minquarterDate=(select MIN(Quarter_EndDt) from Quarter_Period where Cover_Id=@CoverID)                                                                          
 --set @lastUpdateDate=(select DateInitorUpdt from Cover where Cover_Id=@CoverID)                                                                
                                        
                                                                                  
declare @Quarter_Period TABLE(                                                                                                   
 [Indx] INT IDENTITY(1,1) NOT NULL,                                                         
 [Quarter_FillingDt] date                                                                                                                   
 )                                                                          
                                                               
-- insert into @Quarter_Period([Quarter_FillingDt])                                                                                              
-- select Quarter_EndDt from Quarter_MasterData where Quarter_EndDt>=@ScoreMedianFromDate                                                                                      
-- and Quarter_EndDt<=@lastUpdateDate                                                                                
                                                                            
-- order by   Quarter_EndDt desc                                                                                   
                                                                                    
---- insert into @Quarter_Period([Quarter_FillingDt])                                                                             
-- -- --select DateInitorUpdt from Cover where Cover_id=@CoverID                                                                               
-- --select @lastUpdateDate                                                                 
                                                               
-- if (select COUNT(*) from @Quarter_Period where [Quarter_FillingDt]=@lastUpdateDate)=0                                                                
--  begin                                                                
--  insert into @Quarter_Period([Quarter_FillingDt])                                                                                    
-- select @lastUpdateDate                                                                        
-- end                                                               
                                                                                           
---end-----                       
print @lastUpdateDate                                                                                                                        
     ------------------********************-----------------                              
 delete from @Quarter_Period                              
                              
  insert into @Quarter_Period                              
  values(@snapdate)                                                                     
                                                                                           
---end-----                                                                   
                                                    
----LookUp Table                            
  DECLARE @LookUpDetails TABLE (                                                                                                    
   [LookUpId] int PRIMARY KEY NOT NULL,                                                                                          
   [FeildName] varchar(150)  NULL                                                                                                                            
)                                                                                                
insert into @LookUpDetails([LookUpId],[FeildName])                                               
select [LookUpId],[FeildName] from LookUpDetails                                                                                                                          
                                                                                          
 ----GSM_MASTER Table                                                                                                                       
  DECLARE @GSM_MASTER TABLE (                                                                                                         
   [GSM_NUM] varchar(10)  PRIMARY KEY NOT  NULL,                                                                                      
   [DeriveMeasureVariable] decimal(18,3)                                                                                                                                         
)                                                                                                                                               
                                              
insert into @GSM_MASTER([GSM_NUM],[DeriveMeasureVariable])                                                                                                                                    
                                                                              
                                                            
SELECT                                                              
[GSM_NUM],                                                          
(case                                                         
 when (OTH_VARIABLE_1>OTH_VARIABLE_2 and OTH_VARIABLE_1>OTH_VARIABLE_3) then OTH_VARIABLE_1                                                         
 when  (OTH_VARIABLE_2>OTH_VARIABLE_1 and OTH_VARIABLE_2>OTH_VARIABLE_3) then OTH_VARIABLE_2                                                                                           
 when  (OTH_VARIABLE_3>OTH_VARIABLE_1 and OTH_VARIABLE_3>OTH_VARIABLE_2) then OTH_VARIABLE_3                                                                 
 end) DeriveMeasureVariable                                                                                          
  FROM preprocess_Measure_Variable_Instance_Details                                                                                              
                                                                                                                       
----EventDataEnv_Social                                                                                                              
DECLARE @EventDataEnv_Social TABLE (                                                                                                                              
   [EnvSocialId] int NULL,                                                                                                                          
   [CoverId] uniqueidentifier  NULL,                                                                                                                                           
   [GRISustDate] date NULL  ,                                                                                                           
   [EnvSocialPubYear] int,                                    
   [EnvSocialG3App] int ,                                                                           
   [EnvSocialISO] int,                       
   [EnvSocialCDP] int ,                                                          
   [EnvSocialUNGC] int ,                                                        
   [EnvSocialOECD] int  ,                                                                                                                        
   [EnvSocialIFC] int,                                
   EnvSocialAA1000 int,                   EnvSocialEA int,                                  
 EnvSocialSPEO int                                                                                                               
)                                                                                         
                                                                                       
insert into @EventDataEnv_Social([EnvSocialId],[CoverId],[GRISustDate],[EnvSocialPubYear],[EnvSocialG3App],[EnvSocialISO],[EnvSocialCDP],[EnvSocialUNGC],[EnvSocialOECD],[EnvSocialIFC], EnvSocialAA1000, EnvSocialEA, EnvSocialSPEO)                         
  
    
      
        
          
            
              
                
                  
                    
                      
                        
                          
                            
                                                                           
                                                       
select [EnvSocialId],[CoverId],[GRISustDate],[EnvSocialPubYear],[EnvSocialG3App],[EnvSocialISO],[EnvSocialCDP],[EnvSocialUNGC],[EnvSocialOECD],[EnvSocialIFC], EnvSocialAA1000, EnvSocialEA, EnvSocialSPEO from EventDataEnv_Social                            
  
    
      
        
          
            
              
                
                  
                    
                      
                        
                          
                            
                                                                           
where CoverId=@CoverID                                                                                                                       
DECLARE @Outcome NVARCHAR(1000)=NULL                                                                                                                   
                                                                   
                                                                                                                                               
                                                                              
                                                                                             
 DECLARE @Fdate DATETIME                                                                                               
                                                                                                                                                             
 DECLARE @sec VARCHAR(100) = NULL                                                                                                          
                                                                                                                               
                                                                                                  
                                                                                          
 ----Proxy_Period Table                                            
  DECLARE @Proxy_Period TABLE (                                                                                                                    
  [Proxy_ID] int NULL,                                                                                                                                                
   [Fiscal_ID] int  PRIMARY KEY NOT  NULL,               
   [Cover_Id] uniqueidentifier  NULL,                                                                                                            
   [Proxy_FillingDt] date NULL          
)                                                                         
                                                          
insert into @Proxy_Period([Fiscal_ID],[Cover_Id],[Proxy_FillingDt])                                      
select [Fiscal_ID],[Cover_Id],[Proxy_FillingDt] from Proxy_Period                                                           
where Cover_Id=@CoverID                                                                              
                                                                                                         
                                                                                     
 DECLARE @SnapShotDate DATE                                                                    
                                                                                                                                                              
 --------Start Calculation                                                                          
                                                                                                    
 declare @count int                                                                                           
  declare @cnt int                                                                                          
  set @count=(select COUNT(*) from @Quarter_Period)                                                               
  set @cnt=1                                                                                                             
                                                                                                          
 WHILE @cnt<= @count                                                                                 
 BEGIN                                                                                                                   
 set @SnapShotDate=(select Quarter_FillingDt from @Quarter_Period where [Indx]=@cnt)                                                                                                                                                        
                                                                                                                   
 SET @Fdate= (select top 1 Proxy_FillingDt FROM @Proxy_Period WHERE Proxy_FillingDt<=@SnapShotDate order by Proxy_FillingDt desc)                                                            
                                                                                                            
                                                                                                  
                                                                                               
   -----------------------------------------Section 5.1.1----------------------------------                                                                                             
                                                                                               
   SET @Outcome = NULL                                                                                                                                                                             
   SET @sec='5.1.1'                                                                               
 SET @dtmax = NULL                                                                      
      SET @NoofRestatement =  NULL                                                                                                                                          
   set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                    
                                                                                                                                                
      set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                                                                                     
                                                                                            
 SET @Outcome =                          
 ( select isnull(count(EventDataEPAId),0) from EventData_EPA                                                                
        WHERE CoverId = @CoverID AND IniDisclosureDate BETWEEN @dtmax3  AND @SnapShotDate                                                                                               
                                                                                                           
        )                                                                                            
        INSERT INTO AggMedianPreprocessData5_Daily_Third([GsmNo],[Cover_id],[SnapShotDate],[OutCome])                              
   SELECT   @sec,@CoverID,@SnapShotDate,@Outcome                                                                                              
                                                                                               
   -----------------------------------------Section 5.1.2----------------------------------                                                                                             
                                                                                               
   SET @sec='5.1.2'                                                                                             
   SET @dtmax = NULL                                                                                                                                                     
       SET @NoofRestatement =  NULL                                                                                                                                                                     
                                                                                                                                          
     set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                             
                                                                                                                                                                                                                        
       set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                                                                  
                                                                                                
        SET @Outcome =                                                                                                                           
          (select count(EventDataEPAId) from EventData_EPA                                                                                                    
       where                                                                                                                                                              
       --IniDisclosureDate <= @DtFrom                                                                                                              
       --and CloseDisclosureDate is  null                                                       
       @SnapShotDate between IniDisclosureDate and  isnull(CloseDisclosureDate,'2100-01-01')                                                                                                            
   
                                                  
                                                
                                                     
       and CoverId=@CoverID)                                                                                             
                                                    
 INSERT INTO AggMedianPreprocessData5_Daily_Third([GsmNo],[Cover_id],[SnapShotDate],[OutCome])                                                                                                                                     
   SELECT @sec,@CoverID,@SnapShotDate,@Outcome                                           
                                                                                    
   -----------------------------------------Section 5.2.1----------------------------------                                                                                             
                                                                           
   SET @sec='5.2.1'                                                                                             
   SET @NoofRestatement =  NULL                                                                           
                                                            
    set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                                                                                                
 
    
      
        
          
            
              
                
                  
                    
                      
                        
                          
                            
                             
                                                               
      set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                                                                                                                       
  
    
      
        
          
           
                                              
                                                                                            
       SET @Outcome =                                                                                                                                                                                        
         (SELECT ISNULL(COUNT(EventDataRHRIId),0)                                                                                  
      FROM  dbo.EventData_rhri                                                                                                                                  
        WHERE CoverId = @CoverID AND [IniDisclosureDate] BETWEEN @dtmax3  AND @SnapShotDate)                                                                                             
                                                                                                    
      INSERT INTO AggMedianPreprocessData5_Daily_Third([GsmNo],[Cover_id],[SnapShotDate],[OutCome])                                                                                                                                     
   SELECT   @sec,@CoverID,@SnapShotDate,@Outcome                                                                                              
                                                                                               
   -----------------------------------------Section 5.2.2----------------------------------                                                                                      
                                                          
   SET @sec='5.2.2'                                                                                             
   SET @dtmax = NULL                                                   
      SET @NoofRestatement =  NULL                                                                                                                                                                       
                                                                           
     set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                                                                                     
                                                                                       
      set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                                                           
                                                                               
       SET @Outcome =                                           
      (SELECT count(EventDataRHRIId) from EventData_rhri                                                                                                                                               
       where   @SnapShotDate between IniDisclosureDate and  isnull(CloseDisclosureDate,'2100-01-01')                                                     
       and CoverId=@CoverID)                                                                                             
                                                  
       INSERT INTO AggMedianPreprocessData5_Daily_Third([GsmNo],[Cover_id],[SnapShotDate],[OutCome])                                                                                                                                     
   SELECT   @sec,@CoverID,@SnapShotDate,@Outcome                                                                          
                                                                                      
   -----------------------------------------Section 5.2.3----------------------------------                                                                                             
                  
   SET @sec='5.2.3'                                                      
   SET @dtmax = NULL                                                                                                                                         
      SET @NoofRestatement =  NULL                                                      
                                                                                                             
      set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                  
                                                                                                                                                                                                  
      set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                       
                                                                                                                                                    
       --SET @Fdate =  (SELECT [dbo].[funcGetNearestProxyFilingDate_ScoreView] (@CoverID, @SnapShotDate,'Proxy_EndDate') Proxt_FilingDate)                                                                                                                   
                                                       
                                                              
        SET @Fdate =( SELECT top 1 Proxy_FillingDt from Proxy_Period where Cover_Id = @CoverID                                                        
   and Proxy_FillingDt < = @SnapshotDate order by Proxy_FillingDt desc)                                                        
                                                                  
                                  
                                                                      
                                                                        
                                                                          
                                
                                                                              
                                                                                
                                            
                                                            
                                                                                          
           declare @woman decimal(18,3)                                                                                                                                              
           declare @totwom decimal(18,3)                                                                                           
                                                                                
       SET @woman =                                                                                                                                              
       (select COUNT(dr.director_Name) from director dr join                                                                                                                        
       ProxyDirectorRecords pdr on dr.Director_Id = pdr.Director_Id                                                                   
  where  dr.Cover_Id= @CoverId                                                                             
   and pdr.Proxy_ID =                                                                                                                                      
   ( select top 1 Proxy_ID from proxy_period where Proxy_FillingDt = @Fdate                                                  
   and Cover_Id = @CoverId)                                                                                                                                  
    and  dr.Gender=23  )                                                                                                                        
                                                                                       
    SET @totwom =                                                                                
       (select COUNT(dr.director_Name) from director dr join                                                                                                              
   ProxyDirectorRecords pdr on dr.Director_Id = pdr.Director_Id                                                                                                                                                       
  where  dr.Cover_Id= @CoverId                            
   and pdr.Proxy_ID =                                                                                                                                               
   ( select top 1 Proxy_ID from proxy_period where Proxy_FillingDt = @Fdate                                                                                                                                              
   and Cover_Id = @CoverId)                                                                                                                                              
   )                                                                
                                                                                                          
         if @totwom >0                                                                                                                                                                                                                                
    SET @Outcome =  convert(decimal(18,2),iSNULL(@woman,0)/@totwom*100)                                                  
          else                                            
            SET @Outcome = convert(decimal(18,2),0)                                                                
                                                                              
                                                                                                      
   INSERT INTO AggMedianPreprocessData5_Daily_Third([GsmNo],[Cover_id],[SnapShotDate],[OutCome])                                                                                                                
   SELECT   @sec,@CoverID,@SnapShotDate,@Outcome                                                                                              
                                                                                               
   -----------------------------------------Section 5.2.4----------------------------------                                                                                             
           
   SET @sec='5.2.4'                                               
   SET @dtmax = NULL                                                                                         
       SET @NoofRestatement =  NULL                                                                                                                                                                                       
                                             
   set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                                                                                         
                                                                                                    
       set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                  
                                  --declare @prdt datetime                                                             
--declare @ctx int                                                                                                       
--declare @ctxf int                        
                                                                                                                    
set @ctx=0                                                                         
set @ctxf=0                                                                                
--set @prdt=(SELECT [dbo].[funcGetNearestProxyFilingDate_ScoreView] (@CoverId, @SnapShotDate,'Proxy_EndDate'))                                                        
SET @prdt =( SELECT top 1 Proxy_FillingDt from Proxy_Period where Cover_Id = @CoverID                                                        
   and Proxy_FillingDt < = @SnapshotDate order by Proxy_FillingDt desc)                                                                                                                    
                                                                            
set @ctx = @ctx +(select COUNT(*) from cEO_Details where Cover_Id=@CoverId AND EmpStatus=28                                                                                      
and Date_Central_Id in                                                                                           
(                                                                                                       
select top 1 Fiscal_ID from Fiscal_Period where Cover_Id=@CoverId                                                                  
and Fiscal_ID in                                                                                                                     
(select top 1 Fiscal_ID from proxy_Period where Cover_Id=@CoverId                                                                                                                    
and Proxy_FillingDt <=@prdt order by Proxy_FillingDt desc                                                                                             
)                                                                                  
))                                                                                                                    
                                                                                                                    
set @ctx = @ctx +(select COUNT(*) from cfO_Details where Cover_Id=@CoverId AND EmpStatus=28                                                                                                                     
and Date_Central_Id in                                                                                                
(                                                                                                                     
select top 1 Fiscal_ID from Fiscal_Period where Cover_Id=@CoverId                                                                                                                     
and Fiscal_ID in                                             
(select top 1 Fiscal_ID from proxy_Period where Cover_Id=@CoverId                                                                                   
and Proxy_FillingDt <=@prdt order by Proxy_FillingDt desc                                                                                                                    
)                                                                                
))                                                   
                                                                                                                    
set @ctx = @ctx +(select COUNT(*) from NEO_Details where Cover_Id=@CoverId AND Emp_Status=28                                                                                                                     
and Date_Central_Id in                                                                                                       
(                                                                                                                     
select top 1 Fiscal_ID from Fiscal_Period where Cover_Id=@CoverId                   
and Fiscal_ID in                                                                                                                     
(select top 1 Fiscal_ID from proxy_Period where Cover_Id=@CoverId                                                                
and Proxy_FillingDt <=@prdt order by Proxy_FillingDt desc                                                                                                                    
)                                                                            
))                                                                                                                    
                                                                                              
                        
                                                                                                                    
set @ctxf = @ctxf +(select COUNT(*) from cEO_Details                                                                                                                      
ce join CEO c on c.CEO_Id= ce.CEO_Id                             
                                                                                                                    
where ce.Cover_Id=@CoverId AND EmpStatus=28                                                                                                                     
and c.Gender=23                                                                                                                    
and Date_Central_Id in                                                                                                                    
(                                                          
select top 1 Fiscal_ID from Fiscal_Period where Cover_Id=@CoverId                                                           
and Fiscal_ID in                                                                      
(select top 1 Fiscal_ID from proxy_Period where Cover_Id=@CoverId                                                                                                                    
and Proxy_FillingDt <=@prdt order by Proxy_FillingDt desc                                                                                                                 
)                                                                                                     
))                             
                                                                         
set @ctxf = @ctxf +(select COUNT(*) from cfO_Details                                                                                                                      
ce join CfO c on c.CfO_Id= ce.CfO_Id                                                                                                                    
                                                         
where ce.Cover_Id=@CoverId AND EmpStatus=28                                                                                                                     
and c.Gender=23                                and Date_Central_Id in                                                                                                                    
(                                                       
select top 1 Fiscal_ID from Fiscal_Period where Cover_Id=@CoverId                                                                     
and Fiscal_ID in                                                                                                                     
(select top 1 Fiscal_ID from proxy_Period where Cover_Id=@CoverId                                                                                                                    
and Proxy_FillingDt <=@prdt order by Proxy_FillingDt desc                                                        
)                                                                                                                    
))                                                                                                                    
                              
set @ctxf = @ctxf +(select COUNT(*) from NEO_Details where Cover_Id=@CoverId                                                                                               
and gender=23 AND Emp_Status=28                                                               
and Date_Central_Id in                                                                                                                    
(                                                                      
select top 1 Fiscal_ID from Fiscal_Period where Cover_Id=@CoverId                                                                                   
and Fiscal_ID in                                                                                   
(select top 1 Fiscal_ID from proxy_Period where Cover_Id=@CoverId                                                                                                         
and Proxy_FillingDt <=@prdt order by Proxy_FillingDt desc                                                                                                                    
)                                                                                                                    
))                                                         
                                                                                                                                                                                                                 
        if @ctx>0                                                                                                                                                     
    SET @Outcome = convert(decimal(18,2),((ISNULL(convert(decimal(18,2),@ctxf),0)/convert(decimal(18,2),@ctx))*100) )                                         
                                                                                              
                                          
                                                                                                                                                                                                                                
        else                                                                                                                                      
    SET @Outcome =  convert(decimal(18,2),0)                                                                                                       
                                                                                                                                        
                                                                                 
                                                   
     INSERT INTO AggMedianPreprocessData5_Daily_Third([GsmNo],[Cover_id],[SnapShotDate],[OutCome])                                                                                                                      
SELECT   @sec,@CoverID,@SnapShotDate,@Outcome                                                                                              
                                                               
 -----------------------------------------Section 5.3.1----------------------------------                                                                                            
                                                     
   SET @Outcome = NULL                                                                                                                                                                          
   SET @sec='5.3.1'                                                                                                                                                                            
                                                                                                                                         
   SET @Outcome =                                                            
        (select FeildName from @lookupdetails                                                                                                                                                                                             
        where LookUpId in (select top 1 EnvSocialG3App from @EventDataEnv_Social                                                                                                                                                                              
  
    
      
        
          
            
              
                
                  
                    
                       
                       
                          
                            
                             
       where GRISustDate<= @SnapShotDate                                                                                                                         
   and CoverId=@CoverID order by GRISustDate desc,EnvSocialPubYear desc))                                                                                                        
                                
  /*                                                                                                                                                                                                                                       
if @Outcome='C' or @Outcome='Non-GRI' or @Outcome='Undeclared'                                                                                               
         set @Outcome='C or Undeclared or Non-GRI'                                                                                                                                               
                                                                                       
           if @Outcome is null                                                                                  
    set @Outcome ='No Sustainability Reporting'                                                                                                                
                                                                                                                           
           declare @GRISust1 datetime                                                                                                               
    set @GRISust1=(select  top 1 GRISustDate from @EventDataEnv_Social                                                                         
     where GRISustDate <= @SnapShotDate                                                                                                                                            
           and CoverId=@CoverID order by GRISustDate desc)                                                            
                                                                                                                     
           if( datediff(day,@GRISust1,@SnapShotDate)<390)                                                                                                                                                                 
     set @Outcome=@Outcome                                                                                                                        
  else                                                             
              set @Outcome='No Sustainability Reporting'                                                                                                                                 
                                           
       declare @Outcome_531 nvarchar(30)                                              
                        
       set  @Outcome_531=@Outcome                                                                                                                                    
  */                                                                                                                  
                                   
                                  
   --///////////////////// Changed by Tapas - 02.07.2015 //////////////////                          
    --unbundle                                   
 -------- if @Outcome='A' or @Outcome='A+' or @Outcome='In Accordance-Comprehensive'                                                                 
  --------set @Outcome='A or A+ or In Accordance-Comprehensive'                                     
                                         
  -------- if @Outcome='B' or @Outcome='B+' or @Outcome='C' or @Outcome='C+'or @Outcome='In Accordance-Core'                                       
                
                 
                    
                        
  --------set @Outcome='B or B+ or C or C+ or In Accordance-Core'                                  
                                       
  -------- if @Outcome='Undeclared' or @Outcome='Non-GRI'                                  
  --------set @Outcome='Undeclared or Non-GRI'                                     
                                       
  -------- if (select FeildName from lookupdetails where LookUpId in (select top 1 EnvSocialGRIReportingGuidline from EventDataEnv_Social                                                                                                                      
 
    
       
        
         
            
              
                 
                  
                    
                             
                        
                          
                            
                              
                                
                                  
                                                                    
  --------     where GRISustDate<= @SnapShotDate and CoverId=@CoverID order by EnvSocialPubYear desc)) ='Non-GRI'                                                                                               
                    
                            
 --------set @Outcome='Undeclared or Non-GRI'                      
   --end unbundle                                      
                                         
 declare @GRISust10 datetime                                                                                                                                    
    set @GRISust10=(select top 1 GRISustDate from EventDataEnv_Social where GRISustDate <= @SnapShotDate  and CoverId=@CoverID order by GRISustDate desc)                       
      
        
          
            
              
               
                   
                    
                      
                        
                          
                            
                              
                                
                                  
                                                                                           
    if( datediff(day,@GRISust10,@SnapShotDate)>390)                                                                                                                                                                                                           
  
     
      
        
          
            
              
          
                  
                    
                      
                        
                          
                            
                              
                                
                                  
                                                                                                                                               
   set @Outcome='No sustainability Reporting'                                  
                                     
    if @Outcome= '' or  @Outcome is null                                    
       set @Outcome='No sustainability Reporting'                                     
                                        
                                        
  declare @Outcome_531 nvarchar(30)                                                                    
  set  @Outcome_531=@Outcome                                      
                                       
   --///////////////////// Changed by Tapas - 02.07.2015 //////////////////                              
                                                                                                                     
   INSERT INTO AggMedianPreprocessData5_Daily_Third([GsmNo],[Cover_id],[SnapShotDate],[OutCome])                                                 
   SELECT   @sec,@CoverID,@SnapShotDate,@Outcome                                                                                                 
                               
                                                                    
 -----------------------------------------Section 5.3.2----------------------------------                                                                                                                                                                      
  
    
      
        
          
                                                                                                          
   SET @Outcome = NULL                                                                                                                                                                       
   SET @sec='5.3.2'                                                                                                                      
                                                                                                               
   if(@Outcome_531='No Sustainability Reporting')                                              
   begin                            
         set @Outcome='No Sustainability Reporting'                                                    
   end                      
   else                        
   begin                                                                                         
    declare @GRISust01 datetime                                                                                                                                                                                                          
     DECLARE @EnvSocialISO01 int                               
     DECLARE @EnvSocialCDP01 int                                                                                                                                
     DECLARE @EnvSocialUNGC01 int                                                                                                              
      
        
          
            
             
                 
                  
                   
                    
                       
                          
                             
                               
     DECLARE @EnvSocialOECD01 int                                                  
     DECLARE @EnvSocialIFC01 int                                 
                                   
      Declare @EnvSocialAA1000 int                                    
 Declare @EnvSocialEA int                                    
 Declare @EnvSocialSPEO int                                                                                                                                   
                                      
        set @EnvSocialISO01= (select count(EnvSocialId) from @EventDataEnv_Social                                  
     where GRISustDate                                                                                                                                            
 in(select top 1 GRISustDate from @EventDataEnv_Social where GRISustDate <=@SnapShotDate                                                                                                                                             
 and CoverId=@CoverID                                                                  
 order by GRISustDate desc,EnvSocialPubYear desc)                                 
 and                                
 EnvSocialPubYear                                                              
 in(select top 1 EnvSocialPubYear from @EventDataEnv_Social where GRISustDate <=@SnapShotDate                                                                                                                                             
 and CoverId=@CoverID                   
 order by GRISustDate desc,EnvSocialPubYear desc)                                 
                                                                                                                                                                              
     and EnvSocialISO=1                                                                                                                                                                                                                                       
 
       and CoverId=@CoverID)                                                                                                                                                                                      
                                      
      set @EnvSocialCDP01= (select count(EnvSocialId) from @EventDataEnv_Social                                                                               
     where GRISustDate                                                                           
 in(select top 1 GRISustDate from @EventDataEnv_Social where GRISustDate <=@SnapShotDate                                                                                                                                    
 and CoverId=@CoverID                                                                                                                                           
 order by GRISustDate desc,EnvSocialPubYear desc)                              
  and                                
 EnvSocialPubYear                                                                                                                                            
 in(select top 1 EnvSocialPubYear from @EventDataEnv_Social where GRISustDate <=@SnapShotDate                                                                                                                                             
 and CoverId=@CoverID                                                  
 order by GRISustDate desc,EnvSocialPubYear desc)                                                                                                                                                    
 and EnvSocialCDP=1                                                                                                                                                                           
       and CoverId=@CoverID)                                                                                                                
                                                                                                              
       set @EnvSocialUNGC01= (select count(EnvSocialId) from @EventDataEnv_Social                                                               
     where GRISustDate                                                                                                      
 in(select top 1 GRISustDate from @EventDataEnv_Social where GRISustDate <=@SnapShotDate                                                                                             
 and CoverId=@CoverID                                                                                                                                            
 order by GRISustDate desc,EnvSocialPubYear desc)                                
  and                      
 EnvSocialPubYear                                                                                                                                            
 in(select top 1 EnvSocialPubYear from @EventDataEnv_Social where GRISustDate <=@SnapShotDate                                                                                                                                             
 and CoverId=@CoverID                   
 order by GRISustDate desc,EnvSocialPubYear desc)                                 
   and EnvSocialUNGC=1                                                                                                                                                          
                    
       and CoverId=@CoverID)                                                                                                                                               
                                                                                                                
       set @EnvSocialOECD01= (select count(EnvSocialId) from @EventDataEnv_Social                                                                                                                                                                        
     where GRISustDate                                                                                                                                            
 in(select top 1 GRISustDate from @EventDataEnv_Social where GRISustDate <=@SnapShotDate                                                                                    
 and CoverId=@CoverID                                                                                                           
 order by GRISustDate desc,EnvSocialPubYear desc)                                
  and                                
 EnvSocialPubYear                                                                                                                                            
 in(select top 1 EnvSocialPubYear from @EventDataEnv_Social where GRISustDate <=@SnapShotDate                                                                         
 and CoverId=@CoverID                                                                  
 order by GRISustDate desc,EnvSocialPubYear desc)                                                                                                       
     and EnvSocialOECD=1                                                                                                                                                          
  and CoverId=@CoverID)                                                                                                                          
                                                                                                                                      
       set @EnvSocialIFC01= (select count(EnvSocialId) from @EventDataEnv_Social                                                               
     where GRISustDate                                                                                                                                      
 in(select top 1 GRISustDate from @EventDataEnv_Social where GRISustDate <=@SnapShotDate                                                                                                                                             
 and CoverId=@CoverID                                                                                     
 order by GRISustDate desc,EnvSocialPubYear desc)                                
  and                    
 EnvSocialPubYear             
 in(select top 1 EnvSocialPubYear from @EventDataEnv_Social where GRISustDate <=@SnapShotDate                                                                                                                                             
 and CoverId=@CoverID                                                          
 order by GRISustDate desc,EnvSocialPubYear desc)                                                                                                                                                                               
     and EnvSocialIFC=1                                                                                                                                                                                     
       and CoverId=@CoverID)                               
       --///////////////////// Changed by Tapas - 28.07.2015 //////////////////                                    
      set @EnvSocialAA1000= (select count(EnvSocialId) from @EventDataEnv_Social                             
     where GRISustDate                                                                                                                                            
 in(select top 1 GRISustDate from @EventDataEnv_Social where GRISustDate <=@SnapShotDate                                                                                                                          
 and CoverId=@CoverID                                                                                           
 order by GRISustDate desc,EnvSocialPubYear desc)                                      
  and                                      
 EnvSocialPubYear                                                                                                                                                  
 in(select top 1 EnvSocialPubYear from @EventDataEnv_Social where GRISustDate <=@SnapShotDate                                                                                                                                             
 and CoverId=@CoverID                                                                
 order by GRISustDate desc,EnvSocialPubYear desc)                                                                                                                                                                               
     and EnvSocialAA1000=1                                                                                        
   and CoverId=@CoverID)                                    
                                          
    set @EnvSocialEA= (select count(EnvSocialId) from @EventDataEnv_Social                                                                                                                                                                                   
     where GRISustDate                 
 in(select top 1 GRISustDate from @EventDataEnv_Social where GRISustDate <=@SnapShotDate                                                                                                                                                
 and CoverId=@CoverID                                                                                           
 order by GRISustDate desc,EnvSocialPubYear desc)                                      
  and                      
 EnvSocialPubYear                                                                                                                                                  
 in(select top 1 EnvSocialPubYear from @EventDataEnv_Social where GRISustDate <=@SnapShotDate                                                                                                                                               
 and CoverId=@CoverID                                                                
 order by GRISustDate desc,EnvSocialPubYear desc)                   
     and EnvSocialEA=1         
       and CoverId=@CoverID)                                    
                                    
  set @EnvSocialSPEO= (select count(EnvSocialId) from @EventDataEnv_Social                                                                                                                                                                                   
     where GRISustDate                                                                                                                                            
 in(select top 1 GRISustDate from @EventDataEnv_Social where GRISustDate <=@SnapShotDate                                                                                                                                               
 and CoverId=@CoverID                                                                                           
 order by GRISustDate desc,EnvSocialPubYear desc)                                      
  and                                      
 EnvSocialPubYear                
 in(select top 1 EnvSocialPubYear from @EventDataEnv_Social where GRISustDate <=@SnapShotDate                                                                                                                                               
 and CoverId=@CoverID                                                                
 order by GRISustDate desc,EnvSocialPubYear desc)                                                                                                                                                                               
     and EnvSocialSPEO=1                                                                                                     
       and CoverId=@CoverID)                                    
                                    
  --///////////////////// Changed by Tapas - 28.07.2015 //////////////////                                  
                                     
                                     
                                                                                  
 set  @Outcome= @EnvSocialIFC01+@EnvSocialOECD01+@EnvSocialUNGC01+@EnvSocialCDP01+@EnvSocialISO01 +@EnvSocialAA1000 + @EnvSocialEA+ @EnvSocialSPEO                                                                                                            
  
    
      
        
           
           
              
                
                  
                    
                       
                       
                          
                            
                              
                                          
        IF @Outcome is null                                                                                                                      
      set @Outcome='No Sustainability Reporting'                                                                           
     ELSE                                                                                                                                                                                                                       
    SET @Outcome = @Outcome                                                                                                                                                                
                                                                                                  
      end                                                                                                                                                                                                                              
                                                                                                                                                 
                                                                                                                                                    
                                                                                                      
      INSERT INTO AggMedianPreprocessData5_Daily_Third([GsmNo],[Cover_id],[SnapShotDate],[OutCome])                                                  
   SELECT   @sec,@CoverID,@SnapShotDate,@Outcome                                                                                                
                                                                          
                                        
                                                                                                                                                                                          
 set @cnt=@cnt+1                                                                                                                                              
                                                                                                                                             
                                                                                                                    
 END                                                                                                                    
                                                            
--select * from @Tab                                                                                                                      
                                                                                      
                                                                                                   
 --update AggMedianPreprocessData5_Daily_Third                                                                   
 --      set PeerSnapShotDate=                                                                  
 --      (case when SnapShotDate=@lastUpdateDate then                                                                  
 --      (select top 1  Quarter_EndDt from Quarter_MasterData where Quarter_EndDt<=@lastUpdateDate order by Quarter_EndDt desc)                                                   
 --      else [SnapShotDate] end)                                                         
 --      where Cover_id=@CoverID                                                                                                                  
                           
                                                                                               
-- ------------------------- End of Calculation                                                                        
       END    ------END Check for scorable company   
       
          declare @MINDT date          
SET @MINDT=(select convert(varchar,DATEADD(Day,365*3,MIN(Proxy_FillingDt)),101)  from Proxy_Period where Cover_Id=@coverid)                
          
IF((@SnapShotDate IS NOT NULL) AND (@SnapShotDate<@MINDT))                
BEGIN   
  
  UPDATE AggMedianPreprocessData5_Daily_Third set outcome=''             
  where cover_id=@coverid            
  and snapshotdate=@SnapShotDate            
  and GsmNo in ('5.1.1','5.2.1'             
  )   
    
  end  
            
                                                                                                                                                                                                 
                                                                                                                                          
END 