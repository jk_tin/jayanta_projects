
/****** Object:  StoredProcedure [dbo].[usp_SSIS_ScoreAggMedian3_Daily_SingleDay_first]    Script Date: 04/03/2019 10:12:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                                                                                                                                          
-- =============================================                                                                                                                                                                                                    
-- Author:  Arindam Mukhopadhyay                                                                                                                                                                                                   
-- Create date: 15/07/2013                                                                                                                                                                                                    
-- Description: Get Score General Outcome for section 3                                                                                                                                                                                                    
-- =============================================                                                                                                                                                              
ALTER Procedure [dbo].[usp_SSIS_ScoreAggMedian3_Daily_SingleDay_first]                                                                                                                                                                                        
        
              
(                                                                                                                                                                                                            
 @CoverID varchar(100),    
 @snapdate date                                                                                                                                                                                                       
)                                                                                                                                                                                                            
                                                                                                                                                                                                                                  
AS                                                                                                                                                                                                             
BEGIN                                                                                                                                                 
                                                                                                                                                
                                                                                                                                             
  delete from AggMedianPreprocessData3_Daily where Cover_id=@CoverID  and snapshotdate=@snapdate                                                                                                                                                               
 
                                                                                                                                                                                                                                                  
  ----------------------------------------------From Date Checking-------------------                                                                      
  declare @lastUpdateDate date                                                     
  declare @toDate date                                                                        
  declare @fromDate date                         
  declare @ScoreMedianFromDate date                                                 
   declare @OTH_VARIABLE_1 decimal(18,3)                      
                                
    set @OTH_VARIABLE_1=(select MAX(OTH_VARIABLE_1) from preprocess_Measure_Variable_Instance_Details                                                     
    where GSM_DESC like '%trailing%')                                               
                                                         
                                                       
 -- set @FromDate=(select convert(varchar,DATEADD(Day,365*3,MIN(Proxy_FillingDt)),101)  from Proxy_Period where Cover_Id=@CoverID)                                                                            
                                                                                        
 --set @ScoreMedianFromDate= CONVERT(DATETIME, (select top 1 Proxy_FillingDt from Proxy_Period                                                                           
 --where Cover_Id=@CoverID                                                                                                                                                                         
 --order by Proxy_FillingDt asc)) +(365* CAST(ROUND(@OTH_VARIABLE_1,0) AS INT))                                                                                       
                                                                                       
 -- if(@FromDate>=@ScoreMedianFromDate)                                                                                                                                                                            
 --set @ScoreMedianFromDate=@FromDate                                                                                       
                                                                                       
                                                                                                         
 --set @minquarterDate=(select MIN(Quarter_EndDt) from Quarter_Period where Cover_Id=@CoverID)                                                                                                                                      
  set @lastUpdateDate=(select distinct TOP 1 Last_Update_Date from Company_Submission_Details                                                                                                    
where Cover_Id=@CoverID ORDER BY Last_Update_Date DESC)                                                                                                    
------Check for scorable company                                                                          
--IF @lastUpdateDate>=@ScoreMedianFromDate                                                                          
  BEGIN                                                     
                                                                                                                                                                                                                                      
                                                                                                                                                                                                  
  ---------------------------------------From Date Checking----------------------                                                                                                                                                   
 DECLARE @RES VARCHAR(200)                                                                                                         
 DECLARE @NoofRestatement   DECIMAL(18,3)= 0.000                                                                                                                      
                                                                                                           
 DECLARE @TOT_CNTRES INT = NULL              
 DECLARE @DirCount INT =0                        
 DECLARE @Director INT =0                                                               
 DECLARE @TOT_CNTRES1 INT = NULL                                                                                          
 DECLARE @CHAIR_CNT1 INT  = NULL                                                                        
 DECLARE @CEO_CNT1 INT = NULL                                                             
 DECLARE @CFO_CNT1 INT = NULL                                                                  
 DECLARE @DIRECTOR_CNT1 INT  = NULL                                                                        
 DECLARE @DirPercent DECIMAL(18,2)  = NULL                                                                                      
 DECLARE @GSM_DESC NVARCHAR(300)= NULL                                                                                      
 DECLARE @GmsIndx INT = NULL                                                                                                           
 declare @dtmin datetime = null                                                                                                          
 DECLARE @prFilingdt DATE = NULL                                                                                        
  declare @var decimal(10,3)                                                                        
                                                                                
                                                                              
                                                                                                                  
                                                                                
                                                
                                                                              
                                                                                                                                                  
----LookUp Table                                                                                                                                                                     
  DECLARE @LookUpDetails TABLE (                                                                                                                                                    
   [LookUpId] int  primary key  not NULL,                                                                                                                                                                                                                     
  
    
      
        
          
            
               
               
   [FeildName] varchar(150)  NULL                                                                                                                                                                                                    
)                                                                                                                                                                              
insert into @LookUpDetails([LookUpId],[FeildName])                                                                                                                                                                        
select [LookUpId],[FeildName] from LookUpDetails                                                                                                                 
                                                                                                                            
                                                                                                                                                  
                                                                                                                                        
     
                              
----GSM_MASTER Table                   
  DECLARE @GSM_MASTER TABLE (                                                                                                       
   [GSM_NUM] varchar(10)  primary key  not NULL,                                                   
   [DeriveMeasureVariable] decimal(18,3)                                                                                                
)                              
                                                                                               
                                                 
insert into @GSM_MASTER([GSM_NUM],[DeriveMeasureVariable])                                   
                                                                                                                  
                                              
SELECT                                                                                                                                                   
[GSM_NUM],                                                                                                                    
(case                                      
 when (OTH_VARIABLE_1>OTH_VARIABLE_2 and OTH_VARIABLE_1>OTH_VARIABLE_3) then OTH_VARIABLE_1                                                                                                                   
 when  (OTH_VARIABLE_2>OTH_VARIABLE_1 and OTH_VARIABLE_2>OTH_VARIABLE_3) then OTH_VARIABLE_2                                             
 when  (OTH_VARIABLE_3>OTH_VARIABLE_1 and OTH_VARIABLE_3>OTH_VARIABLE_2) then OTH_VARIABLE_3                                                                                                                                       
 end) DeriveMeasureVariable                                                                                                                           
  FROM Preprocess_Measure_Variable_Instance_Details                                                                                                                                    
                                                                                                                                                                               
                                                                                                                                                         
                                                                                                                                                                 
  --declare @PFactor3 decimal(18,5)                                                                                                                                                                                                                     
  --set @PFactor3= (select CONVERT(DECIMAL(18,5),BOARD_P_NOR_FACTOR) from GROUP_WEIGHT_MASTER)                                                                                                                                                                 
  
    
      
       
          
                                                                                                                                                                           
 DECLARE @Outcome NVARCHAR(1000)=NULL                                                                                                                                          
                                                                                                                                                                                                                                               
 DECLARE @Fdate DATETIME                                                                                            
 DECLARE @FID int       
      
        
          
           
               
               
        
                    
                      
                        
      
                           
         
                                
                                  
                                    
                         
 DECLARE @dtmax DATETIME                 
 DECLARE @FENDDateCalculated DATETIME                                                                                
                                                                           
                                                               
 DECLARE @sec VARCHAR(100) = NULL                                                                                                                                                                     
 declare @dtmax3 datetime                                                                    
                                                   
 ----Proxy_Period Table                                                                       
  DECLARE @Proxy_Period TABLE (                                                                                                                                          
  [Proxy_ID] int   primary key  not null,                                                                                                                                                                   
   [Fiscal_ID] int NULL,                                                                                                                                                                                                           
   [Cover_Id] uniqueidentifier  NULL,                                                                                                                                                                                           
   [Proxy_FillingDt] date NULL                                                                                
)                                                               
                                                                                                         
insert into @Proxy_Period([Proxy_ID],[Fiscal_ID],[Cover_Id],[Proxy_FillingDt])                                                                                                                                                                
select [Proxy_ID],[Fiscal_ID],[Cover_Id],[Proxy_FillingDt] from Proxy_Period                                                                                                           
where Cover_Id=@CoverID                                                                                                                                                                                                    
----ProxyDirectorRecords                                                                                                                                          
 DECLARE @ProxyDirectorRecords TABLE (                                                                                                                                                                  
   [director_Id] uniqueidentifier  NULL,                                                                                                          
    [Proxy_Id] int null,                                                                                                                 
    [IsIndependentDirector] bit,                                                                                                                                        
    [CommitteeAudit] nvarchar(150),                                                                                                                                                                        
   [CommitteeCompensation] nvarchar(150),                                                                                                                                        
   [CommitteeNominating] nvarchar(150),                                                                                             
   [Independence] nvarchar(150) ,                                                                                                                                 
   [LeadDirector] nvarchar(150)                                                                                                                                               
)                                        
                                              
                                              
                                                                                                 
insert into @ProxyDirectorRecords([director_Id],[Proxy_Id],[IsIndependentDirector],[CommitteeAudit]                                                                                           
,[CommitteeCompensation],[CommitteeNominating],[Independence],LeadDirector)                                                                                                                                                                        
select [director_Id],prd.[Proxy_Id],[IsIndependentDirector],[CommitteeAudit]                                                                                                                              
,[CommitteeCompensation],[CommitteeNominating],[Independence],LeadDirector from ProxyDirectorRecords prd join                                             
@Proxy_Period px on px.Proxy_ID=prd.Proxy_Id                                                                                                                               
--where [Proxy_Id] in (select [Proxy_Id] from @Proxy_Period)                                               
                                                                                                   
----director                                             
 DECLARE @directorTab TABLE (                                                                    
   [director_Id] uniqueidentifier   primary key  not  NULL,                                                                                                                                  
    [Cover_Id] uniqueidentifier null,                                            
    [director_Name] varchar(50) null,                                                                                 
    [DirectorDateArival] date null,                                                                                    
   [DirectorDateDeparture] date null ,                                                                               
   [Type]  varchar(10) null                                                                       
)                                                               
                                                                                                                      
insert into @directorTab([director_Id],[Cover_Id],[director_Name],[DirectorDateArival]                                                                                                                          
,[DirectorDateDeparture],[Type])                                                                                                                                                            
select [director_Id],[Cover_Id],[director_Name],[DirectorDateArival]                                                                                                                                        
,[DirectorDateDeparture],[Type] from director                                                                                                                                
where [Cover_Id]=@CoverID                                                                               
                                                                                                      
----director                                                                                                                                          
 DECLARE @EventDataGov_DiscGovGuide TABLE (         
   [DiscGovGuideId] int  NULL,                                                                                          
    [CoverId] uniqueidentifier null,                                                                                              
    [DiscGovGuideRefFileDate] datetime null,                                    
    [DiscGovGuideStatusEffec] varchar(max) null                                                                                                                                     
)                                                                                       
                                                                          
insert into @EventDataGov_DiscGovGuide([DiscGovGuideId],[CoverId],[DiscGovGuideRefFileDate],                                                                                                      
[DiscGovGuideStatusEffec])                                                          
select [DiscGovGuideId],[CoverId],[DiscGovGuideRefFileDate],[DiscGovGuideStatusEffec]                                                                                                       
from   EventDataGov_DiscGovGuide where [CoverId]=@CoverID                                                                       
                                                                                                                                                                                     
                              
                                                                                              
                                                                                                  
declare @Quarter_Period TABLE(                                                                       
 [Indx] INT IDENTITY(1,1) NOT NULL,                                                         
 [Quarter_FillingDt] date                                                                                                                                                              
 )                                   
                                                                                                   
 --insert into @Quarter_Period([Quarter_FillingDt])                                                                   
 --select Quarter_EndDt from Quarter_MasterData where Quarter_EndDt>=@ScoreMedianFromDate                                                                                          
 --and Quarter_EndDt<=@lastUpdateDate                                                                            
                                                                             
 --order by   Quarter_EndDt desc                                                                             
                                                                              
 -- if (select COUNT(*) from @Quarter_Period where [Quarter_FillingDt]=@lastUpdateDate)=0                                                                  
 -- begin                                                                  
 -- insert into @Quarter_Period([Quarter_FillingDt])                                                                                      
 --select @lastUpdateDate                                                                          
 --end                                                                              
                      
     ------------------********************-----------------                        
 delete from @Quarter_Period                        
                        
  insert into @Quarter_Period                        
 values(@snapdate)                                                                      
                                                                                          
---end-----                        
                      
                                       
 DECLARE @SnapShotDate DATE                
                                                                                                                             
                                                                                                                           
 declare @count int                                                                                  
  declare @cnt int                                                                                                
  set @count=(select COUNT(*) from @Quarter_Period)                                                                                          
  set @cnt=1                                                                                                                   
                                                      
 WHILE @cnt<= @count                                                                       
 BEGIN                                                                                                                         
 set @SnapShotDate=(select Quarter_FillingDt from @Quarter_Period where [Indx]=@cnt)                                                                     
                                                                                                              
 SET @Fdate= (select top 1 Proxy_FillingDt FROM @Proxy_Period WHERE Proxy_FillingDt<=@SnapShotDate order by Proxy_FillingDt desc)                                                                                                                             
  
    
      
       
 SET @FID= (select top 1 Proxy_ID FROM @Proxy_Period WHERE Proxy_FillingDt<=@SnapShotDate order by Proxy_FillingDt desc)                                                                                                           
                                                                                                                                    
                                 
 -----------------------------------------Section 3.1.1----------------------------------                                                                                                                                   
                                                                                                                                                                                                        
  SET @Outcome = NULL                                                                                                                       
  SET @sec='3.1.1'                                       
                                        
                                        
                                                                                                                                                                                               
  SET @OutCome = (SELECT ISNULL([dbo].[Fn_CXOInFiscalPeriod_SSD](@CoverID,@SnapShotDate),'No') OutCome)                                                                                   
   if @OutCome='No'                                      
   begin                                      
 set @OutCome=@OutCome                                      
   end                                                                                                                     
   else                                      
begin                                      
   --from 3.1.2 yes or no                                      
  declare @Outcome_312 varchar(50)                                      
                                        
        SET @Outcome_312 =   ( SELECT Top 1 IsIndependentDirector FROM      dbo.ProxyDirectorRecords                                          
       
          
            
              
                
                  
                    
                      
                        
                          
                            
                              
                              
  
                                     
                                        
               
 JOIN dbo.Proxy_Period                                                                                                                    
     ON Proxy_Period.Proxy_ID = ProxyDirectorRecords.Proxy_Id                                                          
     JOIN DIRECTOR DR ON DR.director_Id = ProxyDirectorRecords.director_Id                                                                                                                                
     WHERE                                                                     
     --ProxyDirectorRecords.IsIndependentDirector = 'True'                                          
     --AND                                    
      Proxy_Period.Proxy_FillingDt IN(@Fdate)                                                                                                                                                         
     AND DR.Cover_Id = @CoverID                                                                                                                        
     )                                                                                                                            
       IF @Outcome_312 =0                                                                             
           SET @Outcome_312 ='No'                                                                                                                                                                                        
        else                                                                                                                                                                                 
     SET @Outcome_312 ='Yes'                                                           
 if ((SELECT ISNULL([dbo].[Fn_CXOInFiscalPeriod_SSD](@CoverID,@SnapShotDate),'No'))!='No')                                                      
 begin                                                                                   
    SET @Outcome_312 =   ( SELECT Top 1  ISNULL(ProxyDirectorRecords.LeadDirector,'No') FROM      dbo.ProxyDirectorRecords                                                                                              
                        
                          
                            
                              
                               
                                   
                                    
                                         
                                         
                                                                
   JOIN dbo.Proxy_Period                                                                                                                                     
   ON Proxy_Period.Proxy_ID = ProxyDirectorRecords.Proxy_Id                                                                                                                                                                              
   JOIN DIRECTOR DR ON DR.director_Id = ProxyDirectorRecords.director_Id                                                       
   WHERE ProxyDirectorRecords.IsIndependentDirector = 'True'                                                                                                                                                                                      
   AND Proxy_Period.Proxy_FillingDt IN(@Fdate)                                                                                                               
   AND DR.Cover_Id = @CoverID                                                                                                                
   )          
   IF @Outcome_312 IS NULL                                                                                                                                                                      
   SET @Outcome_312 ='No'    
   else                                                                                             
   SET @Outcome_312 ='Yes'                                                      
 end                                                       
else                                                          
  begin                                                          
    SET @Outcome_312='No'                    
  end                                          
                                        
  --end from 3.1.2 yes or no                                          
    if @Outcome_312='Yes'                                      
   begin                                      
 set @OutCome='Yes; however, has disclosed lead/ presiding independent director'                                      
   end                                           
    if @Outcome_312='No'                                      
   begin                                      
 set @OutCome='Yes; and there is no disclosed lead/ presiding independent director, either'                                      
   end                                        
end                                       
                                                                               
  insert into AggMedianPreprocessData3_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                          
    select @CoverID,@sec,@SnapShotDate,@OutCome                                                                                
 -----------------------------------------Section 3.1.2----------------------------------                                                                                                                   
                                                                                                               
    SET @sec = '3.1.2'                                                                  
    SET @Outcome = NULL                                                                    
                                                                                                                                                                                               
                                                                                                                     
                                                                                                                
     -- SET @Outcome =   ( SELECT Top 1  IsIndependentDirector FROM      dbo.ProxyDirectorRecords                                                                                                                                                             
  
    
      
        
          
            
              
                
                  
                    
                       
                        
                         
                             
                              
                               
                                  
                                     
                                     
                                        
                                          
                        
                                             
                                                
                                                  
                                                    
                                                      
                                                        
                                                          
                                                            
                                                            
                                                                
                                                                  
                                                                    
                                                                                              
                                   
                                                      
     --JOIN dbo.Proxy_Period                                                        
     --ON Proxy_Period.Proxy_ID = ProxyDirectorRecords.Proxy_Id                                                    
     --JOIN DIRECTOR DR ON DR.director_Id = ProxyDirectorRecords.director_Id                                                                                                               
     --WHERE                                                                     
     ----ProxyDirectorRecords.IsIndependentDirector = 'True'                                                                               
            
              
               
                            
                              
                                
                                  
                                    
     ----AND                                                                                   
     -- Proxy_Period.Proxy_FillingDt IN(@Fdate)                                                                                                                                                         
     --AND DR.Cover_Id = @CoverID                     
     --)                    
     --  IF @Outcome =0                                                                                                                                                                              
     --      SET @Outcome ='No'                                                                                         
     --   else                                                                      
     --SET @Outcome ='Yes'                                                           
 --if ((SELECT ISNULL([dbo].[Fn_CXOInFiscalPeriod_SSD](@CoverID,@SnapShotDate),'No'))!='No')                                                            
 --begin                                                                                   
 --  SET @Outcome =   ( SELECT Top 1  ISNULL(ProxyDirectorRecords.LeadDirector,'No') FROM      dbo.ProxyDirectorRecords                                                                                                                                        
 
     
     
        
          
            
              
                 
                  
                    
                     
                        
                          
                            
                               
                                
                                  
                                    
                                      
                                        
                                            
                                            
                                              
                                                
                                                 
                                                     
                                                                   
 -- JOIN dbo.Proxy_Period                                                                                                                                     
 -- ON Proxy_Period.Proxy_ID = ProxyDirectorRecords.Proxy_Id                                                                                                            
 -- JOIN DIRECTOR DR ON DR.director_Id = ProxyDirectorRecords.director_Id                                                       
 -- WHERE ProxyDirectorRecords.IsIndependentDirector = 'True'                                                                                                                                                                                      
 -- AND Proxy_Period.Proxy_FillingDt IN(@Fdate)                                                                                                               
 -- AND DR.Cover_Id = @CoverID                                                                                                                
 -- )                                                                                                               
 -- IF @Outcome IS NULL                                                                                                                                                                      
 --    SET @Outcome ='No'                                                            
 -- else                                                                                                                                                           
 -- SET @Outcome ='Yes'                                                              --end                                                          
 --else                                                          
 --begin                                                          
   SET @Outcome=0                                                             
 --end                              
                                                                                                                                           
                                                                                                   
   insert into AggMedianPreprocessData3_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                             select @CoverID,@sec,@SnapShotDate,@OutCome                                            
   
   
       
       
           
           
                            
                              
                                
                                  
                                    
                            
                                        
                                          
                                             
                            
                                                                                                          
                                                                                                                                    
 -----------------------------------------Section 3.1.3----------------------------------                                                            
                                                            
    SET @dtmax = NULL                                                                                                                                                                                                                       
    SET @NoofRestatement =  NULL                                                                                       
                                                         
    SET @sec = '3.1.3'                                                                                                                    
                                                                                       
      set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                                                                                              
  
    
      
        
          
            
              
                
                 
                     
                     
                        
                          
                             
                              
                                
                                  
                                    
                                      
                                        
                                                                                                                                                                                         
                                  
                                    
                                                                                                       
   -- IF @Fdate != NULL                                                                                                 
    BEGIN                                                                                               
    DECLARE @INDCHAIR_CNT INT               
    DECLARE @INDDIR_CNT INT                                             
 DECLARE @INDCEO_CNT INT                                                                                                                    
    DECLARE @INDCFO_CNT INT                                                                                                                                                          
    DECLARE @TOT_CNT1 INT                                                                                            
                                                                
    DECLARE @CHAIR_CNT INT                                                                                                                      
    DECLARE @CEO_CNT INT                   
DECLARE @CFO_CNT INT                                                                                  
    DECLARE @DIRECTOR_CNT INT                                                                                                                                                
    --DECLARE @TOT_CNTRES INT                                                                                                    
                                                                                                                              
    SELECT @INDCHAIR_CNT=COUNT(*)   FROM @directorTab  dr                   
    INNER JOIN @ProxyDirectorRecords PRXCH ON  PRXCH.director_Id=dr.director_Id                                                                                
    JOIN    @Proxy_Period p  ON   p.Proxy_Id = PRXCH.Proxy_Id                                                                                                                                            
WHERE dr.Cover_Id=@CoverID AND  PRXCH.Independence='Independent' AND                                                                                                                                
    p.Proxy_FillingDt IN(@Fdate)    and dr.Type='chair'                                                                           
     and                                                                                                           
 (@Fdate)                                            
 between                                                                                                           
 dr.DirectorDateArival and isnull(dr.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                                                                                                                                                         
  
    
       
       
          
             
             
                 
                 
                    
                      
                        
                                                                                                      
    select @INDDIR_CNT=COUNT(*)   FROM @directorTab  dr                                                                                                                                                                                       
    INNER JOIN @ProxyDirectorRecords PRXCH ON  PRXCH.director_Id=dr.director_Id                                                                                                                               
    JOIN    @Proxy_Period p  ON      p.Proxy_Id = PRXCH.Proxy_Id                                                                                           
                                   
                                    
                                     
                                        
                                           
                                           
                                              
                   
                                                   
                                             
                                                       
               
                                                          
                                              
                      
    WHERE dr.Cover_Id=@CoverID AND  PRXCH.Independence='Independent' AND                                                                                                                                                                        
    p.Proxy_FillingDt IN(@Fdate)    and dr.Type='director'                                         
    and                                                                                                           
 (@Fdate)                                                                                                          
 between                            
 dr.DirectorDateArival and isnull(dr.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                                                                                                                                                          
  
    
     
         
          
            
              
                
                  
            
                       
                        
                         
                             
                             
                                 
                                  
                                    
                                      
                                        
                                          
                                            
                                              
                                                
                                                  
                                                    
                                                      
                                                        
                                                          
                                                            
                                          
                                                               
                                                                 
                                                                    
                                                                       
                                                                       
                                                                                                          
    select @INDCEO_CNT=COUNT(*)   FROM @directorTab  dr                                                                                                                                                    
    INNER JOIN @ProxyDirectorRecords PRXCH ON  PRXCH.director_Id=dr.director_Id                                            
                  
                    
                      
                        
                          
                            
                              
                                          
                                    
                                      
                                       
                                           
                                            
                                              
                                                
                                                  
                                                    
JOIN    @Proxy_Period p  ON      p.Proxy_Id = PRXCH.Proxy_Id                                                                                                                                       
                                                                                                              
                                                
                                                                                                                   
    WHERE dr.Cover_Id=@CoverID AND  PRXCH.Independence='Independent' AND                                                                                                                                                   
    p.Proxy_FillingDt IN(@Fdate)   and dr.Type='ceo'                                                                                                            
    and                                                         
 (@Fdate)                                                                                                          
 between                                                                                                           
 dr.DirectorDateArival and isnull(dr.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                                                            
                                                       
    select @INDCFO_CNT=COUNT(*)   FROM @directorTab  dr                                                                                                              
    INNER JOIN @ProxyDirectorRecords PRXCH ON  PRXCH.director_Id=dr.director_Id                                                                                                                         
              
                                        
                                          
                                            
                                             
                                                 
                                         
                                                    
                                                     
                           
                                                           
                                                            
                                                      
                                                                
                                                                 
    JOIN    @Proxy_Period p  ON      p.Proxy_Id = PRXCH.Proxy_Id                                                                                                                                                                                               
  
   
       
       
    WHERE dr.Cover_Id=@CoverID AND  PRXCH.Independence='Independent' AND                                                                 
    p.Proxy_FillingDt IN(@Fdate)   and dr.Type='cfo'                                                                   
    and                                                                                                           
 (@Fdate)                                                              
 between                                                                                           
 dr.DirectorDateArival and isnull(dr.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                                                                                                                  
                         
                                                                                                    
    SET @TOT_CNT1= @INDCHAIR_CNT  +@INDDIR_CNT  +@INDCEO_CNT+@INDCFO_CNT                                                                                                                                                     
                                                                                                                                          
                     
    select @CHAIR_CNT = COUNT(director_Name) FROM @directorTab dr inner join @ProxyDirectorRecords pdr                                                                                                                                                
    on pdr.director_Id=dr.director_Id                                                                            
    JOIN    @Proxy_Period p  ON  p.Proxy_Id =  pdr.Proxy_Id                                 
    WHERE dr.Cover_Id=@CoverID AND p.Proxy_FillingDt IN(@Fdate)   and dr.Type='chair'                                                                                                                                               
     and                                                                                             
 (@Fdate)                                                                                                          
 between                              
 dr.DirectorDateArival and isnull(dr.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                                                                                    
                                                                                                           
                                                                                                                                  
                                                                                                                                    
                                                                                                                                                                                                                                                    
    select @CEO_CNT = COUNT(director_Name) FROM @directorTab dr inner join @ProxyDirectorRecords pdr                                                               
    on pdr.director_Id=dr.director_Id                                                                                              
    JOIN @Proxy_Period p  ON  p.Proxy_Id =  pdr.Proxy_Id                                                                                                     
    WHERE dr.Cover_Id=@CoverID AND p.Proxy_FillingDt IN(@Fdate)   and dr.Type='ceo'                 
    and                                                                                                           
 (@Fdate)                                                                                              
 between                                           
 dr.DirectorDateArival and isnull(dr.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                                                                                                      
                                                                                                                            
                                                                                                             
    select @CFO_CNT = COUNT(director_Name) FROM @directorTab dr inner join @ProxyDirectorRecords pdr                                                                         
                                                  
                                                      
                                                        
                                                           
                                                           
    on pdr.director_Id=dr.director_Id                                                    
    JOIN @Proxy_Period p ON  p.Proxy_Id =  pdr.Proxy_Id                                                                                       
    WHERE dr.Cover_Id=@CoverID AND p.Proxy_FillingDt IN(@Fdate)   and dr.Type='cfo'                                                                                                                                                                           
  
    
       
       
          
            
              
                
                   
                   
                      
                        
                          
                            
                              
                                
                                  
                                    
                                       
                                        
                                          
         
                                              
                                                
     and                                                                                                           
 (@Fdate)                                                                                                          
 between                                                                                                           
 dr.DirectorDateArival and isnull(dr.DirectorDateDeparture,'2100-01-01 14:59:10.533')        
    SELECT @DIRECTOR_CNT = COUNT(*) FROM @directorTab dr inner join @ProxyDirectorRecords pdr                                                          
    on pdr.director_Id=dr.director_Id                                                                                                                                                                
    JOIN    @Proxy_Period p  ON  p.Proxy_Id =  pdr.Proxy_Id                                                                                                                                 
     WHERE dr.Cover_Id=@CoverID AND p.Proxy_FillingDt IN(@Fdate)  and dr.Type='director'                                                                               
                                                                      
                                                                        
                                                                          
                                                                            
                     
                                                                                
                                                  
                                                                                    
                                                                                     
                                                                                                          
  and                                                                                            
 (@Fdate)                                                            
 between                                                                                                           
 dr.DirectorDateArival and isnull(dr.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                                                         
                                                                                                               
                                                    
                                                                                                                                                                                 
    SET @TOT_CNTRES= @CHAIR_CNT+@CEO_CNT+@CFO_CNT+@DIRECTOR_CNT                                                                                                                                                        
                                                        
    IF(@TOT_CNTRES!=0)                                                            
                                                                                                                                              
                                                                                      
SET @Outcome =  convert(decimal(18,2),(CAST (@TOT_CNT1 AS DECIMAL(18,2))/CAST(@TOT_CNTRES AS DECIMAL(18,2)))*100)                                                                                   
 ELSE                                                                                        
   SET @Outcome = 0                                                                                                                                                
   END                                                                                                              
                                                              
                                                         
                                                                                                                                   
                                                      
    insert into AggMedianPreprocessData3_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                            
    select @CoverID,@sec,@SnapShotDate,@OutCome                                                                               
                                                                                        
     -----------------------------------------Section 3.2.1----------------------------------                                                                                                                                                                  
  
   
       
       
           
           
               
               
                  
                    
                       
                        
                    
                            
                              
                                                                                                                   
  SET @Outcome = NULL                                                                  
  SET @sec='3.2.1'                                                                                                                                                                            
   set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)             
                                                             
   SET @dtmax = NULL                                                                                                                                                         
   SET @NoofRestatement =  NULL                                                                                                                                                                                                                          
                 
                                      
                                                                                                                                                                                                
                                                                                                                                                             
                                                                                                           
                                                                                                                                                                                                     
 BEGIN                                                                                                                                                     
   DECLARE @TOTInd1 DECIMAL (18,2)                                                             
   DECLARE @TOT1 INT                                                                                               
                                                                                                                                         
   DECLARE @PERIND1 DECIMAL(18,2)                                                                                               
                                                                                                                                  
   SELECT @TOTInd1 = COUNT(dr.director_Id)   FROM @directorTab  dr                                                                                            
   INNER JOIN @ProxyDirectorRecords PRXCH ON  PRXCH.director_Id=dr.director_Id         
   JOIN    @Proxy_Period p  ON  p.Proxy_Id =  PRXCH.Proxy_Id                                                                
                  
                   
                       
                        
                          
                                
                                                                              
                                                                               
                                                                                  
                                                                                    
                                                                                      
                                        
                                                    
                                                    
                                                                                                          
                                                                       
                                                                                                              
                                                                                                                
                                                                                             
                                                                                                                    
                                                                                                                     
   WHERE dr.Cover_Id=@CoverID  AND  PRXCH.Independence='Independent' and                                                                                                                                                                                      
   
    
   p.Proxy_FillingDt = @Fdate   --and dr.Type='chair'                                                                                                                                               
    and (prxch.CommitteeAudit='Audit: Chair' OR prxch.CommitteeAudit='Audit: Member' )                                                                                                          
    and                                                                                                           
 (@Fdate)                                                                                                     
 between                        
 dr.DirectorDateArival and isnull(dr.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                      
                                                                                                                                                
 SELECT @TOT1= COUNT(dr.director_Id)   FROM @directorTab  dr                                                                                         
   INNER JOIN @ProxyDirectorRecords PRXCH ON  PRXCH.director_Id=dr.director_Id                                                                                                                                                                                 
  
    
     
        
          
             
             
                 
                 
                     
                      
                        
                         
                            
                               
                                
                                  
                                    
                                     
                                        
                         
                                            
                                              
                                                
                                              
                                                    
                                                      
                                                        
                                                           
                                    
                                               
                                                                
                        
                                                                    
   JOIN    @Proxy_Period p  ON  p.Proxy_Id =  PRXCH.Proxy_Id                                                                                                                                                                    
 WHERE dr.Cover_Id=@CoverID  AND                                                                                                                                                                                                                  
   p.Proxy_FillingDt = @Fdate                                                                         
   and (prxch.CommitteeAudit='Audit: Chair' OR prxch.CommitteeAudit='Audit: Member' )                                          
   and                                                       
 (@Fdate)                                                                                                          
 between                                                                                                           
 dr.DirectorDateArival and isnull(dr.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                                                                            
                                                                        
                                                                           
                                                                           
                                                                              
                                                                                 
                                                                                  
                                                                                  
                             
                                                                                         
                                                     
                                                        
                                         
                                                                                                
                                                                                                  
                                                                                                                             
                                                                     
   --SET @TOT_COMP1 =@CEO_CNT_COMP+@CFO_CNT_COMP+@CHAIR_CNT_COMP+@DIREC_CNT_COMP                                                                                                                            
                            
                             
                                 
                                 
                                     
                                      
                                        
                                          
                                           
                                               
                                                
                                                  
                                                    
                                                      
                                                        
                                                          
                       
                                                              
                                                               
                                                    
                                                                    
                                                                      
                                                                                      
   IF(@TOT1=0)                                                                                   
   begin                                                                                                            
  SET @PERIND1=0                                                                                                                      
   SET @Outcome = 'No Audit Committee'                                                                                                                                                                                                                        
   
    
      
        
         
             
             
                 
                  
                    
                     
 end                                                                                    
   ELSE                                                            
   begin                                                                                                 
 if @TOTInd1 >0                                                                                                                          
 SET @PERIND1=(@TOTInd1/@TOT1)*100                                                     
else                                                                                                                                                                                  
     SET @PERIND1=0                                                                                       
                                                                                                                                                                                                                                                          
   -- IF(@PERIND1!=0)                                         
   set @Outcome = convert(decimal(18,2),@PERIND1)                                                        
    --if convert(decimal(18,2),@PERIND1)>=CAST(ROUND(@var,0)AS INT)                                                                                                                   
    --SET @Outcome = 'Yes'                                                                 
    --ELSE                                                                                                          
    --SET @Outcome = 'No'                                                                            
    end                                              
                                       
     END                                                                                                                                                                                           
                                                                                                                                                                
                                                                                                                                                            
          
   insert into AggMedianPreprocessData3_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                            
    select @CoverID,@sec,@SnapShotDate,@OutCome                                                                                              
     -----------------------------------------Section 3.2.2----------------------------------                                                                                                                                                                  
 
    
      
         
          
            
             
                 
                  
                   
                      
                        
                          
                                                       
  SET @Outcome = NULL                                                                           
  SET @sec='3.2.2'                       
                                                                                                                           
                                                                                                                                                               
SET @NoofRestatement =  NULL                                                                                                                                             
  set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                                                        
                                             
                                                                                                          
                                                                                   
                                                                                     
                                                                                               
   BEGIN                                                                                         
   DECLARE @TOTAL1 DECIMAL (18,2)                                                                                            
   DECLARE @CEO_CNT_COMP INT                                                                                                                       
   DECLARE @CFO_CNT_COMP INT                                                                                                                                                                                                                          
   DECLARE @CHAIR_CNT_COMP INT                                                                                                                              
   DECLARE @DIREC_CNT_COMP INT                                                                                                       
   DECLARE @TOT_COMP1 DECIMAL(18,2)                                                                                                                    
   DECLARE @PERIND_NOM_COM_MEMBER DECIMAL(18,2)                                                                                                                                    
                                   
   SELECT @TOTAL1 = COUNT(dr.director_Id)   FROM @directorTab  dr                                                                                                                
   INNER JOIN @ProxyDirectorRecords PRXCH ON  PRXCH.director_Id=dr.director_Id                                                
   JOIN    @Proxy_Period p  ON  p.Proxy_Id =  PRXCH.Proxy_Id                                                                                                                                                                                                  
 
                                                                                                        
                                                       
WHERE dr.Cover_Id=@CoverID  AND  PRXCH.Independence='Independent' and                                                                                                                                 
   p.Proxy_FillingDt = @Fdate   --and dr.Type='chair'                                                                                                                           
    and (prxch.CommitteeCompensation='Compensation: Chair' OR prxch.CommitteeCompensation='Compensation: Member' )                                                                   
    and                                                      
 (@Fdate)                                                                                                          
 between                          
 dr.DirectorDateArival and isnull(dr.DirectorDateDeparture,'2100-01-01 14:59:10.533')                 
                                                                                                                                                
   SELECT @CEO_CNT_COMP= COUNT(dr.director_Id)   FROM @directorTab  dr                                                                                                                                    
   INNER JOIN @ProxyDirectorRecords PRXCH ON  PRXCH.director_Id=dr.director_Id                                                                                          
   JOIN    @Proxy_Period p  ON  p.Proxy_Id =  PRXCH.Proxy_Id                                                                                                 
                        
                          
                            
                             
 WHERE dr.Cover_Id=@CoverID  AND                                                                                                                                                                          
       
        
         
             
             
                 
                 
                    
                       
                        
                          
                            
                           
                                
                                  
                                    
                                       
                                       
                                          
                                            
                                              
                                          
                                                  
                                                    
                                  
   p.Proxy_FillingDt = @Fdate                                                                                                                
    and (prxch.CommitteeCompensation='Compensation: Chair' OR prxch.CommitteeCompensation='Compensation: Member' )                                                                                                                
                                                                 
                                                                                      
                                                                                        
                                                                                          
                                                                                            
                     
                                                                                               
                                                             
                                                                                                    
                                                                                                       
                                                                                      
                                                                                                           
  and                                              
 (@Fdate)                                                                                                          
 between                                                                              
 dr.DirectorDateArival and isnull(dr.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                                                                                          
                                                                                                                         
                                                                                                                               
                                                                                                       
   SELECT @CFO_CNT_COMP = 0                                                                                                                             
                                                            
                                                
                                                  
                                                    
                                                      
                                                        
                                                         
                                                       
             
                         
                                                                  
                                                                   
                                                                       
                                                                       
                                                                          
                                                                            
                                          
                                                                                      
                                                                                        
                                                                                          
                                                                                            
                          
                                              
                                                                                                 
                                                                                      
   SELECT @CHAIR_CNT_COMP=0                                                                                                                       
                                                                    
   SELECT @DIREC_CNT_COMP=0                                                                                          
                                                                        
   SET @TOT_COMP1 =@CEO_CNT_COMP+@CFO_CNT_COMP+@CHAIR_CNT_COMP+@DIREC_CNT_COMP                                                              
                                                                                               
   IF(@TOT_COMP1=0)                                                                                                                                                                                       
   begin                                                                                             
   SET @PERIND_NOM_COM_MEMBER=0                            
   SET @Outcome = 'No Compensation Committee'                                                                                                                                    
  end                                                                                                                                       
   ELSE                                                           
    begin                                                                                                                                                 
  SET @PERIND_NOM_COM_MEMBER=(@TOTAL1/@TOT_COMP1)*100                                                
    SET @Outcome =convert(decimal(18,2),@PERIND_NOM_COM_MEMBER)                          
            
             
                 
                  
                   
                      
                         
                          
                            
                             
                                 
                                 
  --if convert(decimal(18,2),@PERIND_NOM_COM_MEMBER)>=CAST(ROUND(@var,0)AS INT)                                                                                                                                                                               
  
     
     
        
           
            
              
             
                  
                    
                      
                        
                          
                            
                              
                                
                                  
                                    
                                     
                                         
  --SET @Outcome = 'Yes'                                                                                            
                                                                         
  --ELSE                                                              
  --SET @Outcome = 'No'                      
  end                                                                                                                                                                                                  
                                                                           
     END                                                        
                                                                                               
                         
                                                                                                                                                               
  insert into AggMedianPreprocessData3_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                            
    select @CoverID,@sec,@SnapShotDate,@OutCome                                         
     -----------------------------------------Section 3.2.3----------------------------------                                                                                                                                                                  
  
   
       
        
         
             
              
                
                  
                    
                      
                       
                           
                            
                             
                               
                                                                               
  SET @Outcome = NULL                                                                                                                   
  SET @sec='3.2.3'                                                                                                    
                                                              
   set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                         
  SET @NoofRestatement =  NULL                                                                                                                                              
                                                                     
BEGIN                                                                                                         
   DECLARE  @ind_nom_com_mem_tot INT                                                                                                                                                                                                            
   DECLARE @COM_NOM_MEM_perc DECIMAL(18,2)                                                                                           
   DECLARE @C1 INT                                                                                  
   DECLARE @C2 INT                                                                   
   DECLARE @C3 INT                              
   DECLARE @C4 INT                                                         
DECLARE @RESULT1 DECIMAL(18,2)                                                                                                                                      
                                                                                                                  
   SET @ind_nom_com_mem_tot = (select COUNT(dr.director_Id)   FROM @directorTab  dr                                                                                                                                                                            
  
    
      
       
           
            
              
               
                   
                   
                       
                        
                         
                            
                               
                                
                                  
                                    
                                      
                                       
                                                                                             
                                                      
                                                                                                                                                                 
                                                                                                    
                                                                                                                       
                                
   INNER JOIN @ProxyDirectorRecords PRXCH ON  PRXCH.director_Id=dr.director_Id                                                                                                                                                                                 
  
    
      
        
          
            
              
                
                  
                    
                      
                        
                          
                            
                              
                                
                   JOIN    @Proxy_Period p  ON  p.Proxy_Id =  PRXCH.Proxy_Id                                                                   
   WHERE dr.Cover_Id = @CoverID  and                                                                                                     
   p.Proxy_FillingDt = @Fdate    and prxch.Independence='Independent'                                                                                                        
                                        
    and (prxch.CommitteeNominating='Nominating: Member' OR prxch.CommitteeNominating='Nominating: Chair' )                                                                                                          
    and                                                                                                           
 (@Fdate)                                                                                                          
 between                                                 
 dr.DirectorDateArival and isnull(dr.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                                                                                          
    )                                                                                
                                                                                                                                                         
   SELECT @C1= COUNT(dr.director_Id)   FROM @directorTab  dr                                                                                                                                                                                 
   INNER JOIN @ProxyDirectorRecords PRXCH ON  PRXCH.director_Id=dr.director_Id                                                                                                                                                                                 
  
    
      
        
          
           
               
                
                  
                    
                                       
                                          
                                            
                                              
                                                
                               
                                                     
              
                                                         
                                                         
                                                            
                                                              
                                                                
                                                                  
                                                                    
   JOIN    @Proxy_Period p  ON  p.Proxy_Id =  PRXCH.Proxy_Id                                    
   WHERE dr.Cover_Id=@CoverID  and                                                                                                                                           
   p.Proxy_FillingDt = @Fdate   and dr.Type='chair'                                                                                                                                                                    
    and (prxch.CommitteeNominating='Nominating: Member' OR prxch.CommitteeNominating='Nominating: Chair')                                                                                                                                                      
  
    
      
        
          
        
              
               
                   
                    
                      
                       
                          
                            
                       
                               
                                   
                                   
                                       
                                        
                                          
                                            
                                              
                                                
                                                  
                                                    
                                                      
                                                        
                                                   
                                                            
                                                             
                                                                 
                                                                  
                                                                   
                                                                      
                                                                   
                                                                          
                                                                            
                                                                             
                                                                             
                                                                                    
                                               
                                                                                        
                                                                                        
                                                                                       
                                                                
                                                                                               
                                                                                                   
                         
                                                                                                      
                                                                                                        
                                                                                                          
  and                                                                           
 (@Fdate)                                                                   
 between                                                                                                           
 dr.DirectorDateArival and isnull(dr.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                                                                                          
                                                                                 
                                 
                                                                                           
                                                    
                                                                                                                      
                                                                                                                                      
                                                                           
    SELECT @C2=COUNT(dr.director_Id)   FROM @directorTab  dr                                                                                                                                               
   INNER JOIN @ProxyDirectorRecords PRXCH ON  PRXCH.director_Id=dr.director_Id                                                                                                                                    
   JOIN    @Proxy_Period p  ON  p.Proxy_Id = PRXCH.Proxy_Id                                                                              
                                                                        
                                                                          
                                                                           
                                                                               
                                                                                
                                                                                 
                                                                                    
                                                                
                                                                                      
                   
                                                                                                             
   WHERE dr.Cover_Id=@CoverID  and                                              
                                
                                  
                                    
                                      
                                        
   p.Proxy_FillingDt = @Fdate   and dr.Type='ceo'                                                                                                                                                        
    and (prxch.CommitteeNominating='Nominating: Member' OR prxch.CommitteeNominating='Nominating: Chair')                                                                                                                     
                                                                                                          
  and                                                                         
 (@Fdate)                                                                                                          
 between                                                                                                           
 dr.DirectorDateArival and isnull(dr.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                                                                                          
                                                                                                              
                                                                                                         
                                                                                                                  
                                                    
                                                      
                                                                                                                       
                                                                                                      
                                                                       
                                                                                                    
                                                                                                                                                            
    SELECT @C3= COUNT(dr.director_Id)   FROM @directorTab  dr                                                                                              
 INNER JOIN @ProxyDirectorRecords PRXCH ON  PRXCH.director_Id=dr.director_Id                                                                                                      
   JOIN    @Proxy_Period p  ON  p.Proxy_Id =  PRXCH.Proxy_Id                                                                                                                         
   WHERE dr.Cover_Id=@CoverID  and                                                                                                                  
   p.Proxy_FillingDt = @Fdate   and dr.Type='cfo'                                                  
    and (prxch.CommitteeNominating='Nominating: Member' OR prxch.CommitteeNominating='Nominating: Chair')                                       
                                                                   
                                                                      
                                                                        
                                                                           
                                                        
                   
                                                                               
                                                                                   
                                                                                    
                                     
     and                                                                                                           
 (@Fdate)                                           
 between                                                                                                           
 dr.DirectorDateArival and isnull(dr.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                                                                                                    
                                                                             
    SELECT @C4= COUNT(dr.director_Id)   FROM @directorTab  dr                                                                           
   INNER JOIN @ProxyDirectorRecords PRXCH ON  PRXCH.director_Id=dr.director_Id                                                                                                                   
   JOIN    @Proxy_Period p  ON  p.Proxy_Id =  PRXCH.Proxy_Id                                                          
   WHERE dr.Cover_Id=@CoverID  and                                                                                              
    p.Proxy_FillingDt = @Fdate   and dr.Type='director'                                                                                             
                                                                                                                              
   and (prxch.CommitteeNominating='Nominating: Member' OR prxch.CommitteeNominating='Nominating: Chair')                                                                                                
    and                                                             
 (@Fdate)                                                                                  
 between                        
 dr.DirectorDateArival and isnull(dr.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                                                                                          
                                                                                                                                                SET @RESULT1=(@c1 + @c2 + @c3 + @c4)                                                                          
  
    
      
        
           
           
              
                 
                 
                     
                     
                 
                          
                           
                               
                               
                                  
                                    
                                       
                                       
                                           
                                                                                  
                                            
                             
                                                    
                                                       
                                                 
                                                         
                                                             
                                                             
                                                                
   IF(@RESULT1>0)                                                                  
    set @COM_NOM_MEM_perc=((@ind_nom_com_mem_tot/@RESULT1)*100)                                                          
                                                               
                                                                   
                                                                    
                                                                      
                                                                        
                                                                          
                                                                           
                                                                               
                                                           
                                                                                  
                                                           
                                
                                                           
                                                                                          
                                                                                 
                                                                                                     
                                                                                                   
                                                                                            
                                     
                                                                                                         
   else                                                                                                           
    set @COM_NOM_MEM_perc=0                                                                                                                             
                                                                                                                                                                                                                                                         
  -- SET @Outcome = @COM_NOM_MEM_perc                                                                                                           
                                                                                         
      -- IF(@COM_NOM_MEM_perc!=0)                          
   if convert(decimal(18,2),@COM_NOM_MEM_perc)>=CAST(ROUND(@var,0)AS INT)                                         
                                                                                                                           
   SET @Outcome = @COM_NOM_MEM_perc                                              
   ELSE                                                                                                                                                        SET @Outcome = @COM_NOM_MEM_perc                                                               
  
     
      
        
          
           
               
                
                  
                   
                      
                         
                          
                            
                                                                                          
 if @RESULT1=0                                                                                                                                                                                                            
 SET @Outcome = 'No Nominating Committee'                                                                                  
                                                           
END                                                                                                                                                                                                                                                    
                                             
                                                                                                                                                        
                          
  insert into AggMedianPreprocessData3_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                  
    select @CoverID,@sec,@SnapShotDate,@OutCome                                                                                         
                                                                                                
                                                                                                
    -----------------------------------------Section 3.3.1----------------------------------                                                                      
                                                                                                           
  SET @Outcome = NULL                                                                                                 
  SET @sec='3.3.1'                                            
   set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                         
                                                                                                                                                                                                                     
   SET @dtmax = NULL                                                                                                            
   SET @NoofRestatement =  NULL                                       
                                                                                                                                                                        
       -------------------PERS DIR CALC DIR--------------------------------------------------------                                                                                                                          
declare @percnt    decimal(10,2)                                                                                             
declare @totcnt    decimal(10,2)                                                                                                                                                                                                 
declare @Pers      decimal(10,2)                                                                                                
         
             
                                                                    
set @percnt=(select COUNT(*) as dirperc from                                                             
(                                                                                                                     
Select authority_id , isnull(COUNT(Authority_Id),0) DirCount                                                                                             
from Other_Director  od                                                             
inner join director co on co.director_Id= od.Authority_Id                                                                                                
inner join ProxyDirectorRecords prxceo on prxceo.director_Id=co.director_Id                                                                                                                   
where Cover_Id=@coverid and prxceo.Proxy_Id=(SELECT TOP 1 Proxy_ID FROM Proxy_Period WHERE Cover_Id=@CoverID AND Proxy_FillingDt=@Fdate ORDER BY Proxy_FillingDt DESC)                                                                                         
  
    
     
       
            
              
               
                  
                 
                     
                         
                         
                            
                              
                                
                                  
                                    
                                      
         
                                           
                                            
                                             
                                                
                                                  
                                                    
                                                      
                                                        
                                                           
                                                            
                                                              
                                                               
                                                                 
and od.Proxy_Id=(SELECT TOP 1 Proxy_ID FROM Proxy_Period WHERE Cover_Id=@CoverID AND Proxy_FillingDt=@Fdate ORDER BY Proxy_FillingDt DESC)                                                                                                  
and co.Type='ceo'                                                                                               
and                                                      
 (select Proxy_RecordDt from Proxy_Period where Proxy_ID=@FID)                                    
 between                                                               
 co.DirectorDateArival and isnull(co.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                                                                                  
Group by authority_id                                                                                                                                                                                                             
Having Count(*)>= CAST(ROUND(@var,0) AS INT)                       
union                                                                                                  
Select authority_id , isnull(COUNT(Authority_Id),0) DirCount                                                                                                                    
from Other_Director  od                                                                                                                                                    
inner join director co on co.director_Id= od.Authority_Id       
inner join ProxyDirectorRecords prxceo on prxceo.director_Id=co.director_Id                                                                                                
where Cover_Id=@coverid and prxceo.Proxy_Id=(SELECT TOP 1 Proxy_ID FROM Proxy_Period WHERE Cover_Id=@CoverID AND Proxy_FillingDt=@Fdate ORDER BY Proxy_FillingDt DESC)                                                                                    
 and od.Proxy_Id=(SELECT TOP 1 Proxy_ID FROM Proxy_Period WHERE Cover_Id=@CoverID AND Proxy_FillingDt=@Fdate ORDER BY Proxy_FillingDt DESC)                                                                                                                   
   
    
      
       
          
             
              
               
                   
                   
                       
                        
  and co.Type='cfo'                                                                                               
  and                                                                                                       
 (select Proxy_RecordDt from Proxy_Period where Proxy_ID=@FID)                                                                                                      
 between                                                                          
 co.DirectorDateArival and isnull(co.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                                                                                    
Group by authority_id                            
Having Count(*)>= CAST(ROUND(@var,0) AS INT)                                                                                                                 
union                                                                                                                                                              
Select authority_id , isnull(COUNT(Authority_Id),0) DirCount                                                               
from Other_Director  od                                                                                                                                                             
inner join director co on co.director_Id= od.Authority_Id                                                                                                                                                
inner join ProxyDirectorRecords prxceo on prxceo.director_Id=co.director_Id                                                                                                                                                                                    
  
    
      
       
         
where Cover_Id=@coverid                                                          
and prxceo.Proxy_Id=(SELECT TOP 1 Proxy_ID FROM Proxy_Period WHERE Cover_Id=@CoverID AND Proxy_FillingDt=@Fdate ORDER BY Proxy_FillingDt DESC)                                                                                                                 
  
    
      
        
          
            
             
                 
                 
                     
                     
                         
                          
                           
                              
                                
                                   
                                   
                                      
                                        
 and od.Proxy_Id=(SELECT TOP 1 Proxy_ID FROM Proxy_Period WHERE Cover_Id=@CoverID AND Proxy_FillingDt=@Fdate ORDER BY Proxy_FillingDt DESC)                                                             
  and co.Type='chair'                                                                                                     
  and                                                                                                       
 (select Proxy_RecordDt from Proxy_Period where Proxy_ID=@FID)                                                                                                      
 between                                                                            
 co.DirectorDateArival and isnull(co.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                                                                                                                      
Group by authority_id                                                                                                           
Having Count(*)>= CAST(ROUND(@var,0) AS INT)                                                                                                           
union                                                        
Select authority_id , isnull(COUNT(Authority_Id),0) DirCount                                                                                                                     
from Other_Director  od                                                                         
inner join director co on co.director_Id= od.Authority_Id                                       
inner join ProxyDirectorRecords prxceo on prxceo.director_Id=co.director_Id                                                       
where Cover_Id=@coverid                                                                      
and prxceo.Proxy_Id=(SELECT TOP 1 Proxy_ID FROM Proxy_Period WHERE Cover_Id=@CoverID AND Proxy_FillingDt=@Fdate ORDER BY Proxy_FillingDt DESC)                                                                                                                 
  
    
     
         
          
            
              
               
                   
                    
                      
                        
                         
                             
                             
                                
                                  
    
                                     
                                        
 and od.Proxy_Id=(SELECT TOP 1 Proxy_ID FROM Proxy_Period WHERE Cover_Id=@CoverID AND Proxy_FillingDt=@Fdate ORDER BY Proxy_FillingDt DESC)                                     
  and co.Type='director'                                                                                                
  and                                                                                                       
 (select Proxy_RecordDt from Proxy_Period where Proxy_ID=@FID)                                                                                                     
 between                                                                                                       
 co.DirectorDateArival and isnull(co.DirectorDateDeparture,'2100-01-01 14:59:10.533')                   
Group by authority_id                                                                                        
Having Count(*)>= CAST(ROUND(@var,0) AS INT)                                                     
                                                                             
) as x )                                                                                                                  
                                                                                                                                                                                            
                                                                                                  
set @totcnt=(select COUNT(*) as dirperc from                                                                 
(                                                                                                                                                                                                           
Select co.director_Id from director co join ProxyDirectorRecords odr on odr.director_Id=co.director_Id                                                                       
             
                 
                 
                     
                     
                         
                          
                           
                             
                                
                                  
                                   
                                       
                                       
                           
                                             
                                             
                                                 
                                                 
                         
                                                      
                                                       
where Cover_Id=@coverid                                                                      
and odr.Proxy_Id= (SELECT TOP 1 Proxy_ID FROM Proxy_Period WHERE Cover_Id=@CoverID AND Proxy_FillingDt=@Fdate ORDER BY Proxy_FillingDt DESC) and co.Type='ceo'                                                                                                 
  
    
      
       
                    
             
                        
                          
                            
                              
                                
                                  
                                    
                                      
                                
                                          
                                           
                                               
                                                
                                                  
                                                                                              
and                                                                                                       
 (select Proxy_RecordDt from Proxy_Period where Proxy_ID=@FID)                                                                                                      
 between                                                                        
 co.DirectorDateArival and isnull(co.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                                                              
                                                                                                 
                                                                                                                                                  
union                                 
Select co.director_Id from director co join ProxyDirectorRecords odr on odr.director_Id=co.director_Id                                                                                                                                                         
  
    
      
       
          
             
              
               
                   
                    
                      
                        
                          
                            
                              
                                
                                
                                    
                                      
                                        
                                          
                                            
                                              
                                                
                                                  
                                           
                                                      
where Cover_Id=@coverid and odr.Proxy_Id= (SELECT TOP 1 Proxy_ID FROM Proxy_Period WHERE Cover_Id=@CoverID AND Proxy_FillingDt=@Fdate ORDER BY Proxy_FillingDt DESC) and co.Type='cfo'                                                                         
  
    
      
        
          
            
              
                
                  
                    
                      
                        
                          
                            
                              
                                
                                  
                                    
                                      
                              
                                          
                                            
                                              
                                                
                                                  
                                                    
                                                      
                                                       
                                                          
                                                            
                                                              
                                                    
                                                                 
                                                                    
                                                                      
                                                                        
                                                                          
                                                                            
                                                                              
                                                                                
                                                                      
                                                                                    
                                                                                     
                                                                       
  and                                                                                     
 (select Proxy_RecordDt from Proxy_Period where Proxy_ID=@FID)                                                                                                      
 between                                                                                                       
 co.DirectorDateArival and isnull(co.DirectorDateDeparture,'2100-01-01 14:59:10.533')                
                                                                  
                                                                                                    
                                                                                         
union                                                                                                                                   
Select co.director_Id from director co join ProxyDirectorRecords odr on odr.director_Id=co.director_Id                                       
where Cover_Id=@coverid and odr.Proxy_Id= (SELECT TOP 1 Proxy_ID FROM Proxy_Period WHERE Cover_Id=@CoverID AND Proxy_FillingDt=@Fdate ORDER BY Proxy_FillingDt DESC) and co.Type='chair'                                                                       
 
     
      
        
          
           
               
                
                  
                    
           
                       
                           
                           
                               
                                                                                
                                                                                  
                                                                                   
                                                                                           
 and                                                       
 (select Proxy_RecordDt from Proxy_Period where Proxy_ID=@FID)                                                                                                      
 between                                                                                                  
 co.DirectorDateArival and isnull(co.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                                                                                 
                                                                                                  
                                                                                           
                                                                                                                           
union                                                                                               
Select co.director_Id from director co join ProxyDirectorRecords odr on odr.director_Id=co.director_Id                             
where Cover_Id=@coverid and odr.Proxy_Id= (SELECT TOP 1 Proxy_ID FROM Proxy_Period WHERE Cover_Id=@CoverID AND Proxy_FillingDt=@Fdate ORDER BY Proxy_FillingDt DESC) and co.Type='director'                                                
                                                                         
                                                                            
                                                                              
                                                                                 
                                    
                                                                             
                                                                                              
and                                                                                                       
 (select Proxy_RecordDt from Proxy_Period where Proxy_ID=@FID)                                                                                                   
 between                                                                                                       
 co.DirectorDateArival and isnull(co.DirectorDateDeparture,'2100-01-01 14:59:10.533')                                                                          
                                                                                                  
                                                    
                                                                                                                                                                                       
) as x )                                                                                                                                                    
                                                             
if(@totcnt>0)                                                                                                             
   begin                                                                              
  set @Pers = (@percnt/@totcnt)*100                                                                                                                                         
   end                                                                                                         
   else                                                                                                                                       
   begin                                                                                                                 
  set @Pers =0                                                                                                                  
   end                                                                                            
    SET @Outcome = convert(decimal(18,2),@Pers)                                                                                                                                                                           
                      
                                                                                    
                                                                                                                                                               
   insert into AggMedianPreprocessData3_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                         
    select @CoverID,@sec,@SnapShotDate,@OutCome                  
                                                                                                
                                                           
    -----------------------------------------Section 3.3.2----------------------------------                                                                                                                         
                                                                                                                                                        
  SET @Outcome = NULL                                                        
  SET @sec='3.3.2'                                                                                                                   
   set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                                   
                                            
   SET @dtmax = NULL                                                        
   SET @NoofRestatement =  NULL                                                                                                                                                                            
              
                
                  
                    
                      
                        
                           
                            
                             
                                 
                                  
                                   
                                       
                                                                                                                                      
                                            
    declare @ot1 int                                                              
     declare @ot2 int                                                                                                    
                                                             
    set @ot1 = (select COUNT(Other_Director_ID) from Other_Director where Authority_Id                                                              
        in                                                                                                    
        (                                                                                                     
        select director_id from director where director_Name=                                                                                                    
         ( SELECT  top 1 CEO_Name FROM  ceo                                                                                      
           where @Fdate /*@SnapShotDate*/ between CXODateArival and isnull(CXODateDeparture,'2100-01-01')                        
           and Cover_Id=@CoverID   order by CXODateArival desc                                                                                           
         )                                                                                                    
        )                             
        and Proxy_Id=                                                                   
        (select top 1 Proxy_ID from proxy_period where Proxy_FillingDt=@Fdate                                                                
        and Cover_Id=@CoverID                                                                                                    
        )                                                                                                    
          )                                                                              
    set @ot2 = (select COUNT(Other_Director_ID) from Other_Director where Authority_Id                                          
        in                                                                                                    
        (                                     
        select director_id from director where director_Name=                                                                                                    
         ( SELECT  top 1 CEO_Name FROM  ceo                                                                                                                                                                                                                   
  
    
      
        
          
            
               
               
                  
           where @Fdate /*@SnapShotDate*/  between CXODateArival and isnull(CXODateDeparture,'2100-01-01')                                  
           and Cover_Id=@CoverID   order by CXODateArival asc                                                       
         )                                                                                           
        )                                                                                                    
        and Proxy_Id=                                                                                                    
        (select top 1 Proxy_ID from proxy_period where Proxy_FillingDt=@Fdate                                                                                                    
        and Cover_Id=@CoverID                                                                
        )                                                    
          )                                                                         
   if( @ot1>=@ot2)                                                               
    set @Outcome=@ot1                                                                                                    
   else                                                                                                    
    set @Outcome=@ot2                                              
                                                                                 
                                                                                                                          
    SET @Outcome=convert(decimal(18,2),@Outcome)                                                                                          
                                                                                                            
                                                                                                                    
                                                                                                   
                                                                                                                                                                
                                                                                    
                                                                                                                                                               
   insert into AggMedianPreprocessData3_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                  select @CoverID,@sec,@SnapShotDate,@OutCome                                        
 
     
     
         
         
             
             
                 
                  
                   
                      
                               
                         
                             
                             
                                                       
                                   
                    
                                       
                                           
                                           
                                            
                  
                                                  
                                                    
                                                      
                                                        
                                                     
                                                            
                                                              
                                                                    
                                                                      
                                                              
     -----------------------------------------Section 3.3.3----------------------------------                                                                                                                         
                                                                                                                                                                   
  SET @Outcome = NULL                                                                                                                                
  SET @sec='3.3.3'                                                                                                                                                                            
                                                                                                                           
                                                                                                    
   SET @dtmax = NULL                                                     
                                                                                                                                                                          
                                                                                                                                                       
                                                                                               
     SET @Outcome = (SELECT top 1 proxyReference.Percgt75 FROM dbo.proxyReference                                                                               
     JOIN dbo.Proxy_Period ON Proxy_Period.Proxy_ID = proxyReference.Proxy_ID                                                               
                               
     WHERE proxyReference.Cover_Id=@CoverID AND                                                                       
     Proxy_Period.Proxy_FillingDt = @Fdate                                                                 
     and proxyReference.NonExecDir is not null                                  
     order by proxyrefid desc                                                                                    
     )                                                                      
                                                                                                           
                                                                                                       
                                                                                                                                                   
                                                                                           
   insert into AggMedianPreprocessData3_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                            
    select @CoverID,@sec,@SnapShotDate,@OutCome                                                                                               
                                                                                                
                                                                                                             
     -----------------------------------------Section 3.4.1----------------------------------                                                        
                  
  SET @Outcome = NULL       
  SET @sec='3.4.1'                                                                               
                                                                                                                    
                                                                                                                                                
   SET @Outcome = (SELECT FeildName from @LookUpDetails                                                                                                      
    where LookUpId                                                                                                                                                                                                                                   
   in(                                
   select top 1 DiscGovGuideStatusEffec from @EventDataGov_DiscGovGuide                                                                       
 where DiscGovGuideRefFileDate <= @SnapShotDate                                                                                                                                                                                                               
  
     
      
       
           
           
               
                
                 
                    
                      
                        
                          
                            
                              
                                 
                                  
                                   
                                       
                                       
                                          
                                             
                        
                                                
                                                  
                                                     
                                                     
                                                       
   --and SRPSCRefExpDate is not null                                                                                                              
              
                
                  
                    
                      
                        
                          
                                                        
                           
                                  
                                    
                                      
                                        
                                          
                                           
   and CoverId=@CoverID order by DiscGovGuideRefFileDate desc))                                                                                       
                                                                                                                        
        if @Outcome is null                                                                                                                                                                          
   set @Outcome ='No'                                                                                                              
                                                                                                                                                                
                                                                                   
                                                    
   insert into AggMedianPreprocessData3_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                            
    select @CoverID,@sec,@SnapShotDate,@OutCome                                                                         
                                                                                                                                          
                   
 -----------------------------------------Section 3.4.2----------------------------------               
                                                                                                                                                                                                                                
    SET @Outcome = NULL                                                                                                                                                                    
    SET   @sec='3.4.2'                                                                                                                 
     SET @Outcome = (SELECT top 1 proxyReference.NonExecDir FROM dbo.proxyReference                                                                                                                                                                            
  
    
      
       
          
             
              
               
                   
                    
                      
                        
                         
                             
                             
                                 
                                  
                             
                                     
                                         
                                          
                                           
                                               
                                               
 JOIN dbo.Proxy_Period ON Proxy_Period.Proxy_ID = proxyReference.Proxy_ID                                                                                                                                  
     WHERE proxyReference.Cover_Id=@CoverID AND                                                                                                                    
     Proxy_Period.Proxy_FillingDt = @Fdate                                               
 and proxyReference.NonExecDir is not null                                
 order by proxyrefid desc                                                                     
     )                                                                                                                             
                                                          
     if @Outcome is null                                                         
 set @Outcome='Not Reported'                             
            
                                                                                                                                 
                                                                                                                                                                                           
                                                                                                                                                                     
   insert into AggMedianPreprocessData3_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                            
    select @CoverID,@sec,@SnapShotDate,@OutCome                                                                                                         
                                                                                           
 -----------------------------------------Section 3.4.3----------------------------------                                                                             
                                                                                                                            
                                                                                         
   SET @Outcome = NULL                                                                                                                                                         
 SET @sec='3.4.3'                                                     
                                                                                                         
   SET @Outcome = (SELECT FeildName from @LookUpDetails                                                    
             
             
                 
                  
                    
                      
                        
                          
                           
                               
                                
                                 
                                     
                                      
  where LookUpId                                                                                                         
     in(                                                                                                         
     select top 1 DAWMCStatusEffec from EventDataGov_DAWMC                                                                                                               
     where DAWMCRefFileDate <= @SnapShotDate                                                                                      
     --and SRPSCRefExpDate is not null                                                                                                                           
     and CoverId=@CoverID order by DAWMCRefFileDate desc))                                                                                                                              
                                                                                                            
 if @Outcome is null                                                                                                                                                                    
  set @Outcome ='Not Disclosed'                                                   
                                                                                                                                         
                                        
                                                                                                                                                                    
  insert into AggMedianPreprocessData3_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                   
    select @CoverID,@sec,@SnapShotDate,@OutCome          
                         
 -----------------------------------------Section 3.4.4----------------------------------                                                                           
                                                            
                                                                                                                                                                            
   SET @Outcome = 0                                                                                                                                                                                       
   SET @sec='3.4.4'                                                                               
   set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                                                                                                 
 
     
      
       
          
     set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                               
                                                                                                                    
                                                                                                                                                     
     SET @Outcome = (SELECT ISNULL(COUNT([RPTIBCId]),0)                                                                                    
     FROM  dbo.EventDataGov_RPTIBC                                                                                              
     WHERE CoverId = @CoverID AND [RPTIBCRefFileDate] BETWEEN @dtmax3 AND @SnapShotDate)                                      
                                              
                          
  insert into AggMedianPreprocessData3_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                            
    select @CoverID,@sec,@SnapShotDate,@OutCome                      
                                                                                                                    
  ---------------------------------------Section 3.5.1----------------------------------                                                                                                                              
                                                                                                                                         
                                                                                                                                                                            
   SET @Outcome = 0                                                                                                                                    
   SET @sec='3.5.1'                                                                                                           
   set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                
                                                                                                  
    SET @dtmax = NULL                                                                                             
     SET @NoofRestatement =  NULL                                                                                                                                    
                                                                                                                                                                                                                
     set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                              
                           
                                                                                              
      SET @Outcome = (SELECT ISNULL(COUNT([Chair_Id]),0)                                                      
     FROM  dbo.CHAIR                                
     WHERE Cover_Id = @CoverID                                                                                                                   
     --AND [DirectorDateArival] BETWEEN @dtmax3 AND @SnapShotDate                                                                                                                                
      AND ChairDateArival <= @SnapShotDate                                                                                                                  
     and isnull(ChairDateDeparture,'2100-01-01')>=@dtmax3                                                                                 
     --and Type='chair'                                                                      
     )                                                                                              
                                                                           
                                                                                                                            
                            
                                                                                                                                                   
                                                        
   insert into AggMedianPreprocessData3_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                   
    select @CoverID,@sec,@SnapShotDate,@OutCome                                                                                      
                                                  
    ---------------------------------------Section 3.5.2----------------------------------                                                                                                                                        
                                                  
                                                                                                   
SET @Outcome = 0                                                                                                  
   SET @sec='3.5.2'                                                                                              
                                        
       exec dbo.SP_GetAudit_ALL_yoy @CoverID,@SnapShotDate--working for 3.5.2/3 and 4                                              
       Declare @Audit_YOY int                                              
       Declare @Com_YOY int                                              
       Declare @Nom_YOY int                                              
                                                     
       select top 1  @Audit_YOY=A,@Com_YOY=C,@Nom_YOY=N from Tbl_GetAudit_ALL_yoy with (nolock)                                              
                                                                                                            
      SET @Outcome = @Audit_YOY--(select dbo.F_GetAuditChair_yoy(@CoverID,@SnapShotDate))                                                                                                                                                                     
   
   
      
        
           
           
               
               
                   
                   
                       
                       
                           
                                                                      
                                                                                              
   insert into AggMedianPreprocessData3_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                            
    select @CoverID,@sec,@SnapShotDate,@OutCome                                                                                                          
                                                  
   ---------------------------------------Section 3.5.3----------------------------------                                                                                                        
                                                                                                
   SET @Outcome = NULL                                                                                                                                               
    SET @sec='3.5.3'                                                                                                                                                                              
      SET @OutCome = @Com_YOY--(select dbo.F_GetAuditCom_yoy(@CoverID,@SnapShotDate))                                                                                              
                                                                                                                                         
     insert into AggMedianPreprocessData3_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                            
    select @CoverID,@sec,@SnapShotDate,@OutCome                                          
 -----------------------------------------Section 3.5.4----------------------------------                                                                                                                                                                     
                                                                    
                                            
                                                                                                              
    SET @Outcome = NULL                                                                                           
    SET @sec='3.5.4'                                                                        
                                                                                                   
   SET @Outcome = @Nom_YOY--(select top 1  dbo.F_GetAuditNom_yoy(@CoverID,@SnapShotDate))                                                                                                                       
                                                                                                                                                                                     
                                                  
                                           
                                                                                                    
    insert into AggMedianPreprocessData3_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                              
    select @CoverID,@sec,@SnapShotDate,@OutCome                                                                                                                             
                                                                                                     
                                                                                                                                                                                
                                                                                                                               
                               
                                                                                                                                                                                                         
 set @cnt=@cnt+1                                                                                                         
                                                                                                     
                                         
 END                                                              
                                                                                                    
 END    ------END Check for scorable company    
	declare @MINDT date        
	SET @MINDT=(select convert(varchar,DATEADD(Day,365*3,MIN(Proxy_FillingDt)),101)  from Proxy_Period where Cover_Id=@coverid)                                                                                                                        
	                                                                                        
	IF((@SnapShotDate IS NOT NULL) AND (@SnapShotDate<@MINDT))              
	BEGIN                                                                                                 
		UPDATE AggMedianPreprocessData3_Daily set outcome=''           
		where cover_id=@coverid          
		and snapshotdate=@SnapShotDate          
		and GsmNo in ('3.4.4','3.5.1','3.5.2','3.5.3','3.5.4'           
		) 
    end                 
                                          
END 