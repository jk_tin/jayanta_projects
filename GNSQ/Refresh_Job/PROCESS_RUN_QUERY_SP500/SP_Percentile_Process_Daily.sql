
/****** Object:  StoredProcedure [dbo].[SP_Percentile_Process_Daily]    Script Date: 04/03/2019 12:38:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	--CREATED BY: ARINDAM MUKHOPADHYAY                      
	--CREATION DATE: 09-OCT-2017                      
	--DESCRIPTION: DATE WISE PERCENTILE CAUCLATION                      
	--------------------------------------------------------------                      
	ALTER PROCEDURE [dbo].[SP_Percentile_Process_Daily]                        
	(                        
	@DT DATE                        
	)                        
	AS                         
	                 
	BEGIN                        
	BEGIN TRY                      
	PRINT CONVERT(VARCHAR(100), GETDATE(),108)-- TO KNOW START TIME                      
	              
	BEGIN    -- DELETING AND CREATING REPLICATE                      
	DELETE FROM DATASET2_PERCENTILE_TOT WHERE DATE=@DT                        
	INSERT INTO DATASET2_PERCENTILE_TOT                        
	SELECT 
	[ISIN]
	,[DATE]
	,[1#1#1]
	,[1#1#2]
	,[1#1#3]
	,[1#1#4]
	,[1#2#1]
	,[1#2#2]
	,[1#2#3]
	,[1#2#4]
	,[1#2#5]
	,[1#2#6]
	,[1#3#1]
	,[1#3#2]
	,[1#3#3]
	,[1#3#4]
	,[1#3#5]
	,[1#3#6]
	,[1#3#7]
	,[1#3#8]
	,[1#3#9]
	,[1#3#10]
	,[1#3#11]
	,[1#3#12]
	,[1#3#13]
	,[1#3#14]
	,[1#3#15]
	,[2#1#1]
	,[2#1#2]
	,[2#1#3]
	,[2#1#4]
	,[2#2#1]
	,[2#2#2#1]
	,[2#2#2#2]
	,[2#2#2#3]
	,[2#2#2#4]
	,[2#2#2#5]
	,[2#2#3]
	,[2#3#1]
	,[2#3#2]
	,[2#4#1]
	,[2#4#2]
	,[3#1#1]
	,[3#1#2]
	,[3#2#1]
	,[3#2#2]
	,[3#2#3]
	,[3#3#1]
	,[3#3#2]
	,[3#3#3]
	,[3#4#1]
	,[3#4#2]
	,[3#4#3]
	,[3#4#4]
	,[3#5#1]
	,[3#5#2]
	,[3#5#3]
	,[3#5#4]
	,[4#1#1]
	,[4#1#2]
	,[4#1#3]
	,[4#1#4]
	,[4#1#5]
	,[4#1#6]
	,[4#1#7]
	,[4#1#8]
	,[4#2#1]
	,[4#2#2]
	,[4#3#1]
	,[4#3#2]
	,[5#1#1]
	,[5#1#2]
	,[5#2#1]
	,[5#2#2]
	,[5#2#3]
	,[5#2#4]
	,[5#3#1]
	,[5#3#2]
	,[COVERID]
	FROM DATASET1_OUTCOME_TOT  WHERE DATE=@DT                        
	END                      
	              
	BEGIN  -- SETTING PERCENTILE 0.00 FOR THE BELOW VALUE                      
	UPDATE DATASET2_PERCENTILE_TOT SET [1#1#1]='0.00' WHERE [1#1#1]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#1#2]='0.00' WHERE  [1#1#2]='UNQUALIFIED' AND DATE=@DT                        
	--1.1.3 MIN RISK                    
	UPDATE DATASET2_PERCENTILE_TOT SET [1#1#3]='0.00' WHERE CONVERT(DECIMAL(18,2),[1#1#3])                         
	<= CONVERT(DECIMAL(18,2),ISNULL((SELECT GSQ_OPTION_1 FROM                         
	PREPROCESS_MEASURE_VARIABLE_INSTANCE_DETAILS WHERE  GSM_NUM='1.1.3'),1.000))  AND DATE=@DT AND [1#1#3]!=''                      
	                 
	               
	UPDATE DATASET2_PERCENTILE_TOT SET [1#1#4]='0.00' WHERE [1#1#4]='1' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#2#1]='0.00' WHERE [1#2#1]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#2#2]='0.00' WHERE [1#2#2]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#2#3]='0.00' WHERE [1#2#3]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#2#4]='0.00' WHERE [1#2#4]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#2#5]='0.00' WHERE [1#2#5]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#2#6]='0.00' WHERE [1#2#6]='NO' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#3#1]='0.00' WHERE [1#3#1]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#3#2]='0.00' WHERE [1#3#2]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#3#3]='0.00' WHERE [1#3#3]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#3#4]='0.00' WHERE [1#3#4]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#3#5]='0.00' WHERE [1#3#5]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#3#6]='0.00' WHERE [1#3#6]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#3#7]='0.00' WHERE [1#3#7]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#3#8]='0.00' WHERE [1#3#8]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#3#9]='0.00' WHERE [1#3#9]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#3#10]='0.00' WHERE [1#3#10]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#3#11]='0.00' WHERE [1#3#11]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#3#12]='0.00' WHERE [1#3#12]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#3#13]='0.00' WHERE [1#3#13]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#3#14]='0.00' WHERE [1#3#14]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [1#3#15]='0.00' WHERE [1#3#15]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [2#1#1]='0.00' WHERE [2#1#1]='YES' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [2#1#2]='0.00' WHERE [2#1#2]='MAJORITY VOTING WITH DIRECTOR RESIGNATION POLICY' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [2#1#3]='0.00' WHERE [2#1#3]='SUPERMAJORITY IS REQUIRED; HOWEVER, IT’S A CONTROLLED COMPANY' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [2#1#4]='0.00' WHERE [2#1#4]='SUPERMAJORITY IS REQUIRED; HOWEVER, IT’S A CONTROLLED COMPANY' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [2#2#1]='0.00' WHERE [2#2#1]='NO' AND DATE=@DT                      
	UPDATE DATASET2_PERCENTILE_TOT SET [2#2#2#1]='0.00' WHERE [2#2#2#1]='NO SHAREHOLDER RIGHTS PLAN' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [2#2#2#2]='0.00' WHERE [2#2#2#2]='NO SHAREHOLDER RIGHTS PLAN' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [2#2#2#3]='0.00' WHERE [2#2#2#3]='NO SHAREHOLDER RIGHTS PLAN' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [2#2#2#4]='0.00' WHERE [2#2#2#4]='NO SHAREHOLDER RIGHTS PLAN' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [2#2#2#5]='0.00' WHERE [2#2#2#5]='NO SHAREHOLDER RIGHTS PLAN' AND DATE=@DT                     
	UPDATE DATASET2_PERCENTILE_TOT SET [2#2#3]='0.00' WHERE [2#2#3]='NO' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [2#3#2]='0.00' WHERE [2#3#2]='YES' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [2#4#1]='0.00' WHERE [2#4#1]='0' AND DATE=@DT              
	UPDATE DATASET2_PERCENTILE_TOT SET [2#4#2]='0.00' WHERE [2#4#2]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [3#1#1]='0.00' WHERE [3#1#1]='NO' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [3#3#1]='0.00' WHERE [3#3#1]='0' AND DATE=@DT                        
	--3.3.2 MIN RISK                         
	UPDATE DATASET2_PERCENTILE_TOT SET [3#3#2]='0.00' WHERE  CONVERT(DECIMAL(18,2),[3#3#2])<= CONVERT(DECIMAL(18,2),ISNULL((SELECT GSQ_OPTION_1 FROM                         
	PREPROCESS_MEASURE_VARIABLE_INSTANCE_DETAILS WHERE  GSM_NUM='3.3.2'),2.000)) AND DATE=@DT                         
	                   
	UPDATE DATASET2_PERCENTILE_TOT SET [3#3#3]='0.00' WHERE [3#3#3]='YES' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [3#4#1]='0.00' WHERE [3#4#1]='YES' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [3#4#2]='0.00' WHERE [3#4#2]='YES' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [3#4#3]='0.00' WHERE [3#4#3]='YES' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [3#4#4]='0.00' WHERE [3#4#4]='0' AND DATE=@DT                        
	                   
	--3.5.1 MIN RISK                         
	UPDATE DATASET2_PERCENTILE_TOT SET [3#5#1]='0.00' WHERE ISNUMERIC([3#5#1])=1 AND  CONVERT(DECIMAL(18,2),[3#5#1])<= CONVERT(DECIMAL(18,2),ISNULL((SELECT GSQ_OPTION_1 FROM                         
	PREPROCESS_MEASURE_VARIABLE_INSTANCE_DETAILS WHERE  GSM_NUM='3.5.1'),2.000))  AND DATE=@DT     AND [3#5#1]!=''                      
	                   
	--3.5.2 MIN RISK                         
	UPDATE DATASET2_PERCENTILE_TOT SET [3#5#2]='0.00' WHERE  ISNUMERIC([3#5#2])=1 AND CONVERT(DECIMAL(18,2),[3#5#2])<=  CONVERT(DECIMAL(18,2),ISNULL((SELECT GSQ_OPTION_1 FROM                         
	PREPROCESS_MEASURE_VARIABLE_INSTANCE_DETAILS WHERE  GSM_NUM='3.5.2'),2.000))  AND DATE=@DT    AND [3#5#2]!=''                       
	                   
	--3.5.3 MIN RISK                         
	UPDATE DATASET2_PERCENTILE_TOT SET [3#5#3]='0.00' WHERE ISNUMERIC([3#5#3])=1 AND CONVERT(DECIMAL(18,2), [3#5#3])<=                        
	CONVERT(DECIMAL(18,2),ISNULL((SELECT GSQ_OPTION_1 FROM                         
	PREPROCESS_MEASURE_VARIABLE_INSTANCE_DETAILS WHERE  GSM_NUM='3.5.3'),2.000))  AND DATE=@DT     AND [3#5#3]!=''                      
	                   
	--3.5.4 MIN RISK                         
	UPDATE DATASET2_PERCENTILE_TOT SET [3#5#4]='0.00' WHERE ISNUMERIC([3#5#4])=1 AND  CONVERT(DECIMAL(18,2),[3#5#4])<= CONVERT(DECIMAL(18,2),ISNULL((SELECT GSQ_OPTION_1 FROM           
	PREPROCESS_MEASURE_VARIABLE_INSTANCE_DETAILS WHERE  GSM_NUM='3.5.4'),2.000))  AND DATE=@DT     AND [3#5#4]!=''                      
	                   
	                   
	UPDATE DATASET2_PERCENTILE_TOT SET [4#1#1]='0.00' WHERE REPLACE([4#1#1],' ','')=REPLACE('TOP QUARTILE TSR /BOTTOM DECILE CEO COMP ',' ','') AND DATE=@DT                       
	UPDATE DATASET2_PERCENTILE_TOT SET [4#1#2]='0.00' WHERE REPLACE([4#1#2],' ','')=REPLACE('TOP QUARTILE TSR /BOTTOM DECILE TOP5NEO COMP ',' ','') AND DATE=@DT                    
	UPDATE DATASET2_PERCENTILE_TOT SET [4#1#3]='0.00' WHERE REPLACE([4#1#3],' ','')=REPLACE('TOP QUARTILE ROTC /BOTTOM DECILE CEO COMP ',' ','') AND DATE=@DT                         
	UPDATE DATASET2_PERCENTILE_TOT SET [4#1#4]='0.00' WHERE REPLACE([4#1#4],' ','')=REPLACE('TOP QUARTILE ROTC /BOTTOM DECILE TOP5NEO COMP ',' ','') AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [4#1#5]='0.00' WHERE REPLACE([4#1#5],' ','')=REPLACE('TOP QUARTILE TSR /BOTTOM DECILE CEO COMP ',' ','') AND DATE=@DT                      
	UPDATE DATASET2_PERCENTILE_TOT SET [4#1#6]='0.00' WHERE REPLACE([4#1#6],' ','')=REPLACE('TOP QUARTILE TSR /BOTTOM DECILE TOP5NEO COMP ',' ','') AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [4#1#7]='0.00' WHERE REPLACE([4#1#7],' ','')=REPLACE('TOP QUARTILE ROTC /BOTTOM DECILE CEO COMP ',' ','') AND DATE=@DT                 
	UPDATE DATASET2_PERCENTILE_TOT SET [4#1#8]='0.00' WHERE REPLACE([4#1#8],' ','')=REPLACE('TOP QUARTILE ROTC /BOTTOM DECILE TOP5NEO COMP ',' ','') AND DATE=@DT                      
	UPDATE DATASET2_PERCENTILE_TOT SET [4#2#1]='0.00' WHERE [4#2#1]='YES BINDING' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [4#2#2]='0.00' WHERE [4#2#2]='YES' AND DATE=@DT                        
	--MIN RISK                        
	UPDATE DATASET2_PERCENTILE_TOT SET [4#3#1]='0.00' WHERE ISNUMERIC([4#3#1])=1 AND  CONVERT(DECIMAL(18,2),[4#3#1])<= CONVERT(DECIMAL(18,2),ISNULL((SELECT GSQ_OPTION_1 FROM                         
	PREPROCESS_MEASURE_VARIABLE_INSTANCE_DETAILS WHERE  GSM_NUM='4.3.1'),2.000))  AND DATE=@DT                        
	--MIN RISK                        
	UPDATE DATASET2_PERCENTILE_TOT SET [4#3#2]='0.00' WHERE ISNUMERIC([4#3#2])=1 AND  CONVERT(DECIMAL(18,2),[4#3#2])<= CONVERT(DECIMAL(18,2),ISNULL((SELECT GSQ_OPTION_1 FROM                         
	PREPROCESS_MEASURE_VARIABLE_INSTANCE_DETAILS WHERE  GSM_NUM='4.3.2'),2.000))  AND DATE=@DT                        
	                   
	UPDATE DATASET2_PERCENTILE_TOT SET [5#1#1]='0.00' WHERE [5#1#1]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [5#1#2]='0.00' WHERE [5#1#2]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [5#2#1]='0.00' WHERE [5#2#1]='0' AND DATE=@DT                        
	UPDATE DATASET2_PERCENTILE_TOT SET [5#2#2]='0.00' WHERE [5#2#2]='0' AND DATE=@DT                        
	--MIN RISK                        
	UPDATE DATASET2_PERCENTILE_TOT SET [5#2#3]='0.00' WHERE ISNUMERIC([5#2#3])=1 AND CONVERT(DECIMAL(18,2),[5#2#3])BETWEEN  CONVERT(DECIMAL(18,2),ISNULL((SELECT GSQ_OPTION_1 FROM                         
	PREPROCESS_MEASURE_VARIABLE_INSTANCE_DETAILS WHERE  GSM_NUM='5.2.3'),2.000))                         
	AND '100.00' AND DATE=@DT       
	UPDATE DATASET2_PERCENTILE_TOT SET [5#2#4]='0.00' WHERE ISNUMERIC([5#2#4])=1 AND CONVERT(DECIMAL(18,2),[5#2#4])BETWEEN  CONVERT(DECIMAL(18,2),ISNULL((SELECT GSQ_OPTION_1 FROM                         
	PREPROCESS_MEASURE_VARIABLE_INSTANCE_DETAILS WHERE  GSM_NUM='5.2.4'),2.000))  AND '100.00' AND DATE=@DT                        
	END                        
	           
	DECLARE @ST INT =1                  
	DECLARE @EN INT =1                  
	           
	WHILE(@ST<=@EN)                  
	                  
	BEGIN  --76 MEASURE WISE PROCESSING                      
	                 
	EXEC UPD_111 @DT                        
	EXEC UPD_112 @DT                        
	EXEC UPD_113 @DT                        
	EXEC UPD_114 @DT                     
	EXEC UPD_121 @DT                     
	EXEC UPD_122 @DT                        
	EXEC UPD_123 @DT                        
	EXEC UPD_124 @DT                        
	EXEC UPD_125 @DT                        
	EXEC UPD_126 @DT                        
	EXEC UPD_131 @DT                        
	EXEC UPD_132 @DT                        
	EXEC UPD_133 @DT                        
	EXEC UPD_134 @DT                        
	EXEC UPD_135 @DT                        
	EXEC UPD_136 @DT                        
	EXEC UPD_137 @DT                        
	EXEC UPD_138 @DT                        
	EXEC UPD_139 @DT                        
	EXEC UPD_1310 @DT                        
	EXEC UPD_1311 @DT                        
	EXEC UPD_1312 @DT                        
	EXEC UPD_1313 @DT                        
	EXEC UPD_1314 @DT                        
	EXEC UPD_1315 @DT                        
	              
	EXEC UPD_211 @DT                        
	EXEC UPD_212 @DT                        
	EXEC UPD_213 @DT                        
	EXEC UPD_214 @DT                        
	EXEC UPD_221 @DT                        
	EXEC UPD_2221 @DT                        
	EXEC UPD_2222 @DT                        
	EXEC UPD_2223 @DT                        
	EXEC UPD_2224 @DT                        
	EXEC UPD_2225 @DT                        
	EXEC UPD_223 @DT                        
	EXEC UPD_231 @DT                        
	EXEC UPD_232 @DT                        
	EXEC UPD_241 @DT                        
	EXEC UPD_242 @DT                        
	              
	EXEC UPD_311 @DT                        
	EXEC UPD_312 @DT                        
	EXEC UPD_321 @DT                        
	EXEC UPD_322 @DT                        
	EXEC UPD_323 @DT                        
	EXEC UPD_331 @DT                        
	EXEC UPD_332 @DT                        
	EXEC UPD_333 @DT                        
	EXEC UPD_341 @DT                        
	EXEC UPD_342 @DT                        
	EXEC UPD_343 @DT                        
	EXEC UPD_344 @DT                        
	EXEC UPD_351 @DT                        
	EXEC UPD_352 @DT                        
	EXEC UPD_353 @DT                        
	EXEC UPD_354 @DT                        
	             
	EXEC UPD_411 @DT                        
	EXEC UPD_412 @DT                        
	EXEC UPD_413 @DT                        
	EXEC UPD_414 @DT                        
	EXEC UPD_415 @DT                        
	EXEC UPD_416 @DT                        
	EXEC UPD_417 @DT                        
	EXEC UPD_418 @DT                        
	EXEC UPD_421 @DT                        
	EXEC UPD_422 @DT                        
	EXEC UPD_431 @DT                        
	EXEC UPD_432 @DT                        
	              
	EXEC UPD_511 @DT                        
	EXEC UPD_512 @DT                        
	EXEC UPD_521 @DT                        
	EXEC UPD_522 @DT                        
	EXEC UPD_523 @DT                        
	EXEC UPD_524 @DT                        
	EXEC UPD_531 @DT                        
	EXEC UPD_532 @DT                    
	              
	SET @ST=@ST+1                  
	                  
	END                        
	              
	PRINT CONVERT(VARCHAR(100), GETDATE(),108)-- TO KNOW END TIME                      
	                  
	END TRY                        
	              
	BEGIN CATCH                      
	SELECT     
	ERROR_NUMBER() AS ERRORNUMBER                        
	,ERROR_MESSAGE() AS ERRORMESSAGE;                       
	END CATCH                       
	                
	END 