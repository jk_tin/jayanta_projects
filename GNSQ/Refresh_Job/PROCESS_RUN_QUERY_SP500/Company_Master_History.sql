

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Company_Master_History](
	[History_Id] [int] NOT NULL,
	[Company_id] [int] NOT NULL,
	[Company_Name] [nvarchar](500) NULL,
	[Ticker_Symbol] [nvarchar](500) NULL,
	[GicsSector] [bigint] NULL,
	[GicsIndustryGroup] [bigint] NULL,
	[IndexSP] [bigint] NULL,
	[IndexRusell] [bigint] NULL,
	[Country] [bigint] NULL,
	[Exchange] [bigint] NULL,
	[Active] [bit] NULL,
	[Edited_by] [int] NULL,
	[Edited_on] [datetime] NULL,
	[Action] [char](1) NULL,
	[SicNo] [nvarchar](50) NULL,
	[CusipNo] [nvarchar](50) NULL,
	[Currency] [bigint] NULL,
	[SecondaryCountry] [bigint] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CIK] [float] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


