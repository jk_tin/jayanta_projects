
GO
/****** Object:  StoredProcedure [dbo].[SP_GENERATEPEERLIST_HISTORY]    Script Date: 03/29/2019 12:08:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GENERATEPEERLIST_HISTORY]                                                   
(                                                    
@SNAPDATE DATE                                                    
)                                                    
AS                                                    
BEGIN                                                    
       
 DELETE FROM  TBL_PEERLIST WHERE [SNAPSHOTDATE]=@SNAPDATE  
                                                                                                 
 DECLARE @TABCOMPANY TABLE([INDX] INT IDENTITY(1,1),[COMPANY_ID] INT,[COVER_ID] UNIQUEIDENTIFIER)                                                                   
 DECLARE @STR VARCHAR(500)                                                            
                 
 IF OBJECT_ID('TEMPDB..#TABALLCOMPANY' , 'U') IS NOT NULL                                                                                   
 DROP TABLE #TABALLCOMPANY                                                               
                 
 CREATE TABLE #TABALLCOMPANY 
 ([COMPANY_ID] INT,[HISTORY_ID] INT,[INDEXSP] INT,[INDEXRUSELL] INT,[GICSSECTOR] INT,                                                   
  [GICSINDUSTRYGROUP] INT,[ACTIVE] INT)                                                  
                 
  
 INSERT INTO #TABALLCOMPANY([COMPANY_ID],[HISTORY_ID],[INDEXSP],[INDEXRUSELL],[GICSSECTOR],
                            [GICSINDUSTRYGROUP],[ACTIVE])                                                                                          
 SELECT A.[COMPANY_ID],[HISTORY_ID],[INDEXSP],[INDEXRUSELL],[GICSSECTOR], [GICSINDUSTRYGROUP],[ACTIVE] 
 FROM (SELECT ACTIVE,HISTORY_ID,STARTDATE,COMPANY_ID,INDEXSP,INDEXRUSELL,GICSSECTOR,GICSINDUSTRYGROUP                                                                     
              FROM DBO.COMPANY_MASTER_HISTORY 
              WHERE COMPANY_ID IN (SELECT COMPANY_ID 
                                   FROM DBO.COVER 
                                   WHERE COVER_ID IN (SELECT DISTINCT COVER_ID FROM DBO.COVER)) 
                AND CONVERT(DATE,@SNAPDATE) BETWEEN CONVERT(DATE,STARTDATE) AND ISNULL((CONVERT(DATE,ENDDATE)),GETDATE()))A                                                                 
                 
                 
 -- SET @CONDITION=(SELECT TOP 1 SINGLE_UNIVERSE FROM PREPROCESS_TBLREFSINGLEUNIVERSE)                                                                       
 SET @STR='SELECT [COMPANY_ID],(SELECT COVER_ID FROM DBO.COVER WHERE COMPANY_ID=A.COMPANY_ID)                                                                                      
           FROM #TABALLCOMPANY A  
           WHERE 1=1 AND INDEXSP IN (18) AND ACTIVE=1 
           GROUP BY COMPANY_ID'
             
 INSERT INTO @TABCOMPANY([COMPANY_ID],[COVER_ID])                                                                    
 EXEC(@STR)                                                        
                                    
                                    
 DECLARE @GROUPID INT                                        
 DECLARE @ST INT =1                                                    
 DECLARE @EN INT=(SELECT COUNT(*) FROM  @TABCOMPANY)                                                    
 DECLARE @CV UNIQUEIDENTIFIER                                                    
 
 WHILE(@ST<=@EN)                                                    
   BEGIN                                                    
   SET @CV=(SELECT TOP 1 COVER_ID FROM @TABCOMPANY WHERE INDX=@ST)                                                      
    
   IF EXISTS(SELECT COVER_ID 
             FROM  TBL_PEERLIST 
             WHERE [SNAPSHOTDATE]=@SNAPDATE AND COVER_ID=@CV)  
	   BEGIN  
	   PRINT 'NOT TO DO'  
	   END  
   ELSE  
	   BEGIN  
	   
	   SET @GROUPID= ISNULL((SELECT MAX(PEERGROUP_ID)+1 FROM TBL_PEERLIST),1)   
	   
	   PRINT @GROUPID  
	   INSERT INTO TBL_PEERLIST  
	   SELECT @GROUPID,[SNAPSHOTDATE],[COVER_ID] 
	   FROM [FUNGETPEERGROUP_History](@CV,CONVERT(VARCHAR(100), @SNAPDATE) +'^')  
	   END  
  
                                                        
  SET @ST=@ST+1                                                    
 END                                             
                                             
                                    
                                                    
END