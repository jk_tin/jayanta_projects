
/****** Object:  StoredProcedure [dbo].[UPD_114]    Script Date: 04/03/2019 12:41:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[UPD_114]    
( @snapshotdate date    
)    
AS BEGIN    
    
-- @snapshotdate='15-aug-2017'    
    
declare @n int =((select count(distinct isin) from DATASET2_PERCENTILE_TOT where     
Date=@SnapShotDate )-1)     
    
declare @tbbelow table(ii int identity(1,1), val decimal(18,2))    
    
declare @b decimal(18,2)    
declare @vl decimal(18,2)    
declare @perc decimal(18,2)    
    
insert into @tbbelow    
select distinct [1#1#4] from DATASET1_OUTCOME_TOT where date=@SnapShotDate    
and [1#1#4]='0'   
    
    
declare @st int =1    
declare @en int =(select COUNT(ii) from @tbbelow)    
while(@st<=@en)    
begin    
  set @vl=(select val from @tbbelow where ii=@st)    
    
  set @b=(select count(distinct isin) from DATASET1_OUTCOME_TOT where     
  Date=@SnapShotDate and   
  [1#1#4]IN('1')  
   )     
    
  set @perc=(@b/@n)*100    
    
  --print @vl      
  --print @b    
  --print @n    
  --print @perc    
      
  UPDATE DATASET2_PERCENTILE_TOT SET [1#1#4]=@perc where date=@SnapShotDate AND ISIN IN    
  (SELECT DISTINCT ISIN  FROM DATASET1_OUTCOME_TOT where     
  Date=@SnapShotDate and CONVERT(DECIMAL(18,2),[1#1#4])=CONVERT(DECIMAL(18,2),@vl))    
    
  set @st=@st+1    
    
  END  
end  