GO
/****** Object:  StoredProcedure [dbo].[SP_DAILYFEED_SINGLEDATE_PART_HISTORY]    Script Date: 03/29/2019 11:27:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SP_DAILYFEED_SINGLEDATE_PART_HISTORY]                                                                                                            
(                                                                                                            
@SNAPDATE DATE                                                                                                            
)                                                                                                            
AS                                                                                          


                  
BEGIN  
                                                                                                            
 BEGIN TRY  
                                                                                                        
  DELETE FROM PRECALCULATEDPPA_DAILY WHERE SNAPSHOTDATE=@SNAPDATE                                                                                                                                                    
  DELETE FROM AGGMEDIANPREPROCESSDATA1_DAILY WHERE SNAPSHOTDATE=@SNAPDATE                                                                                                                  
  DELETE FROM AGGMEDIANPREPROCESSDATA2_DAILY WHERE SNAPSHOTDATE=@SNAPDATE                                                                                                                  
  DELETE FROM AGGMEDIANPREPROCESSDATA3_DAILY WHERE SNAPSHOTDATE=@SNAPDATE                                                                                                                 
  DELETE FROM AGGMEDIANPREPROCESSDATA4_DAILY  WHERE SNAPSHOTDATE=@SNAPDATE                                                                                                                 
  DELETE FROM AGGMEDIANPREPROCESSDATA5_DAILY  WHERE SNAPSHOTDATE=@SNAPDATE                         
  
  DELETE FROM AGGMEDIANPREPROCESSDATA1_DAILY_SECOND WHERE SNAPSHOTDATE=@SNAPDATE                                                                                                                  
  DELETE FROM AGGMEDIANPREPROCESSDATA2_DAILY_SECOND WHERE SNAPSHOTDATE=@SNAPDATE                                                                                                                  
  DELETE FROM AGGMEDIANPREPROCESSDATA3_DAILY_SECOND WHERE SNAPSHOTDATE=@SNAPDATE                                                                                                                 
  DELETE FROM AGGMEDIANPREPROCESSDATA4_DAILY_SECOND  WHERE SNAPSHOTDATE=@SNAPDATE                                                                                                                 
  DELETE FROM AGGMEDIANPREPROCESSDATA5_DAILY_SECOND  WHERE SNAPSHOTDATE=@SNAPDATE                         
  
  DELETE FROM AGGMEDIANPREPROCESSDATA1_DAILY_THIRD WHERE SNAPSHOTDATE=@SNAPDATE                                                                                                                  
  DELETE FROM AGGMEDIANPREPROCESSDATA2_DAILY_THIRD WHERE SNAPSHOTDATE=@SNAPDATE                                                                                                                  
  DELETE FROM AGGMEDIANPREPROCESSDATA3_DAILY_THIRD WHERE SNAPSHOTDATE=@SNAPDATE                                                                                                                 
  DELETE FROM AGGMEDIANPREPROCESSDATA4_DAILY_THIRD  WHERE SNAPSHOTDATE=@SNAPDATE                                                                                                                 
  DELETE FROM AGGMEDIANPREPROCESSDATA5_DAILY_THIRD  WHERE SNAPSHOTDATE=@SNAPDATE                         
  
  --TRUNCATE TABLE DATASET1_OUTCOME_TOT                                                                                                       
  --TRUNCATE TABLE DATASET2_PERCENTILE_TOT                                                                                        
  
  DECLARE @TABCOMPANY TABLE([INDX] INT IDENTITY(1,1),[COMPANY_ID] INT,[COVER_ID] UNIQUEIDENTIFIER)                                  
  DECLARE @STR VARCHAR(500)                      
  
  IF OBJECT_ID('TEMPDB..#TABALLCOMPANY' , 'U') IS NOT NULL                                                                                                                                           
  DROP TABLE #TABALLCOMPANY                                                                                                                       
                                          
  CREATE TABLE #TABALLCOMPANY ([COMPANY_ID] INT,[HISTORY_ID] INT,[INDEXSP] INT,[INDEXRUSELL] INT,[GICSSECTOR] INT,                                                                                                           
                               [GICSINDUSTRYGROUP] INT,[ACTIVE] INT)                                                                                                          
                  
  
  INSERT INTO #TABALLCOMPANY([COMPANY_ID],[HISTORY_ID],[INDEXSP],[INDEXRUSELL],[GICSSECTOR],[GICSINDUSTRYGROUP],[ACTIVE])
                                                                                                                                                    
  SELECT A.[COMPANY_ID],[HISTORY_ID],[INDEXSP],[INDEXRUSELL],[GICSSECTOR],[GICSINDUSTRYGROUP],[ACTIVE] 
  FROM ((SELECT ACTIVE,HISTORY_ID,STARTDATE,COMPANY_ID,INDEXSP,INDEXRUSELL,GICSSECTOR,GICSINDUSTRYGROUP                                                                     
              FROM DBO.COMPANY_MASTER_HISTORY 
              WHERE COMPANY_ID IN (SELECT COMPANY_ID 
                                   FROM DBO.COVER 
                                   WHERE COVER_ID IN (SELECT DISTINCT COVER_ID FROM DBO.COVER)) 
                AND CONVERT(DATE,@SNAPDATE) BETWEEN CONVERT(DATE,STARTDATE) AND ISNULL((CONVERT(DATE,ENDDATE)),GETDATE()))A                                        
  
  INNER JOIN (SELECT  CM.COMPANY_ID,MIN(PROXY_FILLINGDT) AS MNPRX 
              FROM DBO.PROXY_PERIOD PR 
              JOIN COVER C 
              ON C.COVER_ID=PR.COVER_ID                                        
              JOIN COMPANY_MASTER CM 
              ON CM.COMPANY_ID=C.COMPANY_ID                                                                                                                               
  GROUP BY CM.COMPANY_ID 
  HAVING MIN(PROXY_FILLINGDT)<=CONVERT(DATE,@SNAPDATE)) CS 
  ON  CS.COMPANY_ID=A.COMPANY_ID)           -- ** SNAPSHOT                                         
                                                                                                                            
  
  -- SET @CONDITION=(SELECT TOP 1 SINGLE_UNIVERSE FROM PREPROCESS_TBLREFSINGLEUNIVERSE)                                                                                                                     
  SET @STR='SELECT [COMPANY_ID],(SELECT COVER_ID FROM DBO.COVER WHERE COMPANY_ID=A.COMPANY_ID)                                                             
            FROM #TABALLCOMPANY A  
            WHERE 1=1 AND INDEXSP IN (18) AND ACTIVE=1  GROUP BY COMPANY_ID'  -- INDEXSP 1000 ACTIVE 1     
                                                                                                                                         
  INSERT INTO @TABCOMPANY([COMPANY_ID],[COVER_ID])                                                                                                                            
  EXEC(@STR)                                                                                                                
                                                                                       
                                  
  DECLARE @ST INT =1                                                                                                            
  DECLARE @EN INT=(SELECT COUNT(1) FROM  @TABCOMPANY)                                                                                                            
  DECLARE @CV UNIQUEIDENTIFIER                         
  DECLARE @CV1 UNIQUEIDENTIFIER                       
  DECLARE @CV2 UNIQUEIDENTIFIER   
                                                                                                           
  WHILE(@ST<=@EN)                                                                                           
	  BEGIN                                                                                                            
		  SET @CV=(SELECT TOP 1 COVER_ID FROM @TABCOMPANY WHERE INDX=@ST)                                                                                                            
		  EXEC [USPCOMPANYPPA_DAILY_SINGLEDATE_HISTORY] @CV,@SNAPDATE                                                                                                                      
		  SET @ST=@ST+1                                                                                                            
	  END                                                                                                     
                                  
  EXEC SP_GENERATEPEERLIST_HISTORY @SNAPDATE                                                            
                                           
  SET @ST  =1                                                                                                            
                                        
  WHILE(@ST<=@EN)                                                                                                      
  BEGIN                                      
	  SET @CV=(SELECT TOP 1 COVER_ID FROM @TABCOMPANY WHERE INDX=@ST)                          
	  SET @CV1=(SELECT TOP 1 COVER_ID FROM @TABCOMPANY WHERE INDX=@ST+1)                        
	  SET @CV2=(SELECT TOP 1 COVER_ID FROM @TABCOMPANY WHERE INDX=@ST+2)                        
	  
	  EXEC [USP_SSIS_SCOREAGGMEDIAN1_DAILY_SINGLEDAY_FIRST] @CV,@SNAPDATE                                                                                                            
	  EXEC [USP_SSIS_SCOREAGGMEDIAN2_DAILY_SINGLEDAY_FIRST] @CV,@SNAPDATE                                                          
	  EXEC [USP_SSIS_SCOREAGGMEDIAN3_DAILY_SINGLEDAY_FIRST] @CV,@SNAPDATE                                                
	  EXEC [USP_SSIS_SCOREAGGMEDIAN4_DAILY_SINGLEDAY_FIRST] @CV,@SNAPDATE                              
	  EXEC [USP_SSIS_SCOREAGGMEDIAN5_DAILY_SINGLEDAY_FIRST] @CV,@SNAPDATE                            
	  
	  EXEC [USP_SSIS_SCOREAGGMEDIAN1_DAILY_SINGLEDAY_SECOND] @CV1,@SNAPDATE                                                                                                            
	  EXEC [USP_SSIS_SCOREAGGMEDIAN2_DAILY_SINGLEDAY_SECOND] @CV1,@SNAPDATE                                                          
	  EXEC [USP_SSIS_SCOREAGGMEDIAN3_DAILY_SINGLEDAY_SECOND] @CV1,@SNAPDATE                                                
	  EXEC [USP_SSIS_SCOREAGGMEDIAN4_DAILY_SINGLEDAY_SECOND] @CV1,@SNAPDATE                                                                             
	  EXEC [USP_SSIS_SCOREAGGMEDIAN5_DAILY_SINGLEDAY_SECOND] @CV1,@SNAPDATE                         
	  
	  
	  EXEC [USP_SSIS_SCOREAGGMEDIAN1_DAILY_SINGLEDAY_THIRD] @CV2,@SNAPDATE                                            
	  EXEC [USP_SSIS_SCOREAGGMEDIAN2_DAILY_SINGLEDAY_THIRD] @CV2,@SNAPDATE                                                          
	  EXEC [USP_SSIS_SCOREAGGMEDIAN3_DAILY_SINGLEDAY_THIRD] @CV2,@SNAPDATE                         
	  EXEC [USP_SSIS_SCOREAGGMEDIAN4_DAILY_SINGLEDAY_THIRD] @CV2,@SNAPDATE                                                                                                                                                                   
	  EXEC [USP_SSIS_SCOREAGGMEDIAN5_DAILY_SINGLEDAY_THIRD] @CV2,@SNAPDATE                                                                                                                                                                 
	  
	  
	  SET @ST=@ST+3                                                                                                            
  END                                                                                                          
  
  BEGIN                                                                                                                                                                                                                     
  EXEC [SP_PIVOTSCORE_SINGLEDAY] @SNAPDATE                                                                                                                                                                             
  END                       
  
  
  BEGIN                                   
  EXEC SP_PERCENTILE_PROCESS_DAILY @SNAPDATE                                                       
  END                                                     
  
  BEGIN                                                          
  EXEC USP_PROCESS_DATASET3_DAILY @SNAPDATE                                                        
  END                                                     
  
  --BEGIN                                                    
  --EXEC SP_MERGEINTO_HISTORICAL @SNAPDATE                                                      
  --END                                                         
          
    END TRY  
 BEGIN CATCH  
  -- EXECUTE THE ERROR RETRIEVAL ROUTINE.
  IF(ERROR_NUMBER() IS NOT NULL)
  BEGIN  
	  INSERT INTO DAILY_ERROR_LOG  
	  SELECT   
	  ERROR_NUMBER() AS ERRORNUMBER,  
	  ERROR_SEVERITY() AS ERRORSEVERITY,  
	  ERROR_STATE() AS ERRORSTATE,  
	  ERROR_PROCEDURE() AS ERRORPROCEDURE,  
	  ERROR_LINE() AS ERRORLINE,  
	  ERROR_MESSAGE() AS ERRORMESSAGE; 
  END 
 END CATCH  
                        
END