
GO
/****** Object:  StoredProcedure [dbo].[uspCompanyPPA_daily_SingleDate_History]    Script Date: 03/29/2019 11:29:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                                                      
ALTER PROCEDURE [dbo].[uspCompanyPPA_daily_SingleDate_History]                                                                                                                                          
 @COVERID UNIQUEIDENTIFIER,                            
 @SNAPDATE DATE                                                                                                                                                                           
AS                                                                                                                               
                                                                                                                                                   
BEGIN TRY         
        
 SET NOCOUNT ON;           
        
                                                                                                                                                                                 
 DELETE FROM PRECALCULATEDPPA_DAILY WHERE COVER_ID=@COVERID AND SNAPSHOTDATE=@SNAPDATE                                                                                                                        
        
 DECLARE @TABNEOCOMP TABLE([INDX] INT IDENTITY(1,1) NOT NULL,[NEOCOMP] DECIMAL(18,3))                                                                                                            
 DECLARE @TICKER_SYMBOL VARCHAR(6)                                                                                                         
 DECLARE @TOTALYEAR INT                                                                                                                                   
        
 SET @TICKER_SYMBOL=(SELECT TOP 1 TICKER_SYMBOL 
                     FROM Company_Master_History CM     
                     JOIN COVER CV 
                     ON CV.COMPANY_ID=CM.COMPANY_ID    
                     WHERE COVER_ID=@COVERID)                                                                                                          
                                                                                                    
 ----------------------------------------------------------DB TABLE----------------------------                                                                                                                    
 ----ANNUAL DATA TABLE                                                                                                        
 DECLARE @ANUAL_DATA TABLE ( [ANUAL_DATA_ID] UNIQUEIDENTIFIER  NULL,                                                                                                        
							 [COVER_ID] UNIQUEIDENTIFIER  NULL,                                                                                                                       
							 [DATE_CENTRAL_ID] INT NULL,                                                             
							 [FILLINGDT] DATE NULL ,                                                                             
							 [PARTIALYRFACTOR] DECIMAL(18,3) NULL,                                                                                                                                
							 [NET_INCOME] DECIMAL(18,3) NULL,                                                                                                        
							 [SHORT_TERM_DEBT] DECIMAL(18,3) NULL,                                                                                                        
							 [CURR_LONG_TERM_DEBT] DECIMAL(18,3) NULL,                                                                                                  
							 [END_LONG_TERM_DEBT] DECIMAL(18,3) NULL,                                                          
							 [TOTAL_SHAREHOLDERS_EQUITY] DECIMAL(18,3) NULL )    
							 
							                                                                                                     
        
 INSERT INTO @ANUAL_DATA([ANUAL_DATA_ID],[COVER_ID],[DATE_CENTRAL_ID],[FILLINGDT],[PARTIALYRFACTOR],[NET_INCOME],[SHORT_TERM_DEBT],                                      
                         [CURR_LONG_TERM_DEBT],[END_LONG_TERM_DEBT],[TOTAL_SHAREHOLDERS_EQUITY])                                                    
 SELECT [ANUAL_DATA_ID],[COVER_ID],[DATE_CENTRAL_ID],[FILLINGDT],[PARTIALYRFACTOR],[NET_INCOME],[SHORT_TERM_DEBT],[CURR_LONG_TERM_DEBT],
        [END_LONG_TERM_DEBT],[TOTAL_SHAREHOLDERS_EQUITY] 
 FROM ANUAL_DATA                                                                                                        
 WHERE COVER_ID=@COVERID                                                                          
        
        
 ----PROXY_PERIOD TABLE                                                                                                        
 DECLARE @PROXY_PERIOD TABLE ([FISCAL_ID] INT NULL,                                                                      
							  [COVER_ID] UNIQUEIDENTIFIER  NULL,                                                                                                                                     
							  [PROXY_FILLINGDT] DATE NULL)  
							                                                                                                        
        
        
 INSERT INTO @PROXY_PERIOD([FISCAL_ID],[COVER_ID],[PROXY_FILLINGDT])                                                                                                        
 SELECT [FISCAL_ID],[COVER_ID],[PROXY_FILLINGDT] 
 FROM PROXY_PERIOD                                                                                                        
 WHERE COVER_ID =@COVERID                                                                                                        
        
        
 ----DIV_HISTORY TABLE                                                                                                        
 DECLARE @DIV_HISTORY TABLE ([DATE] DATE  NULL,                                                                                                                                                                                                                    
                             [ADJ_CLOSE] DECIMAL(18,3) NULL )  
                             
                             
INSERT INTO @DIV_HISTORY([DATE],[ADJ_CLOSE])                                                                      
SELECT * FROM(SELECT TOP 1 [DATE],ADJ_CLOSE 
              FROM DIV_HISTORY_FINAL 
              WHERE  [DATE]<=@SNAPDATE AND Cover_ID=@COVERID
              ORDER BY [DATE] DESC    
              
              UNION    
			  SELECT TOP 1 [DATE],ADJ_CLOSE  
			  FROM DIV_HISTORY_FINAL 
			  WHERE  [DATE]<=DATEADD(DAY,-365,@SNAPDATE) AND Cover_ID=@COVERID
			  ORDER BY [DATE] DESC    

              UNION    
              SELECT TOP 1 [DATE],ADJ_CLOSE  
              FROM DIV_HISTORY_FINAL 
              WHERE  [DATE]<=DATEADD(DAY,-(365*3),@SNAPDATE) AND Cover_ID=@COVERID
              ORDER BY [DATE] DESC) AS R 
WHERE R.DATE>=(SELECT MIN(ISNULL([PROXY_FILLINGDT],'01-01-2000')) FROM @PROXY_PERIOD)    
AND ISNUMERIC(ADJ_CLOSE)=1                        
AND CONVERT(DECIMAL(18,6), ADJ_CLOSE)>0.00     
                
        
        
 ----FISCAL_PERIOD TABLE                                                                                                        
DECLARE @FISCAL_PERIOD TABLE ([FISCAL_ID] INT NULL,                                                                                                
							  [COVER_ID] UNIQUEIDENTIFIER  NULL,                               
							  [FISCAL_ENDDT] DATE NULL,                                                                                                        
							  [FILLINGDT] DATE NULL)   
							                                                             
        
INSERT INTO @FISCAL_PERIOD([FISCAL_ID],[COVER_ID],[FISCAL_ENDDT],[FILLINGDT])                                                           
SELECT [FISCAL_ID],[COVER_ID],[FISCAL_ENDDT],[FILLINGDT] 
FROM FISCAL_PERIOD                                                                                          
WHERE COVER_ID =@COVERID                                                                                                        
    
        
        
 ----NEO COMPENSATION TABLE                                                 
 DECLARE @NEOCOMPENSATION TABLE ([DATE_CENTRAL_ID] INT NULL,                                                                                                                                         
								 [COVER_ID] UNIQUEIDENTIFIER  NULL,                                                                                                         
								 [TOTCOMPENSATION] DECIMAL(18,3) NULL,        
								 [ADJCOMPENSATION] DECIMAL(18,3) NULL,         
								 [TENUREWEIGHTING] DECIMAL(18,3) NULL,        
								 [CXOTYPE] NVARCHAR(3) NULL )  
								                                                                  
        
 INSERT INTO @NEOCOMPENSATION([DATE_CENTRAL_ID],[COVER_ID],[TOTCOMPENSATION],[ADJCOMPENSATION],[TENUREWEIGHTING],[CXOTYPE])                                                                                                        
 SELECT [DATE_CENTRAL_ID],[COVER_ID],[TOTCOMPENSATION],[ADJCOMPENSATION],[TENUREWEIGHTING],'CEO' 
 FROM CEO_DETAILS                                                                                                          
 WHERE COVER_ID =@COVERID                                             
        
 INSERT INTO @NEOCOMPENSATION([DATE_CENTRAL_ID],[COVER_ID],[TOTCOMPENSATION],[ADJCOMPENSATION],[TENUREWEIGHTING],[CXOTYPE])                                                                         
 SELECT [DATE_CENTRAL_ID],[COVER_ID],[TOTCOMPENSATION],0,0,'' 
 FROM CFO_DETAILS                                                                                                          
 WHERE COVER_ID =@COVERID                                                                                                       
        
 INSERT INTO @NEOCOMPENSATION([DATE_CENTRAL_ID],[COVER_ID],[TOTCOMPENSATION],[ADJCOMPENSATION],[TENUREWEIGHTING],[CXOTYPE])                                                                                                      
 SELECT [DATE_CENTRAL_ID],[COVER_ID],[TOTCOMPENSATION],0,0,'' 
 FROM NEO_DETAILS                                                                                                      
 WHERE COVER_ID =@COVERID                                                                                                        
                                                                                                  
-------------------------END-----------------------------------                                                                                                      
                                                                                        
 DECLARE @ROTC1 DECIMAL(18,3)=0                                                                                   
 DECLARE @ROTC2 DECIMAL(18,3)=0                                                                                                                     
 DECLARE @ROTC3 DECIMAL(18,3)=0                                                
 DECLARE @TSR1 DECIMAL(18,3)=NULL    
 DECLARE @TSR3 DECIMAL(18,3)=NULL                                                               
 DECLARE @CEOCOMP DECIMAL(18,3)                                                                                                
 DECLARE @CEOCOMP2YR DECIMAL(18,3)                                                                                                                 
 DECLARE @CEOTENURE DECIMAL(18,3)                                                                                                                      
 DECLARE @NEOCOMP DECIMAL(18,3)                                                                                                                   
 DECLARE @NEOANNUAL DECIMAL(18,3)                     
 DECLARE @NEOCOMP2 DECIMAL(18,3)                                                                            
                                                                                                                                                                                                      
 --ROTC                                                                                                           
 DECLARE @NET_INCOME DECIMAL(18,3)=0                                          
 DECLARE @PERTIAL_FACTOR DECIMAL(18,3)=0                                                                                                                    
 DECLARE @SHAREHOLDER_EQUITY DECIMAL(18,3)=0                                                                                                                   
 DECLARE @END_DEBT DECIMAL(18,3)=0                                                                                                                    
 DECLARE @FISCAL_PERIOD_CAPITAL DECIMAL(18,3)=0                                                                                                                    
 --CEO TENURE                                                                                                                    
 DECLARE @PARTIALYRFACTOR DECIMAL(18,3)=0                                                                 
 --TSR                                                                                                                    
 DECLARE @SHARE_PRICE DECIMAL(18,3)=0                                                                                                                    
 DECLARE @PREV_SHARE_PRICE DECIMAL(18,3)=0                                             
        
 --NEO                                                                                                                    
                                                                        
 DECLARE @ANUAL_DATA_ID1 UNIQUEIDENTIFIER=NULL                                                             
 DECLARE @ANUAL_DATA_ID2 UNIQUEIDENTIFIER=NULL                                                             
 DECLARE @ANUAL_DATA_ID3 UNIQUEIDENTIFIER=NULL                                                             
        
 DECLARE @FISCAL_ENDDT1 DATE=NULL                                                                                                                        
 DECLARE @FISCAL_ENDDT2 DATE=NULL                                                      
 DECLARE @FISCAL_ENDDT3 DATE=NULL                                                            
        
 DECLARE @FILLINGDT1 DATE=NULL                                                                                                                    
 DECLARE @FILLINGDT2 DATE=NULL                                                                      
 DECLARE @FILLINGDT3 DATE=NULL                                                              
        
 DECLARE @FISCAL1 INT=NULL                                                                                 
 DECLARE @FISCAL2 INT=NULL                                                                                           
 DECLARE @FISCAL3 INT=NULL                   
        
 DECLARE @PROXYFILLINGDT1 DATE                                                                                    
 DECLARE @PROXYFILLINGDT2 DATE                                                          
 DECLARE @PROXYFILLINGDT3 DATE                                                 
        
 DECLARE @PROXYFISCAL1 INT                                                                                        
 DECLARE @PROXYFISCAL2 INT                                                        
 DECLARE @PROXYFISCAL3 INT                                                           
                                                                                                                                          
   BEGIN                                                                                                                 
                                                                                                                                                                                               
                                                                                                                                                            
                                                              
 SET @FISCAL_ENDDT1=(SELECT MAX(FILLINGDT) FROM @FISCAL_PERIOD WHERE COVER_ID=@COVERID AND FILLINGDT<=@SNAPDATE)                                                            
 SET @FISCAL_ENDDT2=(SELECT MAX(FILLINGDT) FROM @FISCAL_PERIOD WHERE COVER_ID=@COVERID AND FILLINGDT<@FISCAL_ENDDT1)                                                            
 SET @FISCAL_ENDDT3=(SELECT MAX(FILLINGDT) FROM @FISCAL_PERIOD WHERE COVER_ID=@COVERID AND FILLINGDT<@FISCAL_ENDDT2)    
                                                                                               
 SET @FISCAL1= (SELECT TOP 1 FISCAL_ID FROM @FISCAL_PERIOD WHERE FILLINGDT=@FISCAL_ENDDT1 AND COVER_ID=@COVERID)                                                             
 SET @FISCAL2= (SELECT TOP 1 FISCAL_ID FROM @FISCAL_PERIOD WHERE FILLINGDT=@FISCAL_ENDDT2 AND COVER_ID=@COVERID)                                                            
 SET @FISCAL3= (SELECT TOP 1 FISCAL_ID FROM @FISCAL_PERIOD WHERE FILLINGDT=@FISCAL_ENDDT3 AND COVER_ID=@COVERID)        
        
 SET @PROXYFILLINGDT1=(SELECT MAX(PROXY_FILLINGDT) FROM @PROXY_PERIOD WHERE PROXY_FILLINGDT<=@SNAPDATE)                           
 SET @PROXYFILLINGDT2=(SELECT MAX(PROXY_FILLINGDT) FROM @PROXY_PERIOD WHERE PROXY_FILLINGDT<@PROXYFILLINGDT1)                          
 SET @PROXYFILLINGDT3=(SELECT MAX(PROXY_FILLINGDT) FROM @PROXY_PERIOD WHERE PROXY_FILLINGDT<@PROXYFILLINGDT2)                           
        
 SET @PROXYFISCAL1= (SELECT TOP 1 FISCAL_ID FROM @PROXY_PERIOD WHERE PROXY_FILLINGDT=@PROXYFILLINGDT1)                                                                                           
 SET @PROXYFISCAL2= (SELECT TOP 1 FISCAL_ID FROM @PROXY_PERIOD WHERE PROXY_FILLINGDT=@PROXYFILLINGDT2)                           
 SET @PROXYFISCAL3= (SELECT TOP 1 FISCAL_ID FROM @PROXY_PERIOD WHERE PROXY_FILLINGDT=@PROXYFILLINGDT3)          
         
                                                              
                                                             
  IF (SELECT COUNT(1) FROM @ANUAL_DATA WHERE COVER_ID=@COVERID AND DATE_CENTRAL_ID=@FISCAL1)=1                                                            
	  
	  BEGIN                                                            
		   SET @ANUAL_DATA_ID1= (SELECT TOP 1 ANUAL_DATA_ID 
		                         FROM @ANUAL_DATA  
		                         WHERE COVER_ID=@COVERID AND DATE_CENTRAL_ID=@FISCAL1)                                                              
	  END
	                                                               
  ELSE                                                            
	  BEGIN                                                            
	     SET @FILLINGDT1=(SELECT MAX(FILLINGDT) 
	                      FROM @ANUAL_DATA 
	                      WHERE COVER_ID=@COVERID AND FILLINGDT<=@SNAPDATE AND DATE_CENTRAL_ID=@FISCAL1)                                                             
	        
	     SET @ANUAL_DATA_ID1=(SELECT TOP 1 ANUAL_DATA_ID 
	                          FROM @ANUAL_DATA 
	                          WHERE FILLINGDT=@FILLINGDT1 AND COVER_ID=@COVERID AND DATE_CENTRAL_ID=@FISCAL1)                                                            
	  END  
	                                                              
  ----------------
  --------2YR---------------------                                    
  IF (SELECT COUNT(1) FROM @ANUAL_DATA WHERE COVER_ID=@COVERID AND DATE_CENTRAL_ID=@FISCAL2)=1                                                            
	  
	  BEGIN                                                         
		   SET @ANUAL_DATA_ID2= (SELECT TOP 1 ANUAL_DATA_ID 
								 FROM @ANUAL_DATA  
								 WHERE COVER_ID=@COVERID AND DATE_CENTRAL_ID=@FISCAL2)                                        
	  END  
	                                                             
  ELSE               
	  BEGIN                                                            
		   SET @FILLINGDT2=(SELECT MAX(FILLINGDT) 
							FROM @ANUAL_DATA 
							WHERE COVER_ID=@COVERID AND FILLINGDT<=@SNAPDATE AND DATE_CENTRAL_ID=@FISCAL2)                                                             
		        
		   SET @ANUAL_DATA_ID2=(SELECT TOP 1 ANUAL_DATA_ID 
								FROM @ANUAL_DATA 
								WHERE FILLINGDT=@FILLINGDT2 AND COVER_ID=@COVERID AND DATE_CENTRAL_ID=@FISCAL2)                                                   
	  END                 
	                                                
  -------------
  -----------3RD---------------------                                                            
  IF (SELECT COUNT(1) FROM @ANUAL_DATA WHERE COVER_ID=@COVERID AND DATE_CENTRAL_ID=@FISCAL3)=1   
                                                           
	  BEGIN                                        
		   SET @ANUAL_DATA_ID3= (SELECT TOP 1 ANUAL_DATA_ID 
		                         FROM @ANUAL_DATA  
		                         WHERE COVER_ID=@COVERID AND DATE_CENTRAL_ID=@FISCAL3)                                                              
	  END                                                             
  ELSE                                                            
	  BEGIN                                                            
	       SET @FILLINGDT3=(SELECT MAX(FILLINGDT) 
	                        FROM @ANUAL_DATA 
	                        WHERE COVER_ID=@COVERID AND  FILLINGDT<=@SNAPDATE  AND DATE_CENTRAL_ID=@FISCAL3)                                                             
	        
		   SET @ANUAL_DATA_ID3=(SELECT TOP 1 ANUAL_DATA_ID 
		                        FROM @ANUAL_DATA 
		                        WHERE FILLINGDT=@FILLINGDT3 AND COVER_ID=@COVERID AND DATE_CENTRAL_ID=@FISCAL3)                                                            
	  END  
	                                                           
	       SET @TOTALYEAR=(SELECT COUNT(1) 
	                       FROM @FISCAL_PERIOD 
	                       WHERE COVER_ID=@COVERID AND FILLINGDT<=@SNAPDATE)                                                             
	  -----TSR1                                                                                                                                                                                                                                         
	       SET @SHARE_PRICE=(SELECT TOP 1 ADJ_CLOSE  
	                         FROM @DIV_HISTORY 
	                         WHERE  [DATE]<=@SNAPDATE 
	                         ORDER BY [DATE] DESC)                                                                                                     
	        
	       SET @PREV_SHARE_PRICE=(SELECT TOP 1 ADJ_CLOSE  
	                              FROM @DIV_HISTORY 
	                              WHERE [DATE]<=DATEADD(DAY,-365,@SNAPDATE) 
	                              ORDER BY [DATE] DESC)                                                                                                                                
	        
   IF  @PREV_SHARE_PRICE<>0  AND @PREV_SHARE_PRICE IS NOT NULL         
                                                                        
	       SET @TSR1=POWER((@SHARE_PRICE/@PREV_SHARE_PRICE),1)-1                                                                                                                                 

   ELSE  IF  @PREV_SHARE_PRICE=0           
	
	       SET @TSR1=0                                             
   ELSE                                 

	       SET @TSR1=NULL                                                                                          

  -----END OF TSR                                                                                                 
                    
  -----TSR3      
                                                                                         
   IF @TOTALYEAR>3                                                                                          
	 BEGIN 
	                                                                         
	      SET @PREV_SHARE_PRICE=0                                                                             
	      SET @PREV_SHARE_PRICE=(SELECT TOP 1 ADJ_CLOSE  
	                             FROM @DIV_HISTORY 
	                             WHERE [DATE]<=DATEADD(DAY,-(365*3),@SNAPDATE) 
	                             ORDER BY [DATE] DESC)                                  
        
                 
   IF @PREV_SHARE_PRICE<>0  AND @PREV_SHARE_PRICE IS NOT NULL                                                                                                     
  -- SET @TSR3 = POWER((1+(ISNULL(@1ST_PART,0) / @PREV_SHARE_PRICE)),1/3)-1                                                                                           
         SET @TSR3=POWER((@SHARE_PRICE/@PREV_SHARE_PRICE),(1.0/3))-1                                                                                                                               
  
   ELSE IF  @PREV_SHARE_PRICE=0                                                                                                                
         
         SET @TSR3=0                                    
   ELSE                                                                                                                   
         
         SET @TSR3=NULL                                                                                                                   
   END  
                                                                                                                              
  -----END OF TSR                                                                                                                           
          
  --ROTC1                                                                                                                                                         
        
  SELECT  @NET_INCOME = ISNULL(NET_INCOME,0),                                                                                                                
  @PERTIAL_FACTOR = ISNULL(PARTIALYRFACTOR,0),                                             
  @SHAREHOLDER_EQUITY = ISNULL(TOTAL_SHAREHOLDERS_EQUITY,0) ,                                                                            
  @END_DEBT = ISNULL(SHORT_TERM_DEBT,0)+ ISNULL(CURR_LONG_TERM_DEBT,0) + ISNULL(END_LONG_TERM_DEBT,0)                                                              
  FROM @ANUAL_DATA WHERE  ANUAL_DATA_ID=@ANUAL_DATA_ID1                                                                                                                              
        
  SET @FISCAL_PERIOD_CAPITAL = ISNULL((@END_DEBT + @SHAREHOLDER_EQUITY),0);                                                       
                          
  --PRINT @FISCAL_PERIOD_CAPITAL                                                                                                                                    
  IF  @FISCAL1 IS NOT NULL                                                 
  BEGIN                                                                         
   IF ISNULL(@FISCAL_PERIOD_CAPITAL,0) = 0                                         
   SET   @ROTC1 = @ROTC1+0;                                                                       
  ELSE                                                                                         
   SET @ROTC1 = ISNULL(((@NET_INCOME * @PERTIAL_FACTOR) / @FISCAL_PERIOD_CAPITAL)*100 ,0);                                    
  END                                                                                    
  ELSE                                                                                    
   SET @ROTC1 =NULL                                                                                                                         
  ----END OF ROTC                                                                                                                            
  --ROTC2                                                                       
                                                                                                               
  SELECT  @NET_INCOME = ISNULL(NET_INCOME,0),                                                                                                       
  @PERTIAL_FACTOR = ISNULL(PARTIALYRFACTOR,0),                                 
  @SHAREHOLDER_EQUITY = ISNULL(TOTAL_SHAREHOLDERS_EQUITY,0),                                                                                                                                         
  @END_DEBT = ISNULL(SHORT_TERM_DEBT,0)+ ISNULL(CURR_LONG_TERM_DEBT,0) + ISNULL(END_LONG_TERM_DEBT,0)                                                                                                            
  FROM @ANUAL_DATA WHERE ANUAL_DATA_ID=@ANUAL_DATA_ID2                                                                                                                   
                                           
  SET @FISCAL_PERIOD_CAPITAL = ISNULL((@END_DEBT + @SHAREHOLDER_EQUITY),0);                               
        
  IF ISNULL(@FISCAL_PERIOD_CAPITAL,0) = 0                                   
   SET   @ROTC2 = @ROTC2+0;                                                                                                
  ELSE                                                                                                                                        
   SET @ROTC2 = ISNULL(((@NET_INCOME * @PERTIAL_FACTOR) / @FISCAL_PERIOD_CAPITAL)*100 ,0);                                  
  ---------------                                            
        
  SELECT  @NET_INCOME = ISNULL(NET_INCOME,0),                                                                           
  @PERTIAL_FACTOR = ISNULL(PARTIALYRFACTOR,0),                                                                                                                
  @SHAREHOLDER_EQUITY = ISNULL(TOTAL_SHAREHOLDERS_EQUITY,0),                                                                                                                                         
  @END_DEBT = ISNULL(SHORT_TERM_DEBT,0)+ ISNULL(CURR_LONG_TERM_DEBT,0) + ISNULL(END_LONG_TERM_DEBT,0)                                                                                                                  
  FROM @ANUAL_DATA  WHERE ANUAL_DATA_ID=@ANUAL_DATA_ID3                                                                                                                                                   
        
  SET @FISCAL_PERIOD_CAPITAL = ISNULL((@END_DEBT + @SHAREHOLDER_EQUITY),0);                                                                                                                                      
        
                           
  IF ISNULL(@FISCAL_PERIOD_CAPITAL,0) = 0                                                                                                                                
   SET   @ROTC3 = @ROTC3+0;                                                                                       
  ELSE                                                                         
   SET @ROTC3 = ISNULL(((@NET_INCOME * @PERTIAL_FACTOR) / @FISCAL_PERIOD_CAPITAL)*100 ,0);                                                                                                                      
        
  --  IF  (@FISCAL2 IS NOT NULL) AND (@FISCAL3 IS NOT NULL)  --OCT 2017   --**                              
  IF  (@PROXYFISCAL2 IS NOT NULL) AND (@PROXYFISCAL3 IS NOT NULL)                                                                                                                    
  BEGIN                                                                                                                             
   SET @ROTC3 = (ISNULL(@ROTC1,0) + ISNULL(@ROTC2,0) + ISNULL(@ROTC3,0)) /3;                                                                                                           
  END               
  ELSE                                                                   
   SET @ROTC3 =NULL                                                                                                                           
  --END OF ROTC3                                                                           
        
  -------------CEO TENURE                      
  SET @PARTIALYRFACTOR=0                                                                                                       
  SET @CEOTENURE=0                                                                                                                       
  SET @PARTIALYRFACTOR=(SELECT TOP 1 ISNULL(PARTIALYRFACTOR,0) FROM @ANUAL_DATA WHERE DATE_CENTRAL_ID=@PROXYFISCAL1 )                                        
  SELECT @CEOTENURE=SUM(ISNULL(ADJCOMPENSATION,0)* ISNULL(TENUREWEIGHTING,0) * ISNULL(@PARTIALYRFACTOR,0)) FROM @NEOCOMPENSATION  WHERE CXOTYPE='CEO' AND DATE_CENTRAL_ID=@PROXYFISCAL1                                        
        
  IF  @PROXYFISCAL1 IS  NULL                                                                                      
   SET @CEOTENURE=NULL                                                                                
                                                                                                                 
  SET @PARTIALYRFACTOR=0                                                             
  SET @CEOCOMP2YR=0                                                                                                              
  SET @PARTIALYRFACTOR=(SELECT TOP 1 ISNULL(PARTIALYRFACTOR,0) FROM @ANUAL_DATA WHERE DATE_CENTRAL_ID=@PROXYFISCAL2)                                                                       
  SELECT @CEOCOMP2YR=SUM(ISNULL(ADJCOMPENSATION,0)* ISNULL(TENUREWEIGHTING,0) * ISNULL(@PARTIALYRFACTOR,0)) FROM @NEOCOMPENSATION  WHERE CXOTYPE='CEO'  AND DATE_CENTRAL_ID=@PROXYFISCAL2                                 
  SET @PARTIALYRFACTOR=0                                                                   
  SET @CEOCOMP=0                                                                                                         
  SET @PARTIALYRFACTOR=(SELECT TOP 1 ISNULL(PARTIALYRFACTOR,0) FROM @ANUAL_DATA WHERE DATE_CENTRAL_ID=@PROXYFISCAL3)                                                                                                                             
  SELECT @CEOCOMP=SUM(ISNULL(ADJCOMPENSATION,0)* ISNULL(TENUREWEIGHTING,0) * ISNULL(@PARTIALYRFACTOR,0)) FROM @NEOCOMPENSATION  WHERE CXOTYPE='CEO'  AND DATE_CENTRAL_ID=@PROXYFISCAL3                                       
        
  --AS DISCUSS WITH SUBHANKAR COMMENTING COMMENT CODE                                                                                        
  --IF @CEOCOMP<>0 AND @CEOCOMP2YR<>0 AND @CEOTENURE<>0                                                                                                      
  --SET @CEOCOMP= (@CEOTENURE+@CEOCOMP2YR+@CEOCOMP)/3                                                                                         
   
  SET @CEOCOMP=ISNULL(@CEOCOMP,0)                                                                                   
        
  IF  (@PROXYFISCAL2 IS NOT NULL) AND (@PROXYFISCAL3 IS NOT NULL)                                                                                                                         
  BEGIN                                                                                        
   IF @CEOCOMP<>0 OR @CEOCOMP2YR<>0 OR @CEOTENURE<>0                                                                                                   
   SET @CEOCOMP= (ISNULL(@CEOTENURE,0)+ISNULL(@CEOCOMP2YR,0)+ISNULL(@CEOCOMP,0))/3                                                                                     
  END                                                                                    
  ELSE                                                                                    
   SET @CEOCOMP=0--NULL--*****                                                                                   
                                                                                                   
        
  -------------CEO COMP                                                                                                                            
                  
  ---NEO ANNUAL -- 1 YR                                                                              
  INSERT INTO @TABNEOCOMP([NEOCOMP])                                                                                     
  SELECT ISNULL(TOTCOMPENSATION,0) FROM @NEOCOMPENSATION WHERE DATE_CENTRAL_ID =@PROXYFISCAL1                                                                
  SET @NEOANNUAL=0                                                                 
  SET @NEOANNUAL=(SELECT SUM([NEOCOMP]) FROM (SELECT TOP 5 [NEOCOMP] FROM @TABNEOCOMP ORDER BY [NEOCOMP] DESC) AS R)                                                                                           
        
  IF @PROXYFISCAL1 IS NULL                 
   SET @NEOANNUAL=NULL                                                                                    
   DELETE FROM @TABNEOCOMP                                                               
                                                                                                             
        
  INSERT INTO @TABNEOCOMP([NEOCOMP])                                                                                                                                
  SELECT ISNULL(TOTCOMPENSATION,0) FROM @NEOCOMPENSATION WHERE DATE_CENTRAL_ID =@PROXYFISCAL2                                                                                                                        
  SET @NEOCOMP2=0                                                                              
  SET @NEOCOMP2=(SELECT SUM([NEOCOMP]) FROM (SELECT TOP 5 [NEOCOMP] FROM @TABNEOCOMP ORDER BY [NEOCOMP] DESC) AS R)                                                                                                                           
        
  DELETE FROM @TABNEOCOMP                                                                               
                   
  --3RD                                                                                                                            
  --2ND YR                                                                                                                            
  INSERT INTO @TABNEOCOMP([NEOCOMP])                                                                                                          
  SELECT ISNULL(TOTCOMPENSATION,0) FROM @NEOCOMPENSATION WHERE DATE_CENTRAL_ID =@PROXYFISCAL3                                                                                                                              
  SET @NEOCOMP=0                                                                                                       
  SET @NEOCOMP=(SELECT SUM([NEOCOMP]) FROM (SELECT TOP 5 [NEOCOMP] FROM @TABNEOCOMP ORDER BY [NEOCOMP] DESC) AS R)                                                                                              
                                                                                     
        
        
  IF  (@PROXYFISCAL2 IS NOT NULL) AND (@PROXYFISCAL3 IS NOT NULL)                                                                                                                         
  BEGIN                                                                                       
   IF @NEOCOMP<>0 OR @NEOCOMP2<>0 OR  @NEOANNUAL<>0                                                           
   SET @NEOCOMP=(ISNULL(@NEOCOMP,0)+ISNULL(@NEOCOMP2,0)+ISNULL(@NEOANNUAL,0))/3                                                                    
  END                                            
  ELSE                                                                                    
   SET @NEOCOMP=NULL                                                                                    
                                                                                                                         
          
   INSERT INTO PRECALCULATEDPPA_DAILY([COVER_ID],[SNAPSHOTDATE],[ROTC1],[ROTC3],[TSR1],[TSR3],[CEOCOMP],[CEOTENURE],[NEOCOMP],[NEOANNUAL])                                                                                    
   SELECT @COVERID, @SNAPDATE,@ROTC1,@ROTC3,@TSR1,@TSR3,@CEOCOMP,@CEOTENURE,@NEOCOMP,@NEOANNUAL           
                                                                                            
                                                                                                                
 END                                                                                                            
                                                                                                               
                                                                                                            
END TRY                                                                                                                                   
                                                                                       
 BEGIN CATCH                                                                                                                              
                                                                                               
  -- RAISE AN ERROR WITH THE DETAILS OF THE EXCEPTION                                                                                                                       
  DECLARE @ERRMSG NVARCHAR(4000), @ERRSEVERITY INT                                                                                                                           
  SELECT @ERRMSG = ERROR_MESSAGE(),                                                                                                                                        
         @ERRSEVERITY = ERROR_SEVERITY()                                                                                                                                        
                                                                        
  RAISERROR(@ERRMSG, @ERRSEVERITY, 1)                                                                                                              
END CATCH 