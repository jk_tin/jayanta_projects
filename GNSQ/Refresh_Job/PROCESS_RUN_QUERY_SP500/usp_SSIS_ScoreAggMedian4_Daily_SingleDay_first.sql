
/****** Object:  StoredProcedure [dbo].[usp_SSIS_ScoreAggMedian4_Daily_SingleDay_first]    Script Date: 04/03/2019 10:13:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                                                                                                                                                                                    
-- =============================================                                                                                                                                                                                                               
  
    
      
        
                           
-- Author:  Arindam Mukhopadhyay                                                                                                                                                                                                                               
  
    
      
        
          
-- Create date: 23/05/2013                                                                                                                                                                                                                                     
  
    
      
        
         
-- Description: Get Score outcome for section 4                                                                                               
                                                                                                                                                                                                                                           
-- =============================================                                                                                                                                                                                                        
ALTER Procedure [dbo].[usp_SSIS_ScoreAggMedian4_Daily_SingleDay_first]                                                                                                                                                                                    
  
          
          
        
          
                                    
(                                                                                                                                                                                                                                                      
 @CoverID varchar(100),              
 @snapdate date                                                                                                                                                                                                                                              
)                                                                                                                                                                                                                                                      
          
AS                                                                                                                                                                                                                                                       
BEGIN              
                                
 set nocount on;                                                                                                                                                                                                                                               
  
    
      
        
                                                                       
    delete from AggMedianPreprocessData4_Daily where  snapshotdate=@snapdate AND Cover_id=@CoverID                          
                                                        
     ----------------------------------------------From Date Checking-------------------                                                                                                                                                                       
  
    
      
        
                              
                                                    
  BEGIN                                                         
                              
  ---------------------------------------From Date Checking----------------------                                            
                                                                                                        
  declare @Quarter_Period TABLE(                                               
  [Indx] INT IDENTITY(1,1) NOT NULL,                                                                                                                                                      
  [Quarter_FillingDt]  date                                                                                        
  )                                                                                                                                            
  ------------------********************-----------------                             
          
  delete from @Quarter_Period                                                                       
  insert into @Quarter_Period                                          
  values(@snapdate)                                                                                                                         
                                                                                                  
  ---end-----                                                                                                                                                                                   
                                                                                                                                                                                                                                                               
 
     
     
        
                    
 DECLARE @dtmax3 DATETIME = NULL                                                                                                                                                             
 DECLARE @NoofRestatement   DECIMAL(18,3)= 0.000           
 DECLARE @res79 varchar(200)                                                                                                                                                                                                                                   
  
    
      
                         
 DECLARE @res80 varchar(200)          
 declare @var decimal(10,2)                                                                                                                                             
 DECLARE @Outcome NVARCHAR(500)=NULL                                                              
 DECLARE @OutcomeDetails NVARCHAR(700)=NULL                                                                                     
 DECLARE @sec VARCHAR(100) = NULL                                                                               
 DECLARE @totalSum DECIMAL(18,3)                                                                                                                                                      
 ---Pair company table                                
 DECLARE @TabPeerComp TABLE([Cover_id] uniqueidentifier NOT NULL)                                                                                                                                                     
                                                                                             
  CREATE TABLE #PreCalculatedPPA                                                                                                           
  ( [Indx] INT identity(1,1) NOT NULL,                                                               
  [SnapShotDate] DATE NULL,                                                                                                                                                                                     
  [Cover_id] uniqueidentifier NOT NULL,                                                                            
  [ROTC1] decimal(18,3) NULL,          
  [ROTC3] decimal(18,3) NULL,                                      
  [CEO1] decimal(18,3) NULL,                                                                                                                                  
  [CEO3] decimal(18,3) NULL,                                                             
  [NEO1] decimal(18,3) NULL,                                                                                                                                                                                                  
  [NEO3] decimal(18,3) NULL,                                                                                   
  [TSR1] decimal(18,3) NULL,                                                                                                 
  [TSR3] decimal(18,3) NULL                                                                                                
  )                                                                         
                                                                                                                                      
  cREATE CLUSTERED  INDEX IDX_C_PreCalculatedPPACover_id ON #PreCalculatedPPA(Cover_id)                                                                                                                                                   
                                                                                                                                                                                                                                     
 ----GSM_MASTER Table                                                                                                                         
 DECLARE @GSM_MASTER TABLE (                                                                                                           
 [GSM_NUM] varchar(10)  PRIMARY KEY NOT  NULL,                                                                                        
 [DeriveMeasureVariable] decimal(18,3)                                                                                                                                           
 )                                                                                                                                                 
        
 insert into @GSM_MASTER([GSM_NUM],[DeriveMeasureVariable])                                                                                                                                      
 SELECT                                                                
 [GSM_NUM],                                                            
 (case                                                           
 when (OTH_VARIABLE_1>OTH_VARIABLE_2 and OTH_VARIABLE_1>OTH_VARIABLE_3) then OTH_VARIABLE_1                                                           
 when  (OTH_VARIABLE_2>OTH_VARIABLE_1 and OTH_VARIABLE_2>OTH_VARIABLE_3) then OTH_VARIABLE_2                                                                                             
 when  (OTH_VARIABLE_3>OTH_VARIABLE_1 and OTH_VARIABLE_3>OTH_VARIABLE_2) then OTH_VARIABLE_3                                                                   
 end) DeriveMeasureVariable                                                                                            
 FROM preprocess_Measure_Variable_Instance_Details            
                                                                 
  DECLARE @SnapShotDate DATE                                                                         
  DECLARE @ROTC1 DECIMAL(18,3)                                                                                                              
  DECLARE   @ROTC3 DECIMAL(18,3)                            
  DECLARE   @CEO1 DECIMAL(18,3)                                                     
  DECLARE   @CEO3 DECIMAL(18,3)                  
  DECLARE   @NEO1 DECIMAL(18,3)                                                        
  DECLARE  @NEO3 DECIMAL(18,3)                                                                                                                                     
  DECLARE  @TSR1 DECIMAL(18,3)                                                                                                                                                  
  DECLARE  @TSR3 DECIMAL(18,3)                                        
                                                                                                                                                   
  --median                                                                                                                                                   
  DECLARE @rotc1Median DECIMAL(18,3)                                                                                                                                            
  DECLARE @rotc3Median DECIMAL(18,3)                                                                                                                                                                    
  DECLARE @tsr3Median DECIMAL(18,3)                                                                                                                                                                                                        
  DECLARE @tsr1Median DECIMAL(18,3)                                                                                                                                                                                  
  DECLARE @CeoCompMedian DECIMAL(18,3)                                                                                                                                                                                       
  DECLARE @CeoTenureMedian DECIMAL(18,3)                                                                                                                                           
  DECLARE @NeoCompMedian DECIMAL(18,3)                                                                                                                                  
  DECLARE @NeoAnnualMedian DECIMAL(18,3)                                              
                                                                                                                                                                     
  --median                                                                                                                                                  
  DECLARE @rotc1per25 DECIMAL(18,3)                                                                
  DECLARE @rotc3per25 DECIMAL(18,3)                                                                                                          
  DECLARE @tsr3per25 DECIMAL(18,3)                                                                                                                                                               
  DECLARE @tsr1per25 DECIMAL(18,3)                                                                                                                                      
                                                                                                                                                
  DECLARE @rotc1per75 DECIMAL(18,3)                                                                                        
  DECLARE @rotc3per75 DECIMAL(18,3)                   
  DECLARE @tsr3per75 DECIMAL(18,3)                                     
  DECLARE @tsr1per75 DECIMAL(18,3)                                                                                                                               
                                                              
  DECLARE @CeoCompper10 DECIMAL(18,3)                                                                                                                 
  DECLARE @CeoTenureper10 DECIMAL(18,3)                                                         
  DECLARE @NeoCompper10 DECIMAL(18,3)                                                          
  DECLARE @NeoAnnualper10 DECIMAL(18,3)                                                                                                   
                                                                                        
  DECLARE @CeoCompper90 DECIMAL(18,3)                               
  DECLARE @CeoTenureper90 DECIMAL(18,3)                                        
  DECLARE @NeoCompper90 DECIMAL(18,3)                                                                                                                                      
  DECLARE @NeoAnnualper90 DECIMAL(18,3)                                                                                                                                                      
                                                                                                                                 
  DECLARE @Percentile25 DECIMAL(18,3)                                                                                                                                                             
  set  @Percentile25= '.25'                                                                                                              
  DECLARE @Percentile75 DECIMAL(18,3)                                                                                                                                                          
  set  @Percentile75= '.75'                                                                        
                                                                                                                                                
  DECLARE @Percentile10 DECIMAL(18,3)                                                                                                                                  
  set  @Percentile10= '.10'                                                                      
                                                                                                                        
  DECLARE @Percentile90 DECIMAL(18,3)                                                 
  set  @Percentile90= '.90'                                                                                                                         
                                                                                                        
                                                                                                                                                      
  --PPA OutCome                                                               
  DECLARE @rotc1PPA varchar(100)                                                                                 
  DECLARE @rotc3PPA varchar(100)                                                                                                                                                                                    
  DECLARE @tsr3PPA varchar(100)                                                                                                                                                                                       
  DECLARE @tsr1PPA varchar(100)                                                                             
  DECLARE @CeoCompPPA varchar(100)                                                                                                             
  DECLARE @CeoTenurePPA varchar(100)                           
  DECLARE @NeoCompPPA varchar(100)                                                               
  DECLARE @NeoAnnualPPA varchar(100)                                                                                                                                 
                                             
  --PPA OutCome Details                                                                    
  DECLARE @rotc1PPA1 varchar(100)                               
  DECLARE @rotc3PPA1 varchar(100)                                                                                                    
  DECLARE @tsr3PPA1 varchar(100)                                                                                                                   
  DECLARE @tsr1PPA1 varchar(100)                                                                             
  DECLARE @CeoCompPPA1 varchar(100)                                                                                                                              
  DECLARE @CeoTenurePPA1 varchar(100)                                                                                                                      
  DECLARE @NeoCompPPA1 varchar(100)                                                                                                                                                   
  DECLARE @NeoAnnualPPA1 varchar(100)                                                                                                                            
                                                                                                                               
  DECLARE @TempVal TABLE(                                                                                                        
  [Val] DECIMAL(18,3)                                                                                                                         
  )                                                                                                           
                                                                                                                           
                                      
 --------Start Calculation                                                                                            
          
 declare @count int                                                                                                                                       
 declare @cnt int                                                        
 set @count=(select COUNT([Indx]) from @Quarter_Period)                                                                                                                                       
 set @cnt=1                                                                                                                      
                    
 WHILE @cnt<= @count                                                                                                                                                                                
 BEGIN                                                                                              
 set @SnapShotDate=(select Quarter_FillingDt from @Quarter_Period where [Indx]=@cnt)                                                                                                     
 delete from @TabPeerComp            
                                                                                                
 TRUNCATE TABLE #PreCalculatedPPA                                                                                      
 INSERT INTO   @TabPeerComp([Cover_id])                                                                                                                      
 select Cover_id from Tbl_Peerlist where PeerGroup_id in(          
 SELECT  top 1 PeerGroup_id FROM   Tbl_Peerlist          
 where SnapShotdate=@SnapShotDate          
 and Cover_id=@coverId)          
                                                                                          
          
 insert into #PreCalculatedPPA([Cover_id],[SnapShotDate],[ROTC1],[ROTC3],[CEO1],[CEO3],[NEO1],[NEO3],[TSR1],[TSR3])                                                          
    
      
        
          
 select PPA.[Cover_id],ppa.[SnapShotDate],[ROTC1],[ROTC3],[CeoTenure],[CeoComp],[NeoAnnual],[NeoComp],[TSR1],[TSR3]                                    
 from PreCalculatedPPA_Daily    ppa  with (nolock) inner join                                                          
 @TabPeerComp PEER ON PEER.COVER_ID=ppa.COVER_ID  inner join                                  
 (select MAX(SnapShotDate) SnapShotDate,Cover_id from PreCalculatedPPA_Daily  where SnapShotDate<=@SnapShotDate                                                          
 group by Cover_id) b  on b.Cover_id=peer.Cover_id and b.SnapShotDate=ppa.SnapShotDate                                                                                                
          
 ----------------------                                                                                                                        
 ----performance                                                                                                         
 ---rotc1                                                                                                                                                                                
 insert into @TempVal([val])                                                                                                                                                
 select   [rotc1] from  #PreCalculatedPPA where [rotc1] is not null --[rotc1]<>0                                                                
 ---rotc1 25                                                                                                            
 set @rotc1per25=0                                                                                                                              
 SET @rotc1per25 =                                                                                 
 ( SELECT dbo.LERP(max(d), 0.00, 1.00, max(a.[val]), max(b.[val])) AS percentile                                                                                                            
 FROM                                                                                                                                                                                              
 ( SELECT floor(kf) AS k, kf-floor(kf) AS d                                         
 FROM ( SELECT 1 + @Percentile25 *(count(*)-1) AS kf FROM @TempVal ) as x1                                                    
 ) as x2                                                                                                                                                           
 JOIN @TempVal a                                                                                                                                
 ON  ( SELECT COUNT(*) FROM @TempVal aa WHERE aa.[val] < a.[val]) < k                                                                                       
 JOIN @TempVal b                                                                                                                            
 ON  ( SELECT COUNT(*) FROM @TempVal bb                                                                                                                  
 WHERE  bb.[val] < b.[val]) < k+1                                                                                                                                                                       
 )                                                                                                                                               
          
 ---rotc1 75                                                           
 set @rotc1per75=0                                                                                                                                                                                     
 SET @rotc1per75 =                              
 ( SELECT dbo.LERP(max(d), 0.00, 1.00, max(a.[val]), max(b.[val])) AS percentile                             
       
          
          
          
          
          
 FROM                                                                                                                                        
 ( SELECT floor(kf) AS k, kf-floor(kf) AS d                                                        
 FROM ( SELECT 1 + @Percentile75 *(count(*)-1) AS kf FROM @TempVal ) as x1                                                                                    
 ) as x2                                                                                                              
 JOIN @TempVal a                                                        
 ON  ( SELECT COUNT(*) FROM @TempVal aa WHERE aa.[val] < a.[val]) < k                                                                                                                                 
 JOIN @TempVal b                                                                                                                                    
 ON  ( SELECT COUNT(*) FROM @TempVal bb                                                                                                                                                                                            
 WHERE  bb.[val] < b.[val]) < k+1                                                                                                                                    
 )                                                            
 delete from  @TempVal                                                                                                                                       
 --rotc3                                                                                                                                      
 insert into @TempVal([val])                                                                                                                                                                
 select   [rotc3] from  #PreCalculatedPPA where [rotc3] is not null --<>0                                       
 ---rotc3 25                                  
 set @rotc3per25=0                                                                                                                             
 SET @rotc3per25 =                                                                                                                      
 ( SELECT dbo.LERP(max(d), 0.00, 1.00, max(a.[val]), max(b.[val])) AS percentile                                                                                                                                      
 FROM                                                                                  
 ( SELECT floor(kf) AS k, kf-floor(kf) AS d                                                       
 FROM ( SELECT 1 + @Percentile25 *(count(*)-1) AS kf FROM @TempVal ) as x1                                                 
 ) as x2                                                                                                                              
 JOIN @TempVal a                                                                                                                            
 ON  ( SELECT COUNT(*) FROM @TempVal aa WHERE aa.[val] < a.[val]) < k                                                                                 
 JOIN @TempVal b                               
 ON  ( SELECT COUNT(*) FROM @TempVal bb                                                       
 WHERE  bb.[val] < b.[val]) < k+1                                                                                                                                                                                                           
 )                                                   
 --print '25'                                                       
 --print @rotc3per25-------test 25                                                         
 ---rotc3 75                                                                       
 set @rotc3per75=0              
 SET @rotc3per75 =                                                                                 
 ( SELECT dbo.LERP(max(d), 0.00, 1.00, max(a.[val]), max(b.[val])) AS percentile                                                   
       
          
          
 FROM                                                                                                                                                               
 ( SELECT floor(kf) AS k, kf-floor(kf) AS d                                                                                                         
 FROM ( SELECT 1 + @Percentile75 *(count(*)-1) AS kf FROM @TempVal ) as x1                                                                    
 ) as x2                                                                                                                                                                                                                  
 JOIN @TempVal a                                                                                                                                                       
 ON  ( SELECT COUNT(*) FROM @TempVal aa WHERE aa.[val] < a.[val]) < k                                                                           
 JOIN @TempVal b                                                                                                                           
 ON  ( SELECT COUNT(*) FROM @TempVal bb                                                                                                                                                                                                                  
 WHERE  bb.[val] < b.[val]) < k+1                                                                                                                                                            
 )                                                       
 --print '75'                                                       
 --print @rotc3per75-------test 75                                                         
 delete from  @TempVal                                                                                                 
 --tsr1                                                      
 insert into @TempVal([val])                                                                              
 select   [tsr1] from  #PreCalculatedPPA where [tsr1] is not null --<>0                                                                                                                                              
 --tsr1 25                                                   
 set @tsr1per25=0                                                                                                                            
 SET @tsr1per25 =                                              
 ( SELECT dbo.LERP(max(d), 0.00, 1.00, max(a.[val]), max(b.[val])) AS percentile                                                                                                                             
 FROM                                               
 ( SELECT floor(kf) AS k, kf-floor(kf) AS d                                                                                                 
 FROM ( SELECT 1 + @Percentile25 *(count(*)-1) AS kf FROM @TempVal ) as x1                                                                                                                                                
 ) as x2                                                                                                                                                   
 JOIN @TempVal a                                                                                
 ON  ( SELECT COUNT(*) FROM @TempVal aa WHERE aa.[val] < a.[val]) < k                                                                       
 JOIN @TempVal b                                                                                      
 ON  ( SELECT COUNT(*) FROM @TempVal bb                                                                                                               
 WHERE  bb.[val] < b.[val]) < k+1                                                       
 )                                                                                                                                                  
 --tsr1 75                                                                                              
 set @tsr1per75=0                                                  
 SET @tsr1per75 =                                                                                                                                                                                                                   
 ( SELECT dbo.LERP(max(d), 0.00, 1.00, max(a.[val]), max(b.[val])) AS percentile                                                                                                                                                  
 FROM                                                                                        
 ( SELECT floor(kf) AS k, kf-floor(kf) AS d                                                                       
 FROM ( SELECT 1 + @Percentile75 *(count(*)-1) AS kf FROM @TempVal ) as x1                                                                                                                                              
 ) as x2                                                                                                                                                                              
 JOIN @TempVal a                                                                                                                                                                                         
 ON  ( SELECT COUNT(*) FROM @TempVal aa WHERE aa.[val] < a.[val]) < k                                                
 JOIN @TempVal b                                                   
 ON  ( SELECT COUNT(*) FROM @TempVal bb                                                                         
 WHERE  bb.[val] < b.[val]) < k+1                                                 
 )                                                                                                                                                                                  
 delete from  @TempVal                                                                                                  
          
 --tsr3                                                               
          
 insert into @TempVal([val])                                                                                                                                                                                   
 select   [tsr3] from  #PreCalculatedPPA where [tsr3] is not null --<>0                                                   
 --tsr3 25                               
 set @tsr3per25=0                                                         
 SET @tsr3per25 =                                                                                                                           
 ( SELECT dbo.LERP(max(d), 0.00, 1.00, max(a.[val]), max(b.[val])) AS percentile                                                             
 FROM                                                                                  
 ( SELECT floor(kf) AS k, kf-floor(kf) AS d                                                                                                                                                                                                    
 FROM ( SELECT 1 + @Percentile25 *(count(*)-1) AS kf FROM @TempVal ) as x1                                                                                                              
 ) as x2                                                                                    
 JOIN @TempVal a              
 ON  ( SELECT COUNT(*) FROM @TempVal aa WHERE aa.[val] < a.[val]) < k               
 JOIN @TempVal b                                                       
 ON  ( SELECT COUNT(*) FROM @TempVal bb                                                                                                                                                                                                                  
 WHERE  bb.[val] < b.[val]) < k+1                                                                                                                                                                                     
 )                     --tsr3 75                                                                                                                                                      
 set @tsr3per75=0                                                                                                                                                                                                       
 SET @tsr3per75 =                                                                                                                                        
 ( SELECT dbo.LERP(max(d), 0.00, 1.00, max(a.[val]), max(b.[val])) AS percentile                                                                                                                                                                              
  
    
      
       
          
          
 FROM                                                
 ( SELECT floor(kf) AS k, kf-floor(kf) AS d                   
 FROM ( SELECT 1 + @Percentile75 *(count(*)-1) AS kf FROM @TempVal ) as x1                                     
 ) as x2                                                                                                                                                                                       
 JOIN @TempVal a                                                          
 ON  ( SELECT COUNT(*) FROM @TempVal aa WHERE aa.[val] < a.[val]) < k                                                                                                         
          
 JOIN @TempVal b                                                                                                                         
 ON  ( SELECT COUNT(*) FROM @TempVal bb                           
 WHERE  bb.[val] < b.[val]) < k+1                                                                               
          
 )                                                                                                                                                                    
 DELETE FROM @TempVal                                                                                    
 -- insert into @PeerCompanyPerformance([SnapShotDate],[rotc3per25],[rotc3per75],[rotc1per25],[rotc1per75],[tsr3per25],[tsr3per75],[tsr1per25],[tsr1per75])                    
          
 --select @SnapShotDate,isnull(@rotc3per25,0),isnull(@rotc3per75,0), isnull(@rotc1per25,0),isnull(@rotc1per75,0),isnull(@tsr3per25,0),                                                                                                           
          
 --isnull(@tsr3per75,0),isnull(@tsr1per25,0),isnull(@tsr1per75,0)                         
          
 ----end of performance                                                                                                            
          
 ----Pay                                                              
 --ceo3                                                                                                                                                                                  
 insert into @TempVal([val])                                                                                                                
 select   [CEO3] from  #PreCalculatedPPA where [CEO3] is not null --<>0                                                                                                                   
          
 --ceo3 10                                                                                      
 set @CeoCompper10=0                                       
 SET @CeoCompper10 =                                                                         
 ( SELECT dbo.LERP(max(d), 0.00, 1.00, max(a.[val]), max(b.[val])) AS percentile                                                                                                                                        
 FROM                                                         
 ( SELECT floor(kf) AS k, kf-floor(kf) AS d                                                                          
 FROM ( SELECT 1 + @Percentile10 *(count(*)-1) AS kf FROM @TempVal ) as x1                                                                                                             
 ) as x2                                                                                                                                 
 JOIN @TempVal a                                                                                                                                                       
 ON  ( SELECT COUNT(*) FROM @TempVal aa WHERE aa.[val] < a.[val]) < k                                                                                                                  
 JOIN @TempVal b                                                                          
 ON  ( SELECT COUNT(*) FROM @TempVal bb                                                                                             
 WHERE  bb.[val] < b.[val]) < k+1                                                                                                                                                                                                                  
 )                                                                                                                                                       
 --ceo3 90                                                                                                                        
 set @CeoCompper90=0                                                                                                            
 SET @CeoCompper90 =                                                                                                                                                                  
 ( SELECT dbo.LERP(max(d), 0.00, 1.00, max(a.[val]), max(b.[val])) AS percentile                                                                                                                                          
 FROM                                                                              
 ( SELECT floor(kf) AS k, kf-floor(kf) AS d                                               
 FROM ( SELECT 1 + @Percentile90 *(count(*)-1) AS kf FROM @TempVal ) as x1                                        
 ) as x2                                                                                                                                                                                    
 JOIN @TempVal a                                                                                                                                                                                                                  
 ON  ( SELECT COUNT(*) FROM @TempVal aa WHERE aa.[val] < a.[val]) < k                                                                                    
 JOIN @TempVal b                                                                                                                                                   
 ON  ( SELECT COUNT(*) FROM @TempVal bb                                                                                                                       
 WHERE  bb.[val] < b.[val]) < k+1                                                                          
 )                                                                                                                                                                        
 DELETE FROM  @TempVal                                                                                                       
 --ceo1           
 insert into @TempVal([val])                                                                                                                
 select   [CEO1] from  #PreCalculatedPPA where [CEO1] is not null --<>0                                                                                                                                                       
 --ceo1 10                                                                                                                                                            
 set @CeoTenureper10=0                               
 SET @CeoTenureper10 =                                                                                                                                        
 ( SELECT dbo.LERP(max(d), 0.00, 1.00, max(a.[val]), max(b.[val])) AS percentile                                                
 FROM                                                                                                                    
 ( SELECT floor(kf) AS k, kf-floor(kf) AS d                                                                                                                             
 FROM ( SELECT 1 + @Percentile10 *(count(*)-1) AS kf FROM @TempVal ) as x1                                                                                                                                                                              
 ) as x2                                                                                                 
 JOIN @TempVal a                                                                                                                                                                                                                  
 ON  ( SELECT COUNT(*) FROM @TempVal aa WHERE aa.[val] < a.[val]) < k                                                                                                                                                                         
 JOIN @TempVal b                                                                                                                                                                          
 ON  ( SELECT COUNT(*) FROM @TempVal bb                                                                                                                                     
 WHERE  bb.[val] < b.[val]) < k+1                               
 )                                                     
 --ceo1 90                                                                                                                                             
 set @CeoTenureper90=0                                                                   
 SET @CeoTenureper90 =                                                         
 ( SELECT dbo.LERP(max(d), 0.00, 1.00, max(a.[val]), max(b.[val])) AS percentile                                                                                                                                                                     
 FROM                                                                                                            
 ( SELECT floor(kf) AS k, kf-floor(kf) AS d                                                                                                                                                                    
 FROM ( SELECT 1 + @Percentile90 *(count(*)-1) AS kf FROM @TempVal ) as x1                                                                                                            
 ) as x2                    
 JOIN @TempVal a                                                           
 ON  ( SELECT COUNT(*) FROM @TempVal aa WHERE aa.[val] < a.[val]) < k                                                                                          
 JOIN @TempVal b                                    
 ON  ( SELECT COUNT(*) FROM @TempVal bb                            
 WHERE  bb.[val] < b.[val]) < k+1                                                                                                                            
 )                                                                     
                 
 DELETE FROM  @TempVal                                                                     
          
 --neo1                                                                                                                    
 insert into @TempVal([val])                                                                                                                                                                                   
 select   [NEO1] from  #PreCalculatedPPA where [NEO1] is not null --<>0                                                                                                  
 --neo1  10                                                                                                                                                                            
 set @NeoAnnualper10=0                                                                                                                                                                                                          
 SET @NeoAnnualper10 =                                                                                                                                                                                 
 ( SELECT dbo.LERP(max(d), 0.00, 1.00, max(a.[val]), max(b.[val])) AS percentile                                                                           
 FROM                                                                                                                                                                                          
 ( SELECT floor(kf) AS k, kf-floor(kf) AS d                                                                                                                                       
 FROM ( SELECT 1 + @Percentile10 *(count(*)-1) AS kf FROM @TempVal ) as x1                                                                                                         
 ) as x2                                                                                                                     
 JOIN @TempVal a                                   
 ON  ( SELECT COUNT(*) FROM @TempVal aa WHERE aa.[val] < a.[val]) < k                                                                                                                                                                                         
  
    
      
 JOIN @TempVal b                                                                                                                  
 ON  ( SELECT COUNT(*) FROM @TempVal bb                                                                                                                                                                
 WHERE  bb.[val] < b.[val]) < k+1      )                                                                                                                                       
 --neo1  90                                                                                                                                        
 set @NeoAnnualper90=0                                                                                                       
 SET @NeoAnnualper90 =                                                         
 ( SELECT dbo.LERP(max(d), 0.00, 1.00, max(a.[val]), max(b.[val])) AS percentile   
 FROM                                                                                                          
 ( SELECT floor(kf) AS k, kf-floor(kf) AS d                                                 
 FROM ( SELECT 1 + @Percentile90 *(count(*)-1) AS kf FROM @TempVal ) as x1                                                                
 ) as x2                                                                           
 JOIN @TempVal a                                            
 ON  ( SELECT COUNT(*) FROM @TempVal aa WHERE aa.[val] < a.[val]) < k                                                                                    
 JOIN @TempVal b                                                                                                                  
 ON  ( SELECT COUNT(*) FROM @TempVal bb                                                                                                                             
 WHERE  bb.[val] < b.[val]) < k+1                                                                                                                                                                                                                  
 )                                                                                                                                                                                  
 DELETE FROM @TempVal                                                                        
 --neo3                                                                                            
 insert into @TempVal([val])                                                                                                                                                                                   
 select   [NEO3] from  #PreCalculatedPPA where [NEO3] is not null --<>0                                           
 --neo3 10                                                                                                                                                                                     
 set @NeoCompper10=0                      
 SET @NeoCompper10 =                                                                                                                                             
 ( SELECT dbo.LERP(max(d), 0.00, 1.00, max(a.[val]), max(b.[val])) AS percentile                                                                                                                          
 FROM                                                       
 ( SELECT floor(kf) AS k, kf-floor(kf) AS d                                                                                                                      
 FROM ( SELECT 1 + @Percentile10 *(count(*)-1) AS kf FROM @TempVal ) as x1                                                                                                                                                                                    
  
   
 ) as x2                                      
 JOIN @TempVal a                                                                                                                     
 ON  ( SELECT COUNT(*) FROM @TempVal aa WHERE aa.[val] < a.[val]) < k                       
 JOIN @TempVal b                                                                                                                                                                 
 ON  ( SELECT COUNT(*) FROM @TempVal bb                                                                                                                                                                
 WHERE  bb.[val] < b.[val]) < k+1                                                                                         
 )                                                                      
 --neo3 90                                                                                
 set @NeoCompper90=0                             
 SET @NeoCompper90 =                           
 ( SELECT dbo.LERP(max(d), 0.00, 1.00, max(a.[val]), max(b.[val])) AS percentile                                                                                              
 FROM               
 ( SELECT floor(kf) AS k, kf-floor(kf) AS d                                                                                                                          
 FROM ( SELECT 1 + @Percentile90 *(count(*)-1) AS kf FROM @TempVal ) as x1                                                                   
 ) as x2                                                                             
 JOIN @TempVal a                                                                                                                                                                                                               
 ON  ( SELECT COUNT(*) FROM @TempVal aa WHERE aa.[val] < a.[val]) < k                                                                                                                                                                          
 JOIN @TempVal b                                                                                                                                                                                                                  
 ON  ( SELECT COUNT(*) FROM @TempVal bb                                                                                                              
 WHERE  bb.[val] < b.[val]) < k+1                                                                                                                                                                                                                  
 )                                                                                                                          
 DELETE FROM @TempVal                                                                                                                                                
 ------------------------                                                                                 
 set @ROTC1=0                                                                                                                                                                           
 SET @ROTC1=(SELECT top 1 [ROTC1] FROM #PreCalculatedPPA where [Cover_id]=@coverId)                                                                                                                                                                
 if @rotc1<=@rotc1per25                                                                                        
 begin                                                                                                                                     
 set @rotc1PPA='Bottom Quartile ROTC'                                                              
 set @rotc1PPA1='Bottom Quartile ROTC ' --(<=25th Percentile Value)           
 end                                                                                                                                                    
 else if  @rotc1>@rotc1per25 and @rotc1<=@rotc1per75                                                                                                 
 begin                                                                                                                                                
 set @rotc1PPA='Mid-Range ROTC'                                                                                                                                                                              
 set @rotc1PPA1='Mid-Range ROTC (>25th AND <=75th Percentile Value)'                                                                                             
 end                                                                                                                                                   
 else if  @rotc1>@rotc1per75           
 begin                                                                                                             
 set @rotc1PPA='Top Quartile ROTC'                                                                                   
 set @rotc1PPA1='Top Quartile ROTC '   --(>75th Percentile Value)                                                                                                                                              
 end                                                                                                                                                                                        
          
 set @ROTC3=0                                   
        
 -- SELECT top 1 [ROTC3] FROM #PreCalculatedPPA where [Cover_id]=@coverId and snapshotdate=@snapshotdate---***** test                                                   
          
 SET   @ROTC3= (SELECT top 1 [ROTC3] FROM #PreCalculatedPPA where [Cover_id]=@coverId)                                            
 if @rotc3<=@rotc3per25                                                                                                                                                    
 begin                                                                                                                                                                                  
 set @rotc3PPA='Bottom Quartile ROTC'                                                                                                                                                  
 set @rotc3PPA1='Bottom Quartile ROTC '   --(<=25th Percentile Value)                                                                     
 end                                                                                                                          
 else if  @rotc3>@rotc3per25 and @rotc3<=@rotc3per75                                                                                                                   
 begin                                                                                                                                                                        
 set @rotc3PPA='Mid-Range ROTC'                                                                                                                                                  
 set @rotc3PPA1='Mid-Range ROTC (>25th AND <=75th Percentile Value)'                                                                                    
 end                                                                                                                                      
 else if  @rotc3>@rotc3per75                                                                                                                     
 begin                                                                             
 set @rotc3PPA='Top Quartile ROTC'                                                                                                                                                  
 set @rotc3PPA1='Top Quartile ROTC '   --(>75th Percentile Value)                                                                  
 end                                                                                                                                                               
          
 set @TSR1=0                                                                                                      
 SET  @TSR1=(SELECT top 1 [TSR1] FROM #PreCalculatedPPA where [Cover_id]=@coverId)                                                      
          
 if @tsr1<=@tsr1per25                          
 begin                                                                                                                                                                                  
 set @tsr1PPA='Bottom Quartile TSR'       
 set @tsr1PPA1='Bottom Quartile TSR '   --(<=25th Percentile Value)                                          
 end                      
 else if  @tsr1>@tsr1per25 and @tsr1<=@tsr1per75                                                                                                                                                  
 begin                                                     
 set @tsr1PPA='Mid-Range TSR'                                                                                                                                                  
 set @tsr1PPA1='Mid-Range TSR (>25th AND <=75th Percentile Value)'                                                                                                    
 end                                                                                                      
 else if  @tsr1>@tsr1per75                                                                                                                                          
 begin                                 
 set @tsr1PPA='Top Quartile TSR'                                                                                                                          
 set @tsr1PPA1='Top Quartile TSR '--(>75th Percentile Value)                                                                                                                                                  
 end                                                                                                                                                       
                         
 set @TSR3=0                                                                  
 SET  @TSR3=(SELECT top 1 [TSR3] FROM #PreCalculatedPPA where [Cover_id]=@coverId)                                        
 if @tsr3<=@tsr3per25                                                                              
 begin                                                                                                                
 set @tsr3PPA='Bottom Quartile TSR'                                                                                                                                                   
 set @tsr3PPA1='Bottom Quartile TSR '--(<=25th Percentile Value)                                                                                                                                                  
 end                                                                                                                   
 else if  @tsr3>@tsr3per25 and @tsr3<=@tsr3per75                                               
 begin                                                                                                                                                                              
 set @tsr3PPA='Mid-Range TSR'                                                                            
 set @tsr3PPA1='Mid-Range TSR (>25th AND <=75th Percentile Value)'                                      
 end                                                                                                    
 else if  @tsr3>@tsr3per75                                                                                                           
 begin                                                      
 set @tsr3PPA='Top Quartile TSR'                                                                                                                                                   
 set @tsr3PPA1='Top Quartile TSR '--(>75th Percentile Value)                                                          
 end                                                                                                                                     
                         
 set @CEO3=0                                                                                                                 
 SET   @CEO3=(SELECT top 1 [CEO3] FROM #PreCalculatedPPA where [Cover_id]=@coverId)              
          
 if @CEO3>@CeoCompper90                                                                                                                                                     
 begin                                                                                                                                     
 set @CeoCompPPA='Top Decile CEO Comp'                                                             
 set @CeoCompPPA1='Top Decile CEO Comp '--(>90th Percentile Value)                                                   
 end                                                                                                                                                          
 else if  @CEO3>@CeoCompper10 and @CEO3<=@CeoCompper90                                                                                                                                                  
 begin                                                      
 set @CeoCompPPA='Mid-Range CEO Comp'                                                                                 
 set @CeoCompPPA1='Mid-Range CEO Comp (>10th AND <=90th Percentile Value)'                                                                                                     
 end                                                                                                                                                       
 else if  @CEO3<=@CeoCompper10                                                                                                                                               
 begin                                                                                                      
 set @CeoCompPPA='Bottom Decile CEO Comp'                                                                                                                                                  
 set @CeoCompPPA1='Bottom Decile CEO Comp '--(<= 10th Percentile Value)                                             
 end                                                         
          
 set @CEO1=0                                                                                                 
 SET   @CEO1=(SELECT top 1 [CEO1] FROM #PreCalculatedPPA where [Cover_id]=@coverId)                                                                                                                                                                           
  
    
      
          
          
          
 if @CEO1>@CeoTenureper90                                                                                                                                                       
 begin                                                                                                                                      
 set @CeoTenurePPA='Top Decile CEO Comp'                                                                              
 set @CeoTenurePPA1='Top Decile CEO Comp '--(>90th Percentile Value)                                                                          
 end                                                                                                                  
 else if  @CEO1>@CeoTenureper10 and @CEO1<=@CeoTenureper90                                                                                                                                                    
 begin                                                                               
 set @CeoTenurePPA='Mid-Range CEO Comp'                                                                                                                                           
 set @CeoTenurePPA1='Mid-Range CEO Comp (>10th AND <=90th Percentile Value)'                                                                  
 end                                                                                 
 else if  @CEO1<=@CeoTenureper10          
 begin                                                                                                               
 set @CeoTenurePPA='Bottom Decile CEO Comp'                                                                                   
 set @CeoTenurePPA1='Bottom Decile CEO Comp '--(<= 10th Percentile Value)                                                                                                                                                  
 end                                                  
                         
 set @NEO3=0                                                                                                              
 SET  @NEO3=(SELECT top 1 [NEO3] FROM #PreCalculatedPPA where [Cover_id]=@coverId)                                                                                                                  
 if @NEO3>@NeoCompper90                                                                                                                                              
 begin                                                                                                                    
 set @NeoCompPPA='Top Decile Top5NEO Comp'                                                                            
 set @NeoCompPPA1='Top Decile Top5NEO Comp '--(>90th Percentile Value)                                                                                                                                                   
 end                                                                                                
 else if  @NEO3>@NeoCompper10 and @NEO3<=@NeoCompper90                                                                                                    
 begin                                                                                                                       
 set @NeoCompPPA='Mid-Range Top5NEO Comp'                                               
 set @NeoCompPPA1='Mid-Range Top5NEO Comp (>10th AND <=90th Percentile Value)'                                                                                                             
 end                                                                                                                                                     
 else if  @NEO3<=@NeoCompper10                                                                                                                                                     
 begin                                                                                                                                                                       
 set @NeoCompPPA='Bottom Decile Top5NEO Comp'                             
 set @NeoCompPPA1='Bottom Decile Top5NEO Comp '--(<= 10th Percentile Value)                                                          
 end                                                                                                                                                                                       
          
 set @NEO1=0                                                                       
 SET   @NEO1=(SELECT top 1 [NEO1] FROM #PreCalculatedPPA where [Cover_id]=@coverId)                                                                                                                            
 if @NEO1>@NeoAnnualper90                                                                                                                                            
 begin                            
 set @NeoAnnualPPA='Top Decile Top5NEO Comp'                                                                                                                                                   
 set @NeoAnnualPPA1='Top Decile Top5NEO Comp '--(>90th Percentile Value)                                                                                             
 end                                                                                         
 else if  @NEO1>@NeoAnnualper10 and @NEO1<=@NeoAnnualper90                                                              
 begin                                                                                                                                                                                
 set @NeoAnnualPPA='Mid-Range Top5NEO Comp'                                                             
 set @NeoAnnualPPA1='Mid-Range Top5NEO Comp (>10th AND <=90th Percentile Value)'                                                                                          
 end                                                                                               
 else if  @NEO1<=@NeoAnnualper10                                                                                                                                                    
 begin                                                      
 set @NeoAnnualPPA='Bottom Decile Top5NEO Comp'                                                                                                          
 set @NeoAnnualPPA1='Bottom Decile Top5NEO Comp '--(<= 10th Percentile Value)                                                                                                                                 
 end                                                                          
              
 -----------------------------------------Section 4.1.1----------------------------------                                                    
          
 SET @sec = '4.1.1'                                                                                                                                                                                                                                           
  
    
      
        
                                                                      
          
 --if @tsr3PPA='Bottom Quartile TSR' and @CeoCompPPA='Top Decile CEO Comp'                                           
 --SET @Outcome= (@tsr3PPA+'/'+@CeoCompPPA)                                                                                                            
 --else if   @tsr3PPA='Mid-Range TSR' and @CeoCompPPA='Top Decile CEO Comp'                                                                                                                                            
 --SET @Outcome= (@tsr3PPA+'/'+@CeoCompPPA)                                                          
 --else if   @tsr3PPA='Top Quartile TSR' and @CeoCompPPA='Bottom Decile CEO Comp'                                               
 --SET @Outcome= (@tsr3PPA+'/'+@CeoCompPPA)                                                                                                                                              
 --else                                                                                                                                                 
 --SET @Outcome=  'Remainder Combinations – TSR/CEO Comp'                                                                                                                                                 
 set @OutcomeDetails=(@tsr3PPA1+'/'+@CeoCompPPA1)                                                                                            
          
 if @tsr3 is null or @CEO3 is null                                                                                            
 begin                                                                                            
 --SET @Outcome=null                                   
 set @OutcomeDetails=null                                                     
 end                                                                             
                                      
 -- SET @Outcome= @tsr3PPA + '/' + @CeoCompPPA                                                                                                                          
          
 insert into AggMedianPreprocessData4_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                     
 select @CoverID,@sec,@SnapShotDate,@OutcomeDetails--@Outcome                                                                                                                                                                                              
          
            
              
 -----------------------------------------Section 4.1.2----------------------------------                                                                                                  
          
 set @sec = '4.1.2'                                   
          
 --if @tsr3PPA='Bottom Quartile TSR' and @NeoCompPPA='Top Decile Top5NEO Comp'                                                                                                                       
 --SET @Outcome= (@tsr3PPA+'/'+@NeoCompPPA)                                                                                                        
 --else if   @tsr3PPA='Mid-Range TSR' and @NeoCompPPA='Top Decile Top5NEO Comp'                                                                                
 --SET @Outcome= (@tsr3PPA+'/'+@NeoCompPPA)                                                            
 --else if   @tsr3PPA='Top Quartile TSR' and @NeoCompPPA='Bottom Decile Top5NEO Comp'                                                                                                              SET @Outcome= (@tsr3PPA+'/'+@NeoCompPPA)                    
 
   
          
 --else                                                                                                                                                  
 --SET @Outcome= 'Remainder Combinations – TSR/ Top5NEO Comp'                                                                                                                                          
 --  SET @Outcome=@tsr3PPA+'/'+@NeoCompPPA                                                       
 set @OutcomeDetails=(@tsr3PPA1+'/'+@NeoCompPPA1)                                                                                            
 if @tsr3 is null or @Neo3 is null                            
 begin                                                            
 SET @Outcome=null                                                                                            
 set @OutcomeDetails=null                                                                                            
 end                                                                                                                                                
          
 insert into AggMedianPreprocessData4_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                  
 select @CoverID,@sec,@SnapShotDate,@OutcomeDetails--@Outcome                                                                                                                                                                               
          
 -----------------------------------------Section 4.1.3----------------------------------                                                                                                                                                                      
  
   
      
        
          
          
 SET @sec = '4.1.3'                                                                                                                                                                               
          
 -- SET @Outcome=@rotc3PPA+'/'+@CeoCompPPA                                                                              
 --if @rotc3PPA='Bottom Quartile ROTC' and @CeoCompPPA='Top Decile CEO Comp'                                                                                        
 --SET @Outcome= (@rotc3PPA+'/'+@CeoCompPPA)                          
 --else if   @rotc3PPA='Mid-Range ROTC' and @CeoCompPPA='Top Decile CEO Comp'                                                                
 --SET @Outcome= (@rotc3PPA+'/'+@CeoCompPPA)                                                                                                                                                       
 --else if   @rotc3PPA='Top Quartile ROTC' and @CeoCompPPA='Bottom Decile CEO Comp'                                                                                                                                                 
 --SET @Outcome= (@rotc3PPA+'/'+@CeoCompPPA)                                                                                                
 --else                
 --SET @Outcome=  'Remainder Combinations – ROTC /CEO Comp'                                                                                                                                                              
          
 set @OutcomeDetails=(@rotc3PPA1+'/'+@CeoCompPPA1)                                                       
 if @rotc3 is null or @CEO3 is null                                                                                            
 begin                                                                                            
 --SET @Outcome=null                    
 set @OutcomeDetails=null                                                                                     
 end                                                                                     
 insert into AggMedianPreprocessData4_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                        
 select @CoverID,@sec,@SnapShotDate,@OutcomeDetails--@Outcome                                                                                                                                                                              
          
 -----------------------------------------Section 4.1.4----------------------------------                                                                                                                    
          
 SET @sec = '4.1.4'                                                   
          
 --if @rotc3PPA='Bottom Quartile ROTC' and @NeoCompPPA='Top Decile Top5NEO Comp'                                                                                          
 --SET @Outcome= (@rotc3PPA+'/'+@NeoCompPPA)                                                                                                                                                
 --else if   @rotc3PPA='Mid-Range ROTC' and @NeoCompPPA='Top Decile Top5NEO Comp'                                                                                                                                                 
 --SET @Outcome= (@rotc3PPA+'/'+@NeoCompPPA)                                                                                                                                                       
 --else if   @rotc3PPA='Top Quartile ROTC' and @NeoCompPPA='Bottom Decile Top5NEO Comp'                                                                                           
 --SET @Outcome= (@rotc3PPA+'/'+@NeoCompPPA)                                                                                                             
 --else                                                                             
 --SET @Outcome=  'Remainder Combinations – ROTC / Top5NEO Comp'                                      
          
 --SET @Outcome=@rotc3PPA+'/'+@NeoCompPPA                                                                          
          
 set @OutcomeDetails=(@rotc3PPA1+'/'+@NeoCompPPA1)                                                                                            
 if @rotc3 is null or @NEO3 is null                                                                                            
 begin                
 --SET @Outcome=null                                   
 set @OutcomeDetails=null                      
 end                                                                                                                                       
 insert into AggMedianPreprocessData4_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                  
 select @CoverID,@sec,@SnapShotDate,@OutcomeDetails--@Outcome                                                                                                               
          
 -----------------------------------------Section 4.1.5----------------------------------                                                
          
 SET @sec = '4.1.5'                                                                                                                                                      
         
 --if @tsr1PPA='Bottom Quartile TSR' and @CeoTenurePPA='Top Decile CEO Comp'                                       
 --SET @Outcome= (@tsr1PPA+'/'+@CeoTenurePPA)                                                                                                                                                     
 --else if   @tsr1PPA='Mid-Range TSR' and @CeoTenurePPA='Top Decile CEO Comp'                                                            
 --SET @Outcome= (@tsr1PPA+'/'+@CeoTenurePPA)                                                                                      
 --else if   @tsr1PPA='Top Quartile TSR' and @CeoTenurePPA='Bottom Decile CEO Comp'                                                                                                                                                 
 --SET @Outcome= (@tsr1PPA+'/'+@CeoTenurePPA)                                                                                                                                                      
 --else                                                             
 --SET @Outcome='Remainder Combinations – TSR/CEO Comp'                                                                  
 set @OutcomeDetails=(@tsr1PPA1+'/'+@CeoTenurePPA1)                                                                                            
 if @tsr1 is null or @CEO3 is null                                                                      
 begin                                                                                            
 --SET @Outcome=null                                                                                            
 set @OutcomeDetails=null                                                                                            
 end                                                                                                                                                                             
 --SET @Outcome=@tsr1PPA+'/'+@CeoTenurePPA                                                                                 
          
          
 insert into AggMedianPreprocessData4_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                          
 select @CoverID,@sec,@SnapShotDate,@OutcomeDetails--@Outcome                                                                                                            
          
 -----------------------------------------Section 4.1.6----------------------------------                                                                                          
          
 SET @sec = '4.1.6'                                                                                                                                                                                            
          
 --if @tsr1PPA='Bottom Quartile TSR' and @NeoAnnualPPA ='Top Decile Top5NEO Comp'                                                    
 --SET @Outcome= (@tsr1PPA+'/'+@NeoAnnualPPA )                                        
 --else if   @tsr1PPA='Mid-Range TSR' and @NeoAnnualPPA ='Top Decile Top5NEO Comp'                                                                                                                                                 
 --SET @Outcome= (@tsr1PPA+'/'+@NeoAnnualPPA )                                                                
 --else if   @tsr1PPA='Top Quartile TSR' and @NeoAnnualPPA ='Bottom Decile Top5NEO Comp'                                                           
 --SET @Outcome= (@tsr1PPA+'/'+@NeoAnnualPPA)                                                                                                                                                      
 --else                                                                  
 --SET @Outcome='Remainder Combinations – TSR/ Top5NEO Comp'                                                                                                                    
 --SET @Outcome=@tsr1PPA +'/'+@NeoAnnualPPA                                                                                     
          
 set @OutcomeDetails=(@tsr1PPA1+'/'+@NeoAnnualPPA1)                                                                                            
 if @tsr1 is null or @NEO1 is null                             
 begin                                                                                            
 --SET @Outcome=null                                                                                            
 set @OutcomeDetails=null                                                                                            
 end                       
          
 insert into AggMedianPreprocessData4_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                        
 select @CoverID,@sec,@SnapShotDate,@OutcomeDetails--@Outcome                                                                                
              
 -----------------------------------------Section 4.1.7----------------------------------                                                                                 
          
 SET @sec = '4.1.7'                                                                                                        
          
 --SET @Outcome=@rotc1PPA+'/'+@CeoTenurePPA                                                                                                                     
          
 --if @rotc1PPA='Bottom Quartile ROTC' and @CeoTenurePPA='Top Decile CEO Comp'                                                                                                                                                 
 --SET @Outcome= (@rotc1PPA+'/'+@CeoTenurePPA)                                                                                                             
 --else if   @rotc1PPA='Mid-Range ROTC' and @CeoTenurePPA='Top Decile CEO Comp'                                                                                                                         
 --SET @Outcome= (@rotc1PPA+'/'+@CeoTenurePPA)                                                                              
 --else if   @rotc1PPA='Top Quartile ROTC' and @CeoTenurePPA='Bottom Decile CEO Comp'                                                       
 --SET @Outcome= (@rotc1PPA+'/'+@CeoTenurePPA)                                                     
 --else                                                                                            
 --SET @Outcome='Remainder Combinations – ROTC /CEO Comp'                                                                                                                                                                
          
 set @OutcomeDetails=(@rotc1PPA1+'/'+@CeoTenurePPA1)                                                                                            
 if @rotc1 is null or @CEO1 is null                                                                                            
 begin                                                                                            
 --SET @Outcome=null                                                          
 set @OutcomeDetails=null                                                                                 
 end                                                                                              
                     
 insert into AggMedianPreprocessData4_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                           
 select @CoverID,@sec,@SnapShotDate,@OutcomeDetails--@Outcome                                              
          
 -----------------------------------------Section 4.1.8----------------------------------                                                                                                                                                                 
          
 SET @sec = '4.1.8'                                                                                                                             
          
 --if @rotc1PPA='Bottom Quartile ROTC' and @NeoAnnualPPA='Top Decile Top5NEO Comp'                                                                                                            
 --SET @Outcome= (@rotc1PPA+'/'+@NeoAnnualPPA)                                                
 --else if   @rotc1PPA='Mid-Range ROTC' and @NeoAnnualPPA='Top Decile Top5NEO Comp'                                                                                                                                                 
 --SET @Outcome= (@rotc1PPA+'/'+@NeoAnnualPPA)                                                                                                                                                  
 --else if   @rotc1PPA='Top Quartile ROTC' and @NeoAnnualPPA='Bottom Decile Top5NEO Comp'                                SET @Outcome= (@rotc1PPA+'/'+@NeoAnnualPPA)                                                                                   
 --else                                                                                                                                                   
 --SET @Outcome='Remainder Combinations – ROTC / Top5NEO Comp'                                                                                       
          
 --SET @Outcome=@rotc1PPA+'/'+@NeoCompPPA                                                                                                                                                                 
 set @OutcomeDetails=(@rotc1PPA1+'/'+@NeoAnnualPPA1)                                                                                            
 if @rotc1 is null or @NEO1 is null                                                       
 begin                                                                                            
 --SET @Outcome=null                                                    
 set @OutcomeDetails=null                                                            
 end                                                                                                                                                             
          
 insert into AggMedianPreprocessData4_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                           
 select @CoverID,@sec,@SnapShotDate,@OutcomeDetails--@Outcome                                                                                                                                              
          
            -----------------------------------------Section 4.2.1----------------------------------                                                                 
                               
   SET @Outcome = NULL                                                                                                                                                                                              
   SET @sec='4.2.1'                                                                                                       
           
        
   set @res79= (select                                                                           
   FeildName from LookUpDetails                                                                                                                                                                                                                               
   where LookUpId                                                                                                                                       
   in(                                                                                                                                                                                                        
   select top 1 SOPbcStatusEffec from EventDataGov_sopbc                                                                                                               
   where SOPbcRefFileDate <= @SnapShotDate                                                                                                 
   and CoverId=@coverId order by SOPbcRefFileDate desc))                                                                      
        
   set @res80=                                                                                                              
   (select                                                                                                                                     
   FeildName from LookUpDetails                                                                           
   where LookUpId                                                         
   in(                                                                                                   
   select top 1 SOPStatusEffec from EventDataGov_sop                                             
   where SOPRefFileDate <= @SnapShotDate                                                                             
   and CoverId=@coverId order by SOPRefFileDate desc))                                                                                                                                                                                                        
  
    
      
        
        
   if (@res79 like '%yes%' and @res80 like '%yes%'  )                                                         
   begin                                                                                              
   set @Outcome='Yes_Binding'                                                                                                                                                      
   end                                                            
   else if (@res79 like '%no%' and @res80 like '%yes%')                                                                      
   begin                                                                                                                                                               
   set @Outcome='Yes_Non Binding'                                                                                                                                                                           
   end                                                                   
   else if (@res79 like '%no%' and @res80 like '%no%')                                                 
   begin                                                                
   set @Outcome ='No'                             
   end                                                                     
   else                                                                       
   if (@res79 is null and @res80 is null)                                                                      
   begin                                                                                                                          
   set @Outcome ='No'                                          
   end                                                                
   else if (@res79 is null and @res80 like '%yes%')                                                                    
   begin                                                                                                              
   set @Outcome='Yes_Non Binding'                                                                                                                                                                            
   end                                                                    
        
   if @Outcome='Yes_Binding'                            
   set @Outcome='Yes Binding'                                                                                                              
   if @Outcome='Yes_Non Binding'                                                                                                            
   set @Outcome='Yes but Non-Binding'                                                                                                          
                                         
   insert into AggMedianPreprocessData4_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                                                    
   
    
     
        
   select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                                                                           
        
   -----------------------------------------Section 4.2.2----------------------------------                                                                                                                                                                    
  
    
      
          
        
   SET @Outcome = NULL                                                                                                                                                                                                   
   SET @sec='4.2.2'                                                                                                     
        
   SET @Outcome =  isnull((SELECT FeildName from LookUpDetails                                                                                               
   where LookUpId                                                                                                                                                                          
   in(                                                                             
   select top 1 DCCPStatusEffec from EventDataGov_dccp                                                               
   where DCCPRefFileDate <= @SnapShotDate                                   
   and CoverId=@coverId order by DCCPRefFileDate desc)),'No')                                                                               
   insert into AggMedianPreprocessData4_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                     
   select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                                                                  
        
   -----------------------------------------Section 4.3.1----------------------------------                                                                                                                                                                    
  
    
      
   SET @Outcome = NULL                                                                                      
   SET @sec='4.3.1'                                                                                                                          
   SET @NoofRestatement =  NULL                                                                               
   set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                             
   set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                                                                                                                         
   
    
      
        
   SET @Outcome =                                                                       
   (SELECT ISNULL(COUNT([CEO_Id]),0)                                  
   FROM  dbo.CEO                                                                                  
   WHERE Cover_Id = @CoverID                    
   AND CXODateArival <= @SnapShotDate                                                                                                                                                    
   and isnull(CXODateDeparture,'2100-01-01')>=@dtmax3                                                                                                          
   )                                   
        
   insert into AggMedianPreprocessData4_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                          
   select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                                                                      
        
   -----------------------------------------Section 4.3.2----------------------------------                                                                                                                                                                    
 
     
      
       
        
   SET @Outcome = NULL                                                                                                                              
   SET @sec='4.3.2'                                                                      
        
   SET @NoofRestatement =  NULL                                                                  
   set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                                                  
   set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                      
   SET @Outcome =                                                    
   (SELECT ISNULL(COUNT([CFO_Id]),0)                                                                                      
   FROM  dbo.CFO                                                                                                               
   WHERE Cover_Id = @CoverID                                                                                                                       
   AND CFODateArival <= @SnapShotDate                                                                                                                                 
   and isnull(CFODateDeparture,'2100-01-01')>=@dtmax3                                                                           
   )                                                                                                                                 
        
   insert into AggMedianPreprocessData4_Daily([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                               
   select @CoverID,@sec,@SnapShotDate,@Outcome          
   ------------------------------------------------------------END  MGMT PART BALANCING                                                                      
                                                                                                                   
    set @cnt=@cnt+1                                                  
                                                   
                                                                  
 END                                                                                                                                  
                                                                                                                            
                                                                                                   
END    ------END Check for scorable company                                                                                                                                                      
    declare @MINDT date          
SET @MINDT=(select convert(varchar,DATEADD(Day,365*3,MIN(Proxy_FillingDt)),101)  from Proxy_Period where Cover_Id=@coverid)                
          
IF((@SnapShotDate IS NOT NULL) AND (@SnapShotDate<@MINDT))                
BEGIN                                                                                                   
   UPDATE AggMedianPreprocessData4_Daily set outcome=''             
  where cover_id=@coverid            
  and snapshotdate=@SnapShotDate            
  and GsmNo in ('4.3.1','4.3.2'            
  )            
end                                                           
                                                                                                                                                                                                    
                                                                                    
-- ------------------------- End of Calculation                                                              
                                                                              
                    
END 