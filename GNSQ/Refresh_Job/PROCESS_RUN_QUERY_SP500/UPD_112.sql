USE [LiveGSQDB_8th_Nov_2018]
GO
/****** Object:  StoredProcedure [dbo].[UPD_112]    Script Date: 04/03/2019 12:39:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[UPD_112]        
( @snapshotdate date        
)        
AS BEGIN        
        
-- @snapshotdate='15-aug-2017'        
        
declare @n int =((select count(distinct isin) from DATASET2_PERCENTILE_TOT where         
Date=@SnapShotDate )-1)         
        
declare @tbbelow table(ii int identity(1,1), val varchar(100))        
        
declare @b decimal(18,2)        
declare @vl varchar(100)       
declare @perc decimal(18,2)        
        
insert into @tbbelow        
select distinct [1#1#2] from DATASET1_OUTCOME_TOT where date=@SnapShotDate        
and [1#1#2]!='Unqualified'       
        
        
declare @st int =1        
declare @en int =(select COUNT(ii) from @tbbelow)        
while(@st<=@en)        
begin        
  set @vl=(select val from @tbbelow where ii=@st)        
          
   IF (@vl='Qualified')                                                                                                                                  
BEGIN                                                                                                       
                                                                                                  
  set @b=(select count(distinct ISIN) from DATASET1_OUTCOME_TOT where     
  Date=@SnapShotDate and [1#1#2] in ('Unqualified'))                                     
                                                                                                                                      
  set @perc=(@b/@n)*100                                                  
                                    
END                                                                                                                                                             
ELSE IF (@vl='Disclaimer')                                                                                                                                    
BEGIN                                                                                                                                                       
                           
 set @b=(select count(distinct ISIN) from DATASET1_OUTCOME_TOT where     
 Date=@SnapShotDate and [1#1#2] in ('Unqualified','Qualified'))                                                                                                      
                                                           
  set @perc=(@b/@n)*100                                                                                    
                                                                
END                                                                                                                                                           
ELSE IF (@vl='Adverse')                                                      
BEGIN                                                                                                                   
 set @b=(select count(distinct ISIN) from DATASET1_OUTCOME_TOT where     
 Date=@SnapShotDate and [1#1#2] in ('Unqualified','Qualified','Disclaimer'))                                     
                                               
  set @perc=(@b/@n)*100                                                                                
END                                                                                                                                                                                                                                                            
  
   
      
    
  --print @vl          
  --print @b        
  --print @n        
  --print @perc        
          
  UPDATE DATASET2_PERCENTILE_TOT SET [1#1#2]=@perc where date=@SnapShotDate AND ISIN IN        
  (SELECT DISTINCT ISIN  FROM DATASET1_OUTCOME_TOT where         
  Date=@SnapShotDate and [1#1#2]=@vl)        
        
  set @st=@st+1        
        
  END      
end        