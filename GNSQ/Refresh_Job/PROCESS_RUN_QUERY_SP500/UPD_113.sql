
/****** Object:  StoredProcedure [dbo].[UPD_113]    Script Date: 04/03/2019 12:41:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	ALTER PROCEDURE [dbo].[UPD_113]          
	( @SNAPSHOTDATE DATE          
	)          
	AS BEGIN         

	  
	-- @SNAPSHOTDATE='15-AUG-2017'          
	  
	DECLARE @N INT =((SELECT COUNT(DISTINCT ISIN) FROM DATASET2_PERCENTILE_TOT WHERE           
	DATE=@SNAPSHOTDATE )-1)           
	  
	DECLARE @TBBELOW TABLE(II INT IDENTITY(1,1), VAL DECIMAL(18,2))          
	  
	DECLARE @B DECIMAL(18,2)          
	DECLARE @VL DECIMAL(18,2)          
	DECLARE @PERC DECIMAL(18,2)          
	  
	INSERT INTO @TBBELOW          
	SELECT DISTINCT [1#1#3] FROM DATASET1_OUTCOME_TOT WHERE DATE=@SNAPSHOTDATE          
	AND ISNULL([1#1#3],'')!=''        
	  
	  
	DECLARE @ST INT =1          
	DECLARE @EN INT =(SELECT COUNT(II) FROM @TBBELOW)          
	WHILE(@ST<=@EN)          
	BEGIN          
	SET @VL=(SELECT VAL FROM @TBBELOW WHERE II=@ST)          
	  
	IF(@VL <=(SELECT GSQ_OPTION_1 FROM     
	PREPROCESS_MEASURE_VARIABLE_INSTANCE_DETAILS WHERE  GSM_NUM='1.1.3')) 
	BEGIN
	SET @PERC='0.00'     
	END   
	IF(@VL >(SELECT GSQ_OPTION_1 FROM     
	PREPROCESS_MEASURE_VARIABLE_INSTANCE_DETAILS WHERE  GSM_NUM='1.1.3')) 
	BEGIN
	SET @B=(SELECT COUNT(DISTINCT ISIN) FROM DATASET1_OUTCOME_TOT WHERE         
	DATE=@SNAPSHOTDATE AND CAST(ROUND([1#1#3],0) AS INT)<CAST(ROUND(@VL,0) AS INT)
	AND ISNULL([1#1#3],'')!=''     
	)       

	SET @PERC=(@B/@N)*100        
	END   
	    
	UPDATE DATASET2_PERCENTILE_TOT SET [1#1#3]=@PERC WHERE DATE=@SNAPSHOTDATE AND ISIN IN          
	(SELECT DISTINCT ISIN  FROM DATASET1_OUTCOME_TOT WHERE           
	DATE=@SNAPSHOTDATE AND CAST(ROUND([1#1#3],0) AS INT)=CAST(ROUND(@VL,0) AS INT))          
	  
	SET @ST=@ST+1          
	  
	END        
	END 