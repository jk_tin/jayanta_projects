
/****** Object:  StoredProcedure [dbo].[usp_SSIS_ScoreAggMedian2_Daily_SingleDay_third]    Script Date: 04/03/2019 11:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                                                                                                                                                                  
-- =============================================                                                                                                                                                                                                               
  
    
      
        
          
            
             
-- Author:  Arindam Mukhopadhyay                                                                                                                                                                                                                          
-- Create date: 23/05/2013                                                                                                                                                                                                                            
-- Description: Get Score outcome for section 2                                                                                                                                                                                                                
  
    
      
        
          
            
-- =============================================                                                                                                                                                                                      
ALTER Procedure [dbo].[usp_SSIS_ScoreAggMedian2_Daily_SingleDay_third]                                                                                                                                                                                      
    
      
        
         
                   
              
                    
                                      
(                                                                                                                                                                                                                                    
 @CoverID varchar(100),                  
 @snapdate date                                                                                                                                                                                                                            
)                                                                                                                                                                                                                                    
                                                                                                                                                                                                                                                          
AS                                                                                                                                                                                                                                     
BEGIN                                                                                                                                                                         
 SET NOCOUNT ON;                
                                                                                                                                                                       
 delete from AggMedianPreprocessData2_Daily_Third where Cover_id=@CoverID  and snapshotdate=@snapdate                                                                                                                                                          
  
    
      
 ----------------------------------------------From Date Checking-------------------                                                                                                                                                                           
  
    
    
 declare @toDate date                                                        
 declare @fromDate date                                             
 declare @ScoreMedianFromDate date                                        
 declare @OTH_VARIABLE_1 decimal(18,3)                      
                
 set @OTH_VARIABLE_1=(select MAX(OTH_VARIABLE_1) from preprocess_Measure_Variable_Instance_Details                                                           
 where GSM_DESC like '%trailing%')                                                                                                                                                                                                                             
 
     
     
         
          
            
              
                                                                              
                                                                                            
  BEGIN                                                                                         
                                                                                                                                                                                                                           
  ---------------------------------------From Date Checking----------------------                                                                                                                                                                     
                                                                                                    
 declare @out varchar(100)                                                                           
 declare @dtno1 datetime                                                                                             
 declare @res29 datetime                                                                                                             
 declare @out1 varchar(100)                                                              
 declare @valx varchar(100)                                             
 declare @var decimal(10,2)                                                  
 declare @setif varchar(100)                                                                
 DECLARE @dtmax3 DATETIME = NULL                                                                                                                                                            
                                                      
                                                                                    
 declare @Quarter_Period TABLE(                                                                                                                                        
 [Indx] INT IDENTITY(1,1) NOT NULL,                                                                                                                                                          
 [Quarter_FillingDt] date                                                                                                                                    
 )                                                                                                                    
                                                                                                 
                         
  ------------------********************-----------------                                      
 delete from @Quarter_Period                                                           
 insert into @Quarter_Period                                      
 values(@snapdate)                                                                                                                 
                                                                                                     
 ---end-----                                        
                                                                    
        
 DECLARE @GSM_MASTER TABLE (                                                         
 [GSM_NUM] varchar(10)  NULL,                                                                                          
 [DeriveMeasureVariable] decimal(18,3)             
 )                                                                                                        
                                                                                                  
 insert into @GSM_MASTER([GSM_NUM],[DeriveMeasureVariable])                                                                                                                      
                                                                                                                         
 SELECT                                                                         
 [GSM_NUM],                                                                                                                                                    
 (case                                                                                                                                                                
 when (OTH_VARIABLE_1>OTH_VARIABLE_2 and OTH_VARIABLE_1>OTH_VARIABLE_3) then OTH_VARIABLE_1                                                                                                
 when  (OTH_VARIABLE_2>OTH_VARIABLE_1 and OTH_VARIABLE_2>OTH_VARIABLE_3) then OTH_VARIABLE_2                                                                                                
 when  (OTH_VARIABLE_3>OTH_VARIABLE_1 and OTH_VARIABLE_3>OTH_VARIABLE_2) then OTH_VARIABLE_3                                                                                                                         
 end) DeriveMeasureVariable                                                                                                                                                     
 FROM preprocess_Measure_Variable_Instance_Details                                                                                                                                                                                                       
                                                                                                     
                                                                                                                                                                                                                                                               
  
   
       
       
           
            
             
                                                                                
 DECLARE @CNT INT                                                                                                                                                                      
 DECLARE @Outcome NVARCHAR(1000)=NULL                                                                                                                                                                                                               
 DECLARE @Fdate DATETIME                                                                                                                                                                         
 declare @dtyes1 datetime                                                                                                                                     
 declare @dtyes2 datetime                                                                                                                                                                                        
 DECLARE @dcom1 varchar(200)                                                                                    
 DECLARE @dcom2 varchar(200)                    
 DECLARE @dcom3 varchar(200)                                                                                
 DECLARE @dcom4 varchar(200)                                                                                                                                              
 DECLARE @RES1 VARCHAR(200)                                                                                     
 DECLARE @RES2 VARCHAR(200)                                          
 DECLARE @comp33 varchar(200)                                                                                                  
 DECLARE @comp34 varchar(200)                                                                                             
 DECLARE @r1 varchar(200)                                  
 DECLARE @r2 varchar(200)                                                      
 DECLARE @sec VARCHAR(100) = NULL                                                                              
 --declare @PFactor2 decimal(18,5)                                  
                                                                                                                        
 declare @dtyes datetime                                                                  
 declare @val varchar(100)                                                                                                                     
 declare @dtyes_dcl datetime                                                                                                                                                                    
 declare @dtno_dcl datetime                                                                                                                                             
 declare @BOIShareAppId int                                                                                                                                               
 declare @resdcl varchar(500)                                                                                                                           
 declare @resshare varchar(500)                                                                                                                                             
                                                
 DECLARE @RES VARCHAR(200)                                                                                                                                                                                                                         
 DECLARE @RES0 VARCHAR(200)                                                                                                                                                                                     
 DECLARE @RES00 VARCHAR(200)                                                                                                                                                                                                                                  
   
    
     
        
          
            
              
                                                                                            
                                                                           
                                                                                                                               
 DECLARE @SnapShotDate DATE                                                                                                                                                                                                                                    
  
    
      
        
          
            
              
                                                 
 declare @count int                                                                                                                           
                                                       
 set @count=(select COUNT(*) from @Quarter_Period)                                                                                         
 set @cnt=1                                                                                                                 
                                   
  WHILE @cnt<= @count                              
  BEGIN                     
                                                                                                                                                 
  set @SnapShotDate=(select Quarter_FillingDt from @Quarter_Period where [Indx]=@cnt)                                                    
          
            
              
                  
  SET @Fdate= (select top 1 Proxy_FillingDt FROM Proxy_Period WHERE cover_id=@CoverID                
  and Proxy_FillingDt<=@SnapShotDate                   
  order by Proxy_FillingDt desc)                                                                                                                                  
                
 -----------------------------------------Section 2.1.1----------------------------------                                                                                                                             
                                                  
  SET @Outcome = NULL                                                                                                                                                                                            
  SET @sec='2.1.1'                                                                                                                 
  set @dcom1=null                                                                                                                                                                                                                                             
   
    
     
         
         
             
             
                                   
  set @dcom2=null                                                                                                                                            
  set @dcom3=null                                                                                                                                  
  set @dcom4=null                                                                                                                       
                                           
  select @dcom1=                                                                                                                                                                           
  ( select top 1 CommEqtShare from EventDataGov_MCCS                                                                                                                                                                         
  where FileDate <= @SnapShotDate                                                                                                                                                
  and CoverId=@CoverID order by FileDate desc)                                                                     
                
  select @dcom2=                                        
  COALESCE(@dcom2+',' , '') +                                                      
  FeildName from LookUpDetails                                                                                                                  
  where LookUpId                                                                                                                                                                                               
  in(                                  
  select top 1 CapOnStatusEffec from EventDataGov_EncumbranceCapon                                            
  where CaponRefFileDate <= @SnapShotDate                                                                  
  and CoverId=@CoverID order by CaponRefFileDate desc)                                                                                              
                
  select @dcom3=                                                                                                                     
  COALESCE(@dcom3+',' , '') +                                                                                                                                                               
  FeildName from LookUpDetails                                                                                     
  where LookUpId                               
  in(                                            
  select top 1 VotingRightsStatusEffec from EventDataGov_EncumbranceVotingRights                                                                                                                                          
  where VotingRightsRefFileDate <=  @SnapShotDate                              
  and CoverId=@CoverID order by VotingRightsRefFileDate desc)                                                                                                     
                
  select @dcom4=                                                                                                                          
  COALESCE(@dcom4+',' , '') +                                                                                                                                                                            
  FeildName from LookUpDetails                                                      
  where LookUpId                                                                                                                        
  in(                                                                                                                    
  select top 1 OtherStatusEffec from EventDataGov_Encumbranceother                                                                                                                                                           
  where OtherRefFileDate <= @SnapShotDate                                                                             
  and CoverId=@CoverID order by OtherRefFileDate desc)                                                                                                                                  
                                                                                                                                         
                       
  if(@dcom1 like '%True%')                                                                                                                                              
  if( ISNULL(@dcom2,'NO') like '%no%' and ISNULL(@dcom3,'NO') like '%no%' and ISNULL(@dcom4,'NO') like '%no%')                                                                    
   set @Outcome='Yes'                                                                                                                         
  else                                                                                     
   set @Outcome='No'                                                                                                                                                                                
  else                                                                                                                
   set @Outcome='No'                                                                                                               
  ----------------------------                                                                                                                                                                                                                                
  
    
      
       
  if @Outcome is null                                                                                                  
   set @Outcome ='No'                                                                                                                   
                                                                                                                                                                                                   
    insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                             
  select @CoverID,@sec,@SnapShotDate,@OutCome                                        
                                                                                                            
  -----------------------------------------Section 2.1.2----------------------------------                                             
                                                                                
  SET @sec='2.1.2'                                                                             
  SET @Outcome = NULL                                                                                                               
  set @RES1=null                                     
  set @RES2=null                                                                                                                                            
                
  select @RES1=                                                                                                                                                                                        
  COALESCE(@RES1+',' , '') +                                                        
  FeildName from LookUpDetails                                                                                                                                  
  where LookUpId                                                                                         
  in(                                                                                                                
  select top 1 DirResgStatusEffec from EventDataGov_dirresg                                                                                                                                                
  where DirResgRefFileDate <= @SnapShotDate                                                                                                      
  and CoverId=@CoverID order by DirResgRefFileDate desc)                                                                                                                 
                    
  select @RES2=                                                                                                                                                 
  COALESCE(@RES2+',' , '') +                                                                                         
  FeildName from LookUpDetails                                                                                                                                                   
  where LookUpId                                                                                                                                                                                                                                              
   
   
       
       
          
            
             
  in(                                                                                            
  select top 1 VotePolStatusEffec from EventDataGov_votepol                                                                                                                               
  where VotePolRefFileDate <= @SnapShotDate                                                                                                                                                                                                                    
  
    
      
        
         
  and CoverId=@CoverID order by VotePolRefFileDate desc)                                                                                                                                                                       
                                  
  set @Outcome=  isnull([dbo].[F_GetEvent_Sec15](@RES2,@RES1),'-')                                                
      
        
          
            
                
  insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                        
  select @CoverID,@sec,@SnapShotDate,@OutCome                                                      
                                                                                              
  -----------------------------------------Section 2.1.3----------------------------------                                                                                          
                                                                                    
  SET @Outcome = NULL                                                          
  SET @sec='2.1.3'                                                               
  set @comp33=null                                                                                                                                             
  set @comp34=null                 
                                                                                                                                    
  select @comp33=                                                                           
  COALESCE(@comp33+',' , '') +                                                                                                                                                                                            
  FeildName from LookUpDetails                                                                                                  
  where LookUpId                                                           
  in(                                                                                                                                               
  select top 1 SMCharterStatusEffec from EventDataGov_SMCharter                                                                                                                                                                                               
  
     
     
        
          
             
             
                                                                                                    
  where SMCharterRefFileDate <= @SnapShotDate                                                             
  and CoverId=@CoverID order by SMCharterRefFileDate desc)                                                                                                                                                                     
                
  select @comp34=                                                                                                                                                                     
  COALESCE(@comp34+',' , '') +   FeildName from LookUpDetails                                                                                                                       
  where LookUpId                                                                                                        
  in(                                                                
  select top 1 CntrlCompStatusEffec from EventDataGov_CntrlComp                                                                                                                                                                                                
  
    
      
        
          
            
              
                                                    
  where CntrlCompRefFileDate <= @SnapShotDate                                                                              
  and CoverId=@CoverID order by CntrlCompRefFileDate desc)                                                                  
                
                
  if @comp33 = 'yes' and @comp34 = 'yes'                                                            
  set @Outcome='Entrenchment Agnostic'                                                
  if @comp33 = 'no' and @comp34 = 'yes'                                                                                                                                                   
  set @Outcome='Pro-entrenchment '                                                                                                                    
  if @comp33 is null and @comp34 = 'yes'                                                                                                                        
          
            
              
                 
                
                
                
  set @Outcome='Pro-entrenchment '                                                          
  if @comp33 = 'no' and @comp34 is null                                                                                                                                                                                
  set @Outcome='Entrenchment Agnostic '                                                                                                                   
                
  if @comp33 = 'yes' and @comp34 = 'no'                                                                    
  set @Outcome='Pro-entrenchment'                                                                                                                     
  if @comp33 = 'yes' and @comp34 is null                                             
  set @Outcome='Pro-entrenchment'                                     
                
  if @comp33 = 'no' and @comp34 = 'no'                                             
  set @Outcome='Entrenchment Agnostic '                                                                               
  if @comp33 is null and @comp34 = 'no'                                                                                                                                                                                   
  set @Outcome='Entrenchment Agnostic '                                                                                                                                                                                                                  
                
  if  @Outcome is null                                                                
  set @Outcome='Entrenchment Agnostic '                                                                                                                                                                                                                       
  
    
      
        
           
           
              
                                                                                                                                                                  
                                                                                                     
  if @Outcome='Entrenchment Agnostic'  and @Outcome not like '% '                                                                                 
  set @Outcome='Supermajority is required; however, it’s a controlled company'                                                                                             
  if @Outcome='Entrenchment Agnostic '   and @Outcome  like '% '                                                                           
  set @Outcome='Supermajority is not required, and it’s not a controlled company'                                                                                                    
  if @Outcome='Pro-entrenchment ' and @Outcome  like '% '                                                                                                    
  set @Outcome='Supermajority is not required; however, it’s a controlled company'                                                                                                    
  if @Outcome='Pro-entrenchment' and @Outcome not like '% '                                                                                                    
  set @Outcome='Supermajority is required, and it’s not a controlled company'                                                                                               
                                                                                        
                                                                                                       
  insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                         
  select @CoverID,@sec,@SnapShotDate,@OutCome                                                                                                                                  
                                                                                                                                           
  -----------------------------------------Section 2.1.4----------------------------------                                                                                                                                                       
            
              
                
                
  SET @sec='2.1.4'                                                                                                                                     
  SET @Outcome = NULL                                                                                               
  set @r1=null                                                                         
  set @r2=null                                                          
                                                                                                                                          
  select @r1=                                                                       
  COALESCE(@r1+',' , '') +                                                                                                                                                           
  FeildName from LookUpDetails                                                                                                                                             
  where LookUpId                                                                                                                                      
  in( select top 1 SMMandAStatusEffec from EventDataGov_SMMandA                                                                                                                                                                                                
  
    
     
        
          
             
             
                                                                                                                
  where SMMandARefFileDate <= @SnapShotDate                                                                                                                                                                                                                    
  
    
      
        
          
            
              
                                                      
  and CoverId=@CoverID order by SMMandARefFileDate desc)                                                                                                       
                                                                                                            
  select @r2=                                                                                                            
  COALESCE(@r2+',' , '') +                           
  FeildName from LookUpDetails                                                                                                       
  where LookUpId                                                                                                                                     
  in( select top 1 CntrlCompStatusEffec from EventDataGov_cntrlcomp                                                                                                                  
  where CntrlCompRefFileDate <= @SnapShotDate                                                   
  and CoverId=@CoverID order by CntrlCompRefFileDate desc)                                                                                              
                                                
  if @r1 = 'yes' and @r2 = 'yes'                                                                                                                       
  set @Outcome='Entrenchment Agnostic'                                                                                                              
  if @r1 = 'yes' and @r2 = 'no'                             
  set @Outcome='Pro-entrenchment'                                                                                                                        
  if @r1 = 'yes' and @r2 is null                                                                          
  set @Outcome='Pro-entrenchment'                
  if @r1 = 'no' and @r2 is null                                                                             
  set @Outcome='Entrenchment Agnostic '                                                                                                                                   
                                                                                 
  if @r1 = 'no' and @r2 = 'yes'                                                
  set @Outcome='Pro-entrenchment '                                                                                                                                                                                
  if @r1 = 'no' and @r2 = 'no'                     
  set @Outcome='Entrenchment Agnostic '                                                                                                          
  if @r1 is null and @r2 = 'yes'                                                                                                                                                      
  set @Outcome='Pro-entrenchment '                                                                                                                      
  if @r1 is null and @r2 = 'no'                                                                                              
  set @Outcome='Entrenchment Agnostic '                                                                                                          
                                                                                                                                                      
  IF @Outcome is null                                                                                  
  set @Outcome='Entrenchment Agnostic '                                                                                                                                                                                                 
                                  
                                                   
  if @Outcome='Entrenchment Agnostic'  and @Outcome not like '% '                                                                                                    
  set @Outcome='Supermajority is required; however, it’s a controlled company'                                                                                                    
  if @Outcome='Entrenchment Agnostic '   and @Outcome  like '% '                                                                                                   
  set @Outcome='Supermajority is not required, and it’s not a controlled company'                                                                                                    
  if @Outcome='Pro-entrenchment ' and @Outcome  like '% '                                                                                              
  set @Outcome='Supermajority is not required; however, it’s a controlled company'                                              
  if @Outcome='Pro-entrenchment' and @Outcome not like '% '                                                                                                    
  set @Outcome='Supermajority is required, and it’s not a controlled company'                                                                                               
                          
                                                                                                                             
  insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                      
  select @CoverID,@sec,@SnapShotDate,@OutCome                                                
                                                                                                                                                                    
  -----------------------------------------Section 2.2.1----------------------------------                 
                                                                                                                                      
  SET @sec='2.2.1'                                                    
  SET @Outcome = NULL                                                                                                                                                    
  set @RES=null                                
  set @RES0=null                                                                                                                         
                
  select @RES=                                                                                                                                                              
  COALESCE(@RES+',' , '') +                                                              
  FeildName from LookUpDetails                                                                                                          
  where LookUpId                                                                                                                                                                                                                
  in(                                                                    
  select top 1 StagBoardStatusEffec from EventDataGov_StagBoard                                                                                                                                                                                               
  
    
      
        
          
                
  where StagBoardRefFileDate <= @SnapShotDate                                        
  and CoverId=@CoverID AND StagBoardRefExpDate IS NULL order by StagBoardRefFileDate desc)                                                                                                                                                  
                
  select @RES0=                                                                                            
  COALESCE(@RES0+',' , '') +                                                                                                                    
  FeildName from LookUpDetails                                                                                                                                                
  where LookUpId                                                                                                                                                                              
  in(                                      
  select top 1 StagBoardStatusEffec from EventDataGov_StagBoard                                                          
  where  @SnapShotDate between  StagBoardRefFileDate  and StagBoardRefExpDate                                                                                                    
  and CoverId=@CoverID AND StagBoardRefExpDate is not NULL order by StagBoardRefFileDate desc)                                                 
                
  if @RES0 is not null                                                              
  set @Outcome ='In The Process Of Converting Into A Non-Staggered Structure'                                                                                                                                              
                
  if @RES0 is null                                                                                                                                                                                 
  set @Outcome = @RES                                                                                                       
                
  if(                                                                                                           
  (                                                                                               
  select top 1 StagBoardRefExpDate from EventDataGov_StagBoard                                                                                               
  where CoverId=@CoverID AND StagBoardRefExpDate IS not NULL                                                                                                                                                                                                  
  
    
      
         
         
            
              
                         
  order by  StagBoardRefExpDate desc)                                 
  <                                                                                                             
  @SnapShotDate)                                                                                                                                              
  set @Outcome='No'                                                                                 
                
  if(                                                                                                                  
  ( select top 1 StagBoardRefExpDate from EventDataGov_StagBoard                                                                      
  where CoverId=@CoverID AND StagBoardRefExpDate IS not NULL                                                                                                                                               
  and StagBoardRefExpDate <= @SnapShotDate                                                                                           
  order by  StagBoardRefExpDate desc)                                                                                                                                 
  <                                                                                                         
  @SnapShotDate  ) and (                                                                                             
  select top 1 StagBoardRefFileDate from EventDataGov_StagBoard                                         
  where CoverId=@CoverID                                                                                  
  and StagBoardRefFileDate >= @SnapShotDate                                                                                                                                           
  order by  StagBoardRefFileDate asc)                                                                                                                   
  >                                                                               
  @SnapShotDate                                      
  and                                                                                                                  
  (select top 1 StagBoardStatusEffec from EventDataGov_StagBoard                                                                                                                                                                                              
   
    
      
        
         
            
             
  where  @SnapShotDate between  StagBoardRefFileDate  and StagBoardRefExpDate                                                                                                                       
  and CoverId=@CoverID AND                                                                         
  StagBoardRefExpDate is not NULL order by StagBoardRefFileDate desc) is null                                                                                                                                              
  and                                                                                                    
  (select top 1 StagBoardRefFileDate from EventDataGov_StagBoard                                         
  where CoverId=@CoverID            
  and StagBoardRefFileDate <= @SnapShotDate                                                                         
  order by  StagBoardRefFileDate desc)<                                                                                                                                              
  (select top 1 StagBoardRefExpDate from EventDataGov_StagBoard                                                                                                                          
            
             
                
  where CoverId=@CoverID                                                                               
  and StagBoardRefExpDate <= @SnapShotDate                                                   
  order by  StagBoardRefExpDate desc)                                                                                                                                               
                
  set @Outcome='No'                                                                                                                   
                
  if @Outcome is null                                                                                                                  
  set @Outcome='No'                                                                                                                                                                                                       
                                                                                                                                              
  insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                                   
  select @CoverID,@sec,@SnapShotDate,@OutCome                                                                                                                                                              
                                                                                                                     
  -----------------------------------------Section 2.2.2.1----Excluded---------------------------                                                                                                  
                                                                                                                                                                                        
   SET @Outcome = NULL                                                                                       
   SET @sec='2.2.2.1'                                                                                                                                                                                         
                
   set @Outcome=0                                                                                                                                            
                                                                                                                                                                                      
    insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                           
  select @CoverID,@sec,@SnapShotDate,@OutCome                                                                                                                                                                   
                                                                                 
  -----------------------------------------Section 2.2.2.2----------------------------------                                                                                                                                          
                                                                                                                                             
  SET @Outcome = NULL                        
  SET @sec='2.2.2.2'                                                                                                                                            
  set @val=null                                                   
  set @dtyes=null                                                                                       
  set @dtyes= (select top 1 SRPERRefFileDate from EventDataGov_SRPER where CoverId=@CoverID                                                                                                                                   
  and SRPERRefFileDate <= @SnapShotDate                                        
  and SRPERStatusEffec=26--y                                                                                             
  order by SRPERId desc)                                                                                                                          
                              
  if @dtyes is not null                                                                  
  set @val= 'Yes'                                                                                                                                          
  else                                                                                                                                         
  set @val= 'No Shareholder Rights Plan'                                                                                                                                                                       
                                                
  select @Outcome =  case   when isnull([dbo].[F_GetEvent_Sec23] (@val,@coverid,@SnapShotDate),'No Shareholder Rights Plan')                                                                                                                                   
 
    
      
        
          
             
             
               
  ='No Shareholder Rights Plan'                                                                                 
  then 'No Shareholder Rights Plan'                                                                                                                                                                                                                            
  
    
      
        
          
            
              
               
  else                                 
  isnull([dbo].[F_GetEvent_25](@val,@coverid,@SnapShotDate),'No Shareholder Rights Plan')  end                                                                                              
                
  insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                      
  select @CoverID,@sec,@SnapShotDate,@OutCome                                                                                                                                                                                                                 
   
    
      
        
          
            
              
                                                                                              
                                                                                                                                               
  -- -----------------------------------------Section 2.2.2.3----------------------------------                                                                                     
                
  --SET @Outcome = NULL                                                                      
                                                                                                                                        
  SET @sec='2.2.2.3'                                                                                                                                    
  --set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                 
                  
  if(@Outcome!='No Shareholder Rights Plan')                
  begin                                                                       
    set @dtyes1=null                                                                                                  
    set @dtyes1= (select top 1 SRPERRefFileDate from EventDataGov_SRPER where CoverId=@CoverID                                           
    and SRPERRefFileDate <= @SnapShotDate                                                                                                                                                              
    and SRPERStatusEffec=26--y                                
    order by SRPERId desc)                                                                                                                                                                                                                                     
  
   
       
       
           
            
              
                                                    
                                                                                               
    if @dtyes1 is not null                               
    set @Out= 'Yes'                                                                                        
    else                                                                               
    set @Out= 'No Shareholder Rights Plan'                                                                                                                                
                
    set @Outcome = case   when isnull([dbo].[F_GetEvent_Sec23] (@Out,@coverid,@SnapShotDate),'No Shareholder Rights Plan')                                                                                                                                     
  
    
      
        
         
             
              
                            
    ='No Shareholder Rights Plan'                                                                                                      
    then 'No Shareholder Rights Plan'                                                                                             
    else                                                                                                                                                      
    isnull([dbo].[F_GetEvent_26](@Out,@coverid,@SnapShotDate),'No') end                                                                                                                           
                                                                                                                              
    if isnumeric(@Outcome)=1                            
    set @Outcome= convert(decimal(18,0),@outcome)                                                                                                                                                                                                             
   
    
      
        
          
           
              
                               
                                                                                                                     
      insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                      
    select @CoverID,@sec,@SnapShotDate,@OutCome                                                  
     end                 
     if(@Outcome='No Shareholder Rights Plan')                
  begin                                                                                                                                          
                                                           
      insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                      
    select @CoverID,@sec,@SnapShotDate,@OutCome                                                                              
     end                                                                                
  -----------------------------------------Section 2.2.2.4-------Excluded---------------------------                       
  --SET @Outcome = NULL                                                                                                                                                                                               
  SET @sec='2.2.2.4'                                                                                                                                                                                                                                           
 
     
     
       
  --set  @Outcome = 0                                                               
                                                                                                                                  
  insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                  
  select @CoverID,@sec,@SnapShotDate,'0'                                                                                                                                                            
  -----------------------------------------Section 2.2.2.5----------------------------------                                                                                                                                                                 
                                                           
  --SET @Outcome = NULL                                                                                                                 
  SET @sec='2.2.2.5'                
  if(@Outcome!='No Shareholder Rights Plan')                
  begin                                                                                                   
   set @dtyes=null                 
   set @val=null                                                                                                                             
   set @dtyes= (select top 1 SRPERRefFileDate from EventDataGov_SRPER where CoverId=@CoverID                                                                                
   and SRPERRefFileDate <= @SnapShotDate                                                                                                                                                                                     
   and SRPERStatusEffec=26--y                                                                                                                                                                           
   order by SRPERId desc)                                                                                                                                    
                
   if @dtyes is not null                                                                                                                   
   set @val= 'Yes'                                                                                                                
   else                                           
   set @val= 'No Shareholder Rights Plan'                                                                                        
                
   select @Outcome =  case                                                                                                                                                                                                                                    
  
     
      
       
           
           
               
                                           
   when isnull([dbo].[F_GetEvent_Sec23] (@val,@coverid,@SnapShotDate),'No Shareholder Rights Plan')                                                                                                                                                            
  
    
      
        
          
            
              
                              
   ='No Shareholder Rights Plan'                                                            
   then 'No Shareholder Rights Plan'                                                                   
   else                                                           
   isnull([dbo].[F_GetEvent_28](@val,@coverid,@SnapShotDate),'No') end                                                                                                                                                           
                
   insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                    
   select @CoverID,@sec,@SnapShotDate,@OutCome                                                                                                                                             
      end                
                      
     if(@Outcome='No Shareholder Rights Plan')                
  begin                                                                                                   
   insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                    
   select @CoverID,@sec,@SnapShotDate,@OutCome               
      end                
                                                                                                                                                    
   -----------------------------------------Section 2.2.2.6----------------------------------                                                      
  --SET @Outcome = NULL                                                                                                                                                                                        
  --set @var=(select DeriveMeasureVariable from @GSM_MASTER where GSM_NUM=@sec)                                                                        
               
  SET @sec='2.2.2.6'                
  if(@Outcome!='No Shareholder Rights Plan')                
  begin                 
                                                            
   set @dtyes1=null                                                                                                                                                                                                     
   set @dtyes1= (select top 1 SRPERRefFileDate from EventDataGov_SRPER where CoverId=@CoverID                                                                                    
   and SRPERRefFileDate <= @SnapShotDate                                                                                                                                                                                   
   and SRPERStatusEffec=26--y                                                                                                                                                                                                                                  
  
    
   order by SRPERId desc)                                                                                                  
                
   set @dtno1= (select top 1 SRPERRefFileDate from EventDataGov_SRPER where CoverId=@CoverID                                                                                           
   and SRPERRefFileDate <= @SnapShotDate                                                                                                                 
   and SRPERStatusEffec=27--n                                                                                                                                                              
   order by SRPERId desc)                                                                                   
                                                                
   if @dtyes1 is not null        
   set @Out= 'Yes'                                                                           
   else                                                   
   set @Out= 'No Shareholder Rights Plan'                                                                                                         
                                        
   select @res29=                                       
   (                                                                                                                                                                   
   select top 1 SRPSCRefExpDate from EventDataGov_SRPsc                                                                                                                                             
   where SRPSCRefFileDate <= @SnapShotDate                                                                                              
   --and SRPSCRefExpDate is not null                                                                                                                                                        
   and CoverId=@CoverID order by SRPSCRefFileDate desc)                                                                                                                                                               
                
   if @res29 is null                                                                          
   set @out = 'No Sunset Clause'                                        
   if @res29 is not null and isdate(@res29)=1                                                                                                                                                                                           
   set @out1 =  ((convert(decimal,@res29) - convert(decimal,convert(datetime,@SnapShotDate)))/365)                                                                                                                                                             
 
    
      
         
         
             
             
                
                                                           
   if ISnumeric(@out1)=1                                                                                 
   begin                                                                                           
   set @out1 = abs(convert(decimal(18,3),@out1))                                                                                                               
   end                                  
                
   set @Outcome =  case when isnull([dbo].[F_GetEvent_26](@Out,@coverid,@SnapShotDate),'No')='No Shareholder Rights Plan'                                                                                                                                      
  
    
      
        
          
           
   then 'No Shareholder Rights Plan'                                                                                                                                               
   when isnull([dbo].[F_GetEvent_Sec23] (@Out,@coverid,@SnapShotDate),'No Shareholder Rights Plan')='No Shareholder Rights Plan'                                                                                                                               
  
    
     
        
          
           
              
                                              
   then 'No Shareholder Rights Plan'                                                                                                                                                         
   else                                                                                                                    
   isnull(@out1,'No') end                                                                                                                  
                                                                                                        
   set @dtyes2= (select top 1 SRPERRefFileDate from EventDataGov_SRPER where CoverId=@CoverID                                                                                                                          
   and SRPERRefFileDate <= @SnapShotDate                                                                                                                                                                                             
   and SRPERStatusEffec=26--y                                                                                                                                                                                                                       
   order by SRPERId desc)                                                           
                
   if @dtyes2 is not null                                                          
   set @valx= 'Yes'                                                                                                                        
   else                                                                                                                                                                       
   set @valx= 'No Shareholder Rights Plan'                                                                                     
                
   select @setif =  case when isdate([dbo].[F_GetEvent_Sec23] (@valx,@coverid,@SnapShotDate))=1 then 'Yes'                                                                                                                                                     
  
   
       
        
          
            
              
                
   when isnull([dbo].[F_GetEvent_Sec23] (@valx,@coverid,@SnapShotDate),'No Shareholder Rights Plan')                                                                                                                                                           
  
    
      
        
          
            
              
                 
                
   ='No Shareholder Rights Plan'                                      
   then 'No Shareholder Rights Plan'                                                                                                       
   when isnull([dbo].[F_GetEvent_Sec23] (@valx,@coverid,@SnapShotDate),'No Shareholder Rights Plan')                                                                                                                                                           
  
    
      
        
         
             
   ='No Sunset Clause'                                
   then 'No Sunset Clause'                                                                             
   else 'No' end                                                                                  
   if @setif='No'                                                                                                                    
   set @Outcome = 'No'                                                              
                        
   if @setif='No Sunset Clause'                                                                                   
   set @Outcome = 'No Sunset Clause'                                                                                                                               
   else                             
   if(isnumeric(@Outcome)=1)                                   
   begin                                                                        
                
   set @Outcome= convert(decimal(18,2),@Outcome)                                                                                                                                                                               
   end                                                                                                                                      
   else                                                                
   set @Outcome='No Shareholder Rights Plan'                 
   insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                           
   select @CoverID,@sec,@SnapShotDate,@OutCome                                                                  
     end                                                
     if(@Outcome='No Shareholder Rights Plan')                
  begin                      
   insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                           
   select @CoverID,@sec,@SnapShotDate,@OutCome                                                                                                                             
     end                
                     
     -----------------------------------------Section 2.2.2.7----------------------------------                                                                                                                                                               
  
     
     
         
          
           
               
                   
  --SET @Outcome = NULL                                                                   
  SET @sec='2.2.2.7'                
  if(@Outcome!='No Shareholder Rights Plan')                
  begin                                                                                                                            
   set @var=(select DeriveMeasureVariable from @GSM_MASTER where GSM_NUM=@sec)                                                                                    
   set @dtyes1= (select top 1 SRPERRefFileDate from EventDataGov_SRPER where CoverId=@CoverID                                                                                                                                                                  
  
    
      
       
           
   and SRPERRefFileDate <= @SnapShotDate                        
   and SRPERStatusEffec=26--y                                               
   order by SRPERId desc)                                                         
                                                                                             
   set @dtno1= (select top 1 SRPERRefFileDate from EventDataGov_SRPER where CoverId=@CoverID                                                               
   and SRPERRefFileDate <= @SnapShotDate                                   
   and SRPERStatusEffec=27--n                     
   order by SRPERId desc)                                                                                                                                         
   if @dtyes1 is not null                                                 
   set @Out= 'Yes'                                                                                                                                                                            
   else                                                                                                               
   set @Out= 'No Shareholder Rights Plan'                                                                                                                                                   
                                                           
   set @res29 =(select top 1 SRPERRefFileDate from EventDataGov_SRPer                                                                                                                    
   where SRPERRefFileDate <= @SnapShotDate and CoverId=@CoverID order by SRPERRefFileDate desc)                                                                               
                
   if @res29 is not null                                                                                                                                                                                                                                      
   
    
      
       
   set @out1= ((convert(decimal,@res29) - convert(decimal,convert(datetime,@SnapShotDate)))/365)                                                                                                                                                               
  
   
                
   set @Outcome =   case  when isdate([dbo].[F_GetEvent_26] (@out,@coverid,@SnapShotDate))=1 then 'Yes'                                                     
   when isnull([dbo].[F_GetEvent_Sec23] (@out,@coverid,@SnapShotDate),'No Shareholder Rights Plan')                                                                                                                                                            
  
   
       
        
         
            
              
                    
   ='No Shareholder Rights Plan'                                                                                                                                                                       
   then 'No Shareholder Rights Plan'  else                                                                                                                                                                                                                     
  
   isnull(@out1,'No') end                  
   IF ISNUMERIC(@Outcome)=1                                                                                                     
   SET @Outcome = ABS(CONVERT(DECIMAL(18,3),@Outcome))                                                                           
   if(isnumeric(@Outcome)=1)                                                                                                                                                                             
   begin                                                                                                                                                                                         
   set @Outcome=CONVERT(DECIMAL(18,2),@Outcome)                  
   END                                                   
  end                
                  
     if(@Outcome='No Shareholder Rights Plan')                
  begin                                                                                                                                                                                                                                          
   insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                        
   select @CoverID,@sec,@SnapShotDate,@OutCome                                                                                                                             
     end                
                       
  insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                           
  select @CoverID,@sec,@SnapShotDate,@OutCome                    
                                                                         
  -----------------------------------------Section 2.2.3----------------------------------                                                                                                                                                                    
   
    
      
       
          
             
              
                 
       
  SET @Outcome = NULL                                                                                                                                                                                                                 
  SET @sec='2.2.3'                                                                                
                
  set @dtyes_dcl= (select top 1 BOIShareAppRefFileDate from EventDataGov_BOIShareApp where CoverId=@COVERID                                                                                                        
  and BOIShareAppRefFileDate <= @SnapShotDate                                                                                                                                              
  and BOIShareAppStatusEffec=26--y                                                                                                                                                            
  order by BOIShareAppId desc)                                                                                                                                                            
                
  set @dtno_dcl= (select top 1 BOIShareAppRefFileDate from EventDataGov_BOIShareApp where CoverId=@COVERID                                                                                          
  --and BOIShareAppRefFileDate <= @SnapShotDate                                                                                                                                
  and BOIShareAppRefFileDate>=@dtyes_dcl                                                                                    
  and BOIShareAppStatusEffec=27--n                                                                                                           
  order by BOIShareAppId desc)                                                                                            
                                                  
  set @BOIShareAppId = (select top 1 BOIShareAppId from EventDataGov_BOIShareApp where CoverId=@COVERID                                                                                                
  and BOIShareAppRefFileDate between @dtyes_dcl and ISNULL(@dtno_dcl,'2100-01-01') order by BOIShareAppId asc)                                                                                                                                                 
  
   
      
         
          
            
              
                                                                                                         
                                   
  select @resshare = (select FeildName from LookUpDetails                                                                                                                                                 
  where LookUpId                                                                                                                
  in(                                                        
  select top 1 BOIShareAppStatusEffec from EventDataGov_boishareapp                                                                                                      
  where                                                                                                                                                                         
  BOIShareAppRefFileDate<=@SnapShotDate                                                                                                                                                                                          
  and CoverId=@CoverID order by BOIShareAppRefFileDate desc))                                      
  if @resshare is null                                                                                 
  set @resshare='No'                                      
                                                                                                                                        
  select @resdcl = (select top 1 FeildName from LookUpDetails                                                                                                                                                                 
  where LookUpId                                                                 
  in(                                                                                                                                                                                                                                           
                
  select top 1 BOIdeclawStatusEffec from EventDataGov_boideclaw                                                                                                                                                                                        
  where BOIdeclawRefFileDate <=@SnapShotDate                                                                                                  
  and CoverId=@CoverID                                                                
  order by BOIdeclawRefFileDate desc                                                                                                                                      
  ))                                                       
                
  if @resdcl is null                                                                                                 
  set @resdcl='No'                                                          
                                                                                  
  if @resdcl like '%yes%' and @resshare like '%yes%'                                                          
  set @Outcome='Yes_declawed'                                                                                                                                                                                                               
  if (@resdcl like '%no%' or @resdcl is null) and @resshare like '%yes%'                                                                                                                                                         
  set @Outcome='Yes'                                                                                       
  if @Outcome is null                                                                                  
  set @Outcome='No'                                                                                                                                                          
                
  insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                              
  select @CoverID,@sec,@SnapShotDate,@OutCome                                                            
                                                                                                
  -----------------------------------------Section 2.3.1----------------------------------                             
                                                       
  SET @Outcome = NULL                                                                                                     
  SET @sec='2.3.1'                                                                                                                                      
  set @var=(select DeriveMeasureVariable from @GSM_MASTER where GSM_NUM=@sec)                                                                                                                
  select @Outcome=                                                                                                 
  COALESCE(@Outcome+',' , '') + FeildName from LookUpDetails                                                                                 
  where LookUpId                                                                                           
  in(                                                                                                                                                
  select top 1 SMRStatusEffec from EventDataGov_smr                                                                                                                                                 
  where SMRRefFileDate <= @SnapShotDate                                                                                                                                        
  and CoverId=@CoverID order by SMRRefFileDate desc)                                                                                                                                                                                                          
  
    
       
       
          
            
              
                 
                
  if @Outcome is null or @Outcome like '%no%'                                                                                                                                           
  begin                                                                                                            
  set @Outcome='No Special Meeting Rights'                                            
  end                                                                                                       
  else                                                                 
  begin                                                                                                                                                                     
   select @Outcome=                                                                                                                                                                               
          
            
              
   (                                                                                                                                  
   select top 1 SMRorStatusEffec from EventDataGov_smror                                                                                                                                                                                                       
  
   
      
         
          
            
             
   where SMRorRefFileDate <= @SnapShotDate                                                                                                                                                                                                       
   and CoverId=@CoverID order by SMRorRefFileDate desc)                                                                                                 
  end                 
                                                                                                                        
  if isnumeric(@Outcome)=1                                                                                                                          
   set @Outcome=  convert(decimal(18,0),@outcome)                                                   
                
  insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                              
    
  select @CoverID,@sec,@SnapShotDate,@OutCome                                                                                                                         
                                                                                                                                                               
  -----------------------------------------Section 2.3.2----------------------------------                                                                                                                                                                    
   
    
      
       
           
            
              
                 
                                                            
  SET @Outcome = NULL                                                                                                                                                                                     
  SET @sec='2.3.2'                                                                                                                   
                                                                                                         
  select @Outcome=                      
  COALESCE(@Outcome+',' , '') + FeildName from LookUpDetails                                                                                                                                                                                                   
 
     
      
        
          
           
              
                      
  where LookUpId                                                                                                                                         
  in(                                                                                                                                                                           
  select top 1 SWConsentStatusEffec from EventDataGov_SWConsent                                                                                                                                                                                                
  
    
      
        
          
            
              
                      
  where SWConsentRefFileDate <= @SnapShotDate                                                                                                                                                                                                                  
  
    
      
        
          
            
              
                                                                 
  and CoverId=@CoverID order by SWConsentRefFileDate desc)                                                                                                                                                                         
                                                                                     
  if @Outcome is null                                      
  begin                                                                                                                                                               
  set @Outcome='No'                                                                                                                                                
  end                                                                                                                                             
                                                                                                                                 
  insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                         
  select @CoverID,@sec,@SnapShotDate,@OutCome                                                                                                                
                                                                                                        
   -----------------------------------------Section 2.4.1----------------------------------                                                                  
                                                                                                                      
  SET @sec = '2.4.1'                                                                                                                                                       
  set @Outcome=null                                                                                                                                       
  set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                                                                              
  set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(3.00,0) AS INT)+2)                                                                                                                
                                                                                                                                      
  SET @Outcome = (SELECT ISNULL(COUNT([VLRNDId]),0)                                                                                        
  FROM  dbo.EventDataGov_VLRND                                                                                                          
  WHERE CoverId = @CoverID AND [VLRNDRefFileDate] BETWEEN @dtmax3 AND @SnapShotDate)                                                                                                                                                                           
  
    
      
        
         
            
               
                                                                                                                 
  insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                                                
  
    
  select @CoverID,@sec,@SnapShotDate,@OutCome                                                                                                                     
                                                      
  -----------------------------------------Section 2.4.2----------------------------------                                                                                                                                                                     
  
    
      
        
          
            
              
                  
  SET @sec = '2.4.2'                                                                                                                         
  set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                
                                                                                                       
  SET @Outcome = (SELECT ISNULL(COUNT([SuspTradeId]),0)                                                                                                   
  FROM  dbo.EventDataGov_SuspTrade                                                                                                                                              
  WHERE CoverId = @CoverID AND [SuspTradeRefFileDate] BETWEEN @dtmax3 AND @SnapShotDate)                                                                                                                                                   
                
  insert into AggMedianPreprocessData2_Daily_Third([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                         
  select @CoverID,@sec,@SnapShotDate,@OutCome                                                                
                                                                                                                                  
  set @cnt=@cnt+1                                                                              
                                                       
                                                                                      
 END                                                                                                                                      
                                                                                                                                   
-- ------------------------- End of Calculation                                                                                                                                                       
   END ------END Check for scorable company         
         
      declare @MINDT date                
SET @MINDT=(select convert(varchar,DATEADD(Day,365*3,MIN(Proxy_FillingDt)),101)  from Proxy_Period where Cover_Id=@coverid)                      
                
IF((@SnapShotDate IS NOT NULL) AND (@SnapShotDate<@MINDT))                 
BEGIN         
        
  UPDATE AggMedianPreprocessData2_Daily_Third set outcome=''                   
  where cover_id=@coverid                  
  and snapshotdate=@SnapShotDate                  
  and GsmNo in ('2.4.1','2.4.2'                
  )         
          
  end        
                                      
                                                                                                                                                                                                                             
END 