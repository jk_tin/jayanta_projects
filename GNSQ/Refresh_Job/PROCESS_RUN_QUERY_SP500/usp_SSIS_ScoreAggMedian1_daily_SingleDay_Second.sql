USE [LiveGSQDB_8th_Nov_2018]
GO
/****** Object:  StoredProcedure [dbo].[usp_SSIS_ScoreAggMedian1_daily_SingleDay_Second]    Script Date: 04/03/2019 11:50:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                                                                                                                                                      
-- =============================================                                                                                                                                                                                                               
 
-- Author:  Arindam Mukhopadhyay                                                                                                                                                                                                                
-- Create date: 23/05/2013                                                                                                                                                                                                                
-- Description: Get Score outcome for section 1                                                                                                                                                                                                                
-- =============================================                                                                                                                                                                          
ALTER Procedure [dbo].[usp_SSIS_ScoreAggMedian1_daily_SingleDay_Second]                                                                                                                                                                                       
  
           
      
        
                    
            
              
                
(                                                                                                                                                                                                                        
 @CoverID varchar(100),          
 @snapdate date                                                                                                                                                                                                                
)                                                                                                                                                                                                                        
                                                                                                                                                                                                                                              
AS                                                                                                                                                                                                                         
BEGIN                                                                                                                                                             
                                                                   
   SET ARITHABORT OFF;                                                                
SET ANSI_WARNINGS OFF                                                                
                                                                                                                                                         
  delete from AggMedianPreprocessData1_Daily_Second where Cover_id=@CoverID  and snapshotdate=@snapdate                                                                                                                                                     
                                                                                               
   ----------------------------------------------From Date Checking-------------------                                                                                            
  declare @lastUpdateDate date                                            
  declare @toDate date                                
  declare @fromDate date                              
  declare @ScoreMedianFromDate date                   
   declare @OTH_VARIABLE_1 decimal(18,3)                                    
                         
    set @OTH_VARIABLE_1=(select MAX(OTH_VARIABLE_1) from preprocess_Measure_Variable_Instance_Details                                                                   
    where GSM_DESC like '%trailing%')                                                                               
                                           
                                                
 -- set @FromDate=(select convert(varchar,DATEADD(Day,365*3,MIN(Proxy_FillingDt)),101)  from Proxy_Period where Cover_Id=@CoverID)                                                                           
                                                  
                                               
                                                            
 --set @ScoreMedianFromDate= CONVERT(DATETIME, (select top 1 Proxy_FillingDt from Proxy_Period                                                                        
 --where Cover_Id=@CoverID                                                            
 --order by Proxy_FillingDt asc)) +(365* CAST(ROUND(@OTH_VARIABLE_1,0) AS INT))                                                                                                             
                                                                                                     
 -- if(@FromDate>=@ScoreMedianFromDate)                                                                                                                               
 --set @ScoreMedianFromDate=@FromDate                                                                                                             
                                                                                                             
                                                                                                                               
 --set @minquarterDate=(select MIN(Quarter_EndDt) from Quarter_Period where Cover_Id=@CoverID)                                                                                                                                                            
  set @lastUpdateDate=(select distinct TOP 1 Last_Update_Date from Company_Submission_Details                                                                                                                          
where Cover_Id=@CoverID ORDER BY Last_Update_Date DESC)                                                                                                                          
------Check for scorable company                                                                                                
--IF @lastUpdateDate>=@ScoreMedianFromDate                                                                                                
  BEGIN                                                                           
                                                                                                                                                                                                                                                            
                                                                                                                                                                                                                        
  ---------------------------------------From Date Checking----------------------                                                                                                                                           
                                                                                                                          
                                                    
 declare @dtmax3 datetime  = NULL               
    declare @var decimal(10,2)                      
    DECLARE @NoofRestatement   DECIMAL(18,3)= 0.000                 
                   
                   
                       
                        
                          
                           
                               
                               
                                   
 DECLARE @FENDDateCalculated DATETIME                                                           
                                                                       
                                                                                  
                            
                                                                                  
declare @Quarter_Period TABLE(                                                                                                                            
 [Indx] INT IDENTITY(1,1) NOT NULL,                                               
[Quarter_FillingDt] date                                          
 )                                                                
                                                    
-- insert into @Quarter_Period([Quarter_FillingDt])                                              
-- select Quarter_EndDt from Quarter_MasterData where Quarter_EndDt>=@ScoreMedianFromDate                                                                 
-- and Quarter_EndDt<=@lastUpdateDate                                                                            
--  order by   Quarter_EndDt desc                                                                                       
                                                                                
--if (select COUNT(*) from @Quarter_Period where [Quarter_FillingDt]=@lastUpdateDate)=0                                                                                        
--  begin                                                                                        
--  insert into @Quarter_Period([Quarter_FillingDt])                                                                                                            
-- select @lastUpdateDate                                                                            
-- end                               
                             
     ------------------********************-----------------                              
 delete from @Quarter_Period                              
                              
  insert into @Quarter_Period                              
 values(@snapdate)                                                                                                            
                                                                                                
---end-----                                                                                                                                
                                                                                                              
----LookUp Table                                                                                                                     
  DECLARE @LookUpDetails TABLE (                                                                                                                                                                
   [LookUpId] int  NULL,                                                                                                                                                         
   [FeildName] varchar(150)  NULL                                                                         
)                                                                                                                                     
insert into @LookUpDetails([LookUpId],[FeildName])                                
select [LookUpId],[FeildName] from LookUpDetails                                         
                                                                                                                  
                                 
                                             
                                                  
----EventDataEnv_Social                                                                           
DECLARE @EventData_MaterialWeakness TABLE (                                     
   [MaterialWeaknessId] int  NULL,                                                                                                                                                            
   [CoverId] uniqueidentifier  NULL,                                          
   [iniDisclosureDate] datetime NULL  ,                                          
   [rmdDisclosureDate] datetime                                                                                                                                                         
)                                                                                                                                                           
                                                              
insert into @EventData_MaterialWeakness([MaterialWeaknessId],[CoverId],[iniDisclosureDate],[rmdDisclosureDate])                                                
                                    
                                       
                                       
                                          
                                           
                                              
                                                 
select [MaterialWeaknessId],[CoverId],[iniDisclosureDate],[rmdDisclosureDate] from EventData_MaterialWeakness                                                                                                                                      
where CoverId=@CoverID                                                                                                                                                            
                                                                                                                                         
                                                                                                              
----EventData_SECReview                                                                                                                                                        
DECLARE @EventData_SECReview TABLE (                                                                              
   [EventDataSECId] int  NULL,                                                                              
   [CoverId] uniqueidentifier  NULL,                                                      
   [IniDisclosureDate] date NULL  ,                                                                                                                                              
   [CloseDisclosureDate] date NULL  ,                                                                                                   
   [RRRI_Ref] varchar(max),                                                                                                                                            
   [RERI_Ref] varchar(max),                                                                                                      
   [RIRR_Ref] varchar(max),                                                                                                                                           
   [RIPT_Ref]  varchar(max),                                                                                  
   [RIPC_Ref] varchar(max),                                                                                                                              
   [RIPE_Ref]  varchar(max)                                                                                                                                            
)                                                                                                                                                                                                                                                    
                      
insert into @EventData_SECReview([EventDataSECId],[CoverId],[IniDisclosureDate],[CloseDisclosureDate],[RRRI_Ref],[RERI_Ref],[RIRR_Ref],[RIPT_Ref],[RIPC_Ref],[RIPE_Ref])                                                   
                 
                    
                      
                        
                          
                            
                               
                               
                                   
                                   
                        
                                        
                           
                                            
                                             
                                                
                                                  
                                                    
                                        
                                                        
                                                          
                                                            
                                                              
                                                             
                                                                 
                                                                    
                                                                 
                                                                        
                                                                           
                                                                            
                                                                             
                                                                                
                                                        
                                                                                     
                                                                                     
                                                               
                                                                                      
                                                                                            
                                                                                   
                                                                                               
                                                                                                  
                                 
                                                                                                     
select [EventDataSECId],[CoverId],[IniDisclosureDate],[CloseDisclosureDate],[RRRI_Ref],[RERI_Ref],[RIRR_Ref],[RIPT_Ref],[RIPC_Ref],[RIPE_Ref] from EventData_SECReview                                      
where   [CoverId]=@CoverID                                                                                                                 
                                                                                                                                                          
                                                                                                                       
----EventDataEnv_Social                                                                                  
DECLARE @EventData_AuditorOpinion TABLE (                                                                                                                                                                
   [AuditorOpinionId] int  NULL,                                                                                                                 
   [CoverId] uniqueidentifier  NULL,                 
 [AuditorName] varchar(100),              
   [DisclosureDate] date NULL  ,                                                                                     
   [OpinionByAuditor] int                                   
)                                                                                                                                        
                                                           
insert into @EventData_AuditorOpinion([AuditorOpinionId],[CoverId],[AuditorName],[DisclosureDate],[OpinionByAuditor])                                                               
select [AuditorOpinionId],[CoverId],[AuditorName],[DisclosureDate],[OpinionByAuditor] from EventData_AuditorOpinion                                                                                  
where CoverId=@CoverID                       
                                                                                 
  ----Annual Data Table                                                                                                            
  DECLARE @Anual_Data TABLE (                                                                                                                     
   [Cover_Id] uniqueidentifier  NULL,                                                                                       
   [Date_Central_Id] int NULL,                                                                                                                                
   [NonAudit_Fees] DECIMAL(18,3) NULL,                                                        
   [Audit_Fees] DECIMAL(18,3) NULL,                                                                                                 
   [AuditRelated_fees] DECIMAL(18,3) NULL                                                                                                                                                    
)                                                           
                                                                                                           
insert into @Anual_Data([Cover_Id],[Date_Central_Id],[NonAudit_Fees],[Audit_Fees],[AuditRelated_fees])                                                                                                                             
select [Cover_Id],[Date_Central_Id],[NonAudit_Fees],[Audit_Fees],[AuditRelated_fees] from Anual_Data                                                                                                                                                          
  
    
      
        
           
           
              
                
                  
                    
                      
                        
where Cover_Id=@CoverID                                                                                                                    
----GSM_MASTER Table                                                                                                                                                                                             
  DECLARE @GSM_MASTER TABLE (                                                                                                
   [GSM_NUM] varchar(10)   NULL,                                                                                                                            
   [OTH_VARIABLE_1] decimal(10,2)  NULL,                                                      
   [OTH_VARIABLE_2] decimal(10,2)  NULL,                                                                                                         
   [OTH_VARIABLE_3] decimal(10,2)  NULL,                                                                                                                    
                        
   [DeriveMeasureVariable] decimal(18,3)                                                                       
)                                                                                                      
                            
insert into @GSM_MASTER([GSM_NUM],[OTH_VARIABLE_1],[OTH_VARIABLE_2]                                              
,[OTH_VARIABLE_3],[DeriveMeasureVariable])                                                                                       
                                                           
                                                                          
SELECT                                                                                             
[GSM_NUM],[OTH_VARIABLE_1],[OTH_VARIABLE_2]                                                                                                                                              
,[OTH_VARIABLE_3],                          
(case                                                                                                               
 when (OTH_VARIABLE_1>OTH_VARIABLE_2 and OTH_VARIABLE_1>OTH_VARIABLE_3) then OTH_VARIABLE_1                                                              
 when  (OTH_VARIABLE_2>OTH_VARIABLE_1 and OTH_VARIABLE_2>OTH_VARIABLE_3) then OTH_VARIABLE_2                                                                                                                                                 
 when  (OTH_VARIABLE_3>OTH_VARIABLE_1 and OTH_VARIABLE_3>OTH_VARIABLE_2) then OTH_VARIABLE_3                                                                                                                        
 end) DeriveMeasureVariable                                                                                                 
  FROM Preprocess_Measure_Variable_Instance_Details                                                                                                                         
                                                          
                                                                                                                                                                     
                                                                                                                                                                                                        
                                                                                                                                                                                         
 DECLARE @Outcome NVARCHAR(1000)=NULL                                                                     
                                                                                                             
                                                                                                                                                     
 DECLARE @Fdate DATETIME                                                                                                                                                     
                                                                                                                                                                 
                                                                                                                    
 DECLARE @dtmax DATETIME                                                                                                                                                 
 DECLARE @dtmin DATETIME                                                
                                                                                                  
                                                                                                    
                                               
                                                                                                        
                                                                                                     
        
                                                                                                  
                                                  
                                                                                                                 
                                                                                                               
                                                                                                                 
                                                                                                                        
 DECLARE @AuditFees   DECIMAL(18,3)= 0.000                                                                                                                                                 
 DECLARE @AuditRelatedFees DECIMAL(18,3) = 0.000                                                                                                                        
 DECLARE @NonAuditFees DECIMAL(18,3)= 0.000                                                   
 DECLARE @TotalAuditFees DECIMAL(18,3) = 0.000                                                                                                                      
 DECLARE @TotalNonAuditFees DECIMAL(18,2) = 0.000                                                                                                                           
                                                                     
                                                        
                                                                                                               
 DECLARE @sec VARCHAR(100) = NULL                                                                                                           
                                                                                                                                                 
 DECLARE @OtherVar1 DECIMAL(18,2)  = NULL                                                                                                                         
    DECLARE @OtherVar2 DECIMAL(18,2)  = NULL                                                                            
    DECLARE @OtherVar3 DECIMAL(18,2)   = NULL                         
                                                                                                                                                                
                                                                                                             
 ----Proxy_Period Table                                                                                                                                                                                  
  DECLARE @Proxy_Period TABLE (                                                                                                                                             
  [Proxy_ID] int NULL,                                                                                                                           
   [Fiscal_ID] int  NULL,                                                                                                                                                                                        
   [Cover_Id] uniqueidentifier  NULL,                                                                                                                                          
   [Proxy_FillingDt] date NULL                                                                                                                        
)                                                                                                           
                                                                                                                                               
insert into @Proxy_Period([Fiscal_ID],[Cover_Id],[Proxy_FillingDt])                                                                                                                       
select [Fiscal_ID],[Cover_Id],[Proxy_FillingDt] from Proxy_Period                                                                                                                   
where Cover_Id=@CoverID                                                                                                                                                                                                      
                                   
 DECLARE @SnapShotDate DATE                                                                                                                 
                                                                                                                                                                             
                    
 --------Start Calculation                                                                                                                 
  declare @count int                                                                                                                   
 declare @cnt int                                                                                                                  
  set @count=(select COUNT(*) from @Quarter_Period)                     set @cnt=1                                                                                                   
                                                                                                                                                                   
 WHILE @cnt<= @count                                                   
 BEGIN                                                                                                                                         
 set @SnapShotDate=(select Quarter_FillingDt from @Quarter_Period where [Indx]=@cnt)                      
                                                    
 SET @Fdate= (select top 1 Proxy_FillingDt FROM @Proxy_Period WHERE Proxy_FillingDt<=@SnapShotDate order by Proxy_FillingDt desc)                                                                                                         
                           
                                                                               
                                  
                                                                                                      
                                                     
                                                                                 
                                                                                                                                                                 
                                                                                             
-----------------------------------------Section 1.1.1----------------------------------                                                                                       
                                                                                                        
  SET @sec='1.1.1'                                                                                                                 
   set @var=(select DeriveMeasureVariable from @GSM_MASTER where GSM_NUM=@sec)                                                                                                                                        
   SET @AuditFees=0                                                                                                                                           
   SET @AuditRelatedFees=0                                                                                                                                         
   SET @NonAuditFees=0                               
   SET @TotalAuditFees=0                                                     
   SET @Outcome=NULL                                                                                                                                                  
                                                                                                                 
   SELECT  @AuditFees = ISNULL(AD.Audit_Fees,0) ,                               
    @AuditRelatedFees = ISNULL(AD.AuditRelated_fees,0),                                                                                                                                                                      
    @NonAuditFees = ISNULL(AD.NonAudit_Fees,0)                                              
    FROM @Anual_Data AS AD                                                                                                                                                      
    --JOIN Fiscal_Period FP                
    --ON FP.Cover_Id = AD.Cover_Id AND FP.Fiscal_ID = AD.Date_Central_Id                                                                                                
            
              
                
                  
                    
                      
                        
                         
    WHERE AD.Cover_Id = @CoverID                                             
    and  AD.Date_Central_Id in                                                                                                                             
  (                    
   select top 1 Fiscal_ID from Fiscal_Period where Cover_Id=@CoverId                                                                                                                                                     
   and Fiscal_ID in                                                                                                                                                     
   (select top 1 Fiscal_ID from @proxy_Period where Cover_Id=@CoverId                                                                                                                                                    
   and Proxy_FillingDt <=@Fdate order by Proxy_FillingDt desc                                                                                                                             
   )                                                                                                            
  ) --order by FP.Fiscal_EndDt desc                                                               
                               
    SET  @TotalAuditFees = ISNULL((@AuditFees + @AuditRelatedFees + @NonAuditFees),0)                                                            
                                                              
                                                                                                                                                             
    IF @TotalAuditFees = 0                                                        
     SET  @Outcome =0.00                                                                                                                                       
    ELSE                                       
     BEGIN                                                              
        SET  @TotalNonAuditFees = (@NonAuditFees / @TotalAuditFees)* 100                                                                                                                                                                                      
  
    
      
        
           
           
              
                 
                 
                    
                       
                       
                          
                            
                              
                                
                                  
                                    
                                      
                                        
                                          
                              
                                              
        SET  @Outcome = @TotalNonAuditFees                                                          
     END                                                                                                                                
                                                                                                             
    insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                    
    select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                    
                                                                                                                           
                             
    -----------------------------------------Section 1.1.2----------------------------------                                                                                                                      
                                                                                                                           
  SET @sec = '1.1.2'                                                                                                                           
 SET @dtmin = NULL                                                
  SET @Outcome = NULL                                                                                                                                                                                                                                          
  
   
       
       
          
             
              
                
                  
                    
                      
                        
                          
                           
                              
                                
                     
                                   
                                                                                                      
  SET @dtmin = (select top 1 DisclosureDate from @EventData_AuditorOpinion                                                                                                                                                   
  where DisclosureDate < = @SnapShotDate                                                                                                                                                              
  and CoverId = @CoverID order by DisclosureDate desc)                                                                                                                                                                                                      
                                                                                                                                      
    SELECT  @Outcome =   (SELECT FeildName FROM @LookUpDetails WHERE LookUpId =                                                                                                                                                              
      ((select TOP 1 OpinionByAuditor from @EventData_AuditorOpinion                                                                                                
      where CoverId=@CoverID    AND  DisclosureDate  between @dtmin  and @SnapShotDate order by DisclosureDate desc ) ))                                                                                                                                       
  
    
      
        
          
           
                                                                                                                                                                         
                                                                                                   
      insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                            
    select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                                         
                                                                      
    -----------------------------------------Section 1.1.3----------------------------------                                                                  
                                                                                   
                                                                                                                                 
   SET @sec='1.1.3'                                                             
    set @var=(select DeriveMeasureVariable from @GSM_MASTER where GSM_NUM=@sec)                                                                                                                                               
   SET @Outcome = null                                                                                   
   SET @dtmax3=NULL                                                               
                                                                                                                                   
   --SET @dtmax=( select top 1 DisclosureDate  from EventData_AuditorOpinion where disclosuredate>@SnapShotDate                 
              
                
                  
                   
                      
                        
                           
                           
                               
              
                                  
                                    
                                     
                                        
                                           
                                            
                                             
                                                
                                                  
                                                    
                                                      
                                                        
                                                          
                                                            
                                                              
                            
                                                                  
                                                                
                                                              
                                                                        
                                                                          
                                                                            
                                                                                                        
     -- and CoverId = @CoverID)                                                                                                             
                                                                                                                      
 set @dtmax3= convert(datetime,@SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                                                                                                                            
  
     
     
         
         
             
              
               
                                                                                                                        
    --SELECT  @Outcome =   (select count(AuditorId) from EventData_Auditor    where CoverId=@CoverID                                                                                     
    --       and  (AppointmentDate between @dtmax3  and @SnapShotDate or                                                                             
    --       ISNULL(DisclosureDate,'01-01-2100') between @dtmax3  and @SnapShotDate ))                    
                                           
             SELECT  @Outcome =    (select count(AuditorId) from EventData_Auditor                                                      
             where CoverId=@CoverID                                              
and  (AppointmentDate between @dtmax3  and @SnapShotDate or                                                                           
ISNULL(DisclosureDate,'01-01-2100') between @dtmax3  and @SnapShotDate                                                   
or (@SnapShotDate between AppointmentDate  and ISNULL(DisclosureDate,'01-01-2100'))))                                                                                                                          
                                                                                              
      insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                         
    select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                                                                                    
                        
 -----------------------------------------Section 1.1.4----------------------------------                                                                                                                                                                     
   
   
       
        
          
            
              
                
                 
                     
                     
                        
                           
                                                                                                                    
  SET @sec='1.1.4'                                       
  SET @Outcome = NULL                                                                                                                                                                        
  SET @Outcome = NULL                                                                         
                                                                                                                 
     set @Outcome=                                                                                                           
      (select top 1 AuditorName from EventData_Auditor                                                                                                                                                                        
      where AppointmentDate <= @SnapShotDate                                                                                                             
      and CoverId=@CoverID order by AppointmentDate desc)                                        
                                                                                                                                                                           
      if(@Outcome like '%PricewaterhouseCoopers%'                                                                                                                                                  
      or @Outcome like '%KPMG%'                                                                          
   or @Outcome like '%Ernst & Young%'                                                                                                             
       or @Outcome like '%Deloitte%'                                       
      )                                        
      set @Outcome = 'True'                                                                                                           
      else                                                                                                                                                                                         
      set @Outcome = 'False'       
                                                                                    
  insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                                
    select @CoverID,@sec,@SnapShotDate,CONVERT(varchar, @Outcome)                                                                                                      
                          
     -----------------------------------------Section 1.2.1----------------------------------                                                                                         
                                                                                                                                       
  SET @sec='1.2.1'                                                                                                                                               
  set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                                                                  
  SET @Outcome = NULL                                                                          
                                                                                                     
  --SET @dtmax=( select top 1 DisclosureDate from EventData_Restatement where disclosuredate>@SnapShotDate                                                                                                                                                    
   
    
      
       
          
            
              
                
                  
                    
                       
                        
                          
                            
                              
                               
                                  
                                    
                                      
                
                                                                             
                                                   
                                                  
                                             
                                                      
                                                        
                                                          
                                                            
                                                              
                                                                
                                                                   
                                                                   
                                                                       
                                                                        
                                                                         
                                                                            
                   
                                                                               
                                                                                  
                                                                                     
                                      
                                                                                        
                                                                                                                                          
                                                                                               
                                                                                                
                                                     
                                                                                                   
                                                
                                                                                                        
                                                       
                                                                                              
                                                                                                              
                                                                                                                
                    
                                                            
                                                                      
                                                                                            
                                                                                   
                                                                                                                     
                                                                                                               
     -- and CoverId = @CoverID)                                                 
     SET @NoofRestatement =null                                                                
  set @dtmax3= convert(datetime,@SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                           
                                                    
  SET @NoofRestatement = (SELECT COUNT(*) FROM                                                                                                                           (                                                                                     
  
   
       
        
          
            
              
                
                  
                   
                      
                         
                          
                            
                              
                                
                                 
                                        
                                          
                                            
                                              
                                                
                                                 
                                             
                           SELECT RestatementId FROM EventData_Restatement WHERE CoverId = @CoverID AND DisclosureDate                                               
                                                        
                                                         
                                                   
                                                              
                                                                                                                              
      BETWEEN @dtmax3  AND @SnapShotDate                                                                                                                                                                
                                                                                                                                                 
      ) AS x)                                                                                                                                                                   
                                                                                    
    SET @Outcome = (convert(int,@NoofRestatement))                                                                                                                         
                                                                                                                                                                                              
                                                                                                            
       insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                        
    select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                                                                              
                                                                                                                                               
    -----------------------------------------Section 1.2.2----------------------------------                                                            
                    
                      
                        
             
                           
                                                                                                                                                                                           
                                                                
     SET  @sec='1.2.2'                                                                                                                                                           
      SET @Outcome=0                                
      SET @Outcome = NULL                                                                                                                                        
      SET   @OtherVar2 = null                                                   
      SET   @OtherVar3 = null                                                                                                                              
                      
      SET   @OtherVar1 = (SELECT ISNULL(OTH_VARIABLE_1,0) FROM @GSM_MASTER where GSM_NUM = @sec)                                                                                                            
     SET   @OtherVar2 = (SELECT ISNULL(OTH_VARIABLE_2,0) FROM @GSM_MASTER where GSM_NUM = @sec)                                                                                                                                                               
   
    
     
         
         
             
              
               
                  
                     
                     
                          
                           
                               
                               
                                 
  SET   @OtherVar3 = (SELECT ISNULL(OTH_VARIABLE_3,0) FROM @GSM_MASTER where GSM_NUM = @sec)                                                                                                                                                     
                                                                                           
--   SET @Outcome =( SELECT  COUNT(*)                                       
-- FROM  QuarterlyData WHERE Quarter_ID                                          
-- IN (SELECT TOP (convert(int,@OtherVar3)) Quarter_ID FROM Quarter_Period WHERE Cover_Id = @CoverID                                                                                                                                             
--   AND Quarter_FillingDt < = @SnapShotDate  ORDER BY Quarter_FillingDt DESC)                                                                                                                                   
--   and ((nonRecChargesOfSales > = @OtherVar1) OR (nonRecGainsOfSales > = @OtherVar1)                                                                                                                                     
--   or (nonRecChargesOfNetAssets > = @OtherVar2) OR (nonRecGainsOfNetAssets > = @OtherVar2))                                                                                                        
--)                                                                                 
                                                                          
   SET @Outcome = [dbo].[FN_GetNonRec_122] (@CoverID ,@SnapShotDate )                                                                                                  
                                                                                                                                                               
     insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                         
    select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                                                          
                                                                                                                          
  -----------------------------------------Section 1.2.3----------------------------------                                                                                                                                                           
                                                                                                   
      SET @sec='1.2.3'                                                                       
    SET @Outcome = NULL                                                                                                                               
    set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                               
                                                                     
                                                                        
                                             
                                 
                                    
                                                                               
                              
     set @dtmax3=null                                                                                                                                            
   set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                            
                                                                                                              
     SET @Outcome =(SELECT COUNT(EarningEstimateId) FROM EventData_EarningEstimate   WHERE CoverId=@CoverID                                                         
                           
                            
                              
                               
                                  
                                    
                                       
                                        
                                         
                                             
                                        
                                                 
                                                 
                                                     
                                           
                                                        
                    
                                                            
                                                              
                                                                
                                                                  
                                                                     
                                                                      
                                                                        
                                                                          
                                                                            
                                                                             
                                                                                
                                                                      
                                                                                   
                                                                                      
                                                                                    
         AND DisclosureDate BETWEEN @dtmax3 and @SnapShotDate )                                           
                                                                          
      insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                                          
    select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                                                        
                                                      
   -----------------------------------------Section 1.2.4----------------------------------                                                                         
                                                                                      
    SET @Outcome = NULL                                                                                                                                                
    SET @sec='1.2.4'                                                                                                           
     set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                      
                                                                                                                                            
    set @dtmax3=null                                                                                                            
    set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                       
                                                                
                                                                 
                                                                                                                                         
                                  
    SET @Outcome =(SELECT count(DelayedFilingId) FROM EventData_DelayedFiling                                                                                                                                                                                  
 
     
      
        
         
            
               
               
                   
                    
                     
                         
                          
                           
                               
                                
                                 
    WHERE CoverId=@CoverID  AND DisclosureDate BETWEEN  @dtmax3  AND @SnapShotDate )                                                                                                           
                                                                                                                                                       
    insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                                          
    select @CoverID,@sec,@SnapShotDate,@Outcome                                                                         
                                                                           
-----------------------------------------Section 1.2.5----------------------------------                                                                                                     
                                                                            
   SET @Outcome = NULL                                                                                                          
   SET @sec='1.2.5'                        
   set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                                                             
                                                     
  set @dtmax3= null                                                                                                                                            
   set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                                    
                                                                                      
                                                               
    SET @Outcome =(SELECT count(MaterialWeaknessId) FROM @EventData_MaterialWeakness                                                                                                        
                                                       
                                                            
                                                               
                                                
                                                                  
                                                                    
                                                                      
                                                                        
                                                                         
                                                                            
                                                                              
                                                                                 
                                                                                         
    WHERE CoverId=@CoverID AND iniDisclosureDate BETWEEN  @dtmax3  and @SnapShotDate )                                                                   
                                                             
                                                                                                                 
   insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                
    select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                                                       
                                                                                                                                 
                                                     
  -----------------------------------------Section 1.2.6----------------------------------                                                                          
                                                                                                                                                                                                           
   SET @Outcome = NULL                                                                                    
   SET @sec='1.2.6'                                                 
                                                                                                                                                
                                                                   
  select @Outcome =                               
        COUNT(MaterialWeaknessId) from @EventData_MaterialWeakness                                                                                                                                                   
                                     
                                     
                                        
                                          
                                            
                                               
                                                
                                      
                                                    
                                                                  
                                                                    
                                                                      
                                                                        
                                                                          
                                                                            
                                                                              
                                                                                
                                                                                 
         
                                                                           
                                                                                       
                                                                                       
                                                                                            
                                                                                              
                                                                     
                                                                                                  
                                                                                                    
                                                                                     
                                                                                                        
                                                                                                          
                                                                                                            
                                      
                                                                                                               
                                                                                                 
                                                                                    
                                          
                                                                                                                        
                                
                                                                                                          
                                                                                                                                                
       where @SnapShotDate between iniDisclosureDate and  isnull(rmdDisclosureDate,getdate())                                                                                                                  
        and CoverId=@CoverID                                                                                                                       
                              
                                                                     
                                                                                                                       
       if @Outcome>='1'                                          
       set @Outcome='Yes'                                
        if @Outcome='0'                                          
       set @Outcome='No'                                          
                                                                                                           
                                                                                                                          
      insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                 
    select @CoverID,@sec,@SnapShotDate,@Outcome              
                                                                                                             
     -----------------------------------------Section 1.3.1----------------------------------                                                                                                                             
                                                                                  
                                                                                    
                                                                                      
                                                                                        
                                                                                          
                       
                                                                                              
                                                
                                                                  
                                                                          
                                                                                                  
                                                                                                        
                                                            
                                                                                                            
                                                                
                                                                                                                 
                                                                                                                                     
   SET @Outcome = NULL                                                                                                                           
   SET @sec='1.3.1'                                                                                            
   set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                                                                   
                                                          
   set @dtmax3=null                                                                                                                                            
    set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                                                             
                                 
    SET @Outcome =(select count(EventDataSRIId) from EventData_SRI                                             
     where CoverId=@CoverID and iniDisclosureDate between  @dtmax3  and @SnapShotDate )                                                                                                                                                                       
   
   
       
       
          
            
               
               
                                                                                                                                               
                                                                                                                                                                           
                                                                                                                                                                           
     insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                   
    select @CoverID,@sec,@SnapShotDate,@Outcome                           
    
      
        
          
    -----------------------------------------Section 1.3.2----------------------------------                                                                   
                                                                                        
                                                                                          
                                                                                           
                                                                                              
                                                                                                 
                                                                    
                                                                                                   
   SET @Outcome = NULL                                                                                                                 
   SET @sec='1.3.2'                                                                               
                                                                                       
  set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                                                 
  set @dtmax3=null                                                                                                            
    set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                  
        select @Outcome=                                                                                                                                                         
        COUNT(EventDataSRIId) from EventData_sri                                                                                                                                                                                  
       where @SnapShotDate between iniDisclosureDate and isnull(CloseDisclosureDate,getdate())                                                                                       
       and CoverId=@CoverID                      
   -- and CloseDisclosureDate is null                                                                                                                                                                                        
                                                                                                                                        
      insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                            
    select @CoverID,@sec,@SnapShotDate,@Outcome                                                                  
                                                                                                                                    
 -----------------------------------------Section 1.3.3----------------------------------                                                                                                                                                     
                                                                                                                                                
   SET @sec='1.3.3'                                                 
   set @Outcome=null                                                                                                                                                                                 
 set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                                                                                                  
   
    
     
        
   
           
               
                
                      
                      
                          
                            
                              
                                
                                   
                                   
                                      
                                        
                                          
                                            
                                              
                                                
                                                  
                                                    
                                                      
                                                         
                                                         
                                                            
                                          
              
                                                                 
                                                                    
                                                                      
                                                                        
   set @dtmax3= null                                                                       
  set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                
                                                                                                                                         
    SET @Outcome =(select count(EventDataSRIEAId) from EventData_SRIEA                                                                                                            
                
                  
                    
                     
                         
                          
                            
                              
                                
                                  
                                    
                                      
                                        
                                          
                                                            
                                                
                                                  
                                               
                                                      
                                                        
                                                          
                                                            
           
                                                                 
                                                                  
                                                                    
                                                                      
                                                                     
                                                                           
                                                                            
                                                                        
                                                                                
                                             
                                                                                    
                                    
                                                                                        
                                                                                         
    WHERE CoverId=@CoverID   AND iniDisclosureDate BETWEEN  @dtmax3  AND @SnapShotDate )                                                                         
                                                                                                                                                                                                                    
     insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                               
    select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                                                                                                                                                
  
    
      
        
          
                                                                                                           
  -----------------------------------------Section 1.3.4----------------------------------                  
                                                                                                
   SET @Outcome = NULL                                                                                                                  
                                                                                
   SET @sec='1.3.4'                                                                                                           
 set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                                                                                                   
  
    
      
        
         
            
               
                
                  
                    
                      
                        
                          
                            
                              
                                
                                  
                                    
                                      
                                        
                                         
                                             
                                              
                                                
                                                  
                                                   
                                      
                                                        
                                                           
                                                            
                                                              
                                         
                                                                  
                                                  
                                                          
                                                                        
                                                                          
                                                                            
                                                                                                                                          
                                                                       
                                                                                     
                                                                                      
                                                                                   
                                                                                          
                                                                                           
                                                                                              
                    
                                                                                                     
                                                                            
                                                                                                        
                                                                                           
                  
                                                                                                             
                                                                                           
    set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                          
                                             
                                                                 
                                                                                                                             
     SET @Outcome =(SELECT ISNULL(COUNT(IniDisclosureDate),0) FROM EventData_DOJSI                                                                               
       WHERE CoverId=@CoverID                                   
       AND iniDisclosureDate BETWEEN  @dtmax3  AND @SnapShotDate )                     
                                                                                                                               
 insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                           
    select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                                                                                                           
                                     
  -----------------------------------------Section 1.3.5----------------------------------                                              
                                                                                                                                                                                    
 SET @Outcome = NULL                                                                                                                                                               
                                                                                                                                                        
    set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                         
 SET @sec='1.3.5'                                                                                                                                                      set @dtmax3=null                                                                        
 
     
      
        
          
            
              
               
                   
                    
                     
                       
                           
                            
                              
                               
                               
                                     
                                      
                                        
        
                                                                      
                                                                       
                                                                           
                                                                            
                                                                              
                                                                               
                                                  
    set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                                                                                                                         
  
    
      
                                                                                                 
SET @Outcome = (SELECT count(EventDataDOJSIId) from EventData_dojsi                                           
                                                                
                                                                        
                                                                          
                                                                            
                                                                              
                                                                                
                                                 
                                                                                    
   WHERE CoverId=@CoverID                                                                                                                                                                                                   
       --AND iniDisclosureDate BETWEEN  @Fdate  AND @FdateOut        
      and @SnapShotDate between iniDisclosureDate and  isnull(CloseDisclosureDate,getdate()))                                                                   
                                                                                                                     
    insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                          
    select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                                                       
                                                                                                                                                                                                         
 -----------------------------------------Section 1.3.6----------------------------------                                                                                                                                                         
                                                                                                                    
    SET @sec = '1.3.6'                                                                              
    set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                           
     set @dtmax3=null                                                                                                                                   
    SET @Outcome = NULL                                                                                       
                                                                                                                              
                                                         
   set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                                                      
                
                 
                     
                                        
                         
                             
                             
                                 
                                  
                                   
                                     
                                        
                                          
                                                                                                       
    SET @Outcome = (SELECT ISNULL(COUNT([EventDataACIId]),0)                                                                                                            
    FROM  dbo.EventData_Aci                                                                                                
    WHERE CoverId = @CoverID AND [IniDisclosureDate] BETWEEN @dtmax3  AND @SnapShotDate)                                                                                           
                                                                  
                                                                                                                                   
                                                                                                                                
    insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                            
    select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                                                     
                                         
                                                                      
-----------------------------------------Section 1.3.7----------------------------------                                                                                                                                                                      
  
    
       
        
         
            
            
                
                  
                    
                      
                        
                                                                                         
   SET @Outcome = NULL                                                                            
   SET @sec='1.3.7'                                                                                                                                     
                                                           
    select @Outcome= COUNT(EventDataACIId) from EventData_aci                   
                                                                                                                                                            
where                                                                              
@SnapShotDate between iniDisclosureDate and  isnull(CloseDisclosureDate,getdate())                                                                                                                                    
      and CoverId=@CoverID                                                                                                                                                
                                                                                                                                                                                      
      insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                         
    select @CoverID,@sec,@SnapShotDate,@Outcome                
     -----------------------------------------Section 1.3.8----------------------------------                                                                                                                            
                                                                                                                                                                                                             
   SET @sec = '1.3.8'                                                                         
   set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                                                                              
   SET @Outcome = NULL                                                                                                                                                                                   
                        
     set @dtmax3=null                                                                                                                                 
    set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                   
                                                                  
                                                                                                                                                               
    SET @Outcome = (SELECT ISNULL(COUNT([EventDataSECId]),0)                                                                                                                                                        
    FROM  @EventData_SECReview                                                                                            
  WHERE CoverId = @CoverID AND [IniDisclosureDate] BETWEEN @dtmax3  AND @SnapShotDate)                                                                                                                                                    
                                               
                                                                                            
                                                                                     
     insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                                     
    select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                   
                                                                                                                                                  
                                                                                                                                                                                                            
 -----------------------------------------Section 1.3.9----------------------------------                                                          
                                                            
  SET @Outcome = NULL                                                                                                                                                                                    
  SET @sec='1.3.9'                                                                                                                                                                                                              
                                                   
      select @Outcome=                                                    
      COUNT(EventDataSECId) from @EventData_secreview                                            
     where  @SnapShotDate between iniDisclosureDate and  isnull(CloseDisclosureDate,getdate())                                                             
        
         
            
               
                
                 
                     
                      
                       
                           
                            
                             
                                 
                                 
                                     
                                     
                                        
                                          
                                             
                                           
                                                 
                                                 
                                                    
                    
                                                         
                                                         
                                                            
                                                              
                                          
                                                                  
                                                                    
                                                                      
                                                                         
                                                                   
                                                                              
                                                                                
                                                                           
                                                                                    
                                                                                      
                                                                                        
                                                                                 
                                                                                            
                                                                
                                                                                                
                                                                                                   
                                                                                                   
                                                                
                                                
                                                                                                           
                                                                                                           
                                                                                                   
                                                                                    
                                                                                             
                                                                                                                    
                                                                                                                      
                                                                                                                        
                                                                                                                                                                                                            
                                                                                                                                                                     
and CoverId=@CoverID                                                                                                                            
                                                                                                             
                                                                                                                 
                           
                                                                                                                             
       insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                
    select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                    
                                                                                                                         
     -----------------------------------------Section 1.3.10----------------------------------                                                        
                                                                                                                                                                                                              
   SET @sec = '1.3.10'                                         
   set @dtmax3= null                                                                                                                              
   SET @Outcome = NULL                                                        
 set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                                                                                                  
  
    
      
         
          
            
              
                
                 
                     
                      
                       
                           
                            
 set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                                                                              
                                                 
                                                     
                                                      
                                                       
                                                           
                                                            
                                                              
                                                               
                                                             
SET @Outcome = (SELECT ISNULL(COUNT([RRRI_Ref]),0)                                                                                     
       FROM  @EventData_SECReview                             
       WHERE CoverId = @CoverID                                                                                                                                                   
  --AND [RRRI_Ref] BETWEEN @FiscalEndDate AND @FEndDateCalculated                                                                                                 
     and  IniDisclosureDate                                                                                                                                              
     between @dtmax3 and @SnapShotDate                                                                                                     
     --and                                                                                          
     --@dtmax3<=CloseDisclosureDate                                                                       
       )                                                                                                                            
                                                                                                                                          
     insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                          
    select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                             
                                
 -----------------------------------------Section 1.3.11----------------------------------                                                                                                                      
                                                                                                                                                                                             
   SET @sec = '1.3.11'                                                                                                                                
   set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                          
   set @dtmax3=null                                                                                                                                                   
   set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                                                                                   
   SET @Outcome =0                                                                                             
     SET @Outcome = (SELECT ISNULL(COUNT([RERI_Ref]),0)                                                                                                                                                 
     FROM  @EventData_SECReview                                                                                                
    WHERE CoverId = @CoverID AND IniDisclosureDate BETWEEN @dtmax3  AND @SnapShotDate)                                                                                                                                   
                                                                                                                                    
                                                                                                                         
    insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                
    select @CoverID,@sec,@SnapShotDate,@Outcome                       
                                                            
 -----------------------------------------Section 1.3.12----------------------------------                                                          
                                                             
   SET @sec = '1.3.12'                                 
   set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                                                                                                 
 
     
     
         
          
            
             
                 
                  
                    
                     
                         
                         
                            
                              
                                 
                                 
                                    
                                       
                                       
                                          
                                                
                                                
                                                  
                                                     
                                                     
                                                         
                                                         
                                                            
                                         
                                                                
                              
                                                                     
                                                                     
                                                                        
                                                         
                                                                           
                                                                               
                                                                               
  set @dtmax3=null                             
  set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                                                                                                                           
  
                                                                                                   
    SET @Outcome =0                                                                  
    SET @Outcome = (SELECT ISNULL(COUNT([RIRR_Ref]),0)            FROM  @EventData_SECReview                                                                            
     
                                                      
                                                        
                                                          
                                                            
                                                              
                                                                
                                                                  
                                                                    
                                                                      
                          
                                                                          
                                                                            
                                                                              
                                                                               
                                  
                                          
                                                                                       
                                                                                        
                                                                                         
                 
                                                                                             
                                                                                                
                                                                                                   
                                                                                                   
                                                    
                                                     
                                                                                                                                                                                                                                             
       WHERE CoverId = @CoverID AND IniDisclosureDate BETWEEN @dtmax3  AND @SnapShotDate)                                                                                                                                             
                                  
                                                                                      
      insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                                          
    select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                                                                         
                                                                                                                       
 -----------------------------------------Section 1.3.13----------------------------------                                                                     
                                                                    
 SET @sec = '1.3.13'                                                                                                                        
   set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                           
  set @dtmax3=null                                                                                                                                
  set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                                                                                      
                                          
   SET @Outcome = NULL                            
    SET @Outcome = (SELECT ISNULL(COUNT([RIPT_Ref]),0) FROM  @EventData_SECReview                                                                                                                                           
    
     
         
          
            
              
               
                  
    WHERE CoverId = @CoverID AND IniDisclosureDate BETWEEN @dtmax3  AND @SnapShotDate)                                                                                       
                                                                                                               
                                                                                                                                                                 
  insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                           
    select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                                                                              
                                                                       
 -----------------------------------------Section 1.3.14----------------------------------                                                                                                   
      
        
          
            
              
                
                  
                    
                     
                         
                          
                           
                               
                               
                                  
                                     
                                      
                                       
                                         
                                                                                                                                                      
  SET @sec = '1.3.14'                                                                                                                                                     
   set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                                                                                                                                                    
   set @dtmax3= null                                                                                                                                
    set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                                                             
    SET @Outcome = NULL                                                                                   
    SET @Outcome = (SELECT ISNULL(COUNT([RIPC_Ref]),0)                                                                                                                                                   
       FROM  @EventData_SECReview                                                                                                                                                                                                                 
  WHERE CoverId = @CoverID AND IniDisclosureDate BETWEEN @dtmax3  AND @SnapShotDate)                                                                                                         
                                                                       
    insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                                                                              
    select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                                                                                        
                                   
   
                                        
                                 
                 
                                               
                                               
                                                   
                                               
                                                       
                                                       
                     
                                                           
                                                              
                                                                
                                                                                           
                                                                      
                                                                        
                                                                         
                                                                             
                                                                        
                                                                      
                                                                                   
                                                                                    
                                                                          
                                                                                                              
 -----------------------------------------Section 1.3.15----------------------------------                                                                                                
                                                               
    SET @sec = '1.3.15'                                                                
    set @Outcome=null                                                                                    
    set @var=(select DeriveMeasureVariable from @GSM_MASTER WHERE GSM_NUM=@sec)                         
     set @dtmax3= null                                                                                                                   
     set @dtmax3= CONVERT(DATETIME, @SnapShotDate) -(364* CAST(ROUND(@var,0) AS INT)+2)                                                                                                           
                                                                                                                                                                                              
    SET @Outcome = (SELECT ISNULL(COUNT([RIPE_Ref]),0)                                                                                                           
    FROM  @EventData_SECReview                                                                                                                                                                                               
    WHERE CoverId = @CoverID AND IniDisclosureDate BETWEEN @dtmax3  AND @SnapShotDate)                                                                                                                                             
                          
                                                                                            
      insert into AggMedianPreprocessData1_Daily_Second([Cover_id],[GsmNo],[SnapShotDate],[OutCome])                                                                                     
    select @CoverID,@sec,@SnapShotDate,@Outcome                                                                                                                                 
                                 
                                                                                                                                                 
   set @cnt=@cnt+1                                                                             
                                                                                     
 END                                                                                                                                 
                                                               
                    
                                                                                                                                                      
                                                                                                                                        
                                                                                                                                  
    END    ------END Check for scorable company   
    
        declare @MINDT date        
SET @MINDT=(select convert(varchar,DATEADD(Day,365*3,MIN(Proxy_FillingDt)),101)  from Proxy_Period where Cover_Id=@coverid)              
        
IF((@SnapShotDate IS NOT NULL) AND (@SnapShotDate<@MINDT))              
BEGIN              
  UPDATE AggMedianPreprocessData1_Daily_Second set outcome=''           
  where cover_id=@coverid          
  and snapshotdate=@SnapShotDate          
  and GsmNo in ('1.1.3','1.2.1','1.2.2','1.2.3','1.2.4','1.2.5',          
  '1.3.1','1.3.3','1.3.4','1.3.6','1.3.8','1.3.10','1.3.11','1.3.12','1.3.13','1.3.14','1.3.15'          
  ) 
  
  end  
                                                                                                                                                                           
                                                                                                                                                                                                                                           
END