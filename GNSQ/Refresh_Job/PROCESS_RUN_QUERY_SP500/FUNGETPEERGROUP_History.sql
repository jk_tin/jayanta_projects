
/****** Object:  UserDefinedFunction [dbo].[FUNGETPEERGROUP_History]    Script Date: 03/29/2019 12:10:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                                                                                                                
-- AUTHOR:  SUSHMITA DAS                                                                                                              
-- CREATE DATE: 05/04/2013                                                    
---UPDATED BY SUSHMITA FOR RESTSTEMENT AND ONE TIME ON 14/01/2014                                                                                                             
-- DESCRIPTION: GET TOTAL PEER COMPANY FOR ACTIVE DEFINITION                                                                                                             
-- =============================================                                                                                            
CREATE FUNCTION [dbo].[FUNGETPEERGROUP_History]                                                                            
(                                                                            
@COVERID UNIQUEIDENTIFIER,                                                  
@STRDATE VARCHAR(500)                                                                                   
)                                                                                                        
RETURNS @TABPEERCOMP TABLE([INDX] INT IDENTITY(1,1) NOT NULL,[SNAPSHOTDATE] DATE NULL,                                            
[COMPANY_ID] INT NOT NULL, [COMPANYNAME] VARCHAR(100)  NULL,[COVER_ID] UNIQUEIDENTIFIER NOT NULL,[TICKER_SYMBOL] VARCHAR(50),[EXCHANGE] VARCHAR(100) NULL)                                                                                     
                                              
                                                
                                                  
 AS                                                                                       
BEGIN                                                                                                        
                                                                              
                                                                           
  ---1                                                                            
  DECLARE @TABPEERCOMPSPINDEXES TABLE([INDX] INT IDENTITY(1,1) NOT NULL, [COVER_ID] UNIQUEIDENTIFIER NOT NULL)                                                                            
  -----2                                                                            
  DECLARE @TABPEERCOMPRUSSELLINDEXES TABLE([INDX] INT IDENTITY(1,1) NOT NULL,[COVER_ID] UNIQUEIDENTIFIER NOT NULL)                                                  
                                                     
  -----3                                                            
  DECLARE @TABPEERCOMPGICSSECTOR  TABLE([INDX] INT IDENTITY(1,1) NOT NULL,[COVER_ID] UNIQUEIDENTIFIER NOT NULL)                                                                            
                                                                            
  -----4                                                                     
  DECLARE @TABPEERCOMPGICSINDUSTRYGROUP   TABLE([INDX] INT IDENTITY(1,1) NOT NULL, [COVER_ID] UNIQUEIDENTIFIER NOT NULL)                                                                            
                                                               
   ----PEER DEFINITION TABLE                                                                
                                                                   
   DECLARE @TABPEERCOMPDEFINITION TABLE(                                                                        
   [INDX] INT  NOT NULL,                                                        
   [PEER_GROUP_INDEX_TYPE_ID] INT NOT NULL,                                                       
   [PEER_GROUP_INDEX_TYPE_NAME] VARCHAR(100)  NULL,                                                          
   [PEER_INDEX_GROUP_NAME] VARCHAR(100)  NULL,                                                   
   [PEER_COMBODETAILSID] INT NULL,                                       
   [PEER_INDEX_ITEM_NAME] VARCHAR(100)  NULL)                                                                                   
                                                           
 INSERT INTO @TABPEERCOMPDEFINITION([INDX],[PEER_GROUP_INDEX_TYPE_ID],[PEER_GROUP_INDEX_TYPE_NAME],                                            
 [PEER_INDEX_GROUP_NAME],[PEER_COMBODETAILSID],[PEER_INDEX_ITEM_NAME])                                                          
                                                          
 SELECT  [INDX], [PEER_GROUP_INDEX_TYPE_ID],[PEER_GROUP_INDEX_TYPE_NAME],                                                   
 [PEER_INDEX_GROUP_NAME],[PEER_COMBODETAILSID],[PEER_INDEX_ITEM_NAME]                                                         
 FROM PRECALCULATEDPEERGROUPDEFINITION  WITH(NOLOCK) 
 
 
                                                                                                    
                                                                              
 DECLARE @PEER_GROUP_INDEX_TYPE_ID INT                                                                              
 DECLARE @PEER_GROUP_INDEX_TYPE_NAME VARCHAR(100)                                                                              
 DECLARE @PEER_INDEX_GROUP_NAME VARCHAR(100)                                                                              
 DECLARE @PEER_INDEX_ITEM_NAME VARCHAR(100)                                                                              
 DECLARE @PEER_COMBODETAILSID INT                                                                            
 DECLARE @COUNT INT                                                                              
 DECLARE @CNT INT                                                               
 DECLARE @IND INT                                                                 
                                                                 
 DECLARE @COMPANY_ID INT                                                                           
 DECLARE @BASECOMPSPINDX INT                                                                            
 DECLARE @BASERUSELINDX INT                                                                
 DECLARE @GICSSECTOR INT                                                                
 DECLARE @GICSINDUSTRYGROUP INT                                                                         
                                      
 DECLARE @SP INT                                                                             
 DECLARE @RUSSELL INT                                                                              
 DECLARE @GICSGR INT                                                       
 DECLARE @GICSIND INT                                                                                      
 SET @SP=0                                                                         
 SET @RUSSELL=0                                                                         
 SET @GICSGR=0                                                                        
 SET @GICSIND=0                                                                  
                   
                                                   
	                                      
 DECLARE @TABSNAPSHOTDATE TABLE([INDX] INT IDENTITY(1,1) NOT NULL,[SNAPSHOTDATE] DATE NULL)                                                                                        
 INSERT INTO @TABSNAPSHOTDATE([SNAPSHOTDATE])                                                                                                             
 SELECT [DATA] 
 FROM SPLIT(@STRDATE,'^') 
 GROUP BY [DATA] ORDER BY [DATA] DESC                                                                                                               
 DELETE FROM @TABSNAPSHOTDATE WHERE [INDX]=(SELECT MAX([INDX]) FROM @TABSNAPSHOTDATE)                                             
	                                          
                                              
 DECLARE @SNAPSHOTDATE DATE                                            
 DECLARE @COUNTDATE INT                                                                                
 DECLARE @CNTDATE INT                                            
 SET  @COUNTDATE=(SELECT COUNT(*) FROM @TABSNAPSHOTDATE)                                            
 -----------------                                            
                                             
 SET @COMPANY_ID=(SELECT COMPANY_ID FROM COVER WHERE COVER_ID=@COVERID)                                              
                                                    
 DECLARE @COMPANY_MASTER1 TABLE(                                              
  --[INDX] INT IDENTITY(1,1),                                              
  [HISTORY_ID] INT NULL,                                              
  [COMPANY_ID] INT NULL,                                              
  [COVER_ID] UNIQUEIDENTIFIER NULL,                                              
  [COMPANY_NAME] VARCHAR(150)  NULL ,                                              
  [TICKER_SYMBOL] VARCHAR(150)  NULL,                                              
  [AFFECTEDDATE] DATETIME NULL,                                             
  [GICSSECTOR] BIGINT NULL,                                              
  [GICSINDUSTRYGROUP] BIGINT NULL,                                              
  [INDEXSP] BIGINT NULL                                             
 ,[INDEXRUSELL] BIGINT NULL,                                              
  [EXCHANGE] VARCHAR(100)  NULL,                                
  [ACTIVE] BIT NULL)                       
                 
 DECLARE @COMPANY_MASTER TABLE(                                              
  --[INDX] INT IDENTITY(1,1),                                              
  [HISTORY_ID] INT NULL,                                              
  [COMPANY_ID] INT NULL,                          
  [COVER_ID] UNIQUEIDENTIFIER NULL,                                              
  [COMPANY_NAME] VARCHAR(150)  NULL ,                                              
  [TICKER_SYMBOL] VARCHAR(150)  NULL,                                              
  [AFFECTEDDATE] DATETIME NULL,                                                    
  [GICSSECTOR] BIGINT NULL,                                              
  [GICSINDUSTRYGROUP] BIGINT NULL,                                              
  [INDEXSP] BIGINT NULL,                                                    
  [INDEXRUSELL] BIGINT NULL,                                              
  [EXCHANGE] VARCHAR(100)  NULL,                                
  [ACTIVE] BIT NULL)                                                  
                                                                            
                                                             
    --SELECT @BASECOMPSPINDX,@BASERUSELINDX,@GICSSECTOR,@GICSINDUSTRYGROUP                                                                       
                                                                                     
 SET @COUNT=(SELECT COUNT([INDX]) FROM @TABPEERCOMPDEFINITION)                                               
                                             
                                             
 SET @CNTDATE=1                                            
 WHILE @CNTDATE<=@COUNTDATE                                                                                
  BEGIN                                                        
                                                                                
    SET @SNAPSHOTDATE=(SELECT [SNAPSHOTDATE] FROM @TABSNAPSHOTDATE WHERE [INDX]=@CNTDATE)                                             
                                              
    DELETE FROM @COMPANY_MASTER1                      
    DELETE FROM @COMPANY_MASTER        
    DECLARE @ISSINGLEREFERENCE INT                                      
    SET @ISSINGLEREFERENCE=0                                 
    SET @ISSINGLEREFERENCE=(SELECT TOP 1 ISACTIVE_SINGLEUNIVERSE FROM PREPROCESS_TBLREFSINGLEUNIVERSE)        
                                             
                    
      
 IF  @ISSINGLEREFERENCE=1                                                
 BEGIN        
       
  INSERT INTO @COMPANY_MASTER1([HISTORY_ID],[COMPANY_ID],[COVER_ID],[COMPANY_NAME],[TICKER_SYMBOL],[AFFECTEDDATE],[GICSSECTOR],                                                    
                               [GICSINDUSTRYGROUP],[INDEXSP],[INDEXRUSELL],[EXCHANGE],[ACTIVE])        
                          
 --------------------------INSERT-----------------                                                    
SELECT [HISTORY_ID],A.[COMPANY_ID],(SELECT COVER_ID FROM COVER WHERE COMPANY_ID =A.[Company_id]) COVER_ID,[COMPANY_NAME],[TICKER_SYMBOL],STARTDATE ,
          [GICSSECTOR], [GICSINDUSTRYGROUP],[INDEXSP],[INDEXRUSELL],(SELECT FEILDNAME FROM LOOKUPDETAILS WHERE LOOKUPID=[EXCHANGE]) EXCHANGE,[ACTIVE] 
          FROM (SELECT *                                                                  
                FROM DBO.COMPANY_MASTER_HISTORY 
                WHERE COMPANY_ID IN (SELECT COMPANY_ID 
                                     FROM DBO.COVER 
                                     WHERE COVER_ID IN (SELECT DISTINCT COVER_ID FROM DBO.COVER)) AND Active=1
                AND CONVERT(DATE,@SNAPSHOTDATE) BETWEEN CONVERT(DATE,STARTDATE) AND ISNULL((CONVERT(DATE,ENDDATE)),GETDATE()))A        
                               
   END            
 ELSE            
   BEGIN       
         
   INSERT INTO @COMPANY_MASTER1([HISTORY_ID],[COMPANY_ID],[COVER_ID],[COMPANY_NAME],[TICKER_SYMBOL],[AFFECTEDDATE],[GICSSECTOR],                                                    
 [GICSINDUSTRYGROUP],[INDEXSP],[INDEXRUSELL],[EXCHANGE],[ACTIVE])        
  SELECT [HISTORY_ID],A.[COMPANY_ID],(SELECT COVER_ID FROM COVER WHERE COMPANY_ID =A.[Company_id]) COVER_ID,[COMPANY_NAME],[TICKER_SYMBOL],StartDate,
          [GICSSECTOR], [GICSINDUSTRYGROUP],[INDEXSP],[INDEXRUSELL],(SELECT FEILDNAME FROM LOOKUPDETAILS WHERE LOOKUPID=[EXCHANGE]) EXCHANGE,[ACTIVE] 
          FROM (SELECT *                                                                  
                FROM DBO.COMPANY_MASTER_HISTORY 
                WHERE COMPANY_ID IN (SELECT COMPANY_ID 
                                     FROM DBO.COVER 
                                     WHERE COVER_ID IN (SELECT DISTINCT COVER_ID FROM DBO.COVER)) AND Active=1
                AND CONVERT(DATE,@SNAPSHOTDATE) BETWEEN CONVERT(DATE,STARTDATE) AND ISNULL((CONVERT(DATE,ENDDATE)),GETDATE()))A        
       
   END                     
---------DELETE DUPLICATE RECORD-----------------------                      
                      
INSERT INTO @COMPANY_MASTER([COMPANY_ID],[COVER_ID],                      
   [COMPANY_NAME],[TICKER_SYMBOL],[AFFECTEDDATE],[GICSSECTOR],                                                    
 [GICSINDUSTRYGROUP],[INDEXSP],[INDEXRUSELL],[EXCHANGE],[ACTIVE])                      
                       
 SELECT DISTINCT [COMPANY_ID],[COVER_ID],                      
   [COMPANY_NAME],[TICKER_SYMBOL],[AFFECTEDDATE],[GICSSECTOR],                                                    
 [GICSINDUSTRYGROUP],[INDEXSP],[INDEXRUSELL],[EXCHANGE],[ACTIVE]          
 FROM    @COMPANY_MASTER1                          
                                   
   --SELECT * FROM @COMPANY_MASTER                      
   --SELECT * FROM @COMPANY_MASTER1                       
                               
 ----------------                              
 --SELECT [HISTORY_ID],[COMPANY_ID],[COVER_ID],[COMPANY_NAME],[TICKER_SYMBOL],[AFFECTEDDATE],[GICSSECTOR],             
 --[GICSINDUSTRYGROUP],[INDEXSP],[INDEXRUSELL],[EXCHANGE],[ACTIVE]  FROM                                                    
                                                     
 --((SELECT MAX([HISTORY_ID]) HISTORY_ID1                                                    
 -- FROM COMPANY_MASTER_AUDIT  WHERE CONVERT(DATE,ISNULL(AFFECTEDDATE,@SNAPSHOTDATE))<=@SNAPSHOTDATE                                      
 ----AND ACTIVE=1                                   
 ----AND [COMPANY_ID] IN (SELECT [COMPANY_ID] FROM COVER)                                                    
 --GROUP BY [COMPANY_ID]) M  INNER JOIN                              
 --(SELECT CMA.[HISTORY_ID],CMA.[COMPANY_ID],COV.[COVER_ID],CMA.[COMPANY_NAME],CMA.[TICKER_SYMBOL],                                              
 --CMA.[AFFECTEDDATE],CMA.[GICSSECTOR],             
 --CMA.[GICSINDUSTRYGROUP],CMA.[INDEXSP],CMA.[INDEXRUSELL],                                        
 --(SELECT FEILDNAME FROM LOOKUPDETAILS WHERE LOOKUPID=CMA.[EXCHANGE]) EXCHANGE,                                
 --CMA.ACTIVE                                        
 -- FROM COMPANY_MASTER_AUDIT CMA                                              
 --INNER JOIN COVER COV ON CMA.[COMPANY_ID]=COV.[COMPANY_ID]) A                
 --ON M.HISTORY_ID1=A.HISTORY_ID) WHERE ACTIVE=1                              
 -----------------                              
                                                     
                              
                                          
                                         
 DELETE FROM @COMPANY_MASTER WHERE COVER_ID NOT IN                                        
 (SELECT COVER_ID FROM COMPANY_SUBMISSION_DETAILS  WITH(NOLOCK)  WHERE LAST_UPDATE_DATE IS NOT NULL) OR COVER_ID NOT IN                                        
     (SELECT DISTINCT COVER_ID FROM PROXY_PERIOD WITH(NOLOCK))                                              
                                             
                                                    
 SET @BASECOMPSPINDX=(SELECT TOP 1 INDEXSP FROM Company_Master_History  WITH(NOLOCK)  WHERE COMPANY_ID=@COMPANY_ID                                                  
  AND CONVERT(DATE,@SNAPSHOTDATE) BETWEEN CONVERT(DATE,STARTDATE) AND ISNULL((CONVERT(DATE,ENDDATE)),GETDATE()))                                               
                                                            
 SET @BASERUSELINDX=(SELECT TOP 1 INDEXRUSELL FROM Company_Master_History  WITH(NOLOCK)  WHERE COMPANY_ID=@COMPANY_ID                                                  
 AND CONVERT(DATE,@SNAPSHOTDATE) BETWEEN CONVERT(DATE,STARTDATE) AND ISNULL((CONVERT(DATE,ENDDATE)),GETDATE()))                                                  
                                                                  
 SET @GICSSECTOR=(SELECT TOP 1 GICSSECTOR FROM Company_Master_History  WITH(NOLOCK)  WHERE COMPANY_ID=@COMPANY_ID                                                  
 AND CONVERT(DATE,@SNAPSHOTDATE) BETWEEN CONVERT(DATE,STARTDATE) AND ISNULL((CONVERT(DATE,ENDDATE)),GETDATE()))                                                  
                                                                 
 SET @GICSINDUSTRYGROUP=(SELECT TOP 1 GICSINDUSTRYGROUP FROM Company_Master_History  WITH(NOLOCK)  WHERE COMPANY_ID=@COMPANY_ID                                                  
 AND CONVERT(DATE,@SNAPSHOTDATE) BETWEEN CONVERT(DATE,STARTDATE) AND ISNULL((CONVERT(DATE,ENDDATE)),GETDATE()))                                             
                                             
                                            
                                             
 -------------------------                                            
                                             
                                             
                                             
                                                                            
 SET @CNT=1                                                                              
  WHILE @CNT<=@COUNT                                                                              
  BEGIN                                                                              
                                                                              
  SET @PEER_GROUP_INDEX_TYPE_NAME=(SELECT REPLACE([PEER_GROUP_INDEX_TYPE_NAME],' ','') FROM @TABPEERCOMPDEFINITION WHERE [INDX]=@CNT)                          
  SET @PEER_COMBODETAILSID=(SELECT PEER_COMBODETAILSID FROM @TABPEERCOMPDEFINITION WHERE [INDX]=@CNT)                                                                            
  SET @PEER_GROUP_INDEX_TYPE_ID=(SELECT PEER_GROUP_INDEX_TYPE_ID FROM @TABPEERCOMPDEFINITION WHERE [INDX]=@CNT)                                      
                                                                     
  SET @PEER_INDEX_GROUP_NAME=(SELECT REPLACE([PEER_INDEX_GROUP_NAME],' ','') FROM @TABPEERCOMPDEFINITION WHERE [INDX]=@CNT)                                               
                                 
                                              
                                             
                                             
                                              
  IF @PEER_GROUP_INDEX_TYPE_ID =1                                                                              
  BEGIN                               
                                                                                                                                 
                                                                              
  IF @PEER_INDEX_GROUP_NAME=REPLACE('UNBLENDED COMBO',' ','')                                                                            
  BEGIN                                                
                                                                          
   --UNBLENDED COMBO AND S&P 500                                                                            
   IF @PEER_COMBODETAILSID=1 AND @BASECOMPSPINDX=18                                                                            
   BEGIN                                                                  
   INSERT INTO @TABPEERCOMPSPINDEXES([COVER_ID])               
   SELECT C.COVER_ID 
   FROM COVER C 
   INNER JOIN @COMPANY_MASTER M
   ON C.COMPANY_ID=M.COMPANY_ID              
       AND M.INDEXSP=@BASECOMPSPINDX                                                                 
   --SELECT COVER_ID FROM COVER WHERE                                                                 
   -- COMPANY_ID IN (SELECT COMPANY_ID FROM @COMPANY_MASTER WHERE INDEXSP=@BASECOMPSPINDX )                                               
                            
                                                   
                                                                          
   SET @SP=1                                                                          
   END                                                                            
   --UNBLENDED COMBO AND S&P MIDCAP 400                                                                            
   IF @PEER_COMBODETAILSID=2 AND @BASECOMPSPINDX=19                                                                            
   BEGIN                                                                       
                                                          
   INSERT INTO @TABPEERCOMPSPINDEXES([COVER_ID])              
   SELECT C.COVER_ID FROM COVER C 
   INNER JOIN @COMPANY_MASTER M 
   ON C.COMPANY_ID=M.COMPANY_ID              
       AND M.INDEXSP=@BASECOMPSPINDX                                                                
   --SELECT COVER_ID FROM COVER WHERE                                                                  
   -- COMPANY_ID IN (SELECT COMPANY_ID FROM @COMPANY_MASTER WHERE INDEXSP=@BASECOMPSPINDX )                                                                                          
                                                                             
   SET @SP=1                                                    
   END                                         
   --UNBLENDED COMBO AND S&P SMALLCAP600                                                                            
   IF @PEER_COMBODETAILSID=3 AND @BASECOMPSPINDX=20                                                                            
   BEGIN            
   INSERT INTO @TABPEERCOMPSPINDEXES([COVER_ID])                
   SELECT C.COVER_ID FROM COVER C INNER JOIN @COMPANY_MASTER M ON C.COMPANY_ID=M.COMPANY_ID              
       AND M.INDEXSP=@BASECOMPSPINDX                                    
   --SELECT COVER_ID FROM COVER WHERE                                                                 
   -- COMPANY_ID IN (SELECT COMPANY_ID FROM @COMPANY_MASTER WHERE INDEXSP=@BASECOMPSPINDX )                                                                
                        
                                                                          
   SET @SP=1                                                                        
                                
   END                                                
                                                                             
  END                                                                            
  ELSE IF @PEER_INDEX_GROUP_NAME=REPLACE('BLENDED COMBO1',' ','')                                                                            
  BEGIN                                      
SET @PEER_COMBODETAILSID=(SELECT PEER_COMBODETAILSID FROM @TABPEERCOMPDEFINITION WHERE [INDX]=@CNT)                                                                            
   ---BLENDED COMBO1 AND S&P 900 [S&P 500 + S&P MIDCAP400]                                                                            
   IF @PEER_COMBODETAILSID=1                                                                   
   BEGIN                                                               
   IF (@BASECOMPSPINDX=18 OR @BASECOMPSPINDX=19)                                                                            
    BEGIN                                                                            
    INSERT INTO @TABPEERCOMPSPINDEXES([COVER_ID])              
    SELECT C.COVER_ID FROM COVER C INNER JOIN @COMPANY_MASTER M ON C.COMPANY_ID=M.COMPANY_ID              
       AND M.INDEXSP IN (18,19)                                                                 
    --SELECT COVER_ID FROM COVER WHERE                                                                  
    --COMPANY_ID IN (SELECT COMPANY_ID FROM @COMPANY_MASTER WHERE INDEXSP IN (18,19) )                                                                                         
                          
    SET @SP=1                                                                            
    END                                                                            
   END                                                                    
   ---BLENDED COMBO1 AND S&P 600                                                                            
                                                                         
   IF @PEER_COMBODETAILSID=2 AND @BASECOMPSPINDX=20                                                                            
   BEGIN                                                                            
   INSERT INTO @TABPEERCOMPSPINDEXES([COVER_ID])               
   SELECT C.COVER_ID FROM COVER C INNER JOIN @COMPANY_MASTER M ON C.COMPANY_ID=M.COMPANY_ID              
       AND M.INDEXSP=@BASECOMPSPINDX                                                               
   --SELECT COVER_ID FROM COVER WHERE                                                                  
   -- COMPANY_ID IN (SELECT COMPANY_ID FROM @COMPANY_MASTER WHERE INDEXSP=@BASECOMPSPINDX )                                                 
                                                                   
   SET @SP=1                                                                            
   END                                                                            
  END                                          
  ELSE IF @PEER_INDEX_GROUP_NAME=REPLACE('BLENDED COMBO2',' ','')                                                                            
  BEGIN                                
   ---BLENDED COMBO2 AND S&P 500                                                                                     
   IF @PEER_COMBODETAILSID=1 AND @BASECOMPSPINDX=18                                                                            
   BEGIN                                                                                     
   INSERT INTO @TABPEERCOMPSPINDEXES([COVER_ID])               
   SELECT C.COVER_ID FROM COVER C INNER JOIN @COMPANY_MASTER M ON C.COMPANY_ID=M.COMPANY_ID              
       AND M.INDEXSP=@BASECOMPSPINDX                                           
   --SELECT COVER_ID FROM COVER WHERE                                                                  
   -- COMPANY_ID IN (SELECT COMPANY_ID FROM @COMPANY_MASTER WHERE INDEXSP=@BASECOMPSPINDX )                                                                 
                                                                                            
                                                                          
   SET @SP=1                                      
   END                                                                       
   ---BLENDED COMBO2 AND S&P 400 &600                                                         
   IF @PEER_COMBODETAILSID=2 AND (@BASECOMPSPINDX=19 OR @BASECOMPSPINDX=20)                                                                           
   BEGIN                                                                                     
   INSERT INTO @TABPEERCOMPSPINDEXES([COVER_ID])                
    SELECT C.COVER_ID FROM COVER C INNER JOIN @COMPANY_MASTER M ON C.COMPANY_ID=M.COMPANY_ID              
       AND M.INDEXSP IN (19,20)                                                                   
   --SELECT COVER_ID FROM COVER WHERE                                                      
   -- COMPANY_ID IN (SELECT COMPANY_ID FROM @COMPANY_MASTER WHERE INDEXSP IN (19,20) )                                                                 
                                                                          
                                                                           
   SET @SP=1                                                                            
   END                                                                         
                                                                              
  END                                                                            
  ELSE IF @PEER_INDEX_GROUP_NAME=REPLACE('COMPOSITE',' ','')                                                                            
  BEGIN                                                                            
  ---COMPOSITE                                                                            
                                                                        
                                                                               
   INSERT INTO @TABPEERCOMPSPINDEXES([COVER_ID])               
   SELECT C.COVER_ID FROM COVER C INNER JOIN @COMPANY_MASTER M ON C.COMPANY_ID=M.COMPANY_ID              
       AND M.INDEXSP IN (18,19,20)                                            
   --SELECT COVER_ID FROM COVER WHERE                                                                    
   -- COMPANY_ID IN (SELECT COMPANY_ID FROM @COMPANY_MASTER WHERE INDEXSP IN (18,19,20) )                                                           
                                                                                            
                                                                          
   SET @SP=1                                                                            
                                                                               
  END                                                                            
                                                                   
                               
                
  END                                                           
                                                                              
                                                                              
                                                                              
                                                                              
  -----RESELL                                                                            
  ELSE IF @PEER_GROUP_INDEX_TYPE_ID =2                                                                             
  BEGIN                                                       
    IF @PEER_INDEX_GROUP_NAME=REPLACE('UNBLENDED_COMBO',' ','')                                                            
 BEGIN                                                             
     ---UNBLENDED COMBO AND RUSSELL 1000                                                                            
     IF @PEER_COMBODETAILSID=1 AND @BASERUSELINDX=21                                                                            
     BEGIN                                                                            
     INSERT INTO @TABPEERCOMPRUSSELLINDEXES([COVER_ID])               
     SELECT C.COVER_ID FROM COVER C INNER JOIN @COMPANY_MASTER M ON C.COMPANY_ID=M.COMPANY_ID              
       AND M.INDEXRUSELL=@BASERUSELINDX                                                                   
    -- SELECT COVER_ID FROM COVER WHERE                                                                 
    --COMPANY_ID IN (SELECT COMPANY_ID FROM @COMPANY_MASTER WHERE INDEXRUSELL=@BASERUSELINDX )                                                                 
                                                                        
                                                                            
     SET @RUSSELL=1                                                                            
     END                                                                            
     ---UNBLENDED COMBO AND RUSSELL 2000                                                                            
     IF @PEER_COMBODETAILSID=1 AND @BASERUSELINDX=22                                                                            
     BEGIN                                                                            
     INSERT INTO @TABPEERCOMPRUSSELLINDEXES([COVER_ID])               
     SELECT C.COVER_ID FROM COVER C INNER JOIN @COMPANY_MASTER M ON C.COMPANY_ID=M.COMPANY_ID              
       AND M.INDEXRUSELL=@BASERUSELINDX                                                                
    --SELECT COVER_ID FROM COVER WHERE                                                                  
    --COMPANY_ID IN (SELECT COMPANY_ID FROM @COMPANY_MASTER WHERE INDEXRUSELL=@BASERUSELINDX )                                                                
                                                                                             
                                                                            
     SET @RUSSELL=1                                                  
     END                                                                            
    END                                                                            
    IF @PEER_INDEX_GROUP_NAME=REPLACE('BROAD_MARKET_[COMPOSITE]',' ','')                                                                            
                                                                            
     INSERT INTO @TABPEERCOMPRUSSELLINDEXES([COVER_ID])               
     SELECT C.COVER_ID FROM COVER C INNER JOIN @COMPANY_MASTER M ON C.COMPANY_ID=M.COMPANY_ID              
       AND M.INDEXRUSELL IN (21,22)                                                                 
    -- SELECT COVER_ID FROM COVER WHERE                                                                 
    --COMPANY_ID IN (SELECT COMPANY_ID FROM @COMPANY_MASTER WHERE INDEXRUSELL IN (21,22) )                                                                
                           
                                                                            
     SET @RUSSELL=1                                           
                                        
                                                                                
                                                                                
  END                                                                              
  ELSE IF @PEER_GROUP_INDEX_TYPE_ID =3                                                                             
  BEGIN                                                                         
  SET @GICSGR=1                    
   INSERT INTO @TABPEERCOMPGICSSECTOR([COVER_ID])                
   SELECT C.COVER_ID FROM COVER C INNER JOIN @COMPANY_MASTER M ON C.COMPANY_ID=M.COMPANY_ID              
       AND M.GICSSECTOR=@GICSSECTOR                                                                 
    --SELECT COVER_ID FROM COVER WHERE                       
    --COMPANY_ID IN (SELECT COMPANY_ID FROM @COMPANY_MASTER WHERE GICSSECTOR=@GICSSECTOR )                                               
                                                                                             
                                                                           
  END                                                                            
  ELSE IF @PEER_GROUP_INDEX_TYPE_ID =4                                
  BEGIN                                                               
  SET @GICSIND=1                                                                            
   INSERT INTO @TABPEERCOMPGICSINDUSTRYGROUP([COVER_ID])                
      SELECT C.COVER_ID FROM COVER C INNER JOIN @COMPANY_MASTER M ON C.COMPANY_ID=M.COMPANY_ID               
      AND M.GICSINDUSTRYGROUP=@GICSINDUSTRYGROUP                             
   --SELECT COVER_ID FROM COVER               
   --WHERE                                                                  
   -- COMPANY_ID IN (SELECT COMPANY_ID FROM @COMPANY_MASTER WHERE GICSINDUSTRYGROUP=@GICSINDUSTRYGROUP )                                                                
                                                                                            
                                                                              
  END                                              
                                                                            
                                                    
                                                                              
  SET @CNT=@CNT+1                                                                            
  END                                                
                                                
                                                                  
  --SELECT * FROM @TABPEERCOMPSPINDEXES                                                                            
  --SELECT * FROM @TABPEERCOMPRUSSELLINDEXES                                                                            
  --SELECT * FROM @TABPEERCOMPGICSSECTOR                                                                            
  --SELECT * FROM @TABPEERCOMPGICSINDUSTRYGROUP                                              
                                               
                                                                             
                                                                              
   --1 2 3 4   ////1                                                                 
                                                                          
  IF @SP=1 AND @RUSSELL=1 AND @GICSGR=1 AND @GICSIND=1                                                                              
  BEGIN                                                                              
  INSERT INTO @TABPEERCOMP([SNAPSHOTDATE],[COMPANY_ID],[COMPANYNAME],[COVER_ID],[TICKER_SYMBOL],[EXCHANGE])                                                                                              
  SELECT @SNAPSHOTDATE,M.[COMPANY_ID],M.[COMPANY_NAME],M.[COVER_ID],M.[TICKER_SYMBOL],M.[EXCHANGE]                                                                                 
  FROM @COMPANY_MASTER M  WHERE M.COVER_ID IN                                                                                  
  (SELECT S.[COVER_ID] FROM @TABPEERCOMPSPINDEXES S                                                                                 
  INNER JOIN @TABPEERCOMPRUSSELLINDEXES R ON S.[COVER_ID]=R.[COVER_ID]                                                                                     
  INNER JOIN @TABPEERCOMPGICSSECTOR GICSSEC ON S.[COVER_ID]=GICSSEC.[COVER_ID]                                                                                  
  INNER JOIN @TABPEERCOMPGICSINDUSTRYGROUP GICSIND ON GICSSEC.[COVER_ID]=GICSIND.[COVER_ID])                                                                                   
   END                                         
   --0 2 3 4 /////2                                                                              
   IF @SP=0 AND @RUSSELL=1 AND @GICSGR=1 AND @GICSIND=1                                                                              
  BEGIN                                                                          
  INSERT INTO @TABPEERCOMP([SNAPSHOTDATE],[COMPANY_ID],[COMPANYNAME],[COVER_ID],[TICKER_SYMBOL],[EXCHANGE])                                                                       
  SELECT @SNAPSHOTDATE,M.[COMPANY_ID],M.[COMPANY_NAME],M.[COVER_ID],M.[TICKER_SYMBOL],M.[EXCHANGE]                                                                                 
  FROM @COMPANY_MASTER M  WHERE  M.COVER_ID IN                                                                                  
  (SELECT R.[COVER_ID] FROM  @TABPEERCOMPRUSSELLINDEXES R                                                                                      
  INNER JOIN @TABPEERCOMPGICSSECTOR GICSSEC ON R.[COVER_ID]=GICSSEC.[COVER_ID]                                                                                  
  INNER JOIN @TABPEERCOMPGICSINDUSTRYGROUP GICSIND ON GICSSEC.[COVER_ID]=GICSIND.[COVER_ID])                                                         
   END                                                                              
   --1 0 3 4 ///3                                                                              
   IF @SP=1 AND @RUSSELL=0 AND @GICSGR=1 AND @GICSIND=1                                                                      
  BEGIN                                                                              
  INSERT INTO @TABPEERCOMP([SNAPSHOTDATE],[COMPANY_ID],[COMPANYNAME],[COVER_ID],[TICKER_SYMBOL],[EXCHANGE])                                                                                   
  SELECT @SNAPSHOTDATE,M.[COMPANY_ID],M.[COMPANY_NAME],M.[COVER_ID],M.[TICKER_SYMBOL],M.[EXCHANGE]                                                                                 
  FROM @COMPANY_MASTER M  WHERE  M.COVER_ID IN                                                  
  (SELECT S.[COVER_ID] FROM @TABPEERCOMPSPINDEXES S                                           
  INNER JOIN @TABPEERCOMPGICSSECTOR GICSSEC ON S.[COVER_ID]=GICSSEC.[COVER_ID]                                                                                  
  INNER JOIN @TABPEERCOMPGICSINDUSTRYGROUP GICSIND ON GICSSEC.[COVER_ID]=GICSIND.[COVER_ID])                                                                                  
   END                                                                         
   --1 2 0 4 ///4                                                                              
   IF @SP=1 AND @RUSSELL=1 AND @GICSGR=0 AND @GICSIND=1                                                                              
  BEGIN                                                                      
  INSERT INTO @TABPEERCOMP([SNAPSHOTDATE],[COMPANY_ID],[COMPANYNAME],[COVER_ID],[TICKER_SYMBOL],[EXCHANGE])                                                                                              
  SELECT @SNAPSHOTDATE,M.[COMPANY_ID],M.[COMPANY_NAME],M.[COVER_ID],M.[TICKER_SYMBOL],M.[EXCHANGE]                                                                                 
  FROM @COMPANY_MASTER M  WHERE  M.COVER_ID IN                                                                                  
  (SELECT S.[COVER_ID] FROM @TABPEERCOMPSPINDEXES S                                    
  INNER JOIN @TABPEERCOMPRUSSELLINDEXES R ON S.[COVER_ID]=R.[COVER_ID]                                                                    
  INNER JOIN @TABPEERCOMPGICSINDUSTRYGROUP GICSIND ON R.[COVER_ID]=GICSIND.[COVER_ID])                                                                            
   END                     
   --1 2 3 0//5                                                                              
   IF @SP=1 AND @RUSSELL=1 AND @GICSGR=1 AND @GICSIND=0                                                                              
  BEGIN                                                                              
  INSERT INTO @TABPEERCOMP([SNAPSHOTDATE],[COMPANY_ID],[COMPANYNAME],[COVER_ID],[TICKER_SYMBOL],[EXCHANGE])                                                                                              
  SELECT @SNAPSHOTDATE,M.[COMPANY_ID],M.[COMPANY_NAME],M.[COVER_ID],M.[TICKER_SYMBOL],M.[EXCHANGE]                                                        
  FROM @COMPANY_MASTER M  WHERE  M.COVER_ID IN                                               
  (SELECT S.[COVER_ID] FROM @TABPEERCOMPSPINDEXES S                                                                         
  INNER JOIN @TABPEERCOMPRUSSELLINDEXES R ON S.[COVER_ID]=R.[COVER_ID]                                                                                     
      INNER JOIN @TABPEERCOMPGICSSECTOR GICSSEC ON S.[COVER_ID]=GICSSEC.[COVER_ID]                                                                              
  )                                                                                   
   END                                                                              
   --1 2 0 0 ///6                                                                              
   IF @SP=1 AND @RUSSELL=1 AND @GICSGR=0 AND @GICSIND=0                                               
  BEGIN                                                                              
  INSERT INTO @TABPEERCOMP([SNAPSHOTDATE],[COMPANY_ID],[COMPANYNAME],[COVER_ID],[TICKER_SYMBOL],[EXCHANGE])                                                                                    
  SELECT @SNAPSHOTDATE,M.[COMPANY_ID],M.[COMPANY_NAME],M.[COVER_ID],M.[TICKER_SYMBOL],M.[EXCHANGE]                                                                                 
  FROM @COMPANY_MASTER M  WHERE M.COVER_ID IN                                                                                 
  (SELECT S.[COVER_ID] FROM  @TABPEERCOMPSPINDEXES S                                                                                  
  INNER JOIN @TABPEERCOMPRUSSELLINDEXES R ON R.[COVER_ID]=S.[COVER_ID])                                                                                   
   END                                                                              
   --1 0 3 0///7                                                                              
   IF @SP=1 AND @RUSSELL=0 AND @GICSGR=1 AND @GICSIND=0                                                                            
  BEGIN                                                    
  INSERT INTO @TABPEERCOMP([SNAPSHOTDATE],[COMPANY_ID],[COMPANYNAME],[COVER_ID],[TICKER_SYMBOL],[EXCHANGE])                                                                                              
  SELECT @SNAPSHOTDATE,M.[COMPANY_ID],M.[COMPANY_NAME],M.[COVER_ID],M.[TICKER_SYMBOL],M.[EXCHANGE]                                                                                 
  FROM @COMPANY_MASTER M  WHERE M.COVER_ID IN                                                                                
  (SELECT S.[COVER_ID] FROM @TABPEERCOMPSPINDEXES S                                                                                    
  INNER JOIN @TABPEERCOMPGICSSECTOR GICSSEC ON S.[COVER_ID]=GICSSEC.[COVER_ID])                                                                                   
   END                                                                              
   --1 0 0 4 ///8                                                                              
 IF @SP=1 AND @RUSSELL=0 AND @GICSGR=0 AND @GICSIND=1                                                                          
  BEGIN                                
  INSERT INTO @TABPEERCOMP([SNAPSHOTDATE],[COMPANY_ID],[COMPANYNAME],[COVER_ID],[TICKER_SYMBOL],[EXCHANGE])                                                                                              
  SELECT @SNAPSHOTDATE,M.[COMPANY_ID],M.[COMPANY_NAME],M.[COVER_ID],M.[TICKER_SYMBOL],M.[EXCHANGE]                                                                                 
  FROM @COMPANY_MASTER M  WHERE M.COVER_ID IN                                                                     
  (SELECT S.[COVER_ID] FROM @TABPEERCOMPSPINDEXES S                                                        
  INNER JOIN @TABPEERCOMPGICSINDUSTRYGROUP GICSIND ON S.[COVER_ID]=GICSIND.[COVER_ID])                                                            
                                                                          
  --1 0 1 0 ///8                                                 
           
                                                                          
                                                                                     
   END                                                                              
   --0 2 3 0 ///9                                                                       
   IF @SP=0 AND @RUSSELL=1 AND @GICSGR=1 AND @GICSIND=0                                                                              
  BEGIN                                                                           
  INSERT INTO @TABPEERCOMP([SNAPSHOTDATE],[COMPANY_ID],[COMPANYNAME],[COVER_ID],[TICKER_SYMBOL],[EXCHANGE])                                                                                              
  SELECT @SNAPSHOTDATE,M.[COMPANY_ID],M.[COMPANY_NAME],M.[COVER_ID],M.[TICKER_SYMBOL],M.[EXCHANGE]                                         
  FROM @COMPANY_MASTER M  WHERE M.COVER_ID IN                                                                                 
  (SELECT R.[COVER_ID] FROM  @TABPEERCOMPRUSSELLINDEXES R                                                                                      
  INNER JOIN @TABPEERCOMPGICSSECTOR GICSSEC ON R.[COVER_ID]=GICSSEC.[COVER_ID]                                                                                  
  )                                                    
   END                                                                              
   --0 2 0 4 ///10                                               
   IF @SP=1 AND @RUSSELL=1 AND @GICSGR=0 AND @GICSIND=0                                                                              
  BEGIN                                                               
  INSERT INTO @TABPEERCOMP([SNAPSHOTDATE],[COMPANY_ID],[COMPANYNAME],[COVER_ID],[TICKER_SYMBOL],[EXCHANGE])                                                                                              
  SELECT @SNAPSHOTDATE,M.[COMPANY_ID],M.[COMPANY_NAME],M.[COVER_ID],M.[TICKER_SYMBOL],M.[EXCHANGE]                                        
  FROM @COMPANY_MASTER M  WHERE M.COVER_ID IN                         
  (SELECT R.[COVER_ID] FROM  @TABPEERCOMPRUSSELLINDEXES R                                                                                          
  INNER JOIN @TABPEERCOMPGICSINDUSTRYGROUP GICSIND ON R.[COVER_ID]=GICSIND.[COVER_ID])                                                                                   
   END                             
   --0 0 3 4 ///11                                                          
   IF @SP=0 AND @RUSSELL=0 AND @GICSGR=1 AND @GICSIND=1                                                                              
  BEGIN                                                                              
  INSERT INTO @TABPEERCOMP([SNAPSHOTDATE],[COMPANY_ID],[COMPANYNAME],[COVER_ID],[TICKER_SYMBOL],[EXCHANGE])                                                                                              
  SELECT @SNAPSHOTDATE,M.[COMPANY_ID],M.[COMPANY_NAME],M.[COVER_ID],M.[TICKER_SYMBOL],M.[EXCHANGE]                                                              
  FROM @COMPANY_MASTER M  WHERE M.COVER_ID IN                                                            
  (SELECT GICSSEC.[COVER_ID] FROM  @TABPEERCOMPGICSSECTOR GICSSEC                                                                                 
  INNER JOIN @TABPEERCOMPGICSINDUSTRYGROUP GICSIND ON GICSSEC.[COVER_ID]=GICSIND.[COVER_ID])                                                                            
   END                                                                              
   --1 0 0 0 ///12                                                 
   IF @SP=1 AND @RUSSELL=0 AND @GICSGR=0 AND @GICSIND=0                                                                              
  BEGIN                                                                          
 INSERT INTO @TABPEERCOMP([SNAPSHOTDATE],[COMPANY_ID],[COMPANYNAME],[COVER_ID],[TICKER_SYMBOL],[EXCHANGE])                     
  SELECT @SNAPSHOTDATE,M.[COMPANY_ID],M.[COMPANY_NAME],M.[COVER_ID],M.[TICKER_SYMBOL],M.[EXCHANGE]                                                                                 
  FROM @COMPANY_MASTER M INNER JOIN @TABPEERCOMPSPINDEXES N ON M.COVER_ID=N.COVER_ID              
  -- WHERE M.COVER_ID IN                                                                            
  --(SELECT [COVER_ID] FROM @TABPEERCOMPSPINDEXES)                                                                                   
   END                                                                              
   --0 2 0 0 ///13                                                                              
   IF @SP=0 AND @RUSSELL=1 AND @GICSGR=0 AND @GICSIND=0                                                                              
  BEGIN                                       
  INSERT INTO @TABPEERCOMP([SNAPSHOTDATE],[COMPANY_ID],[COMPANYNAME],[COVER_ID],[TICKER_SYMBOL],[EXCHANGE])                                                            
  SELECT @SNAPSHOTDATE,M.[COMPANY_ID],M.[COMPANY_NAME],M.[COVER_ID],M.[TICKER_SYMBOL],M.[EXCHANGE]                                                                                 
  FROM @COMPANY_MASTER M  INNER JOIN @TABPEERCOMPRUSSELLINDEXES N ON M.COVER_ID=N.COVER_ID              
  --WHERE M.COVER_ID IN                                                              
  --(SELECT [COVER_ID] FROM @TABPEERCOMPRUSSELLINDEXES)                                                                     
   END                                                                              
   --0 0 3 0 ///14                                                                              
   IF @SP=0 AND @RUSSELL=0 AND @GICSGR=1 AND @GICSIND=0                                                                              
  BEGIN                                                                              
 INSERT INTO @TABPEERCOMP([SNAPSHOTDATE],[COMPANY_ID],[COMPANYNAME],[COVER_ID],[TICKER_SYMBOL],[EXCHANGE])                                                                    
  SELECT @SNAPSHOTDATE,M.[COMPANY_ID],M.[COMPANY_NAME],M.[COVER_ID],M.[TICKER_SYMBOL],M.[EXCHANGE]                                                                                 
  FROM @COMPANY_MASTER M  INNER JOIN @TABPEERCOMPGICSSECTOR N ON M.COVER_ID=N.COVER_ID              
  --WHERE M.COVER_ID IN                                        
  --(SELECT [COVER_ID] FROM @TABPEERCOMPGICSSECTOR)                                                                                   
   END                                                           
   --0 0 0 4 ///15                                                                              
   IF @SP=0 AND @RUSSELL=0 AND @GICSGR=0 AND @GICSIND=1                                                                              
  BEGIN                                                                              
  INSERT INTO @TABPEERCOMP([SNAPSHOTDATE],[COMPANY_ID],[COMPANYNAME],[COVER_ID],[TICKER_SYMBOL],[EXCHANGE])                                                                                     
  SELECT @SNAPSHOTDATE,M.[COMPANY_ID],M.[COMPANY_NAME],M.[COVER_ID],M.[TICKER_SYMBOL],M.[EXCHANGE]                                                                                 
  FROM @COMPANY_MASTER M INNER JOIN @TABPEERCOMPGICSINDUSTRYGROUP N ON M.COVER_ID=N.COVER_ID              
                 
  --WHERE M.COVER_ID IN                                                        
  --(SELECT [COVER_ID] FROM @TABPEERCOMPGICSINDUSTRYGROUP)                                                                         
                
   END                                                                                
                                                                          
     --SELECT * FROM @COMPANY_MASTER                                                                     
                                                                                    
                                                  
                                                  
  DELETE FROM @TABPEERCOMPSPINDEXES                                                      
  DELETE FROM @TABPEERCOMPRUSSELLINDEXES                                         
  DELETE FROM @TABPEERCOMPGICSSECTOR                                                                            
  DELETE FROM @TABPEERCOMPGICSINDUSTRYGROUP                                  
                                  
                                  
                                                            
   SET @CNTDATE=@CNTDATE+1                                                                              
  END -----END OF SNAPSHOTDATE                                              
                                              
  --SELECT * FROM @TABPEERCOMP                                  
                                  
                                  
                                                                            
                                                                             
  RETURN                                                   
                                                                                   
                                                                                        
  --SELECT ROW_NUMBER() OVER(ORDER BY [COVER_ID]) INDX,[COMPANYNAME], [COVER_ID],[TICKER_SYMBOL],[EXCHANGE] FROM @TABPEERCOMP                                                                               
                                                              
                                                                                                       
END 