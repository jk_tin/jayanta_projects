<?php

namespace App\Model;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use DB;

class AllocateZone extends Model
{
    
    protected $table 		= 't_rel_zone_paper_student_allocation';
	protected $primaryKey 	= 't_rel_zone_paper_student_allocation_id';


    public static function getDistinctCollegeName()
    {
        $ret = DB::table('t_rel_affiliated_college')->distinct()->get(['name']);
        return $ret;

    }

    public static function insertStudentData($data)
    {
        $id = DB::table('student_ml')->insertGetId($data);
        return $id;

    }

    public static function insertUnallocatedStudentData($data)
    {
        $id = DB::table('t_rel_zone_paper_student_allocation_fail')->insertGetId($data);
        return $id;

    }

    public static function getDistinctCollegeId()
    {
        $ret = DB::table('t_rel_affiliated_college')->distinct()->get(['t_rel_affiliated_college_id as collid']);
        return $ret;

    }

    public static function getDistinctCollegeIdFromStudent()
    {
        $ret = DB::table('t_rel_students')->distinct()->get(['t_rel_affiliated_college_id as collid']);
        return $ret;

    }

    public static function getData($collid,$yearid)
    {
      //  DB::select('call myStoredProcedure(?,?)',array($p1,$p2));
       // echo $collid;
       
       Log::debug("Here...00=".$collid."yearid =".$yearid); 
        $student =DB::select('call sp_mf_allocate_zone(?,?)',array($collid,$yearid));
      // var_dump($student);
        return $student;

    }

    public static function getStudentData($collid)
    { 
        $student =DB::select('call find_student(?)',array($collid));
      // var_dump($student);
        return $student;

    }

    public static function getAllocatedData($criteria)
    {
      
       // $student =DB::select('call sp_mf_alloacted_zone_student(?)',array($yearid));  
       $results = DB::table('t_rel_zone_paper_student_allocation as t1')
       ->leftjoin('t_mst_academic_years as t2', 't2.t_mst_academic_year_id', 't1.t_mst_academic_year_id')
       ->leftjoin('t_rel_zone as t3', 't3.t_rel_zone_id', 't1.t_rel_zone_id')
       ->leftjoin('t_rel_affiliated_college as t4', 't4.t_rel_affiliated_college_id', 't1.t_rel_affiliated_college_id')
       ->leftjoin('t_rel_students as t5', 't5.t_rel_student_id', 't1.t_rel_student_id')
       ->leftjoin('t_mst_mediums as t6', 't6.t_mst_medium_id', 't1.t_mst_medium_id')
       ->leftjoin('t_rel_subject_paper as t7', 't7.t_rel_subject_paper_id', 't1.t_rel_subject_paper_id')
       ->leftjoin('t_rel_exam_center as t8', 't8.t_rel_exam_center_id', 't1.t_rel_exam_center_id')
       ->leftjoin('t_mst_district as t9', 't9.t_mst_district_id', 't1.t_mst_district_id')        
       ->select('t1.*',
               't2.code as academic_year_code',
               't3.name as zone_name',
               't3.t_rel_zone_id as zone_id',
               't4.name as school_name',
               't5.first_name as student_fname',
               't5.last_name as student_lname',
               't6.name as medium_name',
               't7.name as paper_name',
               't8.name as exam_center',
               't9.name as district_name'
               );  

               if(isset($criteria['t_rel_zone_id']))
               {
                   $results = $results->where('t1.t_rel_zone_id', $criteria['t_rel_zone_id']);
               }
               if(isset($criteria['t_rel_subject_paper_id']))
               {
                   $results = $results->where('t1.t_rel_subject_paper_id', $criteria['t_rel_subject_paper_id']);
               }
               if(isset($criteria['t_mst_medium_id']))
               {
                   $results = $results->where('t1.t_mst_medium_id', $criteria['t_mst_medium_id']);
               }
               
      
            
            return $results;

    }


    public static function getAllocatedSummaryData($criteria)
    {
      
       

       $results = DB::table('t_rel_zone_paper_student_allocation as t1')
       ->leftjoin('t_rel_subject_paper as t2', 't2.t_rel_subject_paper_id', 't1.t_rel_subject_paper_id')
       ->leftjoin('t_mst_mediums as t3', 't3.t_mst_medium_id', 't1.t_mst_medium_id')
       ->leftjoin('t_rel_subject_paper as t4', 't4.t_rel_subject_paper_id', 't1.t_rel_subject_paper_id')
       ->leftjoin('t_rel_zone as t5', 't5.t_rel_zone_id', 't1.t_rel_zone_id')
       ->leftjoin('t_mst_academic_years as t6', 't6.t_mst_academic_year_id', 't1.t_mst_academic_year_id')
        ->select(DB::raw('COUNT(t1.t_rel_subject_paper_id) as total'),
               't6.code as academic_year_code',
               't1.t_rel_zone_id',
               't5.name as zonename',
               't1.t_rel_subject_paper_id',
               't4.name as subjectname',
               't1.t_mst_medium_id',
               't3.name as mediumname'               
               )        
               ->groupBy('t1.t_rel_zone_id')
               ->groupBy('t6.code')
               ->groupBy('t5.name')
               ->groupBy('t1.t_rel_subject_paper_id')
               ->groupBy('t4.name')
               ->groupBy('t1.t_mst_medium_id')
               ->groupBy('t3.name')
               ; 
             
               if(isset($criteria['t_rel_zone_id']))
               {
                     
                    $results = $results->where('t1.t_rel_zone_id', $criteria['t_rel_zone_id']);
               }
               if(isset($criteria['t_rel_subject_paper_id']))
               {
                   $results = $results->where('t1.t_rel_subject_paper_id', $criteria['t_rel_subject_paper_id']);
               }
               if(isset($criteria['t_mst_medium_id']))
               {
                   $results = $results->where('t1.t_mst_medium_id', $criteria['t_mst_medium_id']);
               }
            
            return $results;

    }

    
    public static function getAllocatedSummaryDataByCentre($criteria)
    {
      
       

       $results = DB::table('t_rel_zone_paper_student_allocation as t1')
       ->leftjoin('t_rel_subject_paper as t2', 't2.t_rel_subject_paper_id', 't1.t_rel_subject_paper_id')
       ->leftjoin('t_mst_mediums as t3', 't3.t_mst_medium_id', 't1.t_mst_medium_id')
       ->leftjoin('t_rel_subject_paper as t4', 't4.t_rel_subject_paper_id', 't1.t_rel_subject_paper_id')
       ->leftjoin('t_rel_zone as t5', 't5.t_rel_zone_id', 't1.t_rel_zone_id')
       ->leftjoin('t_mst_academic_years as t6', 't6.t_mst_academic_year_id', 't1.t_mst_academic_year_id')
       ->leftjoin('t_rel_exam_center as t7', 't7.t_rel_exam_center_id', 't1.t_rel_exam_center_id')
        ->select(DB::raw('COUNT(t1.t_rel_subject_paper_id) as total'),
               't6.code as academic_year_code',
               't1.t_rel_zone_id',
               't5.name as zonename',
               't1.t_rel_subject_paper_id',
               't4.name as subjectname',
               't1.t_mst_medium_id',
               't3.name as mediumname'               
               )        
               ->groupBy('t1.t_rel_exam_center_id')
               ->groupBy('t6.code')
               ->groupBy('t5.name')
               ->groupBy('t1.t_rel_subject_paper_id')
               ->groupBy('t4.name')
               ->groupBy('t1.t_mst_medium_id')
               ->groupBy('t3.name')
               ; 
             
            //    if(isset($criteria['t_rel_zone_id']))
            //    {
                     
            //         $results = $results->where('t1.t_rel_zone_id', $criteria['t_rel_zone_id']);
            //    }
            //    if(isset($criteria['t_rel_subject_paper_id']))
            //    {
            //        $results = $results->where('t1.t_rel_subject_paper_id', $criteria['t_rel_subject_paper_id']);
            //    }
            //    if(isset($criteria['t_mst_medium_id']))
            //    {
            //        $results = $results->where('t1.t_mst_medium_id', $criteria['t_mst_medium_id']);
            //    }
            
            return $results;

    }


   

    public static function allocationZoneForDistrict($districtId)
    {
        $ret = DB::table('t_rel_zone')
				 ->where('t_mst_district_id', '=', $districtId)
				 ->select('*')					
				 ->get();
		return $ret;
        
        
    }

    public static function getPhyImpStudentList()
    {
      
        $student = DB::table('t_rel_student_impairments')
        ->where('t_mst_phy_imp_id', '=',1)
        ->select('*')					
        ->get();
        return $student;

    }

      
    public static function getCapacityDataWithOutMedium($zoneid,$paperlist)
    {


      $sql="SELECT c.t_rel_zone_capacity_id,c.t_mst_medium_id,t.t_rel_subject_paper_id,
      c.capacity,c.consumed_capacity,c.consumed_percentage,c.allocation_percentage
       FROM seba_board_exam.t_rel_zone_capacity  c,
      t_rel_zone_capacity_paper_tag t
      where t_rel_zone_id =".$zoneid." and  c.t_rel_zone_capacity_id=t.t_rel_zone_capacity_id".
      " and t.t_rel_subject_paper_id in (".$paperlist.")";
      $ret=DB::select($sql);
      return $ret;

     }


     public static function getCapacityDataWithMedium($zoneid,$paperlist,$medium)
     {
 
 
       $sql="SELECT c.t_rel_zone_capacity_id,c.t_mst_medium_id,t.t_rel_subject_paper_id,
       c.capacity,c.consumed_capacity,c.consumed_percentage,c.allocation_percentage
        FROM seba_board_exam.t_rel_zone_capacity  c,
       t_rel_zone_capacity_paper_tag t
       where t_rel_zone_id =".$zoneid." and  c.t_rel_zone_capacity_id=t.t_rel_zone_capacity_id".
       " and c.t_mst_medium_id=".$medium.
       " and t.t_rel_subject_paper_id in (".$paperlist.")";
       
       $ret=DB::select($sql); 
       return $ret;
 
      }


      public static function updateCapacityData($capacityid,$data)
      {
 
 
         DB::table('t_rel_zone_capacity')->where('t_rel_zone_capacity_id',$capacityid)->update($data);
 
      }

    //check if allocation district and college district is same.
    public static function getDistrictAlloZone($zone_id,$college_district_id)
    {
      
        $zone = DB::table('t_rel_zone')
        ->where('t_rel_zone_id', '=',$zone_id)
        ->where('t_mst_district_id', '=',$college_district_id)
        ->select('*')					
        ->get();
        return $zone;

    }

    public static function getAllZoneExceptZones($zones)
    {
       // $sql="SELECT t_rel_zone_id from t_rel_zone where t_rel_zone_id not in ";
        $zone = DB::table('t_rel_zone')
        ->whereNotIn('t_rel_zone_id', $zones)
        ->select('t_rel_zone_id')					
        ->get();
        return $zone;

    }

    public static function getAllZones()
    {
       // $sql="SELECT t_rel_zone_id from t_rel_zone where t_rel_zone_id not in ";
        $zone = DB::table('t_rel_zone')
                ->select('t_rel_zone_id')					
                ->get();
          return $zone;

    }

    public static function getDummyStudentData()
    {
       // $sql="SELECT t_rel_zone_id from t_rel_zone where t_rel_zone_id not in ";
        $data = DB::table('temp_student_data')
                ->select('*')					
                ->get();
          return $data;

    }
    public static function reseAllocation()
    {
            //truncate t_rel_zone_paper_student_allocation table
            DB::table('t_rel_zone_paper_student_allocation')->truncate();
            //reset capacity in capacity table
            $data=[

                    'consumed_capacity'=>0,
                    'consumed_percentage'=>0
            ];
            DB::table('t_rel_zone_capacity')->update($data);


    }



}
